# wRiTe

## Foreword

***wRiTe*** is a PyQt5 based 2 panels Markdown editor based on [*MultiMarkdown*](https://fletcherpenney.net/multimarkdown/) and several web technologies. Its main purpose is to transform Markdown formatted text into a pdf file.

This application was created for my personal needs and contains several non-standard tags that add many new features.

These include for example:

* pagination (**warning: pages must be explicitly defined!**)
* footnotes
* portrait or landscape mode (whole document or )
* image placement, resizing and rotation
* multicolumn text
* enhanced tables
* math formulas
* diagrams
* graphics + native puMuPdf functions
* Pdf encryption
* video inclusion
* guitar tablatures !


wRiTe has no buttons, so most of the work is done from the keyboard.

For the moment, wRiTe is only available for Linux.


>**DISCLAIMER**
>
>I'm by no means a good programmer, so use software at your own risk!

## How to install ?

* Clone git repository or download zip
* **Ensure that Python 3 is installed**.
* Just do `python install.py`.
* Installer will warn about missing dependencies.

## How to use it ?

* Read the Help file that is loaded on first run.

* Press [Shift] + [F8] to see that Help file again !

**Help file is not finished yet. Many features are still missing...**

## What if I use Gnome desktop ?

Just install *qgnomeplatform-qt5* to see native dialogs.

## Want to support ?

You can [buy me a coffee](https://ko-fi.com/nikkolas666) and I'll do my best to continue developing wRiTe ! Thank you ! :+1: 
