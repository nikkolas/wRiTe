# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import os
import sys
from shutil import which

print("-------------------------")
print("-    wRiTe installer    -")
print("-------------------------")

#  Default path collection
install_path = "/usr/local/share"
binary_path = "/usr/local/bin"
apps_path = "/usr/share/applications"
path_has_changed = False

#  Query installation path
while True:
    custom_path = input(
        "Specify installation path [default: /usr/local/share]:"
        )

    if not custom_path.strip():
        break

    if not os.path.isdir(custom_path.strip()):
        print(
            "Warning: specified path does not exists...\nPlease enter a valid directory."
        )
        continue

    install_path = custom_path
    path_has_changed = True
    break

while True:
    custom_binary_path = input(
        "Specify binary installation path [default: /usr/local/bin]:"
        )

    if not custom_binary_path.strip():
        break

    if not os.path.isdir(custom_binary_path.strip()):
        print(
            "Warning: specified path does not exists...\nPlease enter a valid directory."
        )
        continue

    binary_path = custom_binary_path
    break

path = os.path.join(install_path, "wRiTe")

#  Test critical dependencies

print("\n+++ Check if MultiMarkdown is installed:", end=" ")

if not which('multimarkdown'):
    print("No")
    print("Warning: MultiMarkdown executable must be installed !")
    print("Exiting installer.")
    sys.exit()
else:
    print("OK")


modules = [
    "re",
    "functools",
    "enchant",
    "os",
    "sys",
    "pathlib",
    "urllib",
    "time",
    "hashlib",
    "tempfile",
    "urllib",
    "subprocess",
    "shutil",
    "fitz",
    "popplerqt5",
    "cups",
    "qpageview",
    "numpy",
    "matplotlib",
    "PyQt5.QtWidgets",
    "PyQt5.QtWebEngineWidgets",
    "PyQt5.QtCore",
    "PyQt5.QtGui",
    "PyQt5.QtWebEngineWidgets",
    "PyQt5.QtPrintSupport",
]

print("\n+++ Check if core modules are installed:")

for module in modules:

    try:
        print("Checking {}...".format(module), end=" ")
        exec("import {}".format(module))
        print("OK")
    except ModuleNotFoundError:
        print("No")
        print(
            "Warning: module {} not found".format(
                module if "fitz" not in module else "pymupdf"
            )
        )
        print("Exiting installer.")
        sys.exit()

#  Patching wRiTe files
if path_has_changed:
    content = ""

    with open("wRiTe/wRiTe", "r") as f:
        content = f.read()

    content = content.replace(
        "APP_PATH = '/usr/local/share/wRiTe'", "APP_PATH = '{}'".format(path)
    )

    with open("wRiTe/wRiTeC", "w") as f:
        f.write(content)

    dfc = ""
    with open("wRiTe/wRiTe.desktop", "r") as f:
        dfc = f.read()

    dfc = dfc.replace("/usr/local/share", path)

    with open("wRiTe/wRiTe.desktop", "w") as f:
        f.write(dfc)

#  Create Binary
bash_binary_content = "#!/bin/bash\n\npython3 {}/wRiTe $*".format(path)

with open("wRiTe.sh", "w") as f:
    f.write(bash_binary_content)

#  Installing
print("\nInstallation")

print("Creating wRiTe system folder")
os.system("sudo mkdir -p {} &>/dev/null".format(path))

print("Copying files...")
os.system("sudo cp -a wRiTe {} &>/dev/null".format(install_path))

if path_has_changed:
    os.system(
        "sudo mv {} {} &>/dev/null".format(
            os.path.join(path, "wRiTeC"), os.path.join(path, "wRiTe")
        )
    )

if os.path.isdir(binary_path):
    print("Copying binary")
    os.system("chmod +x wRiTe.sh &>/dev/null")
    os.system(
        "sudo mv wRiTe.sh {} &>/dev/null".format(os.path.join(binary_path, "wRiTe"))
    )
else:
    print("Can't copy binary to {}".format(binary_path))

if os.path.isdir(apps_path):
    print("Copying .desktop file into applications")
    os.system("sudo cp -a wRiTe/wRiTe.desktop {} &>/dev/null".format(apps_path))
else:
    print("Can't copy .desktop file to {}".format(apps_path))

print("\nwRiTe: Installation complete !")
