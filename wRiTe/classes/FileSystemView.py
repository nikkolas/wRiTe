# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import (
    QHeaderView,
    QFileDialog,
    QDialogButtonBox,
    QToolButton,
    QComboBox,
    QLabel,
    QLineEdit,
)


class FileSystemView(QFileDialog):

    fileSelected = pyqtSignal(str, bool)

    def __init__(self, path=""):
        super().__init__()
        self.setSupportedSchemes(["file"])
        self.setOption(
            QFileDialog.DontUseNativeDialog,
            QFileDialog.DontUseCustomDirectoryIcons
        )
        self.setWindowFlags(self.windowFlags() & ~Qt.Dialog)
        self.setMaximumHeight(300)
        self.setAcceptMode(QFileDialog.AcceptOpen)
        self.selectNameFilter("*.*")

        self.ckButton = QToolButton()
        self.ckButton.setText("Copy")
        self.ckButton.setCheckable(True)
        self.ckButton.setAttribute(Qt.WA_StyledBackground)
        self.ckButton.setStyleSheet(
            "QToolButton:checked {background-color: #a1ff8a; border:none;}"
        )

        self.layout().addWidget(self.ckButton, 0, 0)

    def paintEvent(self, event):
        for button in self.findChildren(QDialogButtonBox):
            button.setHidden(True)

        for combobox in self.findChildren(QComboBox):
            if "All Files" in combobox.currentText():
                combobox.setHidden(True)

        for label in self.findChildren(QLabel):
            label.setHidden(True)

        for lineedit in self.findChildren(QLineEdit):
            lineedit.setHidden(True)

        for header in self.findChildren(QHeaderView):
            header.setSectionResizeMode(0, QHeaderView.ResizeToContents)

    def accept(self):
        selectedFiles = self.selectedFiles()
        if len(selectedFiles) == 1:
            self.fileSelected.emit(selectedFiles[0], self.ckButton.isChecked())
            self.ckButton.setChecked(False)

    def done(self, r):
        pass
