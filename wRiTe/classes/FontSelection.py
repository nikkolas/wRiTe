# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.


from classes.emojis import emojis_categories, emojis_icons
from classes.symbols import symbols
from classes.SManager import SManager

from PyQt5.QtCore import (
    QSize,
    Qt,
    pyqtSignal
)

from PyQt5.QtGui import (
    QFont
)

from PyQt5.QtWidgets import (
    QDialog,
    QWidget,
    QComboBox,
    QLabel,
    QVBoxLayout,
    QHBoxLayout,
    QScrollArea,
    QGridLayout,
    QPushButton,
    QFrame,
    QRadioButton,
    QGroupBox,
    QSpinBox,
    QSizePolicy
)


class FontSelection(QDialog):
    sendSelectedEmoji = pyqtSignal(str)
    favoritesUpdated = pyqtSignal()
    favoritesPositionChanged = pyqtSignal(int, bool)
    favoritesWindowToggled = pyqtSignal()

    def __init__(
            self,
            textEditFont,
            favoritesOpened=False,
            parent=None
            ):
        super(FontSelection, self).__init__(parent)
        self.textEditFont = textEditFont
        self.currentPos = 0
        self.favoritesOpened = favoritesOpened
        self.SM = SManager()
        self.settings = self.SM.get_settings()
        self.specials = self.SM.get_setting('Specials')
        self.selected_emoji = ""
        self.emojifont = "Noto Color Emoji"
        self.emojicategory = 6
        self.alphanumcategory = 17
        self.font_size = 19
        self.btn_size = 20
        self.maxcol = 17
        self.categoriesComboBox = QComboBox()
        self.categoriesComboBox.textActivated.connect(self.updateTable)
        exclusiveGroup = QGroupBox()
        self.alphanumRadioButton = QRadioButton("Alphanum")
        self.alphanumRadioButton.setChecked(True)
        self.alphanumRadioButton.toggled.connect(self.togglealphanum)
        self.emojiRadioButton = QRadioButton("Emojis")
        self.emojiRadioButton.setChecked(False)
        buttonsLayout = QHBoxLayout()
        buttonsLayout.addWidget(self.alphanumRadioButton)
        buttonsLayout.addWidget(self.emojiRadioButton)
        exclusiveGroup.setLayout(buttonsLayout)
        selectionLayout = QHBoxLayout()
        selectionLayout.addWidget(self.categoriesComboBox)
        selectionLayout.addWidget(exclusiveGroup)
        selectionWidget = QWidget()
        selectionWidget.setLayout(selectionLayout)

        self.insertButton = QPushButton("Insert selected character")
        self.insertButton.setStyleSheet(
            "QPushButton {background-color: #bde7ff;}"
        )
        self.insertButton.pressed.connect(self.insertChar)
        self.displayLabel = QLabel()
        self.displayLabel.setFrameStyle(QFrame.Panel)
        self.displayLabel.setFixedWidth(220)
        self.displayLabel.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        self.displayLabel.setAlignment((Qt.AlignHCenter | Qt.AlignVCenter))
        self.addFavoritesButton = QPushButton(" Add to Favorites ")
        self.addFavoritesButton.setStyleSheet(
            "QPushButton {background-color: #bdffc9;}"
        )
        self.addFavoritesButton.clicked.connect(self.delOrAddFavorites)
        self.specialScrollArea = QScrollArea()
        self.specialScrollArea.setFixedWidth(630)
        exclusiveGroup.setFixedWidth(int(self.specialScrollArea.width()/3))
        displayWidget = QWidget()
        displayLayout = QVBoxLayout()
        displayLayout.addWidget(self.displayLabel)
        favoritesWidget = QWidget()
        favoritesLayout = QHBoxLayout()
        self.favoritesPosition = QSpinBox()
        self.favoritesPosition.setMinimum(1)
        self.favoritesPosition.setMaximum(len(self.specials)+1)
        self.favoritesPosition.setValue(self.favoritesPosition.maximum())
        self.favoritesPosition.setDisabled(True)
        self.favoritesPosition.valueChanged.connect(self.favoritesPositionUpdated)
        self.favoritesPosition.valueChanged.connect(self.moveInFavorites)
        favoritesLayout.addWidget(self.addFavoritesButton)
        favoritesLayout.addWidget(self.favoritesPosition)
        favoritesWidget.setLayout(favoritesLayout)
        displayLayout.addWidget(favoritesWidget)
        displayWidget.setLayout(displayLayout)
        displayWidget.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        selectWidget = QWidget()
        selectLayout = QVBoxLayout()
        selectLayout.addWidget(selectionWidget)
        selectLayout.addWidget(self.specialScrollArea)
        selectLayout.addWidget(self.insertButton)
        selectWidget.setLayout(selectLayout)
        windowLayout = QHBoxLayout()
        windowLayout.addWidget(selectWidget)
        windowLayout.addWidget(displayWidget)
        self.setLayout(windowLayout)
        self.setModal(False)
        self.togglealphanum()
        self.setMinimumHeight(300)
        self.setMaximumHeight(550)

    def delOrAddFavorites(self):
        if self.selected_emoji in self.specials:
            self.deleteFromFavorites()
            self.displayLabel.setText(" ")
            self.updateTable()
        else:
            self.addToFavorites()

    def addToFavorites(self):
        pos = self.favoritesPosition.value() - 1
        self.specials = "".join((
            self.specials[:pos],
            self.selected_emoji,
            self.specials[pos:]
            ))
        self.settings['Specials'] = self.specials
        self.SM.save_settings(self.settings)
        self.updateTable()
        self.updateFavoritesButton()
        self.favoritesUpdated.emit()
        self.favoritesPositionChanged.emit(pos, True)

    def deleteFromFavorites(self):
        self.specials = self.specials.replace(self.selected_emoji, "")
        self.settings['Specials'] = self.specials
        self.SM.save_settings(self.settings)
        self.updateTable()
        self.updateFavoritesButton()
        self.favoritesUpdated.emit()

    def moveInFavorites(self, val):
        val -= 1
        if self.selected_emoji in self.specials and val != self.currentPos:
            self.specials = self.specials.replace(self.selected_emoji, "")
            self.specials = "".join((
                self.specials[:val],
                self.selected_emoji,
                self.specials[val:]
            ))
            self.settings['Specials'] = self.specials
            self.currentPos = val
            self.SM.save_settings(self.settings)
            self.updateTable()
            self.favoritesPositionUpdated(val+1)

    def updateFavoritesButton(self):
        if self.selected_emoji in self.specials:
            self.addFavoritesButton.setText("Delete from favs")
            self.addFavoritesButton.setStyleSheet(
                "QPushButton {background-color: #ffc5bd;}"
            )
            pos = self.specials.index(self.selected_emoji)
            self.favoritesPosition.setValue(pos+1)
            self.favoritesPosition.setMaximum(len(self.specials))
        else:
            self.addFavoritesButton.setText(" Add to Favorites ")
            self.addFavoritesButton.setStyleSheet(
                "QPushButton {background-color: #bdffc9;}"
            )
            self.favoritesPosition.setMaximum(len(self.specials)+1)
            if self.favoritesPosition.value() == self.favoritesPosition.maximum() - 1:
                self.favoritesPosition.setValue(self.favoritesPosition.value()+1)
        self.favoritesPosition.setEnabled(True)

    def favoritesPositionUpdated(self, val):
        val -= 1
        self.favoritesForcedOpen = True
        self.favoritesPositionChanged.emit(val, True)

    def togglealphanum(self):
        self.addFavoritesButton.setDisabled(True)
        self.displayLabel.setText(" ")
        self.favoritesPosition.setDisabled(True)
        nb_items = self.categoriesComboBox.count()

        for i in range(nb_items):
            self.categoriesComboBox.removeItem(0)
        categories = []

        if self.alphanumRadioButton.isChecked():
            categories = list(symbols)
            categories.append("Favorites")
            categories.sort()
            self.btn_size = 32
            self.font_size = 19
            self.maxcol = 17
            self.categoriesComboBox.addItems(categories)
            self.categoriesComboBox.setCurrentIndex(self.alphanumcategory)
        else:
            categories = list(emojis_categories)
            categories.sort()
            self.btn_size = 55
            self.font_size = 36
            self.maxcol = 11
            self.categoriesComboBox.addItems(categories)
            self.categoriesComboBox.setCurrentIndex(self.emojicategory)

        self.updateTable()

    def updateTable(self):
        self.addFavoritesButton.setDisabled(True)
        category = self.categoriesComboBox.currentText()
        col = 0
        row = 0
        specialchar = QWidget()
        specialGrid = QGridLayout()
        specialGrid.setContentsMargins(10, 10, 10, 10)
        total = 0
        list_syms = ()

        if self.alphanumRadioButton.isChecked():
            self.alphanumcategory = self.categoriesComboBox.currentIndex()

            if category in "Favorites":
                list_syms = tuple(self.specials)
            else:
                list_cat = symbols[category]

                for item in list_cat:

                    if isinstance(item, tuple):
                        list_syms += tuple([chr(code) for code in range(item[0], item[1]+1)])
                    else:
                        list_syms += (chr(item),)
            total = len(list_syms)
        else:
            self.emojicategory = self.categoriesComboBox.currentIndex()
            total = len(emojis_categories[category])

        for i in range(total):
            button = QPushButton()
            qf = QFont()
            qf.setFamilies([self.textEditFont, self.emojifont, "Andale Mono"])

            if self.alphanumRadioButton.isChecked():
                button.setText(list_syms[i])
                qf.setPointSize(19)
            else:
                button.setText(emojis_categories[category][i][0])
                qf.setPointSize(self.font_size)

            button.setFont(qf)
            size = QSize(self.btn_size, self.btn_size)
            button.setMinimumSize(size)
            button.setMaximumSize(size)
            button.setFlat(1)
            button.pressed.connect(self.displayChar)
            specialGrid.addWidget(button, row, col)
            col += 1

            if col == self.maxcol-1:
                col = 0
                row += 1
        specialchar.setLayout(specialGrid)
        self.specialScrollArea.setWidget(specialchar)
        self.specialScrollArea.setContentsMargins(0, 0, 0, 0)

    def displayChar(self):
        self.addFavoritesButton.setEnabled(True)
        self.favoritesPosition.setDisabled(True)
        button = self.sender()
        text = button.text()
        self.selected_emoji = text
        if self.selected_emoji not in self.specials:
            self.favoritesPositionChanged.emit(len(self.specials)+1, False)
        qf = QFont()
        if self.emojiRadioButton.isChecked():
            qf.setFamily(self.emojifont)
        qf.setPointSize(130)
        self.displayLabel.setFont(qf)
        self.displayLabel.setText(text)
        self.updateFavoritesButton()
        self.currentPos = self.favoritesPosition.value() - 1

    def insertChar(self):
        if self.alphanumRadioButton.isChecked():
            self.sendSelectedEmoji.emit(self.selected_emoji)
        else:
            self.sendSelectedEmoji.emit(emojis_icons[self.selected_emoji])
        self.updateFavoritesButton()

    def closeEvent(self, event):
        self.favoritesPositionChanged.emit(len(self.specials)+1, False)
        if not self.favoritesOpened:
            self.favoritesWindowToggled.emit()
