# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re

try:
    from enchant import tokenize
    from enchant.errors import TokenizerNotFoundError
except ModuleNotFoundError:
    print("enchant not installed")

try:
    from PyQt5.QtCore import Qt, QRegularExpression
    from PyQt5.QtGui import (
        QSyntaxHighlighter,
        QTextCharFormat,
        QColor,
        QFont,
        QTextBlockUserData,
    )
except ModuleNotFoundError:
    print("PyQt5 not properly installed")

try:
    from classes.utils import findBalanced
    from classes.colors import colors
except ModuleNotFoundError:
    print("missing core modules")

try:
    from enchant.utils import trim_suggestions
except ImportError:

    def trim_suggestions(word, suggs, maxlen, calcdist=None):
        return suggs[:maxlen]


class Highlighter(QSyntaxHighlighter):
    go = False
    def __init__(self, parent=None):
        super().__init__(parent)
        self._sp_dict = None
        self._chunkers = []
        self.spellCheck = True
        self.textEditFontSize = 17
        self.misspellings = []
        self.highlightingRules = ()
        self.go = False
        self.stop = False
        self.bqrgbcolor = None

    tokenizer = None
    token_filters = (tokenize.EmailFilter, tokenize.URLFilter)
    err_format = QTextCharFormat()
    err_format.setUnderlineColor(Qt.red)
    err_format.setUnderlineStyle(QTextCharFormat.SpellCheckUnderline)

    def chunkers(self):
        #  Gets the chunkers in use
        return self._chunkers

    def dict(self):
        #  Gets the spelling dictionary in use
        return self._sp_dict

    def setChunkers(self, chunkers):
        #  Sets the list of chunkers to be used
        self._chunkers = chunkers
        self.setDict(self.dict())

    def setDict(self, sp_dict):
        #  Sets the spelling dictionary to be used

        try:
            self.tokenizer = tokenize.get_tokenizer(
                sp_dict.tag,
                chunkers=self._chunkers,
                filters=self.token_filters
            )
        except TokenizerNotFoundError:
            self.tokenizer = tokenize.get_tokenizer(
                chunkers=self._chunkers, filters=self.token_filters
            )

        self._sp_dict = sp_dict
        self.rehighlight()

    def spellWords(self, text, index, format, to=None):

        if not self._sp_dict or not self.spellCheck:
            return

        for (word, pos) in self.tokenizer(text[index:to]):
            if pos and text[pos-1:pos] in ("\\", "@", "!", "."):
                continue

            if not self._sp_dict.check(word):
                spellFormat = QTextCharFormat()
                spellFormat.setUnderlineColor(Qt.red)
                spellFormat.setUnderlineStyle(QTextCharFormat.SpellCheckUnderline)
                format.merge(spellFormat)
                self.setFormat(index + pos, len(word), format)
                self.misspellings.append((pos, pos + len(word)))

    def highlightBlock(self, text):
        if not self.go:
            return

        self.misspellings = []
        self.highlightingRules = []

        qFormat = QTextCharFormat()
        qFormat.setFontWeight(QFont.Bold)
        self.highlightingRules.append(('"', qFormat))

        othertitleFormat = QTextCharFormat()
        othertitleFormat.setFontWeight(QFont.Bold)
        othertitleFormat.setForeground(Qt.black)
        othertitleFormat.setFontPointSize(self.textEditFontSize + 1)
        self.highlightingRules.append(("^######+\\s+.+$", othertitleFormat))

        slFormat = QTextCharFormat()
        slFormat.setForeground(Qt.darkGreen)
        self.highlightingRules.append(("(\\[|\\])", slFormat))

        tableFormat = QTextCharFormat()
        tableFormat.setFontWeight(QFont.Bold)
        tableFormat.setForeground(Qt.darkGreen)
        tableFormat.setFontPointSize(self.textEditFontSize + 1)
        self.highlightingRules.append(("\\|", tableFormat))

        brFormat = QTextCharFormat()
        brFormat.setFontWeight(QFont.Bold)
        self.highlightingRules.append(("[\\{\\)\\(\\}\\]\\[]{1}", brFormat))

        pFormat = QTextCharFormat()
        pFormat.setForeground(QColor("blue"))
        self.highlightingRules.append((
            "\\(\\s*[-\\d.]+\\s*,\\s*[-\\d.]+\\s*\\)",
            pFormat
            ))

        quote2Format = QTextCharFormat()
        quote2Format.setForeground(Qt.white)
        if self.bqrgbcolor:
            if isinstance(self.bqrgbcolor, str):
                quote2Format.setBackground(QColor(self.bqrgbcolor))
            elif isinstance(self.bqrgbcolor, tuple):
                quote2Format.setBackground(QColor(*self.bqrgbcolor))
        else:
            quote2Format.setBackground(QColor("gray"))
        self.highlightingRules.append(("^([>]+|\\s{4}[>]+)", quote2Format))

        refFormat = QTextCharFormat()
        refFormat.setForeground(QColor("#166FBF"))
        self.highlightingRules.append(("\\[[^\\]]+\\]\\[[^\\]]+\\]", refFormat))

        itunderFormat = QTextCharFormat()
        itunderFormat.setFontUnderline(True)
        itunderFormat.setFontItalic(True)
        self.highlightingRules.append((
            "(_{\\+\\+.+\\+\\+\\}_|{\\+\\+_.+_\\+\\+\\})",
            itunderFormat
            ))

        ulFormat = QTextCharFormat()
        ulFormat.setFontUnderline(True)
        self.highlightingRules.append(("\\+\\+[^\\+]+?\\+\\+", ulFormat))

        biFormat = QTextCharFormat()
        biFormat.setFontPointSize(self.textEditFontSize)
        self.highlightingRules.append(("\\*(?=\\S)[^|]+(?<=\\S)\\*", biFormat)) 

        tscFormat = QTextCharFormat()
        tscFormat.setFontPointSize(self.textEditFontSize - 2)
        tscFormat.setForeground(Qt.darkGray)
        self.highlightingRules.append(("\\([^\\)\\(]+\\)\\{", tscFormat))

        boldunderFormat = QTextCharFormat()
        boldunderFormat.setFontWeight(QFont.Bold)
        boldunderFormat.setFontUnderline(True)
        self.highlightingRules.append((
            "(\\*\\*\\+\\+[^\\+\\*]+\\+\\+\\*\\*|\\+\\+\\*\\*[^\\+\\*]+\\*\\*\\+\\+\\})",
            boldunderFormat
            ))

        overFormat = QTextCharFormat()
        overFormat.setBackground(Qt.yellow)
        self.highlightingRules.append((
            "(\\=\\=[^=]+?\\=\\=|\\=\\=[^=]+?\\=\\=)",
            overFormat
            ))

        boldoverFormat = QTextCharFormat()
        boldoverFormat.setFontWeight(QFont.Bold)
        boldoverFormat.setBackground(Qt.yellow)
        self.highlightingRules.append((
            "\\{\\=\\=\\*\\*[^\\*=]+\\*\\*\\=\\=\\}",
            boldoverFormat
            ))

        itoverFormat = QTextCharFormat()
        itoverFormat.setFontItalic(True)
        itoverFormat.setBackground(Qt.yellow)
        self.highlightingRules.append((
            "(\\*\\=\\=[^=]+\\=\\=\\*|\\=\\=\\*[^*]+\\*\\=\\=)",
            itoverFormat
            ))

        tagFormat = QTextCharFormat()
        tagFormat.setForeground(Qt.darkYellow)
        self.highlightingRules.append(("<[/]*[a-z]+[^>]*>", tagFormat))

        codeoptionsFormat = QTextCharFormat()
        codeoptionsFormat.setFontWeight(QFont.Bold)
        codeoptionsFormat.setFontPointSize(self.textEditFontSize - 2)
        codeoptionsFormat.setForeground(QColor("dimgray"))
        self.highlightingRules.append((
            "^```[a-z]+\\[*[^\\]]*\\]*\\(*[^)]*\\)*",
            codeoptionsFormat
            ))

        mermaidFormat = QTextCharFormat()
        mermaidFormat.setFontWeight(QFont.Bold)
        mermaidFormat.setFontItalic(True)
        mermaidFormat.setFontPointSize(self.textEditFontSize - 2)
        mermaidFormat.setForeground(QColor("#4e9a06"))
        self.highlightingRules.append(("(^!diagram\\(*[%pxcm.\\d\\s]*\\)*$|^\\.diagram)", mermaidFormat))

        linkFormat = QTextCharFormat()
        linkFormat.setForeground(Qt.blue)
        linkFormat.setFontItalic(True)
        self.highlightingRules.append(("\\[.*\\]\\([^)]+\\)", linkFormat))

        imgFormat = QTextCharFormat()
        imgFormat.setForeground(QColor("#d33682"))
        imgFormat.setFontItalic(True)
        imgFormat.setFontWeight(QFont.Bold)
        imgFormat.setFontPointSize(self.textEditFontSize - 1)
        self.highlightingRules.append(("\\!\\[[^\\]]*\\]\\(", imgFormat))

        matplotFormat = QTextCharFormat()
        matplotFormat.setFontWeight(QFont.Bold)
        matplotFormat.setFontItalic(True)
        matplotFormat.setFontPointSize(self.textEditFontSize - 2)
        matplotFormat.setForeground(QColor("maroon"))
        self.highlightingRules.append((
            "^(\\.graph\\s*|!graph\\[*[^\\]]*\\]*\\(*[^\\)]*\\)*)$", matplotFormat
            ))
        
        ruleFormat = QTextCharFormat()
        ruleFormat.setForeground(QColor("indigo"))
        ruleFormat.setFontWeight(QFont.Bold)
        ruleFormat.setFontItalic(True)
        ruleFormat.setFontPointSize(self.textEditFontSize - 2)
        self.highlightingRules.append(("^!(line|linesolid|linedashed|linedotted)\\(*[^)]*\\)*", ruleFormat))

        codeblockFormat = QTextCharFormat()
        codeblockFormat.setFontWeight(QFont.Bold)
        codeblockFormat.setFontItalic(True)
        codeblockFormat.setFontPointSize(self.textEditFontSize - 2)
        codeblockFormat.setForeground(QColor("#3465a4"))
        self.highlightingRules.append((
            "^(\\.code\\s*|!code\\[*[^\\]]*\\]*\\(*[^\\)]*\\)*)$", codeblockFormat
            ))

        footnoteFormat = QTextCharFormat()
        footnoteFormat.setForeground(QColor("#007486"))
        self.highlightingRules.append(("\\[\\^[^\\]]+\\]", footnoteFormat))

        alignFormat = QTextCharFormat()
        alignFormat.setFontWeight(QFont.Bold)
        alignFormat.setFontItalic(True)
        alignFormat.setForeground(QColor("#268bd2"))
        alignFormat.setFontPointSize(self.textEditFontSize - 2)
        self.highlightingRules.append((
            "^(!(center|right|left)\\(*[^)]*\\)*|\\.(center|left|right)\\s*)$",
            alignFormat
            ))

        valignFormat = QTextCharFormat()
        valignFormat.setFontWeight(QFont.Bold)
        valignFormat.setFontItalic(True)
        valignFormat.setForeground(QColor("#268bd2"))
        valignFormat.setFontPointSize(self.textEditFontSize - 2)
        self.highlightingRules.append((
            "^(!pageframe\\(*[^)]*\\)*|!(vcenter|vhcenter)\\(*[^)]*\\)*|\\.(vcenter|vhcenter)\\s*)$",
            valignFormat
            ))
        
        boxFormat = QTextCharFormat()
        boxFormat.setFontWeight(QFont.Bold)
        boxFormat.setFontItalic(True)
        boxFormat.setForeground(QColor("royalblue"))
        boxFormat.setFontPointSize(self.textEditFontSize - 2)
        self.highlightingRules.append((
            "^(!mc\\d+\\(*.*\\)*|!(box|wbox)\\[*[^\\]]*\\]*\\(*[^\\)]*\\)*|\\.mc\\d+\\s*|\\.(box|wbox)\\s*)$",
            boxFormat
            ))
        
        musicFormat = QTextCharFormat()
        musicFormat.setFontWeight(QFont.Bold)
        musicFormat.setFontItalic(True)
        musicFormat.setForeground(QColor("#da0093"))
        musicFormat.setFontPointSize(self.textEditFontSize - 2)
        self.highlightingRules.append(("^(!gtab\\(*.*\\)*|\\.gtab\\s*)$", musicFormat))

        fontFormat = QTextCharFormat()
        fontFormat.setFontWeight(QFont.Bold)
        fontFormat.setFontItalic(True)
        fontFormat.setForeground(QColor("darkgreen"))
        fontFormat.setFontPointSize(self.textEditFontSize - 2)
        self.highlightingRules.append(("^(!font\\(*.*\\)*|\\.font\\s*)$", fontFormat))

        npFormat = QTextCharFormat()
        npFormat.setFontWeight(QFont.Bold)
        npFormat.setFontItalic(True)
        npFormat.setForeground(Qt.red)
        self.highlightingRules.append(("^!newpage", npFormat))

        wfuncsFormat = QTextCharFormat()
        wfuncsFormat.setFontWeight(QFont.Bold)
        wfuncsFormat.setFontItalic(True)
        self.highlightingRules.append(
            (
                (
                    "((wParams|wLinearfit|wBar|wPie|wFitCurve|wTangent|"
                    "wFunction|wFill|wAngle|wLength|wIntersect|"
                    "wDots|wBigdot|wLine|wPolygon|wCurve|wDeriv|"
                    "wCircle|wRectangle|wEllipse|wText|wArrow|wInteg)"
                    "|(wBar|wPie)\\(\\s*'[\\w\\s]+\\:)"
                ),
                wfuncsFormat,
            )
        ) 

        spaceFormat = QTextCharFormat()
        spaceFormat.setForeground(Qt.lightGray)
        self.highlightingRules.append(("/\\s+/", spaceFormat))
        
        quoteFormat = QTextCharFormat()
        quoteFormat.setForeground(QColor("forestgreen"))
        self.highlightingRules.append(("(?<=[=])[\"'].+?[\"']", quoteFormat))

        truefalseFormat = QTextCharFormat()
        truefalseFormat.setForeground(QColor("mediumvioletred"))
        self.highlightingRules.append(("(?<=[=])(True|False|None)", truefalseFormat))

        paramnumberFormat = QTextCharFormat()
        paramnumberFormat.setForeground(QColor("royalblue"))
        self.highlightingRules.append(("(?<=[=])[+-]*\\d*[.]*\\d*", paramnumberFormat))

        lettrineFormat = QTextCharFormat()
        lettrineFormat.setFontWeight(QFont.Bold)
        lettrineFormat.setFontPointSize(self.textEditFontSize + 1)
        self.highlightingRules.append(("^§\\w", lettrineFormat))

        latexcmdFormat = QTextCharFormat()
        latexcmdFormat.setFontWeight(QFont.Bold)
        latexcmdFormat.setForeground(QColor("#2aa198"))
        self.highlightingRules.append(("\\\\[a-zA-Z]+", latexcmdFormat))

        latexFormat = QTextCharFormat()
        latexFormat.setFontWeight(QFont.Bold)
        latexFormat.setForeground(QColor("#b58900"))
        self.highlightingRules.append(("\\$", latexFormat))

        buttonFormat = QTextCharFormat()
        buttonFormat.setForeground(Qt.black)
        self.highlightingRules.append(("\\[[@#\\da-z]*\\|[^|]+?\\|[@#\\da-z]*\\]", buttonFormat))

        multiplyFormat = QTextCharFormat()
        multiplyFormat.setForeground(QColor("black"))
        self.highlightingRules.append((" \\* ", multiplyFormat))
        
        emojiFormat = QTextCharFormat()
        emojiFormat.setFontWeight(QFont.Bold)
        emojiFormat.setFontPointSize(self.textEditFontSize - 2)
        emojiFormat.setBackground(QColor("#ffd75e"))
        self.highlightingRules.append(("\\:[^\\:]+?\\:", emojiFormat))
        
        titleFormat = QTextCharFormat()
        titleFormat.setFontWeight(QFont.Bold)
        titleFormat.setForeground(QColor("dimgray"))
        titleFormat.setFontPointSize(self.textEditFontSize + 5)
        self.highlightingRules.append((
            "^#[!]{1,2}\\s[^\\s]+.+$",
            titleFormat
            ))

        h1Format = QTextCharFormat()
        h1Format.setFontWeight(QFont.Bold)
        h1Format.setForeground(QColor("#d4282a"))
        h1Format.setFontPointSize(self.textEditFontSize + 3)
        self.highlightingRules.append(("^#\\s[^\\s]+.+$", h1Format))

        h2Format = QTextCharFormat()
        h2Format.setFontWeight(QFont.Bold)
        h2Format.setForeground(QColor("#2b8715"))
        h2Format.setFontPointSize(self.textEditFontSize + 2)
        self.highlightingRules.append(("^##\\s[^\\s]+.+$", h2Format))

        h3Format = QTextCharFormat()
        h3Format.setFontWeight(QFont.Bold)
        h3Format.setForeground(QColor("#176ec2"))
        h3Format.setFontPointSize(self.textEditFontSize + 1)
        self.highlightingRules.append(("^###\\s[^\\s]+.+$", h3Format))

        h4Format = QTextCharFormat()
        h4Format.setFontWeight(QFont.Bold)
        h4Format.setForeground(QColor("#ee7432"))
        h4Format.setFontPointSize(self.textEditFontSize + 1)
        self.highlightingRules.append(("^####\\s[^\\s]+.+$", h4Format))

        h5Format = QTextCharFormat()
        h5Format.setFontWeight(QFont.Bold)
        h5Format.setForeground(QColor("#6e519d"))
        h5Format.setFontPointSize(self.textEditFontSize + 1)
        self.highlightingRules.append(("^#####\\s[^\\s]+.+$", h5Format))

        h6Format = QTextCharFormat()
        h6Format.setFontWeight(QFont.Bold)
        h6Format.setForeground(QColor("#c253a0"))
        h6Format.setFontPointSize(self.textEditFontSize + 1)
        self.highlightingRules.append(("^######\\s[^\\s]+.+$", h6Format))

        optionsFormat = QTextCharFormat()
        optionsFormat.setFontPointSize(self.textEditFontSize - 3)
        optionsFormat.setForeground(QColor("gray"))
        self.highlightingRules.append(("^(\\*|\\-|\\+){4,}[*]*$", optionsFormat))

        optionsboldFormat = QTextCharFormat()
        optionsboldFormat.setFontItalic(True)
        optionsboldFormat.setForeground(QColor("gray"))
        optionsboldFormat.setFontPointSize(self.textEditFontSize - 2)
        self.highlightingRules.append(
            (
                (
                    "^(HEADER|LINE|HIDE|MCOLOR|FONTCOLOR|BGCOLOR|"
                    "FONT|FONTMATH|FONTTITLE|FONTSIZE|PAGENUM|MODE|MARGINS|STYLE|MLEFT|"
                    "MRIGHT|MTOP|MBOTTOM|DIAGRAM|HIGHLIGHT|SIZE|"
                    "PADDING|PTOP|PLEFT|PRIGHT|PBOTTOM|BGIMAGE|"
                    "BGGRADIENT|AUTHOR|TITLE|ENCRYPT|LANG|EXPORTED_PAGES|BQCOLOR):.+$"
                ),
                optionsboldFormat,
            )
        )

        bgFormat = QTextCharFormat()
        bgFormat.setForeground(QColor("tomato"))
        bgFormat.setFontWeight(QFont.Bold)
        bgFormat.setFontItalic(True)
        bgFormat.setBackground(QColor("peachpuff"))
        self.highlightingRules.append((
            "^!((portrait|landscape)\\s*|(bgimage|bggradient)\\(.+\\)\\s*)$",
            bgFormat
            ))

        headfootFormat = QTextCharFormat()
        headfootFormat.setFontWeight(QFont.Bold)
        headfootFormat.setFontItalic(True)
        headfootFormat.setForeground(QColor("#d7005f"))
        headfootFormat.setFontPointSize(self.textEditFontSize - 2)
        self.highlightingRules.append(("^(!header\\s*|\\.header\\s*|!footer\\s*|\\.footer\\s*)$", headfootFormat))

        muPDFFormat = QTextCharFormat()
        muPDFFormat.setFontWeight(QFont.Bold)
        muPDFFormat.setFontItalic(True)
        muPDFFormat.setForeground(QColor("#8d0036"))
        muPDFFormat.setFontPointSize(self.textEditFontSize - 2)
        self.highlightingRules.append(("^(!muPDF\\s*|\\.muPDF\\s*)$", muPDFFormat))

        numlistFormat = QTextCharFormat()
        numlistFormat.setFontWeight(QFont.Bold)
        self.highlightingRules.append((
            "^([\\+\\-]\\s|\\s{4}[\\+\\-]\\s|\\d+\\.\\s|\\s{4}\\d+\\.\\s|\\s{4}[a-z\\d]+\\.[a-z\\d]*\\.*\\s)",
            numlistFormat
            ))

        nlFormat = QTextCharFormat()
        nlFormat.setFontWeight(QFont.Bold)
        nlFormat.setForeground(Qt.red)
        self.highlightingRules.append(("(\\\\[\\d]*!|[\\\\]{2})", nlFormat))

        codeFormat = QTextCharFormat()
        codeFormat.setFontWeight(QFont.Bold)
        codeFormat.setForeground(QColor("#222222"))
        self.highlightingRules.append(("(```|`)", codeFormat))

        colorFormat = QTextCharFormat()
        self.highlightingRules.append((
            r"(?<=[\s({]|\W)@*(@[a-z]++|#[a-fA-F\d]{6})(?=[\s)}|])",
            colorFormat,
                ))

        pycommentFormat = QTextCharFormat()
        pycommentFormat.setForeground(QColor("#5cae5c"))
        pycommentFormat.setFontItalic(True)
        self.highlightingRules.append(("^#\\s\\s\\s*[^#]+", pycommentFormat))

        commentFormat = QTextCharFormat()
        commentFormat.setForeground(Qt.gray)
        commentFormat.setFontItalic(True)
        self.highlightingRules.append(("%%\\s.+$", commentFormat))

        verbatimFormat = QTextCharFormat()
        verbatimFormat.setBackground(QColor("#eeeeee"))
        verbatimFormat.setForeground(QColor("dimgray"))
        self.highlightingRules.append(("(`|```)[^`]+?(`|```)", verbatimFormat))

        gridFormat = QTextCharFormat()
        gridFormat.setForeground(QColor("forestgreen"))
        self.highlightingRules.append(("show_grid\\(\\)", gridFormat))


        if self._sp_dict and self.spellCheck:

            for (word, pos) in self.tokenizer(text):

                if not self._sp_dict.check(word):
                    self.setFormat(pos, len(word), self.err_format)
                    self.misspellings.append((pos, pos + len(word)))
        
        for pattern, format in self.highlightingRules:
            expression = QRegularExpression(pattern)
            matchIterator = expression.globalMatch(text)

            while matchIterator.hasNext():
                match = matchIterator.next()
                index = match.capturedStart()
                length = match.capturedLength()
                disableSpell = False
                
                if format == emojiFormat:
                    for m in list(re.finditer(r"\:[+-]*[a-z\-_\d]+\:", text)):
                        self.setFormat(m.start(), m.end()-m.start(), emojiFormat)
                    disableSpell = True
                
                elif format == colorFormat:
                    try: 
                        color = text[index: index + length]
                        if "@@" in color:
                            color = text[index + 2: index + length]
                        elif "@" in color:
                            color = text[index + 1: index + length]

                        if color in colors:
                            color = colors[color]
                        elif not ("#" in color and (len(color) in [3, 7])):
                            color = False
                        if color:
                            try:
                                val = tuple(
                                    int(color[1:][i: i + 2], 16) for i in (0, 2, 4)
                                )
                                moy = sum(val) / len(val)

                                if moy <= 99 or val[1] < 110:
                                    colorFormat.setForeground(QColor(color))
                                else:
                                    colorFormat.setForeground(QColor(color).darker(150))
                                colorFormat.setFontPointSize(self.textEditFontSize - 3)
                                self.setFormat(index, length, colorFormat)
                            except Exception:
                                pass
                    except Exception:
                        pass
                    disableSpell = True
                
                # BUG: regexp act as '*' and '[|' are the same
                elif format == biFormat and text[index:].replace("*", "").strip():
                    try:
                        def remspaces(m):
                            return " " * len(m.group(1))

                        #  Remove list symbols
                        text = re.sub(
                            r"^(\*\s+|>\s{,3}\*\s|\s{4}\*\s|>\s{4}\*\s)",
                            remspaces,
                            text
                            )

                        if "*" not in text:
                            continue

                        _italics = []
                        _bolds = []
                        _bolditalics = []
                        _marks = re.compile(r"[*]+")
                        _ital = None
                        _bold = None
                        _boldit = None
                        boldF = QTextCharFormat()
                        boldF.setFontWeight(QFont.Bold)
                        italicF = QTextCharFormat()
                        italicF.setFontItalic(True)
                        bolditalicF = QTextCharFormat()
                        bolditalicF.setFontItalic(True)
                        bolditalicF.setFontWeight(QFont.Bold)
                        asteriskFormat = QTextCharFormat()
                        asteriskFormat.setForeground(QColor("gray"))
                        asteriskFormat.setFontPointSize(self.textEditFontSize - 1)

                        for mark in _marks.finditer(text):
                            _m_start = mark.start()
                            _m_end = mark.end()
                            _m_length = _m_end - _m_start

                            if _m_length == 1:
                                if _ital is None:
                                    _ital = _m_end
                                else:
                                    _italics.append((_ital, _m_start - _ital))
                                    _ital = None

                            elif _m_length == 2:
                                if _bold is None:
                                    _bold = _m_end
                                else:
                                    _bolds.append((_bold, _m_start - _bold))
                                    _bold = None

                            elif _m_length >= 3:
                                _delta = 0
                                if _ital is not None and _bold is not None:
                                    _italics.append((_ital, _m_start - _ital))
                                    _bolds.append((_bold, _m_start - _bold))
                                    _ital, _bold = None, None
                                    _delta = 3
                                elif _ital is not None and _bold is None:
                                    _italics.append((_ital, _m_start - _ital))
                                    _ital = None
                                    _bold = _m_end
                                    _delta = 1
                                elif _ital is None and _bold is not None:
                                    _bolds.append((_bold, _m_start - _bold))
                                    _bold = None
                                    _ital = _m_end
                                    _delta = 2
                                elif _ital is None and _bold is None:
                                    _ital = _m_end
                                    _bold = _m_end

                                if _m_length > 3:
                                    _diff = _m_length - _delta
                                    if _diff == 3:
                                        _ital = _m_end
                                        _bold = _m_end
                                    elif _diff == 2:
                                        _bold = _m_end
                                    elif _diff == 1:
                                        _ital = _m_end

                        for it in _italics:
                            _it = set(range(it[0], it[0] + it[1] + 1))
                            for bd in _bolds:
                                _bd = set(range(bd[0], bd[0] + bd[1] + 1))
                                _inter = tuple(_it.intersection(_bd))
                                if _inter:
                                    _bolditalics.append((min(_inter), max(_inter) - min(_inter)))                 

                        for bd in _bolds:
                            self.setFormat(bd[0], bd[1], boldF)
                            self.spellWords(text, bd[0], boldF, bd[0]+bd[1])
                        for it in _italics:
                            self.setFormat(it[0], it[1], italicF)
                            self.spellWords(text, it[0], italicF, it[0]+it[1])
                        for bi in _bolditalics:
                            self.setFormat(bi[0], bi[1], bolditalicF)
                            self.spellWords(text, bi[0], bolditalicF, bi[0]+bi[1])
                        for ast in re.finditer(r"[*]+", text):
                            self.setFormat(ast.start(), ast.end()-ast.start(), asteriskFormat)
                    except Exception:
                        pass
                    disableSpell = True
                
                # BUG: regexp act as '*' and '[|' are the same
                elif format == buttonFormat \
                        and "*" not in text[index: index + 1]:
                    try:
                        buttons = re.search(
                            r"\[([@#\da-z]*)\|[^|]+?\|([@#\da-z]*)\]",
                            text[index: index + length],
                        )

                        if buttons:
                            buttonf = QTextCharFormat()
                            buttonf.setBackground(QColor("#dddddd"))
                            bgcolor = buttons.group(1)
                            fcolor = buttons.group(2)
                            bgcolor = colors[bgcolor[1:]] if bgcolor[1:] in colors else bgcolor
                            fcolor = colors[fcolor[1:]] if fcolor[1:] in colors else fcolor

                            if bgcolor:
                                buttonf.setBackground(QColor(bgcolor))

                            if fcolor:

                                if fcolor == "w" or fcolor == "white":
                                    fcolor = "lightgray"

                                buttonf.setForeground(QColor(fcolor))

                            self.setFormat(index, length, buttonf)
                    except Exception:
                        pass
                    disableSpell = True
                
                elif format in (quoteFormat, truefalseFormat, paramnumberFormat):
                    self.setFormat(index, length, format)
                    disableSpell = True
                
                elif format == imgFormat:
                    endpos = findBalanced("(", text, index + length) + 1
                    self.setFormat(index, endpos - index, format)

                    if text[index + 2: index + length]:
                        legendFormat = QTextCharFormat()
                        legendFormat.setFontItalic(True)
                        legendFormat.setForeground(QColor("forestgreen"))
                        self.setFormat(index + 2, length - 4, legendFormat)
                        self.spellWords(text, index+2, legendFormat, index+length-2)
                        disableSpell = True
                
                elif format == linkFormat:
                    try:
                        endpos = findBalanced("[", text, index + 1) + 2
                        endlink = findBalanced("(", text, endpos)
                        tks = [
                            "/",
                            "http",
                            "www",
                            "mp4",
                            "MP4",
                            "flv",
                            "FLV",
                            "wmv",
                            "WMV",
                            "mov",
                            "MOV",
                            "webm",
                            "WEBM",
                            "avi",
                            "AVI",
                            "mp3",
                            "MP3",
                            "ogg",
                            "OGG",
                            "m4a",
                            "M4A",
                            "flac",
                            "FLAC",
                            "wma",
                            "WMA",
                            "aac",
                            "AAC",
                        ]

                        if not any(
                            [True if t in text[endpos:endlink] else False for t in tks]
                        ):
                            tablelegendFormat = QTextCharFormat()
                            tablelegendFormat.setForeground(Qt.darkMagenta)
                            tablelegendFormat.setFontPointSize(self.textEditFontSize - 2)
                            self.setFormat(index, endlink + 1 - index, tablelegendFormat)
                            legendFormat = QTextCharFormat()
                            legendFormat.setForeground(QColor("forestgreen"))
                            legendFormat.setFontPointSize(self.textEditFontSize - 2)
                            self.setFormat(index+1, endpos - index - 3, legendFormat)
                            self.spellWords(text, index+1, legendFormat, endpos-2)
                            disableSpell = True
                        else:
                            self.setFormat(index, endlink + 1 - index, format)

                    except Exception:
                        pass
                
                elif format in (matplotFormat, codeblockFormat):
                    try:
                        t = text[index:]
                        self.setFormat(index, length, format)
                        stleg = t.find("[", index) + 1
                        endleg = t.find("]", stleg)
                        leg = t[stleg:endleg]
                        if endleg < 0:
                            endleg = 0
                        start = t.find("(", endleg) + 1
                        stop = t.find(")", start)
                        if stop > start >= 0:
                            oformat = QTextCharFormat()
                            oformat.setFontItalic(True)
                            oformat.setFontPointSize(self.textEditFontSize - 2)
                            oformat.setForeground(QColor("blue"))
                            self.setFormat(start, stop - start, oformat)
                            linenum = t.find("#")

                            if linenum >= 0:
                                lnformat = QTextCharFormat()
                                lnformat.setFontWeight(QFont.Bold)
                                lnformat.setFontPointSize(self.textEditFontSize - 2)
                                lnformat.setForeground(QColor("#ce044e"))
                                self.setFormat(linenum, 1, lnformat)

                        if stleg >= 0 and endleg > 0:
                            strboxformat = QTextCharFormat()
                            strboxformat.setFontItalic(True)
                            strboxformat.setForeground(QColor("seagreen"))
                            self.setFormat(stleg, len(leg), strboxformat)
                    except Exception:
                        pass
                    disableSpell = True
                
                elif format == alignFormat:
                    self.setFormat(index, length, format)
                    disableSpell = True
                
                elif format in (boxFormat, fontFormat, valignFormat):
                    try:
                        t = text[index:]
                        stleg = t.find("[") + 1
                        endleg = t.find("]", stleg)

                        if endleg < 0:
                            endleg = 0

                        start = t.find("(", endleg) + 1
                        stop = t.find(")", start)

                        leg = t[stleg:endleg]
                        self.setFormat(index, length, format)

                        if format == boxFormat and leg:
                            strboxformat = QTextCharFormat()
                            strboxformat.setFontPointSize(self.textEditFontSize - 2)
                            strboxformat.setForeground(QColor("seagreen"))
                            self.setFormat(stleg, len(leg), strboxformat)

                        if stop > start >= 0:
                            oformat = QTextCharFormat()
                            oformat.setFontPointSize(self.textEditFontSize - 2)
                            oformat.setForeground(QColor("#dd6902"))
                            oformat.setFontItalic(True)
                            self.setFormat(index + start, stop - start, oformat)
                    except Exception:
                        pass
                    disableSpell = True
                
                elif format == tscFormat:
                    try:
                        t = text[index:]
                        cw = [
                            [m.end(), findBalanced("{", t, m.end())]
                            for m in re.finditer(r"\([^\)\(]*\)\{", t)
                        ]
                        w = ""

                        if not cw:
                            return

                        wstart, wstop = cw[0]

                        if wstart and wstop:
                            w = t[wstart:wstop]
                            if w:
                                self.setFormat(index, wstart, format)
                                self.setFormat(index+wstop, 1, format)

                            topts = t[:wstart]
                            topts = re.sub(r"![^\s]+", "", topts)
                            c = re.findall(r"@([a-z]+)", topts)
                            cwords = QTextCharFormat()

                            if c:
                                c = c[0]

                                if c in colors:
                                    cwords.setForeground(QColor(colors[c]))

                                    if (
                                        sum(
                                            tuple(
                                                int(colors[c][1:][i: i + 2], 16)
                                                for i in (0, 2, 4)
                                            )
                                        )
                                        > 500
                                    ):
                                        cwords.setForeground(QColor(colors[c]).darker(140))
                                    self.setFormat(index + wstart, len(w), cwords)

                                else:
                                    self.setFormat(index + wstart, len(w), cwords)

                            else:
                                c = re.findall(r"#[\da-f]{6}", t[:wstart])

                                if c:
                                    c = c[0]
                                    cwords.setForeground(QColor(c))

                                    if (
                                        sum(
                                            tuple(
                                                int(c[1:][i: i + 2], 16) for i in (0, 2, 4)
                                            )
                                        )
                                        > 500
                                    ):
                                        cwords.setForeground(QColor(c).darker(140))

                                    self.setFormat(index + wstart, len(w), cwords)

                                else:
                                    self.setFormat(index + wstart, len(w), cwords)

                        self.spellWords(w, index + wstart, cwords)

                    except Exception:
                        pass
                    disableSpell = True
                
                else:
                    self.setFormat(index, length, format)
                    
                if not disableSpell:
                    self.spellWords(text[index: index + length], index, format)

        # Store the list so the context menu can reuse this tokenization pass
        # (Block-relative values so editing other blocks won't invalidate them)
        data = QTextBlockUserData()
        data.misspelled = self.misspellings
        self.setCurrentBlockUserData(data)

        if not self.stop:
            self.go = False
