# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

from PyQt5.QtWidgets import (
    QScrollArea
)

from PyQt5.QtCore import (
    Qt,
    QEvent,
    pyqtSignal
)


class ImageScrollArea(QScrollArea):

    keyPressed = pyqtSignal(int)

    def __init__(self, parent=None):
        super(ImageScrollArea, self).__init__(parent)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.setFocus(True)

    def keyPressEvent(self, event):
        self.keyPressed.emit(event.key())

    def event(self, event):
        if event.type() == QEvent.Enter:
            self.setFocus(True)    
        return super().event(event)
