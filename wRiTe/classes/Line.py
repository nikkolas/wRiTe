# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.


from numpy import isclose


class Line:
    def __init__(self, p1, p2):

        if (isinstance(p1, tuple) and len(p1) == 2) and (
            isinstance(p2, tuple) and len(p2) == 2
        ):
            self.type = "line"
            self.isVertical = False
            self.x = None
            self.p1 = p1
            self.p2 = p2
            self.center = ((p2[0] - p1[0]) / 2, (p2[1] - p1[1]) / 2)
            self.length = ((p1[0] - p2[0]) ** 2 + (p1[1] - p2[1]) ** 2) ** 0.5

            x1, y1 = p1
            x2, y2 = p2

            if not isclose(x1, x2, 1e-9):
                self.slope = (y1 - y2) / (x1 - x2)
                self.offset = (y2 * x1 - y1 * x2) / (x1 - x2)
                self.eq = "{}*x + {}".format(self.slope, self.offset)
            else:
                self.isVertical = True
                self.slope = None
                self.offset = None
                self.x = x1
                self.eq = ""
