# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

try:
    import qpageview
except ModuleNotFoundError:
    print("module qpageview not found")

try:
    from qpageview.widgetoverlay import WidgetOverlayViewMixin
except ModuleNotFoundError:
    print("module qpageview.widgetoverlay not found")

try:
    from PyQt5.QtCore import Qt, pyqtSignal
except ModuleNotFoundError:
    print("module QtCore not found")


class PageView(
    qpageview.link.LinkViewMixin,
    qpageview.widgetoverlay.WidgetOverlayViewMixin,
    qpageview.view.View,
):

    keyPressed = pyqtSignal(int)
    clickedPosition = pyqtSignal(int, int)
    doubleClickedY = pyqtSignal(int, int)


    def __init__(self, parent=None):
        super().__init__(parent)
        self.installEventFilter(self)
        self.ctrlPressed = False

    def enterEvent(self, event):
        self.setFocus(True)


    def keyPressEvent(self, event):
        if event.key() == Qt.Key_F:
            self.setViewMode(qpageview.FitWidth)
        if event.modifiers() & Qt.ControlModifier:
            self.ctrlPressed = True
            cursor = self.cursor()
            cursor.setShape(Qt.CursorShape.CrossCursor)
            self.setCursor(cursor)
        self.keyPressed.emit(event.key())


    def keyReleaseEvent(self, event):
        cursor = self.cursor()
        cursor.setShape(Qt.CursorShape.ArrowCursor)
        self.setCursor(cursor)
        self.ctrlPressed = False


    def mousePressEvent(self, event):
        if not self.ctrlPressed:
            return
        pos = event.pos() - self.layoutPosition()
        page = self.pageLayout().pageAt(pos)

        if page:
            x = (pos.x() - page.x)/page.width*page.pageWidth
            y = (pos.y() - page.y)/page.height*page.pageHeight
            self.clickedPosition.emit(int(round(x,0)), int(round(y,0)))


    def mouseDoubleClickEvent(self, event):
        y = event.y()
        pos = event.pos() - self.layoutPosition()
        page = self.pageLayout().pageAt(pos)
        if page:
            self.doubleClickedY.emit(pos.y(), y)

        
