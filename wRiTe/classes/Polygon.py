# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.


from numpy import isclose, array, abs, dot, roll
from classes.Line import Line


class Polygon:
    def __init__(self, points):
        self.lines = []
        self.type = "polygon"
        self.p = points

        if not isclose(points[-1], points[0], 1e-9):
            points.append(points[0])

        self.lines = [Line(points[i], points[i + 1]) for i in range(len(points) - 1)]

        #  Length
        self.length = 0

        for L in self.lines:
            self.length += L.length

        #  Area with Shoelace formula
        #  get x and y vectors
        points = array(points)
        x = points[:, 0]
        y = points[:, 1]
        self.area = 0.5 * abs(dot(x, roll(y, 1)) - dot(y, roll(x, 1)))
