# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

from PyQt5.QtWidgets import (
    QDialog,
    QWidget,
    QLineEdit,
    QPushButton,
    QVBoxLayout,
    QHBoxLayout,
)
from PyQt5.QtCore import Qt, pyqtSignal


class Pwd(QDialog):
    pwdok = pyqtSignal(str)

    def __init__(self, parent=None):
        super(Pwd, self).__init__(parent)
        self.password = None
        self.passwordLE = QLineEdit(self)
        self.passwordLE.setEchoMode(QLineEdit.Password)
        self.buttonEnter = QPushButton("Enter", self)
        self.buttonEnter.setFocus()
        self.buttonCancel = QPushButton("Cancel", self)
        self.buttonCancel.setFocusPolicy(Qt.NoFocus)
        self.buttonEnter.clicked.connect(self.sendPassword)
        self.buttonCancel.clicked.connect(self.cancel)
        buttons = QWidget(self)
        buttonsLayout = QHBoxLayout(self)
        buttonsLayout.addWidget(self.buttonCancel)
        buttonsLayout.addWidget(self.buttonEnter)
        buttons.setLayout(buttonsLayout)
        layout = QVBoxLayout(self)
        layout.addWidget(self.passwordLE)
        layout.addWidget(buttons)
        self.setWindowTitle("Enter password:")

    def sendPassword(self):
        if self.passwordLE.text():
            self.pwdok.emit(self.passwordLE.text())
            self.accept()

    def cancel(self):
        self.reject()

    def keyPressEvent(self, event):
        if event.key() in (Qt.Key_Enter, Qt.Key_Return):
            self.sendPassword()
