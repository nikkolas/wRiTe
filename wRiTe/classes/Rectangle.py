# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.


from classes.Line import Line


class Rectangle:
    def __init__(self, p, lx, ly):
        self.type = "rectangle"
        self.p0 = p
        self.lx = lx
        self.ly = ly
        points = [
            p,
            (p[0], p[1] + ly),
            (p[0] + lx, p[1] + ly),
            (p[0] + lx, p[1]),
            p
        ]
        self.p = points[:-1]
        self.lines = []
        self.length = 2 * (lx + ly)
        self.area = lx * ly
        self.lines = [Line(points[i], points[i + 1]) for i in range(len(points) - 1)]
        self.diag1 = Line(p, (p[0] + lx, p[1] + ly))
        self.diag2 = Line((p[0], p[1] + ly), (p[0] + lx, p[1]))
        self.center = (p[0] + lx / 2, p[1] + ly / 2)
