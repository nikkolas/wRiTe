# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

from pathlib import Path
import os
import re


class SManager:
    def __init__(self):
        file_name = "wRiTe"
        user_dir = ".config"
        sdir = os.path.join(str(Path.home()), user_dir)
        self.sfile = None

        if os.path.isdir(sdir):
            self.sfile = os.path.join(sdir, file_name)

    def check_sfile(self):
        if self.sfile and os.path.exists(self.sfile):
            return True
        return False

    def get_settings(self):
        if self.check_sfile:
            settings = {}
            with open(self.sfile, 'r') as f:
                sline = f.readline()
                while sline:
                    m = re.match(r"@([A-Z][a-z]+)\s*=\s*(.+)", sline)
                    if m:
                        settings[m.group(1)] = m.group(2).strip()
                    sline = f.readline()
            return settings
        return

    def get_setting(self, key):
        settings = self.get_settings()
        if key in settings:
            return settings[key]
        return

    def create_sfile(self):
        sfile = """# wRiTe settings

@Wwidth = 1500

@Wheight = 800

@Font = Calibri Light

@Fontsize = 19

@Specials = αβƔ

@Editorbgcolor = #fffdfa

@Author = Unknown
"""
        if self.sfile:
            with open(self.sfile, 'w') as f:
                f.write(sfile)
            return True
        return False

    def save_settings(self, settings):
        sfile = "# wRiTe settings\n\n\n"
        for k in settings:
            sfile = "".join((sfile, "@{} = {}\n\n".format(k, settings[k])))
        if self.sfile:
            with open(self.sfile, 'w') as f:
                f.write(sfile)
