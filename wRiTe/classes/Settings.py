# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

from pathlib import Path
import os

from PyQt5.QtWidgets import (
    QDialog,
    QWidget,
    QComboBox,
    QLabel,
    QVBoxLayout,
    QHBoxLayout,
    QPushButton,
    QColorDialog,
    QLineEdit
)
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QFontDatabase

from classes.SManager import SManager


class Settings(QDialog):
    textEditFontChanged = pyqtSignal(str, int)
    textEditBgColorChanged = pyqtSignal(str)

    def __init__(self, currentFont="Calibri Light", currentSize="18", parent=None):
        super(Settings, self).__init__(parent)
        SM = SManager()
        self.settings = SM.get_settings()
        self.font = currentFont
        self.fontsize = self.settings['Fontsize'] if 'Fontsize' in self.settings else '17'
        author = self.settings['Author'] if 'Author' in self.settings else ''
        authorLabel = QLabel("Author")
        self.authorLineEdit = QLineEdit(author)
        self.authorLineEdit.textChanged.connect(self.authorChanged)
        fontLabel = QLabel("Editor Font")
        sizeLabel = QLabel("Font size")
        colorLabel = QLabel("Editor background color")
        fontCombo = QComboBox()
        self.textEditBgColor = self.settings['Editorbgcolor'] if 'Editorbgcolor' in self.settings else "#fffdfa"

        sizeCombo = QComboBox()
        self.colorButton = QPushButton()
        self.colorButton.setText("  {}  ".format(self.textEditBgColor))
        self.colorButton.setFlat(True)
        self.colorButton.setStyleSheet(
            "QPushButton {{background-color: {}; border: 1px outset black; border-radius: 5px;}}".format(self.textEditBgColor)
            )
        self.colorButton.clicked.connect(self.chooseBgColor)

        hlayout1 = QHBoxLayout()
        hlayout2 = QHBoxLayout()
        hlayout3 = QHBoxLayout()
        hlayout4 = QHBoxLayout()

        hlayout1.addWidget(fontLabel)
        hlayout1.addWidget(fontCombo)
        hlayout2.addWidget(sizeLabel)
        hlayout2.addWidget(sizeCombo)
        hlayout3.addWidget(colorLabel)
        hlayout3.addWidget(self.colorButton)
        hlayout4.addWidget(authorLabel)
        hlayout4.addWidget(self.authorLineEdit)
        vlayout = QVBoxLayout()
        w4 = QWidget()
        w4.setLayout(hlayout4)
        w1 = QWidget()
        w1.setLayout(hlayout1)
        w2 = QWidget()
        w2.setLayout(hlayout2)
        w3 = QWidget()
        w3.setLayout(hlayout3)
        vlayout.addWidget(w4)
        vlayout.addWidget(w1)
        vlayout.addWidget(w2)
        vlayout.addWidget(w3)
        self.setLayout(vlayout)

        fontBase = QFontDatabase()
        systemFonts = fontBase.families()
        systemFonts = [
            font.split("[")[0].strip() if "[" in font else font
            for font in systemFonts
            ]
        fontCombo.addItems(systemFonts)
        fontCombo.setCurrentText(self.font)
        fontCombo.currentTextChanged.connect(self.fontChanged)
        sizeCombo.addItems([str(e) for e in range(8, 31, 1)])
        sizeCombo.setCurrentText(self.fontsize)
        sizeCombo.currentTextChanged.connect(self.sizeChanged)
        self.adjustSize()
        self.setWindowTitle("wRiTe editor settings")


    def authorChanged(self, author):
        self.settings['Author'] = author

    def fontChanged(self, text):

        if text != self.font:
            self.settings['Font'] = text
            self.font = text
            self.textEditFontChanged.emit(self.font, int(self.fontsize))

    def sizeChanged(self, text):

        if text != self.fontsize:
            self.settings['Fontsize'] = text
            self.fontsize = text
            self.textEditFontChanged.emit(self.font, int(self.fontsize))

    def chooseBgColor(self):
        color = QColorDialog.getColor()

        if color.isValid():
            self.textEditBgColor = color.name()
            self.colorButton.setText("  {}  ".format(self.textEditBgColor))
            self.colorButton.setStyleSheet(
                "QPushButton {{background-color: {}; border: 1px outset black; border-radius: 5px;}}".format(self.textEditBgColor)
                )
            self.textEditBgColorChanged.emit(self.textEditBgColor)
            self.settings['Editorbgcolor'] = self.textEditBgColor



    def closeEvent(self, event):
        SM = SManager()
        SM.save_settings(self.settings)
