# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import os
import re

try:
    from enchant import Dict, list_languages
except ModuleNotFoundError:
    print("enchant not installed")

try:
    from urllib.request import urlretrieve
    from urllib.parse import unquote
except ModuleNotFoundError:
    print("urllib not installed")

from PyQt5.QtCore import (
    QEvent,
    QMimeData,
    Qt,
    pyqtSignal,
    QTimer,
    QPoint,
    QRect,
    QSize,
    QRegularExpression
)

from PyQt5.QtGui import (
    QFocusEvent,
    QTextCursor,
    QDropEvent,
    QColor,
    QPainter,
    QTextFormat,
)

from PyQt5.QtWidgets import (
    QMenu,
    QAction,
    QActionGroup,
    QCompleter,
    QPlainTextEdit,
    QWidget,
    QTextEdit,
)
from classes.Highlighter import Highlighter
from classes.utils import increment
from classes.Tooltip import Tooltip

try:
    from enchant.utils import trim_suggestions
except ImportError:  # Older versions of PyEnchant as on *buntu 14.04

    def trim_suggestions(word, suggs, maxlen, calcdist=None):
        return suggs[:maxlen]


class TextEdit(QPlainTextEdit):

    keyPressed = pyqtSignal(int)
    addNewTitle = pyqtSignal(bool)
    highlightFinished = pyqtSignal()
    released = pyqtSignal()
    textEditSizeChanged = pyqtSignal()
    max_suggestions = 20
    isHL = False
    first = True
    showLineNumbers = False
    delta = 0
    lastPos = 0
    F8 = False

    def __init__(self, parent=None):
        super().__init__(parent)
        self.Tooltip = Tooltip(self)
        self.blockNumber = -1
        self.textChanged.connect(self.test_if_ready)
        self.textChanged.connect(self.tooltip)
        self.cursorPositionChanged.connect(self.tooltip)
        self.blockCountChanged.connect(self.blockcount_has_changed)
        self.document().blockCountChanged.connect(self.updateLastListItem)
        self.completer = None
        self.highlighter = None
        self.data_path = ""
        self.documentLines = 0
        self.subout = False
        self.isReady = False
        self.bchc = False
        self.totalBlocks = 0
        self.replaceBarIsShown = False
        self.first = False
        self.bqrgbcolor = None

        #  line numbers and cursor highlight
        self.lineNumberArea = LineNumberArea(self)
        self.blockCountChanged.connect(self.updateLineNumberAreaWidth)
        self.updateRequest.connect(self.updateLineNumberArea)
        self.textChanged.connect(self.highlightLine)
        self.updateLineNumberAreaWidth(0)


    def enterEvent(self, event):
        self.setFocus(True)


    def initializeHighlighter(self):
        self.highlighter = Highlighter(self.document())
        self.highlighter.setDict(Dict())
        self.highlighter.bqrgbcolor = self.bqrgbcolor
        self.first = True
        self.blockcount_has_changed()


    def deleteHighlighter(self):
        self.highlighter = None


    def showToolTip(self, message=""):
        if self.Tooltip.isVisible() and message in self.Tooltip.text():
            return
        else:
            self.Tooltip.hide()

        cursor = self.textCursor()
        self.Tooltip.setText(message)
        self.Tooltip.setFixedWidth(int(self.size().width()*2/3))
        self.Tooltip.setStyleSheet(
            "background-color: whitesmoke; border: 1px solid black;"
            )
        self.Tooltip.setMargin(10)
        self.Tooltip.adjustSize()
        self.Tooltip.setParent(self)
        self.Tooltip.show()
        cursorY = self.cursorRect(cursor).top()
        teCenter = self.rect().center()

        if cursorY > self.Tooltip.height() + 50:
            moveY = int(cursorY - 50 - self.Tooltip.height())
        else:
            moveY = int(cursorY + 50)

        self.Tooltip.move(
            QPoint(teCenter.x()-int(self.Tooltip.width()/2), moveY)
            )


    def tooltip(self):
        if not self.F8:
            self.Tooltip.hide()
            return

        cursor = self.textCursor()
        cursor.movePosition(QTextCursor.StartOfBlock)
        cursor.movePosition(QTextCursor.EndOfBlock, QTextCursor.KeepAnchor)
        command = re.findall("^!([a-z]+)", cursor.selectedText())
        function = re.findall(r"\s*w([A-Z][a-z]+)", cursor.selectedText())
        cursor.clearSelection()

        if command:
            command = command[0]
            commanddict = {
                "box": '<span style="color: purple;"><b>!box</b></span>[<i>Title</i>](<i>bordercolor backgroundcolor linewidth(px) <u>shadow</u></i>)<br /> => <span style="color: forestgreen;">Draw a frame of 1px around a text block.</span>',
                "wbox": '<span style="color: purple;"><b>!wbox</b></span>[<b>Title</b>](<i>titlebarcolor backgroundcolor <u>shadow</u></i>)<br /> => <span style="color: forestgreen;">Draw a box with a colored titlebar.</span>',
                "pageframe": '<span style="color: blue;"><b>!pageframe</b></span>(<i>bordercolor height</i>)<br /> => <span style="color: forestgreen;">Draw a colored frame around % of page. Default is whole page.</span>',
                "vcenter": '<span style="color: blue;"><b>!vcenter</b></span> <i>no options... </i><br /> => <span style="color: forestgreen;">Text block is centered vertically.</span>',
                "vhcenter": '<span style="color: blue;"><b>!vhcenter</b></span> <i>no options... </i><br /> => <span style="color: forestgreen;">Text block is centered horizontally and vertically.</span>',
                "center": '<span style="color: blue;"><b>!center</b></span> <i>no options... </i><br /> => <span style="color: forestgreen;">Text centered in page.</span>',
                "right": '<span style="color: blue;"><b>!right</b></span>(<i><u>left</u></i>)<br /> => <span style="color: forestgreen;">Text is right aligned in page. Optional left beginning.</span>',
                "left": '<span style="color: blue;"><b>!left</b></span>(<i><u>right</u></i>)<br /> => <span style="color: forestgreen;">Text is left aligned in page. Optional right beginning.</span>',
                "python": '<span style="color: darkblue;"><b>!python</b></span>[<i>output name</i>](<i><u>output</u> <u>num</u></i>)<br /> => <span style="color: forestgreen;">Python code block. Add terminal output or line numbers.</span>',
                "graph": '<span style="color: orange;"><b>!graph</b></span>[<i>legend</i>](<i>s|g|gs|sg right|left width <u>border</u> <u>shadow</u>)<br /> => <span style="color: forestgreen;">Create a graphic (g) or a script (s) from Matplotlib code.</span>',
                "gtab": '<span style="color: purple;"><b>!gtab</b></span>(<i>width(in %) scale-factor(0..1)</i> <i><u>tab</u></i>)<br /> => <span style="color: forestgreen;">Create music scores or tabs for guitar using Vextab syntax.</span>',
                "mc": '<span style="color: purple;"><b>!mc+<i>number</i></b></span>(<i>line height</i>)<br /> => <span style="color: forestgreen;">Multicolumn text. <i>num</i> is column number.</span>',
                "font": '<span style="color: darkblue;"><b>!font</b></span>(<i><u>@font_name</u> <u>font_size</u> <u>font_color</u> <b>italic</b> <b>bold</b></i>)<br /> => <span style="color: forestgreen;">Apply special font to a text block.</span>',
                "diagram": '<span style="color: darkblue;"><b>!diagram</b></span> <i>no options... </i><br /> => <span style="color: forestgreen;">Insert Mermaid diagrams code.</span>',
            }

            commandlist = [item[: len(command)] for item in list(commanddict)]

            if command in commandlist:
                command = list(commanddict)[commandlist.index(command)]
                self.showToolTip(commanddict[command])
            else:
                self.Tooltip.hide()

        elif function:
            function = function[0]
            functiondict = {
                "Function": "<b>wFunction(</b>'<b><i>f(x)</i></b>', <i>xmin= ,xmax= ,ymin= ,ymax= ,xstep= ,ystep= ,xlabel= ,ylabel= ,intx= ,inty= ,restr= ,pts= ,color= ,lw= ,ls= ,y0= ,fs= ,label= ,legendpos= ,xaxpos= ,yaxpos= ,fill= ,fillinv= ,fillcolor= ,fillalpha= </i><b>)</b><br /> => <span style=\"color: forestgreen;\">Draw a function or a list of functions using Matplotlib. Return wFunction object.</span>",
                "Circle": '<b>wCircle(</b> <b>(xcenter, ycenter)</b>, <b>radius</b>, <i>center=True/False, *mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Draw a cercle at a given coordinates and with a specified radius. Return wCircle object.</span>',
                "Rectangle": '<b>wRectangle(</b> <b>(xbottomleft, ybottomleft)</b>, <b>width</b>, <b>height</b>, <i>center=True/False, rotation= , *mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Draw a rectangle from a given coordinates, width and height. Return wRectangle object.</span>',
                "Polygon": '<b>wPolygon(</b> <b>[(x1, y1), (x2, y2), ...]</b>, <i>*mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Draw a polygon from a list of coordinates. Return a wPolygon object.</span>',
                "Curve": '<b>wCurve(</b> <b>[(x1, y1), (x2, y2), ...]</b>, <i>k= , *mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Draw a curve defined by a list of coordinates and optional order <i>k</i> of polynomial fit. Return a wCurve object.</span>',
                "Text": '<b>wText(</b> <b>(x, y), string</b>, <i>rotation= , *mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Insert text at a specified coordinates and rotated in degrees.</span>',
                "Line": '<b>wLine(</b> <b>(x1, y1), (x2, y2)</b>, <i>*mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Draw a line between 2 coordinates. Return a wLine object.</span>',
                "Linearfit": '<b>wLinearfit(</b> <b>xList, yList</b>, <i>*mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Draw (x,y) data and a linear regression line with results. Return a wLinearfit object.</span>',
                "Dots": "<b>wDots(</b> <b>(x,y) or list of tuples</b>, <i>'x', *mplargs</i><b>)</b><br /> => <span style=\"color: forestgreen;\">Draw a (x,y) dot or list of dots with optional marker.</span>",
                "Intersect": '<b>wIntersect(</b> <b>wObj1, wObj2</b><b>)</b><br /> => <span style="color: forestgreen;">Compute intersections between 2 wObjects. Return a list of intersections coordinates.</span>',
                "Bar": '<b>wBar(</b> <b>datalist, valueslist</b>, <i>edgecolor="black", alpha=1, color="#1f77b4", xlabel="", ylabel="", grid=False, zorder=10, *mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Compute and plot an histogram.</span>',
                "Pie": '<b>wPie(</b> <b>datalist of strings "label:value,color,<i>n</i>e"</b>, <i>legend=True, shadow=False, startangle=0, outfontcolor="black", infontcolor="white", outfontsize="x-large", infontsize="x-large", outfontbold=False, infontbold=False, *mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Compute and plot pie charts.</span>',
                "Bigdot": '<b>wBigdot(</b> <b>(x, y)</b>, <i>radius=1, color="black", *mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Draw a big dot at given coordinates.</span>',
                "Ellipse": '<b>wEllipse(</b> <b>(x, y), width, height</b>, <i>fill=False, *mplargs</i> <b>)</b><br /> => <span style="color: forestgreen;">Draw an ellipse at given coordinates, width and height.</span>',
                "Fill": '<b>wFill(</b> <b>wObj1, wObj2</b>, <i>fillinv=False, *mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Fill the area between two wRiTe Objects. NOT WORKING FOR NOW!</span>',
                "Angle": '<b>wAngle(</b> <b>wLine1, wLine2</b>, <i>int pos1, int pos2, string label, string shape, *mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Compute and draw an angle between two sections pos1 and pos2 of two given wLines.</span>',
                "Arrow": '<b>wArrow(</b> <i>from</i> <b>(x1, y1)</b>, <i>to</i> <b>(x2, y2)</b>, <i>style="->", *mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Draw an arrow from coordinates 1 to coordinates 2.</span>',
                "Params": '<b>wParams(</b> <i>xmin=0, ymin=0, xmax=1, ymax=1, xlegend="", ylegend="", xlabel="", ylabel="", xstep=0, ystep=0, xmstep=0, ymstep=0, grid=True, mgrid=False, title="", fontsize=14, intx=False, inty=False, y0=False, x0=True, bgcolor="", axcrossed=False, xaxpos=0, yaxpos=0, axis=True, equal=False, size=None, ticks=True</i><b>)</b><br /> => <span style="color: forestgreen;">Define axis and plot parameters.</span>',
                "Deriv": '<b>wDeriv(</b> <b>wFunction</b>, <i>*mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Compute and plot derivative of function.</span>',
                "Integ": '<b>wInteg(</b> <b>wFunction, (x0, y0)</b> <i>known primitive coordinate</i>, <i>*mplargs</i><b>)</b><br /> => <span style="color: forestgreen;">Compute and plot primitive of function.</span>',
            }
            if function in functiondict:
                self.showToolTip(functiondict[function])
            else:
                self.Tooltip.hide()
        else:
            self.Tooltip.hide()


    def lineNumberAreaWidth(self):
        if self.showLineNumbers:
            digits = 1
            count = max(1, self.blockCount())

            while count >= 10:
                count /= 10
                digits += 1

            space = 3 + self.fontMetrics().width("9") * digits
            return space
        return 0


    def updateLineNumberAreaWidth(self, _):

        if self.showLineNumbers:
            self.setViewportMargins(self.lineNumberAreaWidth(), 0, 0, 0)
        else:
            self.setViewportMargins(0, 0, 0, 0)


    def updateLineNumberArea(self, rect, dy):

        if dy:
            self.lineNumberArea.scroll(0, dy)
        else:
            self.lineNumberArea.update(
                0, rect.y(), self.lineNumberArea.width(), rect.height()
            )

        if rect.contains(self.viewport().rect()):
            self.updateLineNumberAreaWidth(0)


    def resizeEvent(self, event):
        super().resizeEvent(event)
        self.textEditSizeChanged.emit()
        cr = self.contentsRect()
        self.lineNumberArea.setGeometry(
            QRect(cr.left(), cr.top(), self.lineNumberAreaWidth(), cr.height())
        )


    def lineNumberAreaPaintEvent(self, event):
        mypainter = QPainter(self.lineNumberArea)
        mypainter.fillRect(event.rect(), QColor("#dfdfdf"))
        block = self.firstVisibleBlock()
        blockNumber = block.blockNumber()
        top = self.blockBoundingGeometry(block).translated(self.contentOffset()).top()
        bottom = top + self.blockBoundingRect(block).height()

        # use the right font
        height = self.fontMetrics().height()

        while block.isValid() and (top <= event.rect().bottom()):
            if block.isVisible() and (bottom >= event.rect().top()):
                number = str(blockNumber + 1)
                mypainter.setPen(QColor("#666666"))
                mypainter.drawText(
                    0,
                    int(top),
                    self.lineNumberArea.width(),
                    height,
                    Qt.AlignCenter,
                    number,
                )
            block = block.next()
            top = bottom
            bottom = top + self.blockBoundingRect(block).height()
            blockNumber += 1


    def highlightLine(self):
        extraSelections = []
        for pattern in (
                ["!newpage", "#fff0f0"],
                ):
            index = 0
            for _ in range(self.toPlainText().count("\n{}".format(pattern[0]))):
                cursor = self.document().find(
                    QRegularExpression("^\\{}".format(pattern[0])),
                    index
                    )
                index = cursor.position()
                selection = QTextEdit.ExtraSelection()
                lineColor = QColor(pattern[1])
                selection.format.setBackground(lineColor)
                selection.format.setProperty(QTextFormat.FullWidthSelection, True)
                selection.cursor = cursor
                selection.cursor.clearSelection()
                extraSelections.append(selection)
            self.setExtraSelections(extraSelections)

    #  Speed: Highlight visible area first, then rest of doc.
    #  When finished, back to normal behavior
    def highlight_blockrange(self):
        if not self.first:
            return

        self.isHL = True

        for block_number in range(self.to_block + 1):
            self.highlighter.go = True
            self.highlighter.rehighlightBlock(
                self.document().findBlockByNumber(block_number)
            )

        self.from_block = self.to_block + 1
        self.to_block = self.totalBlocks

        QTimer.singleShot(0, self.highlight_block)


    def highlight_block(self):
        if not self.first:
            return

        if self.to_block - self.from_block >= 0:
            self.highlighter.go = True
            block = self.document().findBlockByNumber(self.from_block)
            self.highlighter.rehighlightBlock(block)
            self.from_block += 1
            QTimer.singleShot(0, self.highlight_block)
        else:
            self.isHL = False
            self.bchc = False
            self.first = False
            self.highlighter.stop = True
            try:
                self.blockCountChanged.disconnect()
            except Exception:
                pass
            self.highlightFinished.emit()


    def blockcount_has_changed(self):
        if self.isHL or not self.isReady or not self.first:
            return

        self.bchc = True
        cursor = self.textCursor()
        self.from_block = 0
        end_pos = self.cursorForPosition(
            QPoint(self.viewport().width(), self.viewport().height())
        ).position()
        cursor.setPosition(end_pos)
        self.to_block = cursor.blockNumber()
        cursor.setPosition(0)
        self.totalBlocks = self.document().blockCount()
        QTimer.singleShot(0, self.highlight_blockrange)


    def test_if_ready(self): 
        if not self.document().isEmpty() and not self.isReady:
            self.isReady = True
            self.blockCountChanged.emit(self.document().blockCount())


    def updateLastListItem(self):
        cursor = self.textCursor()
        text = cursor.block().text()
        num = re.search(r"^(\d+)\.", text)
        spaces = re.search(r"^\s{1,4}", text)

        if num:
            self.lastListNumber = int(num.group(1))
        elif not text:
            pass
        elif spaces:
            pass
        else:
            self.lastListNumber = 0


    def mouseReleaseEvent(self, event):
        super().mouseReleaseEvent(event)
        self.released.emit()


    def contextMenuEvent(self, event):
        popup_menu = self.createSpellcheckContextMenu(event.pos())
        popup_menu.exec_(event.globalPos())
        self.focusInEvent(QFocusEvent(QEvent.FocusIn))


    def createSpellcheckContextMenu(self, pos):
        spell_menu = self.createCorrectionsMenu(self.cursorForMisspelling(pos))

        if spell_menu:
            return spell_menu
        else:
            try:
                menu = self.createStandardContextMenu(pos)
            except TypeError:
                menu = self.createStandardContextMenu()

            menu.addSeparator()
            menu.addMenu(self.createLanguagesMenu(menu))
            return menu


    def createCorrectionsMenu(self, cursor):
        if not cursor:
            return None

        text = cursor.selectedText()
        suggests = trim_suggestions(
            text, self.highlighter.dict().suggest(text), self.max_suggestions
        )
        spell_menu = QMenu(self)

        for word in suggests:
            action = QAction(word, spell_menu)
            action.setData((cursor, word))
            spell_menu.addAction(action)

        if spell_menu.actions():
            spell_menu.triggered.connect(self.cb_correct_word)
            spell_menu.addSeparator()
            add_dict_action = QAction("(+) Add to dict", spell_menu)
            add_dict_action.setData(text)
            spell_menu.addAction(add_dict_action)
            spell_menu.adjustSize()
            return spell_menu
        return None


    def createLanguagesMenu(self, parent=None):
        curr_lang = self.highlighter.dict().tag
        lang_menu = QMenu("Language", parent)
        lang_actions = QActionGroup(lang_menu)

        for lang in list_languages():
            action = lang_actions.addAction(lang)
            action.setCheckable(True)
            action.setChecked(lang == curr_lang)
            action.setData(lang)
            lang_menu.addAction(action)

        lang_menu.triggered.connect(self.cb_set_language)
        return lang_menu


    def cursorForMisspelling(self, pos):
        cursor = self.cursorForPosition(pos)
        misspelled_words = getattr(cursor.block().userData(), "misspelled", [])

        for (start, end) in misspelled_words:
            if start <= cursor.positionInBlock() <= end:
                block_pos = cursor.block().position()
                cursor.setPosition(block_pos + start, QTextCursor.MoveAnchor)
                cursor.setPosition(block_pos + end, QTextCursor.KeepAnchor)
                break

        if cursor.hasSelection():
            return cursor

        return None


    def cb_correct_word(self, action):
        if isinstance(action.data(), str):
            curr_lang = self.highlighter.dict().tag
            d = Dict(curr_lang)
            d.add(action.data())
            self.highlighter.rehighlight()
            return
        cursor, word = action.data()
        cursor.beginEditBlock()
        cursor.removeSelectedText()
        cursor.insertText(word)
        cursor.endEditBlock()


    def cb_set_language(self, action):
        lang = action.data()
        self.highlighter.setDict(Dict(lang))


    def cb_set_format(self, action):
        chunkers = action.data()
        self.highlighter.setChunkers(chunkers)


    def setCompleter(self, completer):
        if self.completer:
            self.disconnect(self.completer, 0, self, 0)

        if not completer:
            return

        completer.setWidget(self)
        completer.setCompletionMode(QCompleter.PopupCompletion)
        completer.setCaseSensitivity(Qt.CaseSensitive)
        self.completer = completer
        self.completer.activated.connect(self.insertCompletion)


    def insertCompletion(self, completion):
        tc = self.textCursor()
        extra = len(completion) - len(self.completer.completionPrefix())
        tc.movePosition(QTextCursor.Left)
        tc.movePosition(QTextCursor.EndOfWord)
        tc.insertText(completion[-extra:])
        if completion[-extra:][-1] in ")":
            tc.movePosition(QTextCursor.Left)
        self.setTextCursor(tc)
        super().textChanged.emit()


    def textUnderCursor(self):
        tc = self.textCursor()
        pos = tc.positionInBlock()
        text = tc.block().text()
        if text:
            if text[-1] in " ":
                return ""
        if text[pos:pos+2] in ('$$'):
            tc.movePosition(QTextCursor.PreviousCharacter, 2)
        elif text[pos:pos+1] in (')', '$', ':', '|', '}', ']'):
            tc.movePosition(QTextCursor.PreviousCharacter)
        tc.select(QTextCursor.WordUnderCursor)
        return tc.selectedText()


    def focusInEvent(self, event):
        if self.completer:
            self.completer.setWidget(self)

        QPlainTextEdit.focusInEvent(self, event)


    def keyReleaseEvent(self, event):
        pass


    def keyPressEvent(self, event):
        cursor = self.textCursor()
        blocktext = cursor.block().text()
        delimiters = {"[": "]", '"': '"', "{": "}", "(": ")"}
        text = event.text()
        delimiter = delimiters.get(text)
        pos = cursor.position()
        posb = cursor.positionInBlock()
        cursor.movePosition(QTextCursor.NextCharacter, QTextCursor.KeepAnchor)
        nextchar = cursor.selectedText().strip()
        cursor.clearSelection()

        if text:
            if text in "|":
                if "]" in nextchar:
                    cursor.clearSelection()
                    self.insertPlainText("|")
                    cursor.setPosition(pos)
                    self.setTextCursor(cursor)

        if delimiter is not None:
            if blocktext.count(text) == blocktext.count(delimiter):
                self.insertPlainText(delimiter)
                cursor.setPosition(pos)
                self.setTextCursor(cursor)

        blockSpaceKey = False

        if event.key() == Qt.Key_Space:
            event.ignore()
            self.insertPlainText(" ")
            self.keyPressed.emit(event.key())
            self.addNewTitle.emit(True)
            blockSpaceKey = True

        if event.key() == Qt.Key_F7:
            self.keyPressed.emit(event.key())

        if event.key() == Qt.Key_Right:
            cursor = self.textCursor()
            pos = cursor.position()
            try:
                if self.document().toPlainText()[pos: pos + 2] \
                        in ["}{", ")[", "){", "**", "|]", "](", "]["]:
                    cursor.setPosition(pos + 1)
                    self.setTextCursor(cursor)
            except Exception:
                pass

        blockEnterKey = False

        if not self.completer.popup().isVisible() and event.key() in (
            Qt.Key_Enter,
            Qt.Key_Return,
        ):
            cursor = self.textCursor()

            if event.modifiers() == Qt.ShiftModifier:
                position = self.textCursor().position()
                li = list(
                    re.finditer(
                        r"^(\d+)\.\s", self.toPlainText()[:position], flags=re.MULTILINE
                    )
                )
                subli = list(
                    re.finditer(
                        r"^\s{4}([a-z\d]+)\.([a-z\d]*)(\.*)\s",
                        self.toPlainText()[:position],
                        flags=re.MULTILINE,
                    )
                )

                if subli and cursor.block().text()[:4] == "    ":
                    lastPos = subli[-1].start()
                    text = self.toPlainText()[lastPos:position]

                    if not [m for m in text.splitlines()[1:] if m[:4].strip()]:
                        self.subout = True
                        event.ignore()

                        if subli[-1].group(1) and subli[-1].group(2):
                            self.insertPlainText(
                                "\n    {}.{}{p} ".format(
                                    subli[-1].group(1),
                                    increment(subli[-1].group(2)),
                                    p="." if subli[-1].group(3) else "",
                                )
                            )
                        elif subli[-1].groups(1):
                            self.insertPlainText(
                                "\n    {}. ".format(increment(subli[-1].group(1)))
                            )

                    blockEnterKey = True

                elif li:
                    blockEnterKey = True
                    lastPos = li[-1].start()
                    num = int(li[-1].group(1))
                    text = self.toPlainText()[lastPos:position]

                    if not [m for m in text.splitlines()[1:] if m[:4].strip()]:
                        event.ignore()
                        self.insertPlainText(
                            "\n{nl}{}. ".format(num + 1, nl="" if self.subout else "\n")
                        )
                    else:
                        self.insertPlainText("\n\n1. ")

                    self.subout = False

                elif not self.toPlainText()[:position].strip():
                    event.ignore()
                    self.insertPlainText("1. ")
                    self.subout = False
                    blockEnterKey = True

            elif cursor.block().text()[:4] == "    ":
                event.ignore()
                self.insertPlainText("\n    ")
                blockEnterKey = True

        if event.key() == Qt.Key_Backspace:
            cursor = self.textCursor()
            pos = cursor.positionInBlock()
            stext = cursor.block().text()[:pos]

            if stext == "    ":
                event.ignore()
                cursor.movePosition(QTextCursor.StartOfBlock, QTextCursor.MoveAnchor)
                cursor.movePosition(QTextCursor.NextCharacter, QTextCursor.KeepAnchor, 4)
                cursor.removeSelectedText()
                self.insertPlainText("\n")
                self.subout = True

        if event.key() in (Qt.Key_Enter, Qt.Key_Return) and self.replaceBarIsShown:
            event.ignore()
            return

        if (
            event.key() in (Qt.Key_Enter, Qt.Key_Return)
            and not self.completer.popup().isVisible()
            and not blockEnterKey
            and not blockSpaceKey
        ):
            self.keyPressed.emit(event.key())

        if self.completer and self.completer.popup().isVisible():
            if event.key() in (
                Qt.Key_Escape,
                Qt.Key_Return,
                Qt.Key_Tab,
                Qt.Key_Enter,
                Qt.Key_Backtab,
            ):
                event.ignore()
                return

        if event.key() == Qt.Key_Tab:
            self.insertPlainText("    ")
            return

        # Autocomplete
        if event.key() == Qt.Key_Return:
            self.titleComplete = 0
            customBlock = re.search(r"^(![a-zPDF\d]+)(.*)$", cursor.block().text()[:posb])
            ltx_begin = re.search(r"^\\begin{([a-z\*]+)}\s*$", cursor.block().text()[:posb])

            if re.findall(r"^```[^)]+\)*\s*$", cursor.block().text()):
                num = self.toPlainText().count("```")
                if not num % 2:
                    QPlainTextEdit.keyPressEvent(self, event)
                    return
                self.insertPlainText("\n```")
                cursor.movePosition(QTextCursor.Up)
                cursor.clearSelection()
                self.setTextCursor(cursor)
            elif ltx_begin:
                self.insertPlainText("".join(("\n\\end{", ltx_begin.group(1), "}")))
                cursor.movePosition(QTextCursor.Up)
                cursor.clearSelection()
                self.setTextCursor(cursor)
            elif customBlock:
                opts = customBlock.group(2)
                block = customBlock.group(1)
                if block in ("!newpage", "!mode", "!pageframe"):
                    QPlainTextEdit.keyPressEvent(self, event)
                    return
                if block not in (
                        "!left", "!right", "!center", "!vcenter", '!vhcenter',
                        "!box", "!wbox", "!font", "!gtab", "!header", "!footer",
                        "!graph", "!diagram", "!code", "!muPDF",
                        ) and block[:3] not in '!mc':
                    QPlainTextEdit.keyPressEvent(self, event)
                    return
                if not opts or \
                        ('(' in opts and ')' in opts):
                    if self.toPlainText().count(block) != \
                            self.toPlainText().count(block.replace("!", ".")):
                        self.insertPlainText(block.replace('!', '\n.'))
                        cursor.movePosition(QTextCursor.Up)
                        cursor.clearSelection()
                        self.setTextCursor(cursor)

        isShortcut = event.key() == Qt.Key_Tab

        if (
            (not self.completer or not isShortcut)
            and not blockEnterKey
            and not blockSpaceKey
        ):
            QPlainTextEdit.keyPressEvent(self, event)

        ctrlOrShift = event.modifiers() in (Qt.ControlModifier, Qt.ShiftModifier)

        if ctrlOrShift and not event.text():
            return

        eow = "~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-="

        hasModifier = (event.modifiers() != Qt.NoModifier) and not ctrlOrShift
        completionPrefix = self.textUnderCursor()
        if not isShortcut and (
            hasModifier
            or not event.text()
            or len(completionPrefix) < 2
            or event.text()[-1:] in eow
        ):
            self.completer.popup().hide()
            return

        if completionPrefix != self.completer.completionPrefix():
            self.completer.setCompletionPrefix(completionPrefix)
            # keep window open if more propositions
            #if completionPrefix != self.completer.currentCompletion():
            popup = self.completer.popup()
            popup.setCurrentIndex(self.completer.completionModel().index(0, 0))

            cr = self.cursorRect()
            cr.setWidth(
                self.completer.popup().sizeHintForColumn(0)
                + self.completer.popup().verticalScrollBar().sizeHint().width()
            )
            self.completer.complete(cr)
            #else:
                #self.completer.popup().hide()


    def insertFromMimeData(self, source):
        if source.hasText():
            if "file://" in source.text():
                file = source.text().split("file://")[-1]
                file = file.replace(" ", "").replace("\n", "").replace("\r", "")
                ext = file.split(".")[-1]

                if ext.lower() in ["png", "jpg", "jpeg", "gif", "svg"]:
                    text = "![]({0})".format(file)

                else:
                    text = source.text()
            else:
                text = source.text()

            super().insertPlainText(text)


    def dropEvent(self, event):
        file = ""
        mimeData = event.mimeData()

        if mimeData.hasText():
            event.accept()
            file = mimeData.text()

            ext = file.split(".")[-1].replace("\n", "").replace("\r", "")

            if ext.lower() in ["png", "jpg", "jpeg", "gif", "svg"]:

                if "file://" in file:
                    file = file.split("file://")[-1]
                    file = file.replace(" ", "").replace("\n", "").replace("\r", "")

                elif "http" in file:
                    url = file
                    os.makedirs(self.data_path, exist_ok=True)
                    file = os.path.join(self.data_path, os.path.basename(unquote(file)))
                    urlretrieve(url, file)

                text = "![]({0})".format(os.path.basename(file))

            else:
                text = mimeData.text()

            super().insertPlainText(str(text))

            # Sans la suite, le curseur freeze
            mimeData2 = QMimeData()
            mimeData2.setText("")
            dummyEvent = QDropEvent(
                event.posF(),
                event.possibleActions(),
                mimeData2,
                event.mouseButtons(),
                event.keyboardModifiers(),
            )
            super().dropEvent(dummyEvent)


class LineNumberArea(QWidget):
    def __init__(self, editor):
        super().__init__(editor)
        self.myeditor = editor

    def sizeHint(self):
        return QSize(self.editor.lineNumberAreaWidth(), 0)

    def paintEvent(self, event):
        self.myeditor.lineNumberAreaPaintEvent(event)
