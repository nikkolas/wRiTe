# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import os

try:
    from PyQt5.QtWebEngineWidgets import QWebEnginePage, QWebEngineDownloadItem
except ModuleNotFoundError:
    print("module QtWebEngineWidgets not found")

try:
    from PyQt5.QtWidgets import QFileDialog
except ModuleNotFoundError:
    print("module QtWidgets not found")

try:
    from PyQt5.QtCore import pyqtSignal, pyqtSlot
except ModuleNotFoundError:
    print("module QtCore not found")


class WebEnginePage(QWebEnginePage):
    downloadFinished = pyqtSignal(str, str)

    def __init__(self, *args, **kwargs):
        QWebEnginePage.__init__(self, *args, **kwargs)
        self.profile().downloadRequested.connect(self.on_downloadRequested)
        self.data_path = ""
        self.imgType = ""
        self.senderFunc = ""

    @pyqtSlot(QWebEngineDownloadItem)
    def on_downloadRequested(self, download):
        os.makedirs(self.data_path, exist_ok=True)
        saveFile = str(
            QFileDialog.getSaveFileName(
                self.view(),
                "Save image as...",
                self.data_path,
                self.tr(self.imgType)
            )[0]
        )

        if saveFile:
            ext = ""
            if "." in saveFile:
                ext = saveFile.split(".")[-1]

            if self.senderFunc == "molview" and (not ext or ext != "png"):
                ext = "png"

            elif self.senderFunc == "svgedit" and (not ext or ext != "svg"):
                ext = "svg"

            elif self.senderFunc == "imageeditor" and (
                not ext or ext not in ["jpg", "jpeg", "png", "gif"]
            ):
                ext = "png"

            saveFile = "".join((saveFile.split(".")[0], ".", ext))

            download.setPath(saveFile)
            download.accept()

            self.downloadFinished.emit(saveFile, self.senderFunc)
