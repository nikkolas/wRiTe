# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
from classes.colors import colors

def pbox(match):
    lw = "1px"
    color = "black"

    if match:
        opts = match.group(1).strip()
        opts = re.sub(r"\s+", " ", opts.strip()).split(" ")
        opts.sort()

        if opts:
            if len(opts) == 1:
                if '@' in opts[0]:
                    if opts[0][1:] in colors:
                        color = opts[0][1:]
                else:
                    lw = opts[0]

            if len(opts) == 2:
                if '@' in opts[0]:
                    if opts[0][1:] in colors:
                        color = opts[0][1:]
                    lw = opts[1]
                elif '@' in opts[1]:
                    if opts[1][1:] in colors:
                        color = opts[0][1:]
                    lw = opts[0]

    return (
        '<div class="vcontainer" style="height: 100.8%;'
        "border: {} solid {}; padding-left: 0.5em; "
        'padding-right: 0.5em;">\n\n'.format(lw, color)
    )


def boxedpage(code):
    return re.sub(
        r"^!pageframe\(*(.*?)\)*\s*$", pbox, code, flags=re.MULTILINE
    )
