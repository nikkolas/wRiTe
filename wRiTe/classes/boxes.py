# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
from classes.colors import colors


def boxopt(match):
    width = "100%"
    shadow = "0"
    framepx = "1.5"
    title = ""
    opts = ""
    m = match.group(1)
    content = match.group(2)
    stitle = m.find("[")
    etitle = m.find("]", stitle)

    if stitle >= 0 and etitle >= 0:
        title = m[stitle + 1: etitle].strip()
    else:
        etitle = 0

    sopts = m.find("(", etitle)
    eopts = m.find(")", sopts)

    if sopts >= 0 and eopts >= 0:
        opts = m[sopts + 1: eopts].strip()

    if opts:
        opts = re.sub(r"\s+", " ", opts).split(" ")
        pxi = [i for i, s in enumerate(opts) if "px" in s]
        if pxi:
            framepx = opts[pxi[0]]
            opts.remove(framepx)
            framepx = framepx[:-2]
        w = [i for i, s in enumerate(opts) if "%" in s]
        if w:
            width = opts[w[0]]
            opts.remove(width)
        else:
            w = [i for i, s in enumerate(opts) if "cm" in s]
            if w:
                width = opts[w[0]]
                opts.remove(width)

        if "shadow" in opts:
            shadow = "3px 3px 2px silver;"
            opts.remove("shadow")

        if len(opts) == 2:
            opts = [e  if '@' in e or '#' in e else "" for e in opts]
            opts = [e.replace("@", "") if "@" in e else e for e in opts]
            opts = [colors[e]  if e in colors else e for e in opts]

            if title:
                return (
                    '\n<div class="box" style="margin-top: 1.5em;'
                    " width: {}; border: {}px solid {}; background-color: {};"
                    ' box-shadow: {}; padding-top: 2ex; padding-bottom: 0.5em;'
                    '">\n\n<span class="boxtitle" style="border: {}px solid {}'
                    ';margin-top: calc(-3.5ex - 3/2*{}px);">{}</span>\n\n{}\n\n</div>\n\n'
                ).format(
                    width,
                    framepx,
                    *opts[:2],
                    shadow,
                    framepx,
                    opts[:2][0],
                    framepx,
                    title,
                    content
                )

            return (
                '\n<div class="box" style="margin-top: 1em;'
                ' width: {}; border: {}px solid {}; backgrou'
                'nd-color: {}; box-shadow: {};">\n\n{}\n\n</div>\n\n'
            ).format(
                width, framepx, *opts[:2], shadow, content
            )
        elif len(opts) == 1:
            opts = [e  if '@' in e or '#' in e else "" for e in opts]
            opts = [e.replace("@", "") if "@" in e else e for e in opts]
            opts = [colors[e]  if e in colors else e for e in opts]
            
            if title:
                return (
                    '\n<div class="box" style="margin-top: 1.5em;'
                    ' width: {}; border: {}px solid {}; box-shadow: {};'
                    ' padding-top: 2ex; padding-bottom: 0.5em;">'
                    '\n\n<span class="boxtitle" style="color:{};"'
                    '>{}</span>\n\n{}\n\n</div>\n\n'
                ).format(
                    width, framepx, opts[0], shadow, opts[0], title, content
                )
            return (
                '\n<div class="box" style="margin-top: 1em;'
                ' width: {}; border: {}px solid {};'
                ' box-shadow: {};">\n\n{}\n\n</div>\n\n'
            ).format(
                width, framepx, opts[0], shadow, content
            )

    if title:
        return (
            '\n<div class="box" style="margin-top: 1.5em;'
            ' width: {}; border: {}px solid black; '
            'box-shadow: {}; padding-top: 2ex; '
            'padding-bottom: 0.5em;">\n\n'
            '<span class="boxtitle">{}</span>\n\n{}\n\n</div>\n\n'
        ).format(
            width, framepx, shadow, title, content
        )

    return (
        '\n<div class="box" style="margin-top: 1em;'
        ' width: {}; border: {}px solid black;'
        ' box-shadow: {};">\n\n{}\n\n</div>\n\n'
    ).format(
        width, framepx, shadow, content
    )


def boxes(code):
    return re.sub(r"^!box(.*?)\s*$(.+?)^\.box", boxopt, code, flags=re.M | re.DOTALL)
