# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
from functools import partial
from classes.colors import colors


def button(match, fontsize):
    bgcolor = match.group(1) if match.group(1) else "#e9eaef"
    fcolor = match.group(3) if match.group(3) else "black"
    bgcolor = colors[bgcolor[1:]] if bgcolor[1:] in colors else bgcolor
    fcolor = colors[fcolor[1:]] if fcolor[1:] in colors else fcolor
    text = match.group(2)

    return (
        '<button style="color: {}; background-color: {};'
        " border: {}px solid black; border-radius: 3px;"
        ' padding: 1.5px 2.5px 0px 2px; margin-right: 2px;"> {} </button>'
    ).format(fcolor, bgcolor, round(fontsize / 12, 1), text)


def buttons(code, fontsize):
    return re.sub(
        r"\[([#@\da-z]*)\|([^|]+?)\|([#@\da-z]*)\]",
        partial(button, fontsize=fontsize),
        code,
    )
