# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

#  Collection of custom Matplotlib useful functions
import re
import os

try:
    from shutil import copyfile
except ModuleNotFoundError:
    print("shutil not installed")

try:
    import numpy as np
except ModuleNotFoundError:
    print("numpy not installed")

try:
    import matplotlib.pyplot as plt
except ModuleNotFoundError:
    print("matplotlib not installed")

try:
    from matplotlib.patches import Ellipse
except ModuleNotFoundError:
    print("matplotlib patches not installed")

try:
    from matplotlib.transforms import Affine2D
except ModuleNotFoundError:
    print("matplotlib transforms not installed")

try:
    from matplotlib.ticker import MultipleLocator
except ModuleNotFoundError:
    print("matplotlib ticker not installed")

from classes.utils import gte, lte
from classes.Line import Line
from classes.Circle import Circle
from classes.Polygon import Polygon
from classes.Rectangle import Rectangle
#  Needed for matplotlibToSvg
from classes.wFunction import wFunction
from classes.wFitCurve import wFitCurve
from classes.wLinearFit import wLinearfit


def wBar(
        datalist,
        valueslist,
        edgecolor="black",
        alpha=1,
        color="#1f77b4",
        xlabel="",
        ylabel="",
        grid=False,
        zorder=10,
        **kwargs
        ):

    if len(datalist) == len(valueslist):
        colors = [data.split(":")[1] if ":" in data else color for data in datalist]
        labels = [data.split(":")[0] if ":" in data else data for data in datalist]
        valueslist = [float(val) for val in valueslist]

        plt.bar(
            labels,
            valueslist,
            color=colors,
            edgecolor=edgecolor,
            alpha=alpha,
            zorder=zorder,
            **kwargs
        )

        if xlabel:
            plt.xlabel(xlabel, weight="bold")
        if ylabel:
            plt.ylabel(ylabel, weight="bold")
        if grid:
            plt.grid(ls="dotted")


def wPie(
    datalist,
    legend=True,
    shadow=False,
    format="%1.1f%%",
    e=0.03,
    startangle=0,
    outfontcolor="black",
    infontcolor="white",
    outfontsize="x-large",
    infontsize="x-large",
    normalize=True,
    outfontbold=False,
    infontbold=False,
    **kwargs
):

    labels = []
    sizes = []
    explode = []
    colors = []
    bof = "normal"
    bif = "normal"

    if outfontbold:
        bof = "bold"

    if infontbold:
        bif = "bold"

    for data in datalist:
        val = 0
        e = 0.03
        col = None
        expl = ""
        labl = ""
        opts = []

        data = str(data)

        if ":" in data:
            labl, opts = data.split(":")
            opts = opts.strip().split(",")
        else:
            opts = data.split(",")

        if len(opts) == 3:
            val = opts[0]

            if len(opts[1]) == 2:
                expl, col = opts[1:]
            else:
                col, expl = opts[1:]
        elif len(opts) == 2:

            if len(opts[1]) == 2:
                val, expl = opts
            else:
                val, col = opts
        elif len(opts) == 1:
            val = opts[0]

        val = val.strip()

        if expl:
            expl = expl.strip()

        if col:
            col = col.strip()

        try:
            val = float(val)
        except Exception:
            val = 0

        try:
            expl = expl.replace("e", "").strip()

            if expl:
                e = float(expl) * e
        except Exception:
            e = 0.03

        if legend:
            labels.append(labl)
        else:
            labels.append("")

        sizes.append(val)
        explode.append(e)
        colors.append(col)

    if not any(colors):
        colors = None

    elif any(colors) and not all(colors):
        stdcolors = ("#1f77b4", "#ff7f0e", "#2ca02c")
        colors = [stdcolors[i] if c is None else c for i, c in enumerate(colors)]

    _, _, autotexts = plt.pie(
        sizes,
        explode=explode,
        labels=labels,
        colors=colors,
        autopct=format,
        shadow=shadow,
        startangle=startangle,
        normalize=normalize,
        textprops={"fontsize": outfontsize, "color": outfontcolor, "weight": bof},
    )

    for autotext in autotexts:
        autotext.set_color(infontcolor)
        autotext.set_fontsize(infontsize)
        autotext.set_fontweight(bif)

    plt.axis("equal")


def wDots(p, *args, fontsize=14, **kwargs):

    if "fontsize" in kwargs:
        fontsize = kwargs.get("fontsize")
        del kwargs["fontsize"]

    if not (isinstance(p, tuple) and len(p) == 2) and not isinstance(p, list):
        return ()

    name = ""
    position = ""
    marker = "."
    markers = [
        "",
        ".",
        "o",
        "+",
        "x",
        ",",
        "v",
        "<",
        ">",
        "s",
        "p",
        "*",
        "h",
        "d",
        "|",
        "_",
    ]
    positions = {
        "tr": ("left", "bottom"),
        "tc": ("center", "bottom"),
        "tl": ("right", "bottom"),
        "l": ("right", "center"),
        "r": ("left", "center"),
        "bl": ("right", "top"),
        "bc": ("center", "top"),
        "br": ("left", "top"),
    }

    if "color" not in kwargs and "c" not in kwargs:
        kwargs = dict(kwargs, color="black")

    for e in args:

        if not isinstance(e, str):
            continue

        if e in positions or e[::-1] in positions:
            position = e
            continue
        elif e in markers:
            marker = e
            continue
        else:
            name = e

    if name is not None:

        if position in positions:
            ha, va = positions[position]
        elif position[::-1] in positions:
            ha, va = positions[position[::-1]]
        else:
            ha, va = positions["tr"]

        if "left" in ha:
            name = " {}".format(name)
        elif "right" in ha:
            name = "{} ".format(name)

        if isinstance(p, list):
            for dot in p:
                plt.plot(dot[0], dot[1], marker=marker, **kwargs)
        else:
            plt.text(p[0], p[1], str(name), ha=ha, va=va, fontsize=fontsize)

    if isinstance(p, list):

        for dot in p:
            plt.plot(dot[0], dot[1], marker=marker, **kwargs)
    else:
        plt.plot(p[0], p[1], marker=marker, **kwargs)

    return p


def wBigdot(p, radius=1, color="black", **kwargs):
    # calculate asymmetry of x and y axes:
    x0, y0 = plt.gca().transAxes.transform((0, 0))  # lower left in pixels
    x1, y1 = plt.gca().transAxes.transform((1, 1))  # upper right in pixes
    ax = plt.gca()
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    dx = (xmax - xmin) / (x1 - x0)
    dy = (ymax - ymin) / (y1 - y0)
    width = 10 * radius * dx
    height = 10 * radius * dy
    plt.gca().add_artist(Ellipse(p, width, height, color=color, zorder=10000, **kwargs))


def wEllipse(p, width, height, fill=False, **kwargs):
    plt.gca().add_artist(Ellipse(p, width, height, fill=fill, **kwargs))


def wCircle(p, r=None, center=False, **kwargs):

    if "zorder" not in kwargs:
        kwargs = dict(kwargs, zorder=2)

    fill = kwargs.get("fill", False)

    if "fill" in kwargs:
        del kwargs["fill"]
    fillalpha = kwargs.get("fillalpha", 1)

    if "fillalpha" in kwargs:
        del kwargs["fillalpha"]
    alpha = kwargs.get("alpha", 1)

    if "alpha" in kwargs:
        del kwargs["alpha"]
    fillcolor = kwargs.get("fillcolor", "lightgray")

    if "fillcolor" in kwargs:
        del kwargs["fillcolor"]
    color = kwargs.get("color", "black")

    if "color" in kwargs:
        del kwargs["color"]
    border = kwargs.get("border", True)

    if "border" in kwargs:
        del kwargs["border"]

    if len(p) == 2 and r is not None:
        x0, y0 = p
        circle1 = None
        circle2 = None

        if fill:
            circle1 = plt.Circle(
                (x0, y0),
                radius=r,
                fill=fill,
                color=fillcolor,
                alpha=fillalpha * alpha,
                **kwargs
            )

            if border:
                circle2 = plt.Circle(
                    (x0, y0), radius=r, fill=False, color=color, alpha=alpha, **kwargs
                )
        else:
            circle1 = plt.Circle(
                (x0, y0), radius=r, fill=False, color=color, alpha=alpha, **kwargs
            )

        plt.gca().add_patch(circle1)

        if circle2 is not None:
            plt.gca().add_patch(circle2)

        if center:
            plt.plot(x0, y0, "x", color=color)

        return Circle((x0, y0), r)
    return


def wRectangle(p, lx, ly, center=False, **kwargs):
    angle = kwargs.get("angle", 0)

    if "angle" in kwargs:
        del kwargs["angle"]
    fill = kwargs.get("fill", False)

    if "fill" in kwargs:
        del kwargs["fill"]
    fillalpha = kwargs.get("fillalpha", 1)

    if "fillalpha" in kwargs:
        del kwargs["fillalpha"]
    alpha = kwargs.get("alpha", 1)

    if "alpha" in kwargs:
        del kwargs["alpha"]
    fillcolor = kwargs.get("fillcolor", "lightgray")

    if "fillcolor" in kwargs:
        del kwargs["fillcolor"]
    color = kwargs.get("color", "black")

    if "color" in kwargs:
        del kwargs["color"]
    border = kwargs.get("border", True)

    if "border" in kwargs:
        del kwargs["border"]

    if "zorder" not in kwargs:
        kwargs = dict(kwargs, zorder=2)

    if isinstance(p, tuple) and len(p) == 2:
        x, y = p
        rectangle1 = None
        rectangle2 = None

        if fill:
            rectangle1 = plt.Rectangle(
                (x, y),
                lx,
                ly,
                fill=fill,
                color=fillcolor,
                alpha=fillalpha * alpha,
                **kwargs
            )

            if border:
                rectangle2 = plt.Rectangle(
                    (x, y), lx, ly, fill=False, color=color, alpha=alpha, **kwargs
                )
        else:
            rectangle1 = plt.Rectangle(
                (x, y), lx, ly, fill=False, color=color, alpha=alpha, **kwargs
            )

        if angle:
            transf = Affine2D().rotate_deg(int(angle)) + plt.gca().transData
            rectangle1.set_transform(transf)

            if rectangle2 is not None:
                rectangle2.set_transform(transf)

        plt.gca().add_patch(rectangle1)

        if rectangle2 is not None:
            plt.gca().add_patch(rectangle2)

        if center:
            plt.plot(x + lx / 2, y + ly / 2, "x", color=color)

        return Rectangle(p, lx, ly)

    return


def circle_intersections(Line, Circle):
    x0, y0 = Circle.center
    r = Circle.radius
    a, b = Line.slope, Line.offset

    xmin1 = min(Line.p1[0], Line.p2[0])
    xmax1 = max(Line.p1[0], Line.p2[0])
    ymin1 = min(Line.p1[1], Line.p2[1])
    ymax1 = max(Line.p1[1], Line.p2[1])
    xmin2 = x0 - r
    xmax2 = x0 + r
    ymin2 = y0 - r
    ymax2 = y0 + r

    intersects = []

    A = 1 + a**2
    B = 2 * a * (b - y0) - 2 * x0
    C = x0**2 + (b - y0) ** 2 - r**2

    delta = B**2 - 4 * A * C

    if delta > 0:
        xi1 = (-B + delta**0.5) / (2 * A)
        xi2 = (-B - delta**0.5) / (2 * A)
        yi1 = a * xi1 + b
        yi2 = a * xi2 + b

        if (gte(xi1, xmin1) and lte(xi1, xmax1)) and (
            gte(xi1, xmin2) and lte(xi1, xmax2)
        ):

            if (gte(yi1, ymin1) and lte(yi1, ymax1)) and (
                gte(yi1, ymin2) and lte(yi1, ymax2)
            ):
                intersects.append((xi1, yi1))

        if (gte(xi2, xmin1) and lte(xi2, xmax1)) and (
            gte(xi2, xmin2) and lte(xi2, xmax2)
        ):

            if (gte(yi2, ymin1) and lte(yi2, ymax1)) and (
                gte(yi2, ymin2) and lte(yi2, ymax2)
            ):
                intersects.append((xi2, yi2))

    elif np.isclose(delta, 0.0, 1e-9):
        xi = -B / (2 * A)
        yi = a * xi + b

        if (gte(xi, xmin1) and lte(xi, xmax1)) and (gte(xi, xmin2) and lte(xi, xmax2)):

            if (gte(yi, ymin1) and lte(yi, ymax1)) and (
                gte(yi, ymin2) and lte(yi, ymax2)
            ):
                intersects.append((xi, yi))
    return intersects


def find_func_zeros(dfg, xmin, xmax):
    zeros = []
    nanrepl = (np.nanmin(dfg) + np.nanmax(dfg)) / 2
    threshold = abs(np.nanmax(dfg) - np.nanmin(dfg)) / 1000
    dfg = np.nan_to_num(dfg, nan=nanrepl)

    for i, elem in enumerate(abs(dfg[:-1])):

        if not i and abs(dfg[i + 1]) > elem:
            zeros.append(i)
            continue

        if elem > abs(dfg[i - 1]):
            evo = 1
        else:
            evo = -1

        if (
            (elem < abs(dfg[i - 1]))
            and (elem < abs(dfg[i + 1]))
            or (abs(dfg[i + 1]) == elem and evo < 0)
        ):
            zeros.append(i)

    if dfg[-1] < dfg[-2]:
        zeros.append(len(dfg) - 1)

    if zeros:
        zeros = [i for i in zeros if lte(abs(dfg[i]), threshold)]

    if zeros:
        zeros = [
            zeros[i] for i in range(len(zeros) - 1) if gte(zeros[i + 1] - zeros[i], 500)
        ] + [zeros[-1]]

    return zeros


def remove_recur(tlist):
    nlist = []

    for e in tlist:
        if e not in nlist:
            nlist.append(e)

    return nlist


def wIntersect(obj1, obj2, n=100000):

    if not isinstance(obj1, object) or not isinstance(obj2, object):
        return

    #  intersect 2 lines
    if obj1.type == "line" and obj2.type == "line":

        if obj2.slope is None and obj1.slope is not None:
            obj1, obj2 = obj2, obj1

        if obj1.slope is None and obj2.slope is not None:
            xmin2 = min(obj2.p1[0], obj2.p2[0])
            xmax2 = max(obj2.p1[0], obj2.p2[0])
            ymin1 = min(obj1.p1[1], obj1.p2[1])
            ymax1 = max(obj1.p1[1], obj1.p2[1])
            xi = obj1.x

            if gte(xi, xmin2) and lte(xi, xmax2):
                a = obj2.slope
                b = obj2.offset
                yi = a * xi + b

                if gte(yi, ymin1) and lte(yi, ymax1):
                    return (xi, yi)

        elif obj1.slope is not None and obj2.slope is not None:
            xmin2 = min(obj2.p1[0], obj2.p2[0])
            xmax2 = max(obj2.p1[0], obj2.p2[0])
            ymin2 = min(obj2.p1[1], obj2.p2[1])
            ymax2 = max(obj2.p1[1], obj2.p2[1])
            xmin1 = min(obj1.p1[0], obj1.p2[0])
            xmax1 = max(obj1.p1[0], obj1.p2[0])
            ymin1 = min(obj1.p1[1], obj1.p2[1])
            ymax1 = max(obj1.p1[1], obj1.p2[1])
            a1 = obj1.slope
            b1 = obj1.offset
            a2 = obj2.slope
            b2 = obj2.offset

            if not np.isclose(a1, a2, 1e-9):
                xi = (b2 - b1) / (a1 - a2)
                yi = (a1 * b2 - b1 * a2) / (a1 - a2)

                if (gte(xi, xmin1) and lte(xi, xmax1)) and (
                    gte(xi, xmin2) and lte(xi, xmax2)
                ):

                    if (gte(yi, ymin1) and lte(yi, ymax1)) and (
                        gte(yi, ymin2) and lte(yi, ymax2)
                    ):
                        return (xi, yi)

    #  intersect line with rectangle or polygon
    if obj1.type == "line" and obj2.type in ("rectangle", "polygon"):
        obj1, obj2 = obj2, obj1

    if obj1.type in ("rectangle", "polygon") and obj2.type == "line":
        intersects_list = []
        x1, y1 = obj2.p1
        x2, y2 = obj2.p2

        if obj2.slope is not None:
            a1 = obj2.slope
            b1 = obj2.offset

            for L in obj1.lines:

                if L.slope is not None:
                    a2 = L.slope
                    b2 = L.offset

                    if a1 != a2:
                        intp = (
                            (b2 - b1) / (a1 - a2),
                            (a1 * b2 - b1 * a2) / (a1 - a2)
                            )

                        if lte(min(L.p1[0], L.p2[0]), intp[0]) and lte(
                            intp[0], max(L.p1[0], L.p2[0])
                        ):
                            intersects_list.append(intp)

                elif L.slope is None:
                    x = L.x
                    intp = (x, a1 * x + b1)

                    if (lte(min(x1, x2), intp[0]) and lte(intp[0], max(x1, x2))) and (
                        lte(min(L.p1[1], L.p2[1]), intp[1])
                        and lte(intp[1], max(L.p1[1], L.p2[1]))
                            ):
                        intersects_list.append(intp)

            return remove_recur(intersects_list)

        elif obj2.slope is None:
            x = obj2.x

            for L in obj1.lines:
                if L.slope is not None:
                    a2 = L.slope
                    b2 = L.offset
                    intp = (x, a2 * x + b2)

                    if lte(min(L.p1[0], L.p2[0]), x) and lte(x, max(L.p1[0], L.p2[0])):
                        intersects_list.append(intp)

            return remove_recur(intersects_list)

    #  intersect line with circle
    if obj1.type == "circle" and obj2.type == "line":
        obj1, obj2 = obj2, obj1

    if obj1.type == "line" and obj2.type == "circle":

        if obj1.slope is not None:
            return remove_recur(circle_intersections(obj1, obj2))

        elif obj1.slope is None:
            ymin1 = min(obj1.p1[1], obj1.p2[1])
            ymax1 = max(obj1.p1[1], obj1.p2[1])
            xi = obj1.x
            x0, y0 = obj2.center
            r = obj2.radius

            if lte(x0 - r, xi) and lte(xi, x0 + r):
                intersects = []
                yi1 = y0 - (r**2 - (xi - x0) ** 2) ** 0.5
                yi2 = y0 + (r**2 - (xi - x0) ** 2) ** 0.5

                if lte(ymin1, yi1) and lte(yi1, ymax1):
                    intersects.append((xi, yi1))

                if lte(ymin1, yi2) and lte(yi2, ymax1):
                    intersects.append((xi, yi2))

                return remove_recur(intersects)
            else:
                return []

    #  Intersect rectangle or polygon with rectangle or polygon
    if obj1.type in ("rectangle", "polygon") and obj2.type in ("rectangle", "polygon"):
        intersects = []

        for L1 in obj1.lines:

            for L2 in obj2.lines:
                point = wIntersect(L1, L2)

                if point is not None:
                    intersects.append(point)

        return remove_recur(intersects)

    #  Intersect circle with rectangle or polygon
    if obj1.type == "circle" and obj2.type in ("rectangle", "polygon"):
        obj1, obj2 = obj2, obj1

    if obj1.type in ("rectangle", "polygon") and obj2.type == "circle":
        intersects = []

        for L in obj1.lines:
            intersects += [p for p in wIntersect(L, obj2)]

        return remove_recur(intersects)

    #  intersect 2 functions or function and linearfit
    if obj1.type in ("linearfit", "function") and obj2.type == "function":
        obj1, obj2 = obj2, obj1

    if obj1.type == "function" and obj2.type in ("linearfit", "function"):
        ax = plt.gca()
        xmin, xmax = ax.get_xlim()
        x = np.linspace(xmin, xmax, n)
        f = eval(obj1.eq)
        g = eval(obj2.eq)
        dfg = f - g

        zeros = find_func_zeros(dfg, xmin, xmax)

        return remove_recur([(x[i] + 0, f[i] + 0) for i in zeros])

    # intersect circle and curve model
    if obj1.type in ("curve", "fitcurve") and obj2.type == "circle":
        obj1, obj2 = obj2, obj1

    if obj1.type == "circle" and obj2.type in ("curve", "fitcurve"):
        x0, y0 = obj1.center
        r = obj1.radius
        xmin = x0 - r
        xmax = x0 + r
        intersects = []
        x = np.linspace(xmin, xmax, n)
        f = obj2.func_model(x)

        for circlepart in obj1.eqs:
            g = eval(circlepart)
            dfg = f - g
            zeros = find_func_zeros(dfg, xmin, xmax)
            intersects += [(x[i], f[i]) for i in zeros]

        return remove_recur(intersects)

    # intersect function and curve model
    if obj1.type in ("curve", "fitcurve") and obj2.type == "function":
        obj1, obj2 = obj2, obj1

    if obj1.type == "function" and obj2.type in ("curve", "fitcurve"):
        xmin, xmax = obj2.xmin, obj2.xmax
        x = np.linspace(xmin, xmax, n)
        f = eval(obj1.eq)
        g = obj2.func_model(x)
        dfg = f - g
        zeros = find_func_zeros(dfg, xmin, xmax)
        return remove_recur([(x[i] + 0, f[i] + 0) for i in zeros])

    #  intersect function and circle
    if obj1.type == "function" and obj2.type == "circle":
        obj1, obj2 = obj2, obj1

    if obj1.type == "circle" and obj2.type == "function":
        x0, y0 = obj1.center
        r = obj1.radius
        xmin = x0 - r
        xmax = x0 + r
        intersects = []
        x = np.linspace(xmin, xmax, n)
        f = eval(obj2.eq)

        for circlepart in obj1.eqs:
            g = eval(circlepart)
            dfg = f - g
            zeros = find_func_zeros(dfg, xmin, xmax)
            intersects += [(x[i], f[i]) for i in zeros]

        return remove_recur(intersects)

    #  intersect rectangle or polygon and function
    if obj1.type == "function" and obj2.type in ("rectangle", "polygon"):
        obj1, obj2 = obj2, obj1

    if obj1.type in ("rectangle", "polygon") and obj2.type == "function":
        intersects = []

        for L in obj1.lines:
            intersects += [inters for inters in wIntersect(L, obj2)]

        return remove_recur(intersects)

    #  intersect line and function
    if obj1.type == "function" and obj2.type == "line":
        obj1, obj2 = obj2, obj1

    if obj1.type == "line" and obj2.type == "function":
        intersects = []
        xmin = min((obj1.p1[0], obj1.p2[0]))
        xmax = max((obj1.p1[0], obj1.p2[0]))
        ymin = min((obj1.p1[1], obj1.p2[1]))
        ymax = max((obj1.p1[1], obj1.p2[1]))
        print(xmin, xmax, ymin, ymax)
        x = np.linspace(xmin, xmax, n)
        f = eval(obj2.eq)

        if obj1.slope is not None:
            a = obj1.slope
            b = obj1.offset
            g = eval("{}*x + {}".format(a, b))
            dfg = f - g

            zeros = find_func_zeros(dfg, xmin, xmax)

            for i in zeros:

                if (lte(xmin, x[i], 1e-4) and lte(x[i], xmax, 1e-4)) and (
                    lte(ymin, f[i], 1e-4) and lte(f[i], ymax, 1e-4)
                ):
                    intersects.append((x[i], f[i]))

        elif obj1.slope is None:
            xi = obj1.x
            yi = eval(obj2.eq.replace("x", str(xi)))

            if (lte(xmin, xi) and lte(xi, xmax)) and (lte(ymin, yi) and lte(yi, ymax)):
                intersects.append((xi, yi))

        return remove_recur(intersects)

    #  Intersect line and curve
    if obj1.type in ("curve", "fitcurve") and obj2.type == "line":
        obj1, obj2 = obj2, obj1

    if obj1.type == "line" and obj2.type in ("curve", "fitcurve"):
        intersects = []
        xmin = min((obj1.p1[0], obj1.p2[0]))
        xmax = max((obj1.p1[0], obj1.p2[0]))
        ymin = min((obj1.p1[1], obj1.p2[1]))
        ymax = max((obj1.p1[1], obj1.p2[1]))
        x = np.linspace(xmin, xmax, n)
        f = obj2.func_model(x)

        if obj1.slope is not None:
            a = obj1.slope
            b = obj1.offset
            g = eval("{}*x + {}".format(a, b))
            dfg = f - g

            zeros = find_func_zeros(dfg, xmin, xmax)
            intersects = [
                (x[i], f[i]) for i in zeros
                if (lte(xmin, x[i], 1e-4) and lte(x[i], xmax, 1e-4)) and (
                    lte(ymin, f[i], 1e-4) and lte(f[i], ymax, 1e-4))
                ]
        elif obj1.slope is None:
            xi = obj1.x
            yi = obj2.func_model(xi)

            if (lte(xmin, xi) and lte(xi, xmax)) and (lte(ymin, yi) and lte(yi, ymax)):
                intersects.append((xi, yi))

        return remove_recur(intersects)

    #  Intersect curve with rectange or polygon
    if obj1.type in ("curve", "fitcurve") and obj2.type in ("rectangle", "polygon"):
        obj1, obj2 = obj2, obj1

    if obj1.type in ("rectangle", "polygon") and obj2.type in ("curve", "fitcurve"):
        intersects = []

        for L in obj1.lines:
            intersects += [inters for inters in wIntersect(L, obj2)]

        return remove_recur(intersects)


def wFill(obj1, obj2, fillinv=False, **kwargs):

    if not isinstance(obj1, object) or not isinstance(obj2, object):
        return

    color = "#eaeae5"

    if "zorder" not in kwargs:
        kwargs = dict(kwargs, zorder=0)

    if "color" in kwargs:
        color = kwargs.get("color")
        del kwargs["color"]
    elif "c" in kwargs:
        color = kwargs.get("c")
        del kwargs["c"]

    #  Fill line with circle
    if obj1.type == "circle" and obj2.type == "line":
        obj1, obj2 = obj2, obj1

    if obj1.type == "line" and obj2.type == "circle":
        x0, y0 = obj2.center
        r = obj2.radius
        x = np.linspace(x0 - r, x0 + r, 1000)
        yn = eval(obj2.eqs[0])
        yp = eval(obj2.eqs[1])

        if obj1.slope is None:
            xi = obj1.x

            if not fillinv:
                plt.fill_between(x, y0, yp, where=(x <= xi), facecolor=color, **kwargs)
                plt.fill_between(x, y0, yn, where=(x <= xi), facecolor=color, **kwargs)
            else:
                plt.fill_between(x, y0, yp, where=(x >= xi), facecolor=color, **kwargs)
                plt.fill_between(x, y0, yn, where=(x >= xi), facecolor=color, **kwargs)

        else:

            if len(wIntersect(obj1, obj2)) == 2:
                obj1.type = "function"
                wFill(obj1, obj2, fillinv=fillinv, **kwargs)

    #  Fill function with circle
    if obj1.type == "circle" and obj2.type == "function":
        obj1, obj2 = obj2, obj1

    if obj1.type == "function" and obj2.type == "circle":
        x0, y0 = obj2.center
        r = obj2.radius
        x = np.linspace(x0 - r, x0 + r, 1000)
        yn, yp = obj2.eqs
        yn = eval(yn)
        yp = eval(yp)
        yl = eval(obj1.eq)

        if not fillinv:
            yminp = np.minimum(yl, yp)
            ymaxp = np.maximum(yminp, [y0] * len(x))
            ymaxn = np.maximum(yl, yn)
            yminn = np.maximum(ymaxn, [y0] * len(x))
            plt.fill_between(x, yp, ymaxp, where=(yp >= yl), facecolor=color, **kwargs)
            plt.fill_between(x, y0, ymaxn, where=(yl <= y0), facecolor=color, **kwargs)

        else:
            yminp = np.minimum(yp, yl)
            yminn = np.minimum(yl, [y0] * len(x))
            plt.fill_between(
                x, yminp, [y0] * len(x), where=(yminp >= y0), facecolor=color, **kwargs
            )
            plt.fill_between(x, yminn, yn, where=(yl >= yn), facecolor=color, **kwargs)

    #  Fill between 2 functions
    if obj1.type == "function" and obj2.type == "function":
        ax = plt.gca()
        xmin, xmax = ax.get_xlim()
        ymin, ymax = ax.get_ylim()

        x = np.linspace(xmin, xmax, 1000)
        f = eval(obj1.eq)
        g = eval(obj2.eq)

        if not fillinv:
            plt.fill_between(x, f, g, facecolor=color, **kwargs)
        else:
            fgmin = np.minimum(f, g)
            fgmax = np.maximum(f, g)

            plt.fill_between(x, fgmin, ymin, facecolor=color, **kwargs)
            plt.fill_between(x, fgmax, ymax, facecolor=color, **kwargs)

    #  Fill between function and rectangle
    if obj1.type == "rectangle" and obj2.type == "function":
        obj1, obj2 = obj2, obj1

    if obj1.type == "function" and obj2.type == "rectangle":
        xmin, xmax = obj2.p[0][0], obj2.p[1][1]
        ymin, ymax = obj2.p[0][1], obj2.p[2][1]

        x = np.linspace(xmin, xmax, 1000)
        yl = eval(obj1.eq)

        if not fillinv:
            maxr = np.maximum(ymin, yl)
            plt.fill_between(
                x, maxr, ymax, where=(yl <= ymax), facecolor=color, **kwargs
            )
        else:
            minr = np.minimum(ymax, yl)
            plt.fill_between(
                x, minr, ymin, where=(yl >= ymin), facecolor=color, **kwargs
            )

    #  Fill between function and polygon
    if obj1.type == "polygon" and obj2.type == "function":
        obj1, obj2 = obj2, obj1

    if obj1.type == "function" and obj2.type == "polygon":
        ax = plt.gca()
        xmin, xmax = ax.get_xlim()
        ymin, ymax = ax.get_ylim()
        points = np.array(obj2.p)
        xpoly = points[:, 0]
        ypoly = points[:, 1]

        plt.fill(xpoly, ypoly, color=color, **kwargs)

        x = np.linspace(xmin, xmax, 1000)
        yl = eval(obj1.eq)

        if not fillinv:
            plt.fill_between(x, yl, 0, facecolor=ax.get_facecolor())
        else:
            plt.fill_between(x, yl, 6, facecolor=ax.get_facecolor())


def get_point_angle(x, y, x0, y0):
    num = x - x0
    den = np.sqrt((x - x0) ** 2 + (y - y0) ** 2)

    theta = np.arccos(num / den)

    if y - y0 < 0:
        theta = 2 * np.pi - theta

    return theta


def wAngle(L1, L2, a1, a2, *args, fontsize=14, **kwargs):
    color = "black"

    if "fontsize" in kwargs:
        fontsize = kwargs.get("fontsize")
        del kwargs["fontsize"]

    if "color" in kwargs:
        color = kwargs.get("color")

    if not isinstance(L1, object) or not isinstance(L2, object):
        return

    if L1.type != "line" or L2.type != "line":
        return

    style = ")"
    label = None

    x0, y0 = wIntersect(L1, L2)

    ax = plt.gca()
    xlist = ax.get_xticks()
    ylist = ax.get_yticks()
    xstep = xlist[1] - xlist[0]
    ystep = ylist[1] - ylist[0]
    step = min(xstep, ystep)

    r = step / 2
    p1, p2 = wIntersect(L1, Circle((x0, y0), r))
    p3, p4 = wIntersect(L2, Circle((x0, y0), r))

    x_list = [p1[0], p2[0], p3[0], p4[0]]
    y_list = [p1[1], p2[1], p3[1], p4[1]]

    theta_list = []

    for i in range(len(x_list)):
        x = x_list[i]
        y = y_list[i]
        theta_list.append(get_point_angle(x, y, x0, y0))

    theta_list.sort()
    theta_list.append(2 * np.pi + theta_list[0])
    theta_list.append(theta_list[4] + theta_list[1] - theta_list[0])
    theta_list.append(theta_list[5] + theta_list[2] - theta_list[1])

    theta_1 = theta_list[a1]
    theta_2 = theta_list[a2]
    theta = np.linspace(theta_1, theta_2, 100)

    for e in args:

        if isinstance(e, str) and ")" in e:
            style = e
            continue

        label = e
        continue

    for i in range(style.count(")")):
        x = r * (1 - i * 0.15) * np.cos(theta) + x0
        y = r * (1 - i * 0.15) * np.sin(theta) + y0
        plt.plot(x, y, color=color, lw=1)

    mid_angle = (theta_1 + theta_2) / 2.0
    mid_angle_x = (r + 0.2 * r) * np.cos(mid_angle) + x0
    mid_angle_y = (r + 0.2 * r) * np.sin(mid_angle) + y0
    angle_value = round(np.rad2deg(abs(theta_1 - theta_2)), 1)

    value = angle_value

    if isinstance(label, str) and label is not None:
        value = str(label)

    if mid_angle_y < y0:

        mid_angle_y = mid_angle_y - 0.4 * abs(mid_angle_y - y0)

        if mid_angle_x < x0:
            mid_angle_x = mid_angle_x - 0.4 * abs(mid_angle_x - x0)
            plt.text(
                mid_angle_x,
                mid_angle_y,
                value,
                va="center",
                ha="right",
                color=color,
                fontsize=fontsize - 2,
            )
        else:
            mid_angle_x = mid_angle_x + 0.4 * abs(mid_angle_x - x0)
            plt.text(
                mid_angle_x,
                mid_angle_y,
                value,
                va="center",
                ha="left",
                color=color,
                fontsize=fontsize - 2,
            )
    else:

        mid_angle_y = mid_angle_y + 0.4 * abs(mid_angle_y - y0)

        if mid_angle_x < x0:
            mid_angle_x = mid_angle_x - 0.4 * abs(mid_angle_x - x0)
            plt.text(
                mid_angle_x,
                mid_angle_y,
                value,
                va="top",
                ha="right",
                color=color,
                fontsize=fontsize - 2,
            )
        else:
            mid_angle_x = mid_angle_x + 0.4 * abs(mid_angle_x - x0)
            plt.text(
                mid_angle_x,
                mid_angle_y,
                value,
                va="center",
                ha="left",
                color=color,
                fontsize=fontsize - 2,
            )

    return value


def wLine(p1, p2, lw=1, **kwargs):
    color = "black"

    if "color" in kwargs:
        color = kwargs.get("color")
        del kwargs["color"]

    if len(p1) == 2 and len(p2) == 2:
        x1, y1 = p1
        x2, y2 = p2
        line2D = plt.Line2D(
            (x1, x2),
            (y1, y2),
            lw=lw,
            color=color,
            dash_capstyle="round",
            dash_joinstyle="round",
            solid_capstyle="round",
            solid_joinstyle="round",
            **kwargs
        )
        plt.gca().add_line(line2D)

        return Line(p1, p2)
    return


def wPolygon(points, **kwargs):

    if not isinstance(points, list) or len(points) < 3:
        return

    if not points:
        return

    polygon1 = None
    polygon2 = None

    fill = kwargs.get("fill", False)

    if "fill" in kwargs:
        del kwargs["fill"]
    fillalpha = kwargs.get("fillalpha", 1)

    if "fillalpha" in kwargs:
        del kwargs["fillalpha"]
    alpha = kwargs.get("alpha", 1)

    if "alpha" in kwargs:
        del kwargs["alpha"]
    fillcolor = kwargs.get("fillcolor", "lightgray")

    if "fillcolor" in kwargs:
        del kwargs["fillcolor"]
    color = kwargs.get("color", "black")

    if "color" in kwargs:
        del kwargs["color"]
    border = kwargs.get("border", True)

    if "border" in kwargs:
        del kwargs["border"]

    if "zorder" not in kwargs:
        kwargs = dict(kwargs, zorder=2)

    if fill:
        polygon1 = plt.Polygon(
            points,
            fill=fill,
            color=fillcolor,
            alpha=fillalpha * alpha,
            capstyle="round",
            joinstyle="round",
            **kwargs
        )

        if border:
            polygon2 = plt.Polygon(
                points,
                fill=False,
                color=color,
                capstyle="round",
                joinstyle="round",
                alpha=alpha,
                **kwargs
            )
    else:
        polygon1 = plt.Polygon(
            points,
            fill=False,
            color=color,
            capstyle="round",
            joinstyle="round",
            alpha=alpha,
            **kwargs
        )

    plt.gca().add_line(polygon1)

    if polygon2 is not None:
        plt.gca().add_line(polygon2)

    return Polygon(points)


def wText(p, s, fontsize=14, **kwargs):

    if "fontsize" in kwargs:
        fontsize = kwargs.get("fontsize")
        del kwargs["fontsize"]

    if len(p) == 2:
        x, y = p
        plt.text(x, y, s, fontsize=fontsize, **kwargs)


class wCurve:
    def __init__(self, listPoints, k=3, dots=True, **kwargs):
        self.type = "curve"
        self.p = listPoints
        points = np.array(listPoints)

        # get x and y vectors
        x = points[:, 0]
        y = points[:, 1]

        self.xmin = min(x)
        self.xmax = max(x)

        # calculate polynomial
        z = np.polyfit(x, y, k)
        self.func_model = np.poly1d(z)

        # calculate new x's and y's
        xn = np.linspace(x[0], x[-1], 10000)
        yn = self.func_model(xn)

        if dots or dots == "on":
            plt.plot(x, y, "+", color="dimgray")

        plt.plot(xn, yn, **kwargs)


def wArrow(pfrom, pto, style="->", **kwargs):
    color = kwargs.get("color")

    if not color:
        color = "black"

    if style in ["->", "<-", "-|>", "<|-", "<->", "<|-|>"]:
        style = style
    else:
        style = "->"

    options = dict(
        arrowstyle="{},head_width=0.4,head_length=0.8".format(style),
        shrinkA=0,
        shrinkB=0,
        color=color,
    )
    plt.annotate("", xy=pto, xytext=pfrom, arrowprops=options)


def wParams(**kwargs):
    ax = plt.gca()
    font = kwargs.get("font", "serif")
    xmin = kwargs.get("xmin", ax.get_xlim()[0])
    xmax = kwargs.get("xmax", ax.get_xlim()[1])
    ymin = kwargs.get("ymin", None)
    ymax = kwargs.get("ymax", None)
    xlegend = kwargs.get("xlegend", "")
    ylegend = kwargs.get("ylegend", "")
    xlabel = kwargs.get("xlabel", "")
    ylabel = kwargs.get("ylabel", "")
    xstep = kwargs.get("xstep", 0)
    ystep = kwargs.get("ystep", 0)
    xmstep = kwargs.get("xmstep", 0)
    ymstep = kwargs.get("ymstep", 0)
    grid = kwargs.get("grid", True)
    mgrid = kwargs.get("mgrid", False)
    title = kwargs.get("title", "")
    fontsize = int(kwargs.get("fontsize", 14))
    intx = kwargs.get("intx", False)
    inty = kwargs.get("inty", False)
    y0 = kwargs.get("y0", None)
    x0 = kwargs.get("x0", None)
    backgroundcolor = kwargs.get("bgcolor", "white")
    centeraxis = kwargs.get("centeraxis", False)
    xaxpos = kwargs.get("xaxpos", 0)
    yaxpos = kwargs.get("yaxpos", 0)
    axis = kwargs.get("axis", True)
    equal = kwargs.get("equal", False)
    ticks = kwargs.get("ticks", True)
    legend = kwargs.get("legend", False)
    tcolor = kwargs.get("tcolor", False)
    xtcolor = kwargs.get("xtcolor", 'black')
    ytcolor = kwargs.get("ytcolor", 'black')
    axcolor = kwargs.get("axcolor", False)
    laxcolor = kwargs.get("laxcolor", 'black')
    raxcolor = kwargs.get("raxcolor", 'black')
    taxcolor = kwargs.get("taxcolor", 'black')
    baxcolor = kwargs.get("baxcolor", 'black')
    xlcolor = kwargs.get("xlcolor", 'black')
    ylcolor = kwargs.get("ylcolor", 'black')

    #  Colors
    if axcolor:
        ax.spines['bottom'].set_color(axcolor)
        ax.spines['left'].set_color(axcolor)
        ax.spines['right'].set_color(axcolor)
        ax.spines['top'].set_color(axcolor)
    else:
        ax.spines['bottom'].set_color(baxcolor)
        ax.spines['left'].set_color(laxcolor)
        ax.spines['right'].set_color(raxcolor)
        ax.spines['top'].set_color(taxcolor)

    if tcolor:
        ax.tick_params(axis='x', colors=tcolor)
        ax.tick_params(axis='y', colors=tcolor)
    else:
        ax.tick_params(axis='x', colors=xtcolor)
        ax.tick_params(axis='y', colors=ytcolor)
        rc_fonts = {"font.family": "{}".format(font), "mathtext.fontset": "dejavuserif"}
    plt.rcParams.update(rc_fonts)

    if not axis:
        plt.axis("off")

    if xlegend and not centeraxis:
        plt.xlabel(str(xlegend), fontsize=fontsize, color=xlcolor)

    if ylegend and not centeraxis:
        plt.ylabel(str(ylegend), fontsize=fontsize, color=ylcolor)

    if grid:
        plt.grid(ls="dotted", zorder=1000)

    if mgrid:
        plt.grid(which="minor", ls="dotted", zorder=1000)

    if title:
        plt.title(title, fontsize=fontsize+1, fontweight="bold")

    ax.tick_params(axis="both", which="major", labelsize=str(fontsize - 2))

    if equal:
        ax.set_aspect("equal")

    ax.set_xlim(xmin, xmax)

    if not ymin:
        ymin = ax.get_ylim()[0]
    if not ymax:
        ymax = ax.get_ylim()[1]

    ax.set_ylim(ymin, ymax)
    listx = ax.get_xticks()
    listy = ax.get_yticks()
    
    if xstep:

        if xmin < 0 < xmax:
            pos = np.arange(0, xmax + xstep, xstep)
            neg = -1 * np.flip(np.arange(0, -xmin + xstep, xstep))
            listx = np.concatenate((neg[:-1], pos))

        elif gte(xmin, 0):
            listx = np.arange(xmin, xmax + xstep, xstep)

        elif lte(xmax, 0):
            listx = np.arange(xmin, xmax, xstep)

        ax.xaxis.set_major_locator(MultipleLocator(xstep))

        if xmstep and xmstep < xstep:
            ax.xaxis.set_minor_locator(MultipleLocator(xmstep))

    if intx:
        listx = np.array(listx)
        listx = np.int_(listx)
        ax.set_xticks(listx)
    
    if ystep:
        if ymin < 0 < ymax:
            pos = np.arange(0, ymax + ystep, ystep)
            neg = -1 * np.flip(np.arange(0, -ymin + ystep, ystep))
            listy = np.round(
                np.concatenate((neg[:-1], pos)), len(str(ystep).split(".")[-1])
            )

        elif gte(ymin, 0):
            listy = np.round(
                np.arange(ymin, ymax + ystep, ystep), len(str(ystep).split(".")[-1])
            )

        elif lte(ymax, 0):
            listy = np.round(
                np.arange(ymin, ymax, ystep), len(str(ystep).split(".")[-1])
            )

        ax.set_yticks(listy)
        ax.yaxis.set_major_locator(MultipleLocator(ystep))
        
        if ymstep and ymstep < ystep:
            ax.yaxis.set_minor_locator(MultipleLocator(ymstep))

    if inty:
        listy = np.array(listy)
        listy = np.int_(listy)
        ax.set_yticks(listy)

    if not yaxpos:

        if xmin > 0:
            yaxpos = xmin

        elif xmax < 0:
            yaxpos = xmax

        else:
            yaxpos = 0

    elif yaxpos > xmax:
        yaxpos = xmax

    elif yaxpos < xmin:
        yaxpos = xmin

    if not xaxpos:

        if ymin > 0:
            xaxpos = ymin

        elif ymax < 0:
            xaxpos = ymax

        else:
            xaxpos = 0

    elif xaxpos > ymax:
        xaxpos = ymax

    elif xaxpos < ymin:
        xaxpos = ymin

    if xaxpos:
        if y0 is None:
            y0 = True

    if not yaxpos:
        if x0 is None:
            x0 = False
        if y0 is None:
            y0 = True

    if centeraxis and axis:
        #  center axis
        ax.spines["left"].set_position(("data", yaxpos))
        ax.spines["right"].set_visible(False)
        ax.spines["bottom"].set_position(("data", xaxpos))
        ax.spines["top"].set_visible(False)
        ax.xaxis.set_ticks_position("bottom")
        ax.yaxis.set_ticks_position("left")
        #  add labels
        ax.text(
            (xmax - xmin) / 35 + yaxpos,
            1.05,
            ylabel,
            transform=ax.get_xaxis_transform(),
            fontsize=fontsize,
            style='italic',
            color=laxcolor
        )
        ax.text(
            1.05,
            (ymax - ymin) / 30 + xaxpos,
            xlabel,
            transform=ax.get_yaxis_transform(),
            fontsize=fontsize,
            style='italic',
            color=baxcolor
        )
        #  add arrows arrows
        ax.plot(
            1.04,
            xaxpos,
            ls="",
            marker=">",
            ms=6,
            color=baxcolor,
            transform=ax.get_yaxis_transform(),
            clip_on=False,
        )
        ax.plot(
            [1, 1.04],
            [xaxpos, xaxpos],
            "-",
            color=baxcolor,
            lw=0.8,
            transform=ax.get_yaxis_transform(),
            clip_on=False,
        )
        ax.plot(
            yaxpos,
            1.05,
            ls="",
            marker="^",
            ms=6,
            color=laxcolor,
            transform=ax.get_xaxis_transform(),
            clip_on=False,
        )
        ax.plot(
            [yaxpos, yaxpos],
            [1, 1.05],
            "-",
            color=laxcolor,
            lw=0.8,
            transform=ax.get_xaxis_transform(),
            clip_on=False,
        )
        if not y0 and ticks:
            yticks = ax.yaxis.get_major_ticks()
            yticksvalues = list(ax.get_yticks())
            idxz = yticksvalues.index(0)
            yticks[idxz].label1.set_visible(False)

        if not x0 and ticks:
            xticks = ax.xaxis.get_major_ticks()
            xticksvalues = list(ax.get_xticks())
            idxz = xticksvalues.index(0)
            xticks[idxz].label1.set_visible(False)
    
    if not ticks:
        ax.get_xaxis().set_ticks([])
        ax.get_yaxis().set_ticks([])
    
    for direction in ('left', 'right', 'top', 'bottom'):
        ax.spines['{}'.format(direction)].set_zorder(ax.yaxis.get_label().get_zorder()-2)

    if backgroundcolor not in "white":
        ax.set_facecolor(backgroundcolor)

    if backgroundcolor:
        plt.setp(ax.get_xticklabels(), backgroundcolor=backgroundcolor)
        plt.setp(ax.get_yticklabels(), backgroundcolor=backgroundcolor)


def wTangent(f, px, restr=[], **kwargs):
    g = None
    yp2 = 0
    dp2 = 0

    if isinstance(f, tuple) and len(f) == 3:
        x0, y0, r = f[0]
        f = "{} + ( {}**2 - (x - {})**2 )**0.5".format(y0, r, x0)
        g = "{} - ( {}**2 - (x - {})**2 )**0.5".format(y0, r, x0)

    if not isinstance(f, str):
        return

    ax = plt.gca()
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    x = float(px)

    if g is not None:
        yp2 = eval(g)

    yp = eval(f)
    h = (xmax - xmin) / 1e6
    x = float(px) + h
    dp = (eval(f) - yp) / h

    if g is not None:
        dp2 = (eval(g) - yp2) / h

    x = np.linspace(xmin, xmax, 500)

    if restr:

        if isinstance(restr, list):

            if len(restr) == 2:
                x = np.array([n for n in x if restr[0] <= n <= restr[1]])

    if g is not None:
        plt.plot(x, dp * (x - float(px)) + yp, **kwargs)
        plt.plot(x, dp2 * (x - float(px)) + yp2, **kwargs)

        return [
            ((dp, yp - dp * float(px)), None, None, None),
            (px, yp),
            ((dp2, yp2 - dp2 * float(px)), None, None, None),
            (px, yp2),
        ]
    else:
        plt.plot(x, dp * (x - float(px)) + yp, **kwargs)
        return (((dp, yp - dp * float(px)), None, None, None), (px, yp))


def wInteg(obj, p, pts=10000, **kwargs):
    if obj.type != "function":
        return

    if "color" not in kwargs:
        kwargs = dict(kwargs, color='mediumblue')

    ax = plt.gca()
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()

    if p[0] >= xmin and p[0] <= xmax:
        if p[0] < xmax:
            # First part of curve
            x = np.linspace(p[0], xmax, pts)
            eq = obj.eq
            f = eval(eq)
            F1 = [p[1]]

            for i in range(len(x)-1):
                F1.append(F1[i] + (x[i+1] - x[i]) * f[i])

            plt.plot(x, F1, **kwargs)

        # Second part
        if p[0] > xmin:
            x = np.linspace(xmin, p[0], pts, endpoint=False)
            np.append(x, p[0])
            x = np.flip(x)
            f = eval(eq)
            F2 = [p[1]]

            for i in range(len(x)-1):
                F2.append(F2[i]+(x[i+1]-x[i])*f[i])

            if 'label' in kwargs:
                del kwargs['label']

            plt.plot(x, F2, **kwargs)


def wDeriv(obj, pts=100000, **kwargs):

    if obj.type != "function":
        return

    if 'color' not in kwargs:
        kwargs = dict(kwargs, color="darkgreen")

    ax = plt.gca()
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()

    x = np.linspace(xmin, xmax, pts)
    f = obj.eq
    y = eval(f)
    deriv = tuple((y[i+1] - y[i]) / (x[i+1] - x[i]) for i in range(len(x)-1))
    plt.plot(x[:-1], deriv, **kwargs)


def matplotlibToSvg(mplcode, imgfile, app_path, data_path, export, firstload, imgformat="svg"):
    excepts = (
        "import os",
        "from os"
        "os.remove",
        "os.rmdir",
        "shutils",
        "from shutils"
        "shutil.rmtree",
        "os.rmdir",
        "subprocess",
        "rm -rf",
        "rm -f",
    )

    if all([True for e in excepts if e not in mplcode]):
        if firstload:
            mplcode = """
plt.axis('off')
plt.xticks([], [])
plt.yticks([], [])
plt.text(.15, .6, "SECURITY: PLEASE VERIFY CODE", color="darkred", fontsize=15)
plt.text(.3, .4, "BEFORE EXECUTION...", color="darkred", fontsize=15)
wRectangle((0,0), 1, 1, color="darkred", lw=2)
ax = plt.gca()
ax.set_facecolor('linen')
"""

        try:
            os.chdir(data_path)
            transp = True

            if "wParams(" in mplcode:
                wp = re.search(r"wParams\([^)]+\)", mplcode, flags=re.DOTALL)
                if wp:
                    if "bgcolor=" in mplcode[wp.start():wp.end()]:
                        transp = False
                    leg = re.search(r"legend=(True|False|0|1|['\"]*[a-z]+['\"]*)", mplcode[wp.start():wp.end()])
                    if leg:
                        legend = leg.group(1).replace('"', '').replace("'", "")
                        if legend in ("tl", "tr", "tc", "c", "bl", "bc", "br", "r", "l"):
                            posdict = {
                                "tl": "upper left", "tr": "upper right", "tc": "upper center",
                                "c": "center", "r": "center right", "l": "center left",
                                "bl": "lower left", "bc": "lower center", "br": "lower right",
                                }
                            legend = "plt.legend(loc='{}')".format(posdict[legend])
                        else:
                            legend = "plt.legend()"
                        mplcode = "".join((mplcode, "\n", legend))

            exec(mplcode)
            fig = plt.gca()
            plt.savefig(
                imgfile, facecolor=fig.get_facecolor(), format=imgformat, bbox_inches="tight", transparent=transp,
            )

            if export:
                copyfile(imgfile, os.path.join(data_path, os.path.basename(imgfile)))
            plt.close("all")

        except BaseException as error:

            if error:
                error = str(error)
                error = re.sub(r"\([^)]+\)", "", error)
                error = error.splitlines()
                dtext = ""
                y = 20

                for i in range(len(error)):
                    dtext = "".join(
                        (
                            dtext,
                            '<text x="0" y="{}" fill="red" font-size="1.5em">'.format(
                                y
                            ),
                            error[i],
                            "</text>\n",
                        )
                    )
                    y += 30

                svg = """
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" height="{}" width="800">
  {}
</svg>
""".format(
                    y, dtext
                )

                with open(imgfile, "w") as f:
                    f.write(svg)

            else:
                copyfile("{}/matplot_error.svg".format(app_path), imgfile)

        except Exception:
            copyfile("{}/matplot_error.svg".format(app_path), imgfile)
    return
