# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re


def fontopt(match):
    fontname = ""
    color = ""
    fontsize = ""
    style = ""
    opts = []
    m = match.group(1)
    content = match.group(2)

    sopts = m.find("(", 0)
    eopts = m.find(")", sopts)

    if sopts >= 0 and eopts >= 0:
        opts = m[sopts + 1: eopts].strip().split(" ")

    if opts:
        if 'italic' in opts:
            opts.remove('italic')
            style = "".join((
                style,
                "font-style: italic; "
                ))
        if 'bold' in opts:
            opts.remove('bold')
            style = "".join((
                style,
                "font-weight: bold; "
                ))
        s = re.compile(r"\d+")
        fontsize = list(filter(s.match, opts))
        if fontsize:
            style = "".join((
                style,
                "font-size: {}pt; ".format(fontsize[0])
                ))

        f = re.compile(r"^[a-zA-Z\.]{3,}")
        fontname = list(filter(f.match, opts))

        if fontname:
            style = "".join(
                (
                    style,
                    "font-family: &quot;{}&quot;; ".format(
                        fontname[0].replace(".", " ")
                    ),
                )
            )

        c = re.compile(r"\#[\da-f]{6}")
        color = list(filter(c.match, opts))

        if color:
            style = "".join((
                style,
                "color:{}; ".format(color[0]),
                style
                ))

        re_color = re.compile(r"@[a-z]+")
        color = list(filter(re_color.match, opts))

        if color:
            color = color[0][1:]
            if color:
                style = "".join(("color:{}; ".format(color), style))
        return '<div style="{}">\n\n{}\n\n</div>'.format(style, content)
    return "\n\n{}\n\n".format(content)


def font(code):
    return re.sub(r"^!font(.*?)\s*$(.+?)^\.font", fontopt, code, flags=re.M | re.DOTALL)
