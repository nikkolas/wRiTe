# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
from classes.emojis import emojis
from functools import partial


def fontawesome(code):

    def fontawesomeOrEmoji(match, emojis):
        name = match.group(1)
        fa = False
        bw = False
        if "+" in name[1:2]:
            name = name.replace("+", "")
            fa = True
        if "-" in name[1:2]:
            bw = True
            name = name.replace("-", "")
        if name in emojis and not fa:
            if not bw:
                # keep great quality
                return '<span style="color:black; filter: grayscale(0%);">{}</span>'.format(emojis[name])
            else:
                return '<span style="color:black; filter: grayscale(100%);">{}</span>'.format(emojis[name])

        return '<i class="fa fa-{}"></i>'.format(name[1:-1])

    return re.sub(r"(\:[+-]*[a-z]+[_\-a-z]*\:)", partial(fontawesomeOrEmoji, emojis=emojis), code)
