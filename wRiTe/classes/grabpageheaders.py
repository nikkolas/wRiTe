# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.


def grabpageheaders(code, uniqHeader, pageHeaders, pages):
    #  Get header for each page and save to dict
    pageheader = ""

    for i, page in enumerate(pages):
        start = page.find("!header") + 7

        if start >= 0:
            end = page.find(".header", start)
            if end > 0:
                pageheader = page[start:end].strip()

        if pageheader:
            pageHeaders[i] = pageheader

        pageheader = ""

    #  If header is unique, next occurences equal to the first
    if uniqHeader:
        for key in pageHeaders:
            if key > 0:
                pageHeaders[key] = pageHeaders[0]

    #  Remove !header occurences
    index = 0

    for j in range(code.count("!header")):
        start = code.find("!header", index)
        index = start

        if start >= 0:
            end = code.find(".header", start + 7) + 7
            if end > 0:
                code = "".join((code[:start], code[end:]))

    return code, pageHeaders
