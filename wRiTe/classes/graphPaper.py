# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.


import re


def graphPaper(code):
    code = re.sub(
        r"!graphPaper\(([\d\s]+),([\d\s]+),([\d\s]+),([\d\s]+)[,]*\s*[']*([pxcm\s]*)[']*\s*\)",
        r"\n!muPDF\npage.draw_milli((\1, \2, \3, \4), unit='\5')\n.muPDF\n",
        code
        )
    return code