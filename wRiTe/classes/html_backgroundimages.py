# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import os
import re
from classes.utils import findBalanced


def html_backgroundimages(
    code,
    numofbgimages,
    numofbggradients,
    backgroundimage,
    bgalpha,
    backgroundgradient,
    data_path,
):

    if backgroundimage and bgalpha:
        code = code.replace(
            '<page style="',
            (
                '<page style="background: linear-gradient(rgba(255,255,255,{0}'
                '),rgba(255,255,255,{0})), {1}; background-size: 100% 100%;'
                ' background-repeat: no-repeat; '
                ).format(
                int(bgalpha[:-1]) / 100, backgroundimage
            ),
        )
    elif backgroundimage:
        code = code.replace(
            '<page style="',
            (
                '<page style="background: linear-gradient('
                'rgba(255,255,255,0),rgba(255,255,255,0)), {};'
                ' background-size: 100% 100%; background-repeat:'
                ' no-repeat; '
                ).format(
                backgroundimage
            ),
        )
    #  Background gradient
    elif backgroundgradient:
        code = code.replace(
            '<page style="',
            '<page style="background-image: {}; '.format(backgroundgradient),
        )

    #  Apply individual background images
    # numofbgimages = code.count('!bgimage(')
    index = 0

    for i in range(numofbgimages):
        alpha = 0
        beg = code.find("!bgimage(", index) + 9
        end = findBalanced("(", code, beg)
        endline = code.find("\n", end)
        index = end
        bgimage = code[beg:end].strip()
        bgimage = re.sub(r"\s+", " ", bgimage)
        bgimage = bgimage.split(" ")

        if len(bgimage) == 2:
            if "%" in bgimage[0] and "%20" not in bgimage[0]:
                alpha, bgimage = bgimage
            else:
                bgimage, alpha = bgimage
            alpha = int(alpha.strip()[:-1]) / 100
        elif len(bgimage) == 1:
            bgimage = bgimage[0]

        if bgimage:
            code = "".join((code[: beg - 9], code[endline:]))
            if "/" not in bgimage:
                bgimage = os.path.join(data_path, bgimage)

            bgimage = "url('{}')".format(bgimage)
            pospage = code.rfind('<page style="', 0, beg - 9)

            if code[pospage + 13: pospage + 24] == "background:":
                endimg = code.find("url(", pospage)
                endimg = code.find(");", endimg) + 2
                code = "".join(
                    (
                        code[:pospage],
                        (
                            '<page style="background: linear-gradient('
                            'rgba(255,255,255,{0}),rgba(255,255,255,{0}))'
                            ', {1}; '
                            ).format(
                            alpha, bgimage
                        ),
                        code[endimg:],
                    )
                )
            elif code[pospage + 13: pospage + 30] == "background-image:":
                endgrad = code.find(";", pospage + 13)
                code = "".join(
                    (
                        code[: pospage + 13],
                        (
                            "background: linear-gradient(rgba(255,255,255,{0})"
                            ",rgba(255,255,255,{0})), {1}; "
                            "background-size: 100% 100%; "
                            "background-repeat: no-repeat; "
                            ).format(
                            alpha, bgimage
                        ),
                        code[endgrad + 1:],
                    )
                )
            else:
                code = "".join(
                    (
                        code[:pospage],
                        (
                            '<page style="background: linear-gradient('
                            'rgba(255,255,255,{0}),rgba(255,255,255,{0})),'
                            ' {1}; background-size: 100% 100%;'
                            ' background-repeat: no-repeat; '
                            ).format(
                            alpha, bgimage
                        ),
                        code[pospage + 13:],
                    )
                )

    #  Apply individual background gradients
    # numofbggradients = code.count('!bggradient(')
    index = 0

    for j in range(numofbggradients):
        beg = code.find("!bggradient(", index) + 12
        end = findBalanced("(", code, beg)
        endline = code.find("\n", end)
        index = end
        bgimage = code[beg:end].strip()
        if bgimage:
            if "lg(" in bgimage:
                bgimage = bgimage.replace("lg(", "linear-gradient(")
            code = "".join((code[: beg - 12], code[endline:]))
            pospage = code.rfind('<page style="', 0, beg - 12)

            if code[pospage + 13: pospage + 24] == "background:":
                endimg = code.find("url(", pospage)
                endimg = code.find(");", endimg)
                code = "".join(
                    (
                        code[:pospage],
                        '<page style="background-image: {}; '.format(bgimage),
                        code[endimg + 2:],
                    )
                )
            elif code[pospage + 13: pospage + 30] == "background-image:":
                endgrad = code.find(";", pospage + 13)
                code = "".join(
                    (
                        code[: pospage + 13],
                        "background-image: {}; ".format(bgimage),
                        code[endgrad + 1:],
                    )
                )
            else:
                code = "".join(
                    (
                        code[:pospage],
                        (
                            '<page style="background-image: {};'
                            ' background-size: 100% 100%;'
                            ' background-repeat: no-repeat; '
                            ).format(
                            bgimage
                        ),
                        code[pospage + 13:],
                    )
                )

    return code
