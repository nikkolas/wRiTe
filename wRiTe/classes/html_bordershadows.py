# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.


def html_bordershadows(code, numimg, img_bsopts):
    index = 0
    for im in range(numimg):
        start = code.find('<img src="', index)
        end = code.find("/>", start + 10) + 2

        if img_bsopts[im]["rotation"]:
            startfigure = code.rfind("<figure", 0, start)
            figure = code[startfigure:start]
            if figure.count("\n") == 1:
                endfigure = code.find(">", startfigure) + 1
                figure = code[startfigure:endfigure]
                if 'style="' in figure:
                    figure = figure.replace(
                        'style="',
                        'style="{} '.format(img_bsopts[im]["rotation"]
                                            )
                    )
                else:
                    figure = figure.replace(
                        ">", ' style="{}">'.format(img_bsopts[im]["rotation"])
                    )

                code = "".join((code[:startfigure], figure, code[endfigure:]))

        start = code.find('<img src="', index)
        end = code.find("/>", start + 10) + 2

        if not "".join(img_bsopts[im].values()).strip():
            index = end
            continue

        content = code[start:end]

        if 'style="' in content:
            content = content.replace(
                'style="',
                'style="{} {} {}'.format(
                    img_bsopts[im]["border"],
                    img_bsopts[im]["shadow"],
                    img_bsopts[im]["corner"],
                ),
            )
        else:
            content = content.replace(
                "/>",
                'style="{} {} {}" />'.format(
                    img_bsopts[im]["border"],
                    img_bsopts[im]["shadow"],
                    img_bsopts[im]["corner"],
                ),
            )

        code = "".join((code[:start], content, code[end:]))
        index = end

    return code
