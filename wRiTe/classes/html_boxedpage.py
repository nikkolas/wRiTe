# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
from classes.utils import findBalanced
from classes.colors import colors


def html_boxedpage(code, num):
    index = 0
    for bp in range(num):
        lw = "0.5px"
        color = "black"
        opts = ""
        beg = code.find("!pageframe", index)
        end = code.find("\n", beg)
        begp = code.find("(", beg, end) + 1
        if begp > 0:
            end = findBalanced("(", code, begp)

            if end >= 0:
                opts = code[begp:end].strip()
                opts = re.sub(r"\s+", " ", opts).split(" ")
                opts.sort()

                if len(opts) == 2:
                    if "@" in opts[0]:
                        if opts[0][1:] in colors:
                            color = colors[opts[0][1:]]
                        lw = opts[1]
                    else:
                        if "@" in opts[1]:
                            if opts[1][1:] in colors:
                                color = colors[opts[1][1:]]
                        lw = opts[0]
                elif len(opts) == 1:
                    if "@" in opts[0]:
                        if opts[0][1:] in colors:
                            color = colors[opts[0][1:]]
                    else:
                        lw = opts[0]

        pospage = code.rfind('<page style="', 0, beg - 12)
        repl = "border: {} solid {}; ".format(lw, color)
        code = "".join((code[: pospage + 13], repl, code[pospage + 13:]))
        index = end + len(repl)

    code = re.sub("^<p>!pageframe.*$", "", code, flags=re.MULTILINE)

    return code
