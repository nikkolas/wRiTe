# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.
import re

def html_buildhtmlheader(
    code, APP_PATH, katex, highlighter, highlight, mermaid, mermaidTheme, music, font_size
):

    if katex:
        head = (
            '<link rel="stylesheet" href="{0}/katex/katex.css">'
            '<script src="{0}/katex/katex.js"></script>'
            '<script src="{0}/katex/contrib/auto-render.min.js">'
            '</script><script src="{0}/katex/contrib/mhchem.min.js">'
            '</script>\n</head>'
            ).format(
            APP_PATH
        )

        body = (
            "\n<script>\n"
            "renderMathInElement(document.body,"
            "{strict: false, throwOnError : false, delimiters:"
            " [{left: '\\\\[', right: '\\\\]', display: true},"
            " {left: '\\\\(', right: '\\\\)', display: false}]});\n"
            "</script>\n</body>\n</html>"
        )

        vect = "\\global\\def\\vect#1{\\overrightarrow{\\vphantom{t}#1}}"

        vv = "\\global\\def\\vv#1{\\overrightarrow{#1}}"

        physics = "\\global\\def\\dv#1#2{\\dfrac{\\textrm{d}#1}{\\textrm{d}#2}}"

        exponential = "\\global\\def\\e{\\,\\textrm{e}\\,}"

        differential = "\\global\\def\\d{\\kern+0.13ex\\textrm{d}\\kern-0.09ex}"

        globaldef = "".join((vect, vv, physics, exponential, differential))

        code = code.replace("</head>", head)
        code = code.replace('</body>\n</html>', body)

        code = code.replace(
            '<pagecontent>',
            '<pagecontent>\n<span style="display: none; max-height: 0; max-width: 0; margin:0; padding:0;">\({}\)</span>'.format(globaldef),
            1
        )
        code = code.replace('kern-0.09ex}\)</span>\n\n<p>', 'kern-0.09ex}\)</span>\n\n', 1)
        code = code.replace('</p>', '', 1)        
        
    if highlighter:
        head = "".join(
            (
                '<link rel="stylesheet" href="{}/highlight/styles/',
                highlight,
                (
                    '"><script src="{}/highlight/highlight.js">'
                    '</script><script>hljs.initHighlightingOnLoad();'
                    '</script>\n</head>'
                ),
            )
        ).format(APP_PATH, APP_PATH)
        code = code.replace("</head>", head)

    if mermaid:
        head = "".join(
            (
                (
                    "<script src='{}/mermaid/mermaid.min.js'>"
                    "</script>\n<script>mermaid.initialize({{theme: '"
                ),
                mermaidTheme,
                "', themeVariables: {{ fontSize: '{}pt'}}, startOnLoad:true}});</script>",
            )
        ).format(APP_PATH, font_size-1)
        code = code.replace("</head>", head)

    if music:
        head = (
            '<script src="{}/vexflow/div.prod.js">'
            '</script>\n<script>\n    VEXTAB_USE_SVG = true;'
            '\n</script>\n</head>'
            ).format(
            APP_PATH
        )
        code = code.replace("</head>", head)

    return code
