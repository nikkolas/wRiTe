# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re


def html_figurescorrection(code):
    def align(m):
        src, align, h, w, leg = m.groups()
        if "right" in align:
            return (
                '<figure style="margin:5px; margin-left:35px;'
                ' float:{}; width:{}; ">\n<img src="{}" '
                'style="height:{};" />\n<figcaption>{}</figcaption>'
                '\n</figure>\n'
                ).format(
                align, w, src, h, leg
            )
        elif "left" in align:
            return (
                '<figure style="margin-right:35px;'
                ' float:{}; width:{}; ">\n<img src="{}"'
                ' style="height:{};" />\n<figcaption>{}</figcaption>'
                '\n</figure>\n'
                ).format(
                align, w, src, h, leg
            )
        else:
            return (
                '<figure style="float:{}; width:{};">'
                '\n<img src="{}" style="height:{};"/>\n'
                '<figcaption>{}</figcaption>\n</figure>\n'
                ).format(
                align, w, h, src, leg
            )

    code = re.sub(
        r'<figure>\n<img src="(.+)" alt="(.*)" width="(\d+)" align="(.*)" style="height:\s*(.+);" />\n<figcaption>(.*)</figcaption>\n</figure>\n',
        r'<figure>\n<img src="\1" alt="\2" align="\4" style="height:\5; width:\3px;" />\n<figcaption>\6</figcaption>\n</figure>\n',
        code,
    )
    code = re.sub(
        r'<figure>\n<img src="(.+)" alt=".*" align="" style="height:\s*(.+);\s*width:\s*(.+);" />\n<figcaption>(.*)</figcaption>\n</figure>\n',
        r'<figure style="width:\3;">\n<img src="\1" style="height:\2;" />\n<figcaption>\4</figcaption>\n</figure>\n',
        code,
    )
    code = re.sub(
        r'<figure>\n<img src="(.+)" alt=".*" align="(\w+)" style="height:\s*(.+);\s*width:\s*(.+);" />\n<figcaption>(.*)</figcaption>\n</figure>',
        align,
        code,
    )
    code = re.sub(
        r'<img src="(.+)" alt="(.*)" align="(\w+)" style="height:\s*(.+);\s*width:\s*(.+);" />',
        r'\n<figure style="float:\3; width:\5; height:\3;">\n<img src="\1" />\n<figcaption>\2</figcaption>\n</figure>',
        code,
    )

    return code
