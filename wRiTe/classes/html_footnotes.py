# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re


def html_footnotes(code, num_of_pages):
    footnote_beg = code.find('<div class="footnotes">')
    footnote_end = code.find("</div>", footnote_beg)

    if (footnote_beg >= 0) and (footnote_end >= 0):
        footnote = code[footnote_beg:footnote_end]
        footnote = footnote.replace(">&#160;&#8617;&#xfe0e;</a>", ">&#9741;</a>")
        code = "".join((code[:footnote_beg], code[footnote_end + 6:]))
        index = 0
        for i in range(num_of_pages):
            page_beg = code.find("<page style", index)
            page_beg = code.find(">", page_beg) + 1
            page_end = code.find("</page>", page_beg)
            index = page_end + 7
            page = code[page_beg:page_end]
            expos = re.findall(r'class="footnote"><sup>(\d+)</sup>', page)
            if expos:
                exp = 1
                exp_index = 0

                for _ in expos:
                    exp_beg = page.find('class="footnote"><sup>', exp_index) + 22
                    exp_end = page.find("</sup>", exp_beg)
                    page = "".join((page[:exp_beg], str(exp), page[exp_end:]))
                    exp_index = exp_end
                    exp += 1
                code = "".join((code[:page_beg], page, code[page_end:]))

            refs = re.findall(
                r'<a href="#(fn:\d+)" id="fnref:\d+" title="[^"]+" class="footnote">',
                code[page_beg:page_end],
            )

            if refs:
                footer_items = (
                    '\n<hr style="padding-left:0; margin-left:0;'
                    ' border: none; height: 0.5px; '
                    'background-color: #555; width: 30%;"/>\n<ol>\n'
                )
                for ref in refs:
                    item_beg = footnote.find('<li id="{}">'.format(ref))
                    item_end = footnote.find("</li>", item_beg) + 5
                    footer_items = "".join(
                        (footer_items, "\n\n", footnote[item_beg:item_end])
                    )

                footer_items = "".join((footer_items, "\n\n</ol>"))
                footer_beg = code.find("<footer></footer>", page_beg)
                code = "".join(
                    (code[: footer_beg + 8], footer_items, "\n", code[footer_beg + 8:])
                )
    return code
