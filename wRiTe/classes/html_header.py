# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.


try:
    from classes.colors import colors
except ModuleNotFoundError:
    print("missing core modules")

try:
    import ast
except ModuleNotFoundError:
    print("ast module not found")


def html_header(
    code, APP_PATH, css_file, fontSize, fontName, fontMath, fontTitle, lineSpacing, bqcolor, body, layout, newpages
):

    bqrgbcolor = None
    fontTitles = ''

    if bqcolor:
        if bqcolor.isalpha():
            if bqcolor in colors:
                bqcolor = colors[bqcolor]
            else:
                bqcolor = ""

        if '#' in bqcolor:
            try:
                int(bqcolor[1:], 16)
                bqcolor = bqcolor.lstrip('#')
                bqrgbcolor = tuple(int(bqcolor[i:i+2], 16) for i in (0, 2, 4))
            except Exception:
                bqrgbcolor = None
        elif 'rgb' in bqcolor:
            try:
                bqrgbcolor = ast.literal_eval(bqcolor.replace("rgb", ""))
            except Exception:
                bqrgbcolor = None

    if bqrgbcolor:
        bqcolor = """
blockquote {{
    border-color: rgb({0}, {1}, {2});
    background-color: rgba({0}, {1}, {2}, 0.04);
}}

blockquote blockquote {{
    border-color: rgb({0}, {1}, {2});
    background-color: rgba({0}, {1}, {2}, 0.02);
}}

blockquote blockquote blockquote {{
    border-color: rgb({0}, {1}, {2});
    background-color: rgba({0}, {1}, {2}, 0.04);
}}
""".format(*bqrgbcolor)

    fontDef = 'font-family: inherit, "Noto Color Emoji";'

    if fontName.strip():
        fontDef = 'font-family: "{}", serif, "Noto Color Emoji";'.format(fontName)
    if fontTitle.strip():
        fontTitles = '.title, h1, h2, h3, h4, h5, h6 {{\n    font-family: "{}";\n}}'.format(fontTitle)

    katex_css = "\n.katex {{font-size: 1.0em !important; line-height: {}}}\n".format(lineSpacing)
    
    if fontMath in ('katex', 'tex', 'latex'):
        fontMath = 'KaTeX_Main'
    
    if fontName in ('katex', 'tex', 'latex'):
        fontName = 'KaTeX_Main'
    
    if fontMath:
        katex_css = "".join((
            katex_css,
            '\n.katex .mathnormal,.mathit, .mathbf, .boldsymbol, .mathsf, .textsf, .textbf, .mathbf, .textrm, .mainrm  {{ font-family: "{}"; }}\n'.format(fontMath)
            ))
    if fontName:
        katex_css = "".join((
            katex_css, 
            '\n.katex .mathrm, .text {{font-family: "{}";}}\n'.format(fontName)
            ))
    
    print_css = (
        body
        + katex_css
        + """


@media print {


html {
    height: 100%;
    counter-reset: num;
}

body {
    height: 100%;
    padding: 0;
    margin: 0;
    position: relative;
    text-align: justify;
    text-justify: inter-word;
    hyphens: auto;
    font-variant-ligatures: normal; 
}

page {
    position: static;
    box-sizing: border-box;
    height: 100%;
    width: 100%;
    min-height: 100%;
    max-height: 100%;
    display: grid;
    grid-template-rows: auto 1fr auto auto;
    grid-template-columns: 100%;
    grid-gap: 0;
    page-break-inside: avoid !important;
    margin: 0;
    padding: 0;
    overflow: hidden;
}

pagebreak {
    page-break-after: always;
}

header {
    padding: 0;
    margin: 0;
    page-break-inside: avoid !important;
}

header :first-child :lastchild {
    padding: 0;
    margin: 0;
}

header > figure {
    padding: 0;
    margin: 0;
}

bottom * {
    padding: 0;
    margin: 0;
    page-break-inside: avoid !important;
}

bottom :first-child :lastchild {
    padding: 0;
    margin: 0;
}

bottom > figure, img, figcaption {
    padding: 0;
    margin: 0;
}

bottom p, h1, h2, h3, h4, h5, h6, img, figure, figcaption {
    margin-top: 0
}

pagecontent {
    margin: 0;
    padding: 0;
    width: 100%;
    page-break-inside: avoid !important;
    max-height: 100%;
    overflow: hidden;
}

pagecontent h1:first-child, h2:first-child,
    h3:first-child, h4:first-child, h5:first-child,
    h6:first-child, p:first-child,
    box:first-child, wbox:first-child {
    margin-top: 0;
    padding-top: 0;
}

footer {
    margin: 0;
    padding: 0;
    font-size: 70%;
}


header p, h1, h2, h3, h4, h5, h6 {
    margin-top: 0
}


.math {
    page-break-inside: avoid !important;
    white-space: nowrap;
}


p .katex-display {
    padding-top: 0.5em;
    padding-bottom: 0.5em;
}


hr.solid {
    display: block;
    border: none;
    margin: 1em auto 1em auto;
    border-top: 1px solid black;
}

hr.dashed {
    display: block;
    border: none;
    margin: 1em auto 1em auto;
    border-top: 1px dashed black;
}

hr.dotted {
    display: block;
    border: none;
    margin: 1em auto 1em auto;
    border-top: 1px dotted black;
}

.linefillsolid {
    display: flex;
    align-items: center;
    justify-content: center;
}

.linefillsolid:after {
    margin-left: 0.5em;
    border-top: 1px solid;
    flex: 1;
    content: '';
}

.linefilldashed {
    display: flex;
    align-items: center;
    justify-content: center;
}

.linefilldashed:after {
    margin-left: 0.5em;
    border-top: 1px dashed;
    flex: 1;
    content: '';
}

.linefilldotted {
    display: flex;
    align-items: center;
    justify-content: center;
}

.linefilldotted:after {
    margin-left: 0.5em;
    border-top: 1px dotted;
    flex: 1;
    content: '';
}

.mermaid {
    text-align: center;
}

.katex-display {
    page-break-inside: avoid !important;
}


.katex {
    page-break-inside: avoid !important;
    padding-left: 0.1em;
    padding-right: 0.1em;
}

.vcenter {
  width: 100%;
  position: relative;
  top: 50%;
  -ms-transform: translateX(0%) translateY(-50%);
  -webkit-transform: translate(0%,-50%);
  transform: translate(0%,-50%);
  break-inside: avoid-column;
  display: block;
}

.vhcenter {
    position: relative;
    width: 100%;
    top: 50%;
    text-align: center;
    -moz-transform: translateX(0%) translateY(-50%);
    -webkit-transform: translateX(0%) translateY(-50%);
    transform: translateX(0%) translateY(-50%);
    break-inside: avoid-column;
}

.wboxtitle {
display: block;
box-sizing: border-box;
background-color: dimgray;
font-weight: bold;
border-radius: 1px 1px 0px 0px;
margin-left: -.5em;
margin-right: -.5em;
margin-bottom: 0.5em;
color: white;
padding-bottom: .2em;
padding-top: 0.2em;
padding-left: .5em;
}

.wbox {
clear: both;
box-sizing: border-box;
display: table;
table-layout: fixed;
page-break-inside: avoid !important;
border: 1.5px solid dimgray;
border-radius: 3px;
padding-left: .5em;
padding-right: .5em;
padding-bottom: 0em;
margin-bottom: 1.5em;
margin-top: 1.5em;
margin-left: auto;
margin-right: auto;
break-inside: avoid;
}

.boxtitle {
  display: table;
  border-radius: 3px;
  background-color: #FFF;
  text-align: center;
  position: relative;
  z-index: 1000;
  margin: 0 auto;
  margin-top: -3.5ex;
  padding-top: 0.5ex;
  padding-bottom: 0em;
  padding-left: 1em;
  padding-right: 1em;
}

.box {
    box-sizing: border-box;
    display: table;
    border: 1.5px solid black;
    border-radius: 3px;
    padding-left: 0.5em;
    padding-right: 0.5em;
    padding-top: 0.3em;
    padding-bottom: 0em;
    margin-bottom: 15px;
    margin-left: auto;
    margin-right: auto;
    page-break-inside: avoid !important;
    break-inside: avoid;
}

.multicolumn {
    margin-top: 1ex;
    margin-bottom: 1ex;
    padding-top: 0;
    padding-bottom: 0;
}

.multicolumn p:first-child {
    margin-top: 0;
}

.multicolumn p {
    clear: both;
    page-break-inside: avoid;
}

.boxtitle p {
    margin-top: 0;
}

.box p:last-child {
    margin-bottom: 0.2em;
    padding-bottom: 0;
}

.box ol:first-child, .box ul:first-child {
    margin-top: 1em;
    padding-top: 0;
}

.box ol:last-child, .box ul:last-child {
    margin-bottom: 1em;
    padding-bottom: 0;
}

.lettrine {
    /*clear: both;*/
    display: inline-block;
    float: left;
    font-size: 3em;
    margin: 0;
    margin-right: 5px;
    margin-bottom: -1ex;
    margin-top: -0.5ex;
    padding: 0;
}

@page rotated {
    size: landscape;
    }

@page normal {
    size: portrait;
    }

@page {
    size: portrait;
    }

pre, blockquote {
        page-break-inside: avoid;
    }
}
"""
    ).replace("overflow: hidden;", "overflow: visible;" if not newpages else "overflow: hidden;")

    if layout == "landscape":
        print_css = print_css.replace(
            "@page {\n    size: portrait;\n    }",
            "@page {\n    size: landscape;\n    }",
        )

    code = """
<!DOCTYPE html>
<html lang="fr" xml:lang="fr">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible">
    <link rel="stylesheet" href="{0}/bootstrap/css/bootstrap.css">
    <script src="{0}/bootstrap/js/bootstrap.bundle.min.js"></script>
    <link type="text/css" rel="stylesheet" href="{1}"/>
    <link rel="stylesheet" href="{0}/fontawesome/css/font-awesome.min.css"/>
    <style>
body {{
    font-size: {2}pt !important;
    {3}
    line-height: {4};
    word-break: break-word;
    hyphens: auto;
    font-variant-ligatures: none;
}}

{5}

{6}
{7}
@font-face {{
    font-family: "fontawesome";
    src: url("{8}/fontawesome/fonts/fontawesome-webfont.ttf");
}}

    </style>
</head>
<body>

{9}
</body>
</html>
""".format(
        APP_PATH, css_file, fontSize, fontDef, lineSpacing, fontTitles, bqcolor, print_css, APP_PATH, code
    )

    return code, bqrgbcolor
