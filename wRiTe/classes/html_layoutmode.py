# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re


def html_layoutmode(code, num):
    index = 0

    for i in range(num):
        m = re.compile("^<p>!(portrait|landscape)</p>", re.MULTILINE)
        s = m.search(code, index)

        if s:
            mode = s.group(1)
            index = s.end()
            modes = {"portrait": "normal", "landscape": "rotated"}
            code = "".join((code[: s.start()], code[s.end():]))
            pagebeg = code.rfind('<page style="', 0, s.end())
            code = "".join(
                (
                    code[: pagebeg + 13],
                    "page: {}; ".format(modes[mode]),
                    code[pagebeg + 13:],
                )
            )

    return code
