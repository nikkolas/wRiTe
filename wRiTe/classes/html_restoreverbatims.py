# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.


def html_restoreverbatims(code, verbatims, inlineverbatims):
    for m in verbatims:
        content = m[1]
        if m[0]:
            content = content.replace(m[0], "")

        if content[0] == "\n":
            content = content[1:]

        if m[0]:
            code = code.replace(
                "@@@VERBATIM@@@",
                '<pre><code class="{}">{}</code></pre>'.format(m[0], content),
                1,
            )
        else:
            code = code.replace(
                "@@@VERBATIM@@@", "<pre><code>{}</code></pre>".format(content), 1
            )

    #  Restore inline verbatims
    for m in inlineverbatims:
        code = code.replace("@@@INLINEVERBATIM@@@", "<code>{}</code>".format(m), 1)

    return code
