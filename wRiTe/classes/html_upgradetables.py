# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
from classes.colors import colors


def html_upgradetables(code, numoftables):
    index = 0
    
    for t in range(numoftables):
        try:
            bordercolor = 'black'
            bordersize = '1.5px'
            lencode = len(code)
            tbeg = code.find("<table", index)
            tend = code.find("</table>", tbeg)
            captionbeg = code.find("caption", tbeg, tend)
            captionend = code.find("</caption>", captionbeg, tend)

            if (captionbeg >= 0) and (captionend >= 0):
                captionbeg = code.find(">", captionbeg, captionend)
                captopts = code[captionbeg + 1: captionend].split("/wRiTeTableOpts/")
                caption = captopts[0]
                opts = ""

                if len(captopts) == 2:
                    opts = captopts[1]

                code = "".join((code[: captionbeg + 1], caption, code[captionend:]))

                if opts:
                    opts = re.sub(r"\s+", " ", opts.strip()).split(" ")
                    opts = [e for e in opts if e]
                    talign = ""
                    twidth = ""
                    for opt in opts:
                        if '@' in opt[0]:
                            if opt[1:] in colors:
                                bordercolor = opt[1:]
                        elif '#' in opt[0]:
                            bordercolor = opt
                        elif opt in ('left', 'right'):
                            talign = "float: {}; ".format(opt)
                        elif opt[-2:] in 'px':
                            bordersize = opt
                        else:
                            twidth = "width: calc({} - 2em); ".format(opt)

                    style = '<table style="{} {}" '.format(talign, twidth)
                    code = "".join((code[:tbeg], style, code[tbeg + 6:]))
                    tend = code.find("</table>", tbeg)

            #hascolor = re.findall(r"(@|#)[a-z\d]+", code[tbeg:tend])
            hascolor = re.findall(r"(?<=[({\s|])(@[a-z]++|#[a-fA-F\d]{6})(?=[\s|)}])", code[tbeg:tend])
            hasrowspan = [e for e in re.findall(r"[\^]{2}", code[tbeg:tend]) if e]

            if hascolor or hasrowspan:
                colgroupbeg = code.find("<colgroup>", tbeg, tend)
                colgroupend = code.find("</colgroup>", colgroupbeg, tend)
                theadbeg = code.find("<thead>", tbeg, tend) + 1
                theadend = code.find("</thead>", theadbeg, tend)
                tbodybeg = code.find("<tbody>", tbeg)
                tbodyend = code.find("</tbody>", tbodybeg)
                numofcols = code[tbeg:tend].count("<col ")
                numofrows = code[tbodybeg:tend].count("<tr")
                trindex = tbodybeg
                table = [[None] * numofcols for _ in range(numofrows)]
                activatedcols = [[0, 0] for i in range(numofcols)]
                colcolors = [""] * numofcols
                colgroup = ""

                #  Apply column colors
                thead = code[theadbeg + 7: theadend]

                if thead:
                    nthead = "\n<tr>\n"

                    if "@" in thead or '#' in thead:
                        totalcontent = ""
                        ths = list(re.finditer(r"<th\s*[^<]+</th>", thead))
                        ith = 0

                        for th in range(len(ths)):
                            hcell = ths[th].group(0)

                            if "<th>" in hcell:
                                hcell = hcell.replace(
                                    "<th>", '<th style="text-align:left;">'
                                )
                            color = re.search(r"(?<=[({\s|])(@[a-z]++|#[a-fA-F\d]{6})(?=[\s|)}])", hcell)
                            if color:
                                color = color.group(0).replace('@', '')
                                
                                if color in colors or \
                                        (("#" in color) and (len(color) == 7)):
                                    colcolors[ith] = colors[color] if color in colors else color
                                    hcell = re.sub('(@|#)[a-zA-Z\d]+', '', hcell)

                                if "colspan" in hcell:
                                    hcellcolspan = (
                                        int(
                                            re.sub(
                                                r'.+colspan="(\d+)".+>', r"\1", hcell
                                            )
                                        )
                                        - 1
                                    )

                                    for i in range(hcellcolspan):
                                        colcolors[ith + i + 1] = color
                                        ith += 1

                            ith += 1
                            nthead = "".join((nthead, "\n", hcell))
                            totalcontent += re.sub(".+>([^<]*)</th>", r"\1", hcell)
                        thead = "".join((nthead, "\n</tr>\n"))

                        if not totalcontent:
                            thead = ""

                    #  Apply to all columns
                    colgroup = code[colgroupbeg + 11: colgroupend]
                    colindex = 0

                    for col in range(colgroup.count("<col")):
                        colcellbeg = colgroup.find("<col", colindex)
                        colcellend = colgroup.find("/>", colcellbeg)
                        colindex = colcellend
                        colcell = colgroup[colcellbeg: colcellend + 2]

                        if "<col />" in colcell:
                            colcell = colcell.replace(
                                "<col />", '<col style="text-align:left;">'
                            )

                        if colcolors[col]:

                            if "style" in colcell:
                                colcell = colcell.replace(
                                    '<col style="',
                                    '<col style="background-color: {}; '.format(
                                        colcolors[col]
                                    ),
                                )
                            else:
                                colcell = colcell.replace(
                                    "<col",
                                    '<col style="background-color: {}; '.format(
                                        colcolors[col]
                                    ),
                                )

                            colgroup = "".join(
                                (
                                    colgroup[:colcellbeg],
                                    colcell,
                                    colgroup[colcellend + 2:],
                                )
                            )

                tablebody = ""

                #  Create a virtual table with initial table content + rowspan
                for tr in range(numofrows):
                    trbeg = code.find("<tr", trindex, tend)
                    trend = code.find("</tr>", trbeg, tend)
                    tdindex = trbeg
                    trindex = trend
                    colspan = 0
                    wlcolor = None

                    for td in range(numofcols):

                        if colspan:
                            colspan -= 1

                        else:
                            tdbeg = code.find("<td", tdindex, trend)
                            tdend = code.find("</td>", tdbeg, trend)

                            if tdbeg >= 0 and tdend >= 0:
                                tdindex = tdend
                                cellcontent = code[tdbeg: tdend + 5]
                        
                                if "<td>" in cellcontent:
                                    cellcontent = cellcontent.replace(
                                        "<td>", '<td style="text-align:left;">'
                                    )

                                #  Color whole line
                                if ("@@" in cellcontent or "@#" in cellcontent) and not td:
                                    wlcolor = re.search(r"(?<=[({\s|])(@[a-z]++|#[a-fA-F\d]{6})(?=[\s|)}])", cellcontent)
                                    if wlcolor:
                                        wlcolor = wlcolor.group(0).replace('@', '')

                                        if wlcolor in colors \
                                                or (("#" in wlcolor) and (len(wlcolor) == 7)):
                                            cellcontent = re.sub(r"(?<=[({\s|])(@[a-z]++|#[a-fA-F\d]{6})(?=[\s|)}])", "", cellcontent)
                                        else:
                                            wlcolor = None
                                
                                if wlcolor:
                                    wlcolor = colors[wlcolor] if wlcolor in colors else wlcolor
                                    
                                    if "style" in cellcontent:
                                        cellcontent = cellcontent.replace(
                                            '<td style="',
                                            '<td style="background-color:{}; '.format(
                                                wlcolor
                                            ),
                                        )
                                    else:
                                        cellcontent = cellcontent.replace(
                                            "<td",
                                            '<td style="background-color:{};"'.format(
                                                wlcolor
                                            ),
                                        )
                                #  Manage color
                                
                                if ("@" in cellcontent or '#' in cellcontent):
                                    cellcolor = re.search(r"(?<=[\s>])(@[a-z]++|#[a-fA-F\d]{6})(?=[\s<])", cellcontent)

                                    if cellcolor:
                                        cellcolor = cellcolor.group(0).replace('@', '')
                                        if cellcolor in colors or len(cellcolor) == 7: #re.search(r"#[\da-fA-F]{6}", cellcolor):
                                            cellcontent = re.sub(r"(?<![a-z:])(@[a-z]++|#[a-fA-F\d]{6})(?![.;])", "", cellcontent)
                                            if cellcolor in colors:
                                                cellcolor = colors[cellcolor]
                                            if not wlcolor:
                                                if "style" in cellcontent:
                                                    cellcontent = cellcontent.replace(
                                                        '<td style="',
                                                        '<td style="background-color:{}; '.format(
                                                            cellcolor
                                                        ),
                                                    )
                                                else:
                                                    cellcontent = cellcontent.replace(
                                                        "<td",
                                                        '<td style="background-color:{};"'.format(
                                                            cellcolor
                                                        ),
                                                    )

                                if "^^" in cellcontent:
                                    cellcontent = cellcontent.replace("^^", "")

                                    if not activatedcols[td][0]:
                                        activatedcols[td][0] = 1
                                        activatedcols[td][1] += 1
                                        cellcontent = cellcontent.replace(
                                            "<td", '<td rowspan="1" '
                                        )

                                    else:
                                        nrs = int(
                                            re.sub(
                                                r'<td\s*rowspan="(\d+)".+',
                                                r"\1",
                                                table[tr - activatedcols[td][1]][td],
                                            )
                                        )
                                        if table[tr - activatedcols[td][1]][td]:
                                            table[tr - activatedcols[td][1]][
                                                td
                                            ] = table[tr - activatedcols[td][1]][
                                                td
                                            ].replace(
                                                'rowspan="{}"'.format(nrs),
                                                'rowspan="{}"'.format(nrs + 1),
                                            )
                                        else:
                                            table[tr - activatedcols[td][1]][
                                                td
                                            ] = table[tr - activatedcols[td][1]][td]
                                        activatedcols[td][1] += 1
                                        cellcontent = ""

                                else:
                                    activatedcols[td] = [0, 0]

                                table[tr][td] = cellcontent

                                if "colspan" in cellcontent:
                                    colspan = (
                                        int(
                                            re.sub(
                                                r'.+colspan="(\d+)".+>',
                                                r"\1",
                                                cellcontent,
                                            )
                                        )
                                        - 1
                                    )

                #  Build table body from virtual table

                for tr in table:
                    tablebody = "".join((tablebody, "\n<tr>\n    "))
                    for td in tr:
                        if td:
                            tablebody = "".join((tablebody, td, "\n    "))
                    tablebody = "".join((tablebody, "\n</tr>\n"))

                code = "".join(
                    (
                        code[: colgroupbeg + 11],
                        colgroup,
                        code[colgroupend: theadbeg + 7],
                        thead,
                        code[theadend: tbodybeg + 8],
                        tablebody,
                        code[tbodyend:],
                    )
                )
                
            tend = code.find("</table>", tbeg)
            alltable = code[tbeg:tend+8]
            if bordercolor != 'black' or bordersize != '1.5px':
                alltable = re.sub(r'<td (.*)style="', r'<td \1 style="border: {} solid {}; '.format(bordersize, bordercolor), alltable)
                alltable = re.sub(r'<th style="', '<th style="border: {} solid {}; '.format(bordersize, bordercolor), alltable)
                alltable = re.sub(r'<td>', '<td style="border: {} solid {};">'.format(bordersize, bordercolor), alltable)
                alltable = re.sub(r'<th>', '<th style="border: {} solid {};">'.format(bordersize, bordercolor), alltable)
            code = "".join((code[:tbeg], alltable, code[tend+8:]))
            index = tbeg + len(alltable)
        except Exception:
            print("Error with a table n°", t + 1)

    #  Center tables by default
    code = code.replace("<table", '<div align="center">\n<table')
    code = code.replace("</table>", "</table>\n</div>")
    return code
