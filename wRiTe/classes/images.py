# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
import os

try:
    import fitz
    from fitz.utils import getColor
except ModuleNotFoundError:
    print("pymupdf not installed")

try:
    import numpy as np
except ModuleNotFoundError:
    print("numpy not installed")

from classes.utils import findBalanced


def images(code, path, data_path, num, TMP_DIR):
    index = 0
    img_bsopts = []
    videofiles = []

    for i in range(num):
        starttime = "00:00:00"
        videolink = False
        videofile = ""
        iopts = {}
        width, height, align, rotate, corner, border, shadow = (
            "auto",
            "auto",
            "",
            "",
            "",
            "",
            "",
        )
        start = code.find("![", index)
        imgstart = code.find("](", start) + 2
        imgstop = findBalanced("(", code, imgstart)
        image = code[imgstart:imgstop]

        if image.strip():
            m = re.search(r".+\.(?:jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF|svg|SVG|bmp|BMP|tiff|TIFF|mp4|MP4|avi|AVI|mkv|MKV|mpeg|MPEG|mov|MOV)", image)

            if m:
                rotation = ""
                img = image[: m.end()]

                if "http" not in img:
                    img = img.replace("%20", " ")

                    if not os.path.exists(img) or "/" not in img:
                        img = os.path.join(data_path, os.path.basename(img))
                    else:
                        img = img.replace(" ", "%20")

                        if img:
                            if "/" not in img[0]:
                                img = os.path.join(path, img)
                else:
                    img = img.replace("(", "%28")
                    img = img.replace(")", "%29")

                opts = image[m.end():].strip()
                ext = img.split(".")[-1]

                if ext.lower() in ["mp4", "avi", "mkv", "mpeg", "mov"]:
                    match = re.search(r"\d\d:\d\d:\d\d", opts)

                    if match:
                        starttime = match.group(0)
                        opts = "".join((opts[: match.start()], opts[match.end():])).strip()

                    if "link" in opts:
                        videolink = True
                        opts = opts.replace("link", "").strip()

                    videofile = img
                    data_dir = data_path.split('/')[-1]
                    
                    if data_dir in videofile:
                        videofile = "".join((data_dir, "/", videofile.split('/')[-1]))

                    videofiles.append(videofile)
                    img = "".join((img[: -len(ext)], "jpg"))
                    img = os.path.join(
                        TMP_DIR,
                        "wRiTe_images",
                        "".join((starttime, "_", os.path.basename(img))),
                    )

                    if not os.path.isdir(os.path.join(TMP_DIR, "wRiTe_images")):
                        os.makedirs(
                            os.path.join(TMP_DIR, "wRiTe_images"), exist_ok=True
                        )

                    if not os.path.isfile(img):
                        try:
                            os.system(
                                    (
                                        "ffmpeg -hide_banner -loglevel quiet"
                                    " -y -ss {} -i {} -frames:v 1 {}"
                                ).format(
                                    starttime, videofile, img
                                )
                            )
                        except Exception:
                            print("Warning: ffmpeg not installed on your system !")

                if opts:
                    opts = re.sub(r"\s+", " ", opts).split(" ")

                    for opt in opts:

                        if opt in ("right", "left"):
                            align = opt
                            continue

                        if "border" in opt:
                            border = "border: 1px solid black;"
                            continue

                        if "shadow" in opt:
                            shadow = "box-shadow: rgba(0, 0, 0, 0.15) 5px 5px 2.6px;"
                            continue

                        if opt[0] == "w":
                            width = opt[1:]
                            continue

                        if opt[0] == "h":
                            height = opt[1:]
                            continue

                        if opt[0] == "r":
                            rotation = opt[1:]
                            continue

                        if opt[0] == "c":
                            corner = "border-radius: {};".format(opt[1:])
                            continue

                        match = re.search(r"^(\d+\.*\d*)[a-z%]{1,2}", opt)

                        if match:
                            width = match.group(0)

                    if rotation:

                        if "http" not in img:
                            IMG = None

                            if ".svg" in img.lower():
                                IMG = fitz.open(img)
                                IMG = IMG.get_page_pixmap(0)
                            else:
                                IMG = fitz.Pixmap(img)

                            if "auto" in width:
                                width = "{}px".format(IMG.width)
                            elif width.isdigit():
                                width = "{}px".format(width)

                            if (rotation == "90" or rotation == "-270") and not align:
                                rwidth = re.sub(r"[%a-z]+", "", width)
                                rheight = re.sub(r"[%a-z]+", "", height)
                                rotate = (
                                    "transform: rotate({}deg) translateY(-100%);"
                                    " transform-origin: top left; margin-left: "
                                    "calc(50% - {} * {} / {} / 2); "
                                    "margin-bottom: calc({} - {} * {} / {}); max-width: {}%;"
                                    ).format(
                                    rotation,
                                    width,
                                    rheight,#
                                    rwidth,#
                                    width,
                                    width,
                                    IMG.height,
                                    IMG.width,
                                    int(IMG.width/IMG.height*100)
                                )

                            elif (rotation == "-90" or rotation == "270") and not align:
                                rotate = (
                                    "transform: rotate({}deg) ; transform-origin: top right;"
                                    " margin-left: calc(0% - {} + 50% - {} * {} / {} / 2);"
                                    " margin-bottom: calc({} - {} * {} / {}); max-width: {}%;"
                                    ).format(
                                    rotation,
                                    width,
                                    width,
                                    IMG.height,
                                    IMG.width,
                                    width,
                                    width,
                                    IMG.height,
                                    IMG.width,
                                    int(IMG.width/IMG.height*100)
                                )

                            else:
                                a = float(rotation)
                                if -180 < a < 0:
                                    a = abs(a)
                                if 90 < a < 180:
                                    a = 180 - a
                                if a == 180 or a == -180:
                                    a = 0
                                w = float(re.sub(r"[a-zA-Z%]+", "", width))
                                u = re.sub(r"[\d.]+", "", width)
                                h = w * IMG.height / IMG.width
                                mlr = (
                                    w * (np.cos(np.radians(a)) - 1)
                                    + h * np.sin(np.radians(a))
                                ) / 2
                                mtb = (
                                    h * (np.cos(np.radians(a)) - 1)
                                    + w * np.sin(np.radians(a))
                                ) / 2
                                if "px" in u:
                                    mlr = int(mlr)
                                    mtb = int(mtb)
                                rotate = (
                                    "transform: rotate({0}deg); margin-left:"
                                    " calc({1}{2} + 25px); margin-right:"
                                    " calc({1}{2} + 25px); margin-top: "
                                    "calc({3}{2}); margin-bottom: calc({3}{2});"
                                    ).format(
                                    rotation, mlr, u, mtb
                                )
                if (
                    (rotation != "90" and rotation != "-90")
                    and "\n\n" in code[start - 2: start]
                    and "\n\n" in code[imgstop + 1: imgstop + 3]
                ):
                    centerdiv = '\n\n<div style="text-align: center;">\n\n'
                    code = "".join(
                        (
                            code[:start],
                            centerdiv,
                            code[start: imgstop + 1],
                            "\n\n</div>\n\n",
                            code[imgstop + 1:],
                        )
                    )
                    start += len(centerdiv)
                    imgstart += len(centerdiv)
                    imgstop += len(centerdiv)

                iopts = {
                    "border": border,
                    "shadow": shadow,
                    "rotation": rotate,
                    "corner": corner,
                }

                img_bsopts.append(iopts)

                newimage = '{} width="{}" height="{}" align="{}"'.format(
                    img, width, height, align
                )
                if videolink:
                    code = "".join(
                        (
                            code[:start],
                            "[",
                            code[start:imgstart],
                            newimage,
                            code[imgstop: imgstop + 1],
                            "]",
                            "({})".format(videofile),
                            code[imgstop + 1:],
                        )
                    )
                    index += len(newimage) - len(image) + 4 + len(videofile)
                else:
                    code = "".join((code[:imgstart], newimage, code[imgstop:]))
                    index += len(newimage) - len(image)

            elif "http" in image:
                #  Assume no options if http
                newimage = '{} width="{}" height="{}" align="{}"'.format(
                    image, width, height, align
                )
                code = "".join((code[:imgstart], newimage, code[imgstop:]))
                index += len(newimage) - len(image)

            index = imgstop + 1

    return code, img_bsopts, videofiles

