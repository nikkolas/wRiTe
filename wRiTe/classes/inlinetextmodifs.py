# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
from classes.utils import findBalanced
from classes.colors import colors


def inlinetextmodifs(code, fontSize):
    re_tm = re.compile(r"\(([^\)\(]*)\)\{")
    index = 0
    tm = re_tm.findall(code)

    for i in range(len(tm)):
        m = re_tm.search(code, index)

        if m:
            option = [False] * len(tm)
            startopts = m.start()
            endopts = m.end()
            options = re.sub(r"\s+", " ", m.group(1).strip()).split(" ")
            endcontent = findBalanced("{", code, endopts)

            if all(list(map(lambda x: x > 0, [endopts, endcontent]))) and options:
                content = code[endopts:endcontent]

                if content:

                    if "sc" in options:
                        content = (
                            "<span style='font-variant:small-caps;'>{}</span>".format(
                                content
                            )
                        )
                        option[i] = True

                    if "b" in options:
                        content = (
                            "<span style='display: inline-block;"
                            " margin-left: 0.1em; margin-right: 0.1em;"
                            " border: 1px solid black;"
                            " padding: 1px;'>{}</span>"
                            ).format(
                            content
                        )
                        option[i] = True

                    si = re.compile(r"[+-]\d+")
                    sizeincrease = list(filter(si.match, options))
                    s = re.compile(r"\d+")
                    size = list(filter(s.match, options))

                    if sizeincrease:
                        content = "<span style='font-size: {}pt;'>{}</span>".format(
                            eval("{}{}".format(fontSize, sizeincrease[0])), content
                        )
                        option[i] = True

                    elif size:
                        content = "<span style='font-size: {}pt;'>{}</span>".format(
                            size[0], content
                        )
                        option[i] = True

                    f = re.compile(r"^[a-zA-Z\.]{3,}")
                    font = list(filter(f.match, options))

                    if font:
                        font = font[0].replace(".", " ")
                        content = "<span style='font-family: \"{}\";'>{}</span>".format(
                            font, content
                        )
                        option[i] = True

                    re_mult = re.compile(r"(x\d+|\d+x)")
                    mult = list(filter(re_mult.match, options))
                    if mult:
                        if mult[0]:
                            mult = mult[0].replace('x', '')
                            if mult.isdigit():
                                mult = int(mult)
                                content = "{}".format(content * mult)
                                option[i] = True

                    c = re.compile(r"@(\#[\da-f]{6}|[a-z]+)")
                    color = list(filter(c.match, options))

                    if color:
                        color = color[0][1:]

                        if color in colors:
                            color = colors[color]

                        content = "<span style='color:{};'>{}</span>".format(
                            color, content
                        )
                        option[i] = True

                    align = min(options, key=len)

                    if align in ["c", "r", "l"]:
                        position = {"c": "center", "l": "left", "r": "right"}
                        content = ('<span style="display: block; text-align: {};">'
                                   '{} </span>'
                                   ).format(position[align], content)
                        option[i] = True

                    #  without option -- allow markdown in part of a word
                    if "" in options:
                        option[i] = True

                    if any(option):
                        code = "".join(
                            (code[:startopts], content, code[endcontent + 1:])
                        )
                    index = endopts + 1
    return code
