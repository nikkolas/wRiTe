# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.


def latexspaces(code, num):
    index = 0

    if num:

        for i in range(num):
            find = code.find
            startBlock = find("$$", index)
            endBlock = find("$$", startBlock + 2)
            content = "".join(
                ("$$", code[startBlock + 2: endBlock].replace("\n", "").strip(), "$$")
            )
            code = "".join((code[:startBlock], content, code[endBlock + 2:]))
            index = startBlock + len(content)

    return code
