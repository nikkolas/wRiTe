# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

try:
    from subprocess import run
except ModuleNotFoundError:
    print("module subprocess not installed")

try:
    from shutil import which
except ModuleNotFoundError:
    print("module shutil not found")


def mmdexporthtml(code, lang):

    if not which("multimarkdown"):
        print("Warning: multimarkdown not installed")
        return ""

    try:
        output = run(
            ["multimarkdown", "-s", "--unique", "--lang={}".format(lang)],
            input=bytes(code, "utf8"),
            capture_output=True,
        )

        if not output.returncode:
            code = format(output.stdout.decode("utf8"))

        else:
            code = "MULTIMARKDOWN ERROR!"
    except Exception:
        print("FATAL -- HTML Conversion Error!")
        return

    return code.rstrip()
