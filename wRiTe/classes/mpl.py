# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
from numpy import log10
from os import makedirs, path
from functools import partial
from classes.cmplfuncs import matplotlibToSvg


def mpl(code, mdfile, APP_PATH, data_path, TMP_DIR, firstload):

    def graphcontent(
            match, code=code, mdfile=mdfile, APP_PATH=APP_PATH,
            data_path=data_path, TMP_DIR=TMP_DIR, firstload=firstload,
            matplotlibToSvg=matplotlibToSvg, path=path, makedirs=makedirs
            ):
        mode = 'g'
        options = ""
        export = False
        legend = match.group(1)
        opts = match.group(2).strip()

        if not opts and legend[:1] in "(":
            opts = legend[1:-1]
            legend = ""

        opts = re.sub(r"\s+", " ", opts)
        content = match.group(3)
        contentnumbered = content

        if not content:
            return match.group(0)

        listopts = opts.split(" ")

        for opt in listopts:
            if opt not in ("#", "s", "g", "gs", "sg", "left", "right",
                           "shadow", "border", "export"):
                if not re.findall(r"[-\d%pxrcm.]+", opt):
                    listopts.remove(opt)
                    continue
            if opt in "export":
                export = True
            if opt in ("s", "g", "gs", "sg"):
                mode = opt
                listopts.remove(mode)

        imgfile = "".join((
            "image_{}_".format(hash(content)),
            path.basename(mdfile).split(".")[0],
            ".svg"
            ))
        imgfile = path.join(TMP_DIR, "wRiTe_images", imgfile)

        if listopts:
            options = "".join((" {} ".format(str(item)) for item in listopts))
            options = options.strip()

        image = "![{}]({} {})".format(legend, imgfile, options)

        if "s" in mode:
            if "#" in listopts:
                tab = content.strip().splitlines()
                contentnumbered = "\n"
                lt = int(log10(len(tab))) + 1
                for k, l in enumerate(tab):
                    line = "".join(("{}{}: ".format(
                        (lt - int(log10(k+1))-1)*" ",
                        k+1
                        ), l, "\n"))
                    contentnumbered = "".join((contentnumbered, line))

        if mode in ("gs", "sg"):
            makedirs(path.join(TMP_DIR, "wRiTe_images"), exist_ok=True)
            if not path.exists(imgfile) or not firstload:
                matplotlibToSvg(content, imgfile, APP_PATH, data_path, export, firstload)
            content = "".join(("```python", contentnumbered, "```\n\n", image, "\n"))

        elif mode in "g":
            makedirs(path.join(TMP_DIR, "wRiTe_images"), exist_ok=True)
            if not path.exists(imgfile) or not firstload:
                matplotlibToSvg(content, imgfile, APP_PATH, data_path, export, firstload)
            content = "".join((image, "\n"))

        elif mode in "s":
            content = "".join(("```python", contentnumbered, "```\n\n"))
        return content

    return re.sub(
        r"^!graph\[*([^\]\n]*)\]*\(*([^)\n]*)\)*(.+?)^\.graph",
        partial(graphcontent, code=code, mdfile=mdfile, APP_PATH=APP_PATH,
                data_path=data_path, TMP_DIR=TMP_DIR, firstload=firstload,
                matplotlibToSvg=matplotlibToSvg, path=path, makedirs=makedirs),
        code,
        flags=re.DOTALL | re.MULTILINE
        )
