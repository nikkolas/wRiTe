# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import fitz
import numpy as np


class muPDF:
    def __init__(self, page, cm):
        self.page = page
        self.cm = cm

    def draw_arrow(self, p1, p2, width=0.7, ha=15, color=(0,0,0), hs=1, **kwargs):
        #  Draw arrow
        x0, y0 = p1
        x1, y1 = p2
        #  Head angle
        theta = ha/180*np.pi
        al = ((x1-x0)**2+(y1-y0)**2)**0.5
        hl = (al/20*hs) / np.cos(theta)
        alpha = np.arccos((x1-x0)/al)
        if y1 > y0:
            alpha = -alpha
        yh1 = y1 + hl*np.sin(alpha-theta)
        xh1 = x1 - hl*np.cos(alpha-theta)
        yh2 = y1 + hl*np.sin(alpha+theta)
        xh2 = x1 - hl*np.cos(alpha+theta)
        xh3 = x1 - hl*np.cos(alpha)
        yh3 = y1 + hl*np.sin(alpha)
        xm = (xh3+x1)/2
        ym = (yh3+y1)/2
        shape = self.page.new_shape()
        shape.draw_line((x0,y0), (x1,y1))
        shape.finish(width=width, color=color, lineJoin=1, **kwargs)
        shape.draw_curve((x1,y1), (xm, ym), (xh1, yh1))
        shape.finish(width=width*1.2, color=color, lineJoin=1, closePath=False,)
        shape.draw_curve((x1,y1), (xm, ym), (xh2, yh2))
        shape.finish(width=width*1.2, color=color, lineJoin=1, closePath=False,)
        shape.commit()


    def draw_milli(self, mrect, margin=0, color=(0.9, 0.8, 0.7), unit='px'):
        cm = self.cm
        colorcm = tuple(c-0.1 if c-0.1>=0 else 0 for c in color)
        mrect = mrect[0], mrect[1], mrect[2] + mrect[0], mrect[3] + mrect[1]
        if not unit:
            unit='px'
        if unit in 'cm':
            mrect = tuple(e*cm for e in mrect)
            margin = margin * cm
        elif unit in 'mm':
            margin = margin / 10 * cm
            mrect = tuple(e/10*cm for e in mrect)
        else:
            unit = 'px'
        if isinstance(mrect, tuple) or isinstance(mrect, list):
            mrect = fitz.Rect(mrect) - fitz.Rect(-margin, -margin, margin, margin)
            #  Hack: Enough room because calcs not rounded 
            if unit in ('cm', 'mm'):
                mrect = mrect + fitz.Rect(0, 0, 0.1*cm, 0.1*cm)
            height = mrect.tl.distance_to(mrect.bl, 'cm')
            width = mrect.tl.distance_to(mrect.tr, 'cm')
            deltaw = (width - int(width)) / 2 * cm
            deltah = (height - int(height)) / 2 * cm
            mrect = mrect - fitz.Rect(-deltaw, -deltah, deltaw, deltah)

        for i in range(int(width)*10):
            self.page.draw_line(
                    (mrect.tl.x+i*cm/10, mrect.tl.y), 
                    (mrect.tl.x+i*cm/10, mrect.tl.y+int(height)*cm),
                    color=color,
                    width=0.5
                    )
        for i in range(int(height)*10):
            self.page.draw_line(
                    (mrect.tl.x, mrect.tl.y+i*cm/10), 
                    (mrect.tl.x+int(width)*cm, mrect.tl.y+i*cm/10),
                    color=color,
                    width=0.5
                    )
        for i in range(int(width)+1):
            self.page.draw_line(
                    (mrect.tl.x+i*cm, mrect.tl.y), 
                    (mrect.tl.x+i*cm, mrect.tl.y+int(height)*cm),
                    width=1.2,
                    color=colorcm,
                    lineJoin=1,
                    )
        for i in range(int(height)+1):
            self.page.draw_line(
                    (mrect.tl.x-0.6, mrect.tl.y+i*cm), 
                    (mrect.tl.x+int(width)*cm+0.6, mrect.tl.y+i*cm),
                    width=1.2,
                    color=colorcm,
                    lineJoin=1,
                    )

    def show_grid(self):
        pw, ph = self.page.rect.width, self.page.rect.height
        for i in range(0, int(pw), 10):
            self.page.draw_line((i, 0), (i,ph), color=(.8,.8,.8))
        for i in range(0, int(ph), 10):
            self.page.draw_line((0, i), (pw, i), color=(.8,.8,.8))
        for i in range(0, int(pw), 100):
            self.page.draw_line((i, 0), (i,ph), color=(.6,.6,.6))
            self.page.insert_text((i,0), str(i), rotate=-90)
            self.page.insert_text((i,ph-20), str(i), rotate=-90)
        for i in range(0, int(ph), 100):
            self.page.draw_line((0, i), (pw, i), color=(.6,.6,.6))
            self.page.insert_text((0,i), str(i))
            self.page.insert_text((pw-20,i), str(i))

