# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re


def mcopt(match):
    line = 0
    h = 0
    opts = match.group(2)

    if opts:
        opts = re.sub(r"\s+", " ", opts.strip()).split(" ")
        opts.sort()

        if "line" in opts:
            line = 1
            opts.remove("line")

        if opts:
            h = opts[0]
    if h:
        return (
            '<div class="multicolumn" style="column-count:'
            ' {0}; column-fill: auto; height: {1};'
            ' column-rule: {2}px solid black; '
            'column-gap: 6%; font-size:100%;">\n\n {3}\n\n</div>'
            ).format(
            match.group(1), h, line, match.group(3)
        )

    return ('<div class="multicolumn" style="column-count: {0};'
            ' column-fill: balance; column-rule: {1}px solid black;'
            ' column-gap: 6%; font-size: 100%;">\n\n {2}\n\n</div>').format(
        match.group(1), line, match.group(3)
    )


def multicolumn(code):
    return re.sub(
        r"!mc(\d+)\(*([^)]*?)\)*\s*$(.+?)^\.mc\d*",
        mcopt,
        code,
        flags=re.DOTALL | re.MULTILINE
        )
