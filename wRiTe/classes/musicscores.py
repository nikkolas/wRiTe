# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re


def musicscores(code, layout, pagesize):
    index = 0

    for music in range(code.count("!gtab")):
        opts = []
        width = 1
        zoom = 1.0
        notes = "true"
        tablature = "true"
        start = code.find("!gtab", index)
        end = code.find("\n", start)
        endmusic = code.find(".gtab", start + 5)
        startopts = code.find("(", start + 5, end)

        if startopts > 0:
            endopts = code.find(")", startopts, end)

            if endopts > 0:
                opts = re.sub(
                    r"\s+", " ", code[startopts + 1: endopts].strip()
                    ).split(" ")

                if "tab" in opts:
                    notes = "false"
                    opts.remove("tab")

                if "notes" in opts:
                    tablature = "false"
                    opts.remove("notes")

                if notes == tablature:
                    notes, tablature = "true", "true"
                opts.sort(key=lambda item: "%" in item)
                endopts += 1
        else:
            endopts = start + 5

        music = code[endopts:endmusic]
        music = music.replace(
            "\n\n",
            "\n\noptions space=80\ntabstave notation={} tablature={}\n".format(
                notes, tablature
            ),
        )

        # Verify if custom mode
        startOfPage = code.rfind("<pagecontent>", 0, start)
        startMode = code.find("!mode(", startOfPage, start)

        if startMode > 0:
            endMode = code.find("\n", startMode + 6, start)
            mode = code[startMode + 6: endMode].split(")")[0].strip()

            if mode in ["layout", "landscape"]:
                layout = mode

        if len(opts) > 2:
            opts = opts[:2]

        if len(opts) == 2:
            z, w = opts
            w = w.strip("%")

            if w.isdigit():
                width = int(w) / 100

            try:
                zoom = float(z)
            except Exception:
                pass

        elif len(opts) == 1:

            if "%" in opts[0]:
                w = opts[0].strip("%")

                if w.isdigit():
                    width = int(w) / 100

            else:
                try:
                    zoom = float(opts[0])
                except Exception:
                    pass

        sizes = {"portrait": 715, "landscape": 1040}

        pagesizes = {
            "A0": {"portrait": 841, "landscape": 1189},
            "A1": {"portrait": 594, "landscape": 841},
            "A2": {"portrait": 420, "landscape": 594},
            "A3": {"portrait": 297, "landscape": 420},
            "A4": {"portrait": 210, "landscape": 297},
            "A5": {"portrait": 148, "landscape": 210},
            "A6": {"portrait": 105, "landscape": 148},
            "A7": {"portrait": 74, "landscape": 105},
            "A8": {"portrait": 52, "landscape": 74},
            "A9": {"portrait": 37, "landscape": 52},
            "B0": {"portrait": 1000, "landscape": 1414},
            "B1": {"portrait": 707, "landscape": 1000},
            "B2": {"portrait": 500, "landscape": 707},
            "B3": {"portrait": 353, "landscape": 500},
            "B4": {"portrait": 250, "landscape": 353},
            "B5": {"portrait": 176, "landscape": 250},
            "B6": {"portrait": 125, "landscape": 176},
            "B7": {"portrait": 88, "landscape": 125},
            "B8": {"portrait": 62, "landscape": 88},
            "B9": {"portrait": 44, "landscape": 62},
            "B10": {"portrait": 31, "landscape": 44},
            "C5E": {"portrait": 163, "landscape": 229},
            "Comm10E": {"portrait": 105, "landscape": 241},
            "DLE": {"portrait": 110, "landscape": 220},
            "Executive": {"portrait": 190.5, "landscape": 254},
            "Folio": {"portrait": 210, "landscape": 330},
            "Ledger": {"portrait": 431.8, "landscape": 279.4},
            "Legal": {"portrait": 215.9, "landscape": 355.6},
            "Letter": {"portrait": 215.9, "landscape": 279.4},
            "Tabloid": {"portrait": 279.4, "landscape": 431.8},
        }

        #  Define zoom 1.0 to allow 8 scores in A4 portrait
        zoom = zoom * 0.7

        width = int(
            sizes[layout]
            / pagesizes["A4"][layout]
            * pagesizes[pagesize][layout]
            * width
            / zoom
        )

        repls = ('\n<div style="width:{0}; margin-left: auto;'
                 ' margin-right: auto;">\n<div class="vextab-auto"'
                 ' width={0} scale={1}>\noptions space=0\ntab-stems=true'
                 '\ntab-stem-direction=down\ntabstave notation={2}'
                 ' tablature={3}'
                 ).format(
            width, zoom, notes, tablature
        )

        code = "".join((code[:start], repls, music, code[endmusic:]))
        index = start

    return code
