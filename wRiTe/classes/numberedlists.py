# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re


def numberedlists(code):
    delta = 0
    lasteol = 0
    eol = 0
    lastline = ""
    newlist = True
    fakelist = False
    diagram = None
    numlists = ()
    re_nlp = re.compile(r"^(\d+)\.\s+")
    re_nsp = re.compile(r"^\s{4}[0-9a-zA-Z]+\.[0-9a-zA-Z]*\s+")
    re_nsp2 = re.compile(r"^\s{4}[*\-\+]\s+.+")
    re_fklist = re.compile(r"^\s{4}[^\s><].+")
    re_diagram = re.compile(r"^(!diagram|\.diagram).*")

    while eol >= 0:
        eol = code.find("\n", lasteol)
        if eol == lasteol:
            lasteol = eol + 1
            lastline = ""
            continue
        line = code[lasteol:eol]
        matchnum = re.match(re_nlp, line)
        matchsub = re.match(re_nsp, line)
        matchsub2 = re.match(re_nsp2, line)
        matchfklist = re.match(re_fklist, line)
        matchdiagram = re.search(re_diagram, line)

        if matchdiagram:
            if diagram is None:
                diagram = True
            else:
                diagram = None
        
        if diagram:
            lasteol = eol + 1
            lastline = ""
            continue
        
        if matchfklist and newlist and not lastline:
            if any((True for p in ("*", "-", "+") \
                    if p in matchfklist.group(0).strip()[0])):
                #  <li> added later to avoid mmd ignoring image if any
                newline = '<ul>\n<ul>\n\nLI&nbsp;{}\n\n</ul>\n</ul>'.format(line[5:])
                code = "".join((code[:lasteol], newline, code[eol:]))
                delta = len(newline) - len(line)
            else:
                if not fakelist:
                #  <li> added later to avoid mmd ignoring image if any
                    newline = '<ul style="list-style-type: none;">\n\nLI&nbsp;{}\n\n</li>'.format(line)
                    code = "".join((code[:lasteol], newline, code[eol:]))
                    fakelist = True
                else:
                    newline = '\n\nLI&nbsp;{}\n\n</li>'.format(line)
                    code = "".join((code[:lasteol], newline, code[eol:]))
                delta = len(newline) - len(line)

        elif not matchfklist and fakelist:
            fakelist = False
            newline = '\n</ul>\n\n{}\n\n'.format(line)
            code = "".join((code[:lasteol], newline, code[eol:]))
            delta = len(newline) - len(line)

        if matchnum:
            if newlist:
                numlists += (matchnum.group(1),)
            newlist = False
        elif (matchsub or matchsub2) and not newlist:
            newlist = False
        else:
            if (not lastline.strip() and not matchfklist) and not newlist:
                newlist = True
                
        lasteol = eol + 1 + delta
        lastline = line
        delta = 0
    
    return code, numlists

