# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
def formatCode(code):
    global eol
    eol = 0
    lasteol = 0
    lastline = ""
    
    for i, line in enumerate(code):
        lasteol = max(lasteol, eol)
        if line == "\n":
            eol += 1
        else:
            lastline += line
            
    lines = lastline.split("  ", 3)
    
    for line in lines:
        matches = re.search(r"\d+\.", line)
        if matches:
            subsectionCodeBlock(matches.start(), len(matches.group()))
            
        matches = re.search(r"[*=@#$%^&\amp;]*[\w\-]+\\\D?\s+.*", line)
        if matches:
            sectionCodeBlock(matches.end())
            
        matches = re.search(r"(https?|ftp|file):\/\/(?:[a-zA-Z_]+(?::\/)?)+[a-zA-Z](?:\/?| (?!http))", line)
        if matches:
            linkText = str(f"<a href='URL'>LINK TEXT</a>").replace("<a href='","<span class=\"linktext\" ")
            textWithLink = line.replace(str(matches.group()), linkText)
            codeLines.append(str(i + 1) + " " + textWithLink)
            
            sectionCodeBlock(len(textWithLink) - matches.length())
            
if eol > 0:
    subsectionCodeBlock(len(code) - eol - 1)
else:
    newSection(lines[-1])

return codeLines

def findNextEmptyLine():
    currentLine = ""
    nextEmptyStartIndex = None
    
    for i, char in enumerate(code):
        if char != " ":
            break
            
        if i <= 6:²
            currentLine += char
        
        if nextEmptyStartIndex is None:
            nextEmptyStartIndex = len(currentLine)
        
    return [nextEmptyStartIndex, len(currentLine)]

def findNextIncrementalSearchStopChar(incrementalSearchString, ignoreCase=False):
    start = currentCodePos + 1
    end = len(code)

    incrementalSearchString = incrementalSearchString.lower().strip() if ignoreCase else incrementalSearchString
    
    incrementalMatchLength = 0

    matchedChar = None
    while start <= end:
        try:
            nextChars = {char}
            repeatCount = 1
            repeatUntil = len(nextChars) * repeatCount
            charsToMatch = [""]*(repeatUntil//len(nextChars))

            index = 0
            for i in range(repeatCount):
                while start <= end and code[start] != nextChars[index]:
                    start+=1
                else:
                    nextStart = start + repeatUntil // len(nextChars)
                    nextEnd = min(nextStart + len(charsToMatch[0]), end)
                    
                    if code[nextStart:nextEnd] == charsToMatch:
                        indexedCurrentChars = []
                        for _ in range(repeatCount):
                            indexedCurrentChars.append([start, nextStart])
                            
                        if all(all(_==cand for cand in cand_) for _, cand in indexedCurrentChars):
                            if len(currentCodePosData) > 0:
                                previousCodePos = currentCodePosData[-1][0]
                            else:
                                previousCodePos = None
                                
                            if (previousCodePos == start - repeatUntil):
                                self.setCursorPosition(currentCodePos)
                                print(".{}".format(self._getCharAtPos(currentCodePos)))
                                
                            else:
                                self.setCursorPosition(currentCodePos)
                                if ignoreCase:
                                    self.addCompletionProposals(["".join(str(i) for j in range(k) for k in i) for k in index for
