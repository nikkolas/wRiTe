# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
import os
from classes.cmplfuncs import matplotlibToSvg
from classes.utils import findBalanced


def parseToPandoc(mdfile, APP_PATH, TMP_DIR, pcode, path, data_path):

    #  Verbatim
    def verbatim(match):
        return "".join(('    ', match.group(1).replace('\n', '\n    '), '\n'))
    pcode = re.sub(r'^```[a-z]+([^`]+)```', verbatim, pcode, flags=re.S|re.M)

    pcode = re.sub(r'(?<!`)`([^`]+)`(?!`)', r'\\texttt{\1}', pcode)

    #  Header-less tables
    try:
        num = pcode.count("\n\n|")
        index = 0

        for _ in range(num):
            start = pcode.find("\n\n|", index)
            stop = pcode.find("|\n", start + 3)

            if start >= 0 and stop > 0:
                content = pcode[start + 2: stop + 1]
                ln = content.count("|")

                if not re.sub("[-|:]+", "", content) and "-" in content:
                    content = "".join(("|  " * (ln - 1), "|\n"))
                    pcode = "".join((pcode[: start + 2], content, pcode[start + 2:]))
                    index = stop + 2 + len(content)
    except Exception:
        pass

    #  remove colors in tables
    try:
        pcode = re.sub(r"@@*[#a-z]{3,}\s*\|", " |", pcode)
    except Exception:
        pass

    #  Adapt new pages
    try:
        pcode = pcode.replace("!newpage", "\\newpage")
    except Exception:
        pass

    #  Newlines
    try:
        def nl(match):
            if match.group(1):
                return int(match.group(1)) * " <br> "
            return " <br> "

        pcode = re.sub(r"\\(\d+)!", nl, pcode)
    except Exception:
        pass

    #  Underline
    try:
        pcode = re.sub(r"[+]{2}([^+]+)[+]{2}", r"[\1]{.underline}", pcode)
    except Exception:
        pass

    #  Lines
    try:
        pcode = pcode.replace("- - -", "***")
        pcode = pcode.replace(". . .", "***")
        pcode = pcode.replace("---", "***")
        pcode = re.sub(r"\|[^\S\r\n]*\*{3}", "| --- ", pcode)
    except Exception:
        pass

    #  Boxes into 2 lines
    try:
        pcode = pcode.replace("!box", "***\n")
        pcode = pcode.replace(".box", "\n***")
    except Exception:
        pass

    #  Spaces
    try:
        def space(match):
            nbsp = len(match.group(1))
            return "&nbsp;" * nbsp

        pcode = re.sub(r"/(\s+)/", space, pcode)
    except Exception:
        pass

    #  Remove header if any
    try:
        mh = re.search("^(\*|\-|\+){4,}$", pcode, flags=re.MULTILINE)

        if mh:
            hend = mh.start() - 1
            enddelim = mh.end() + 1
            opts = [
                "MODE:",
                "STYLE:",
                "LINE:",
                "FONT:",
                "FONTSIZE:",
                "FONTCOLOR:",
                "PAGENUM:",
                "HIDE:",
                "MARGINS:",
                "MCOLOR:",
                "BGCOLOR:",
                "HIGHLIGHT:",
                "DIAGRAM:",
                "MLEFT:",
                "MRIGHT:",
                "MBOTTOM:",
                "MTOP:",
                "SIZE:",
                "PADDING:",
                "PTOP:",
                "PLEFT:",
                "PRIGHT:",
                "PBOTTOM:",
                "BGIMAGE:",
                "BGGRADIENT:",
                "HEADER:",
                "AUTHOR:",
                "TITLE:",
                "ENCRYPT:",
                "EXPORTED_PAGES:",
                "LANG:",
            ]

            if any([1 for m in opts if m in pcode[:hend]]):
                pcode = pcode[enddelim:]
    except Exception:
        pass

    #  Need spaces between text and formulas
    try:
        pcode = re.sub(r"(?<!\$)\$([^$]+)\$(?!\$)", r"$~\1~$", pcode)
    except Exception:
        pass

    #  Chemical equations
    index = 0
    if "\ce{" in pcode:
        for _ in range(pcode.count("\ce{")):
            start = pcode.find("\ce{", index)
            end = findBalanced("{", pcode, start+4)
            content = pcode[start+4:end]
            content = re.sub(r"(?<!\\)([a-zA-Z]+)", r"\\textrm{\1}", content)
            content = re.sub(r"(\w+})(\d*)\^(\d+(\+|\-))", r"\1_{\2}^{\\textrm{\3}}", content)
            content = re.sub(r"(\w+})(\+|\-)", r"\1^{\2}", content)
            content = re.sub(r"}(\d+)((\+|\-))", r"}_{\\textrm{\1}}^{\2}", content)
            pcode = "".join((pcode[:start], content, pcode[end+1:]))
            index = start + 4

    #  Numbers + units in formulas not in italic
    try:

        def rmnumbers(match):
            eqt = len(match.group(1))
            s = match.group(2)

            if "\\pu{" in s:
                sc = re.sub(r".*[\\]pu\{([^}]+)\}", r"~\1", s)
                sc = re.sub(r"([\+\d-]+)", r"^{\1}", sc)
                sc = re.sub(r"([a-zA-Z]+)", r"\\\\textrm{\1}", sc)
                s = re.sub(r"[\\]pu\{([^}]+)\}", sc, s)
            return "".join(("$" * eqt, re.sub(r"(?<!\w)(\d+ )", r"\\textrm{\1}", s), "$" * eqt))

        pcode = re.sub("([$]{1,2})([^$]+)[$]{1,2}", rmnumbers, pcode)
    except Exception:
        pass

    #  Remove custom commands from LaTeX code
    try:
        pcode = re.sub(r"\\LaTeX", r"\\textrm{LaTeX}", pcode)
        pcode = re.sub(r"\\d\s*", "d", pcode)
        pcode = re.sub(r"\\textcolor{[a-z]+}{([^}]+)}", r"\1", pcode)
        pcode = re.sub(r"\\e(?!\w)", r"\\textrm{e}", pcode)
        pcode = pcode.replace("\\vv", "\\overrightarrow")
        pcode = re.sub(r"\\dv\{([^}]+)\}\{([^}]+)\}", r"\\frac{d\1}{d\2}", pcode)
    except Exception:
        pass

    #  Remove font sizes
    try:
        for size in [
            "\\tiny",
            "\\scriptsize",
            "\\footnotesize",
            "\\normalsize",
            "\\large",
            "\\Large",
            "\\huge",
            "\\Huge",
        ]:
            pcode = pcode.replace(size, "")
    except Exception:
        pass

    # Remove sized fracs
    try:
        pcode = pcode.replace("\\dfrac", "\\frac")
        pcode = pcode.replace("\\tfrac", "\\frac")

        def pu(match):
            units = match.group(1)
            units = units.replace("*", ".")
            units = re.sub(r"([a-zA-Z]+)", r"\\textrm{\1}", units)
            units = re.sub(r"([+-]?\d+)", r"^{\1}", units)
            return units

        pcode = re.sub(r"\\pu\{([^}]+)\}", pu, pcode)
    except Exception:
        pass

    #  Remove custom inline text modifs
    try:
        re_tm = re.compile(r"(\([^)]+\)\{)")
        index = 0
        tm = re_tm.findall(pcode)

        for i in range(len(tm)):
            m = re_tm.search(pcode, index)
            if m:
                start = m.start()
                endopts = m.end()
                endcontent = findBalanced("{", pcode, endopts)
                pcode = "".join(
                    (pcode[:start], pcode[endopts:endcontent], pcode[endcontent + 1:])
                )
                index += endcontent
    except Exception:
        pass

    #  python and mpl code to python without options
    #  Python
    try:
        index = 0
        num = pcode.count("```python")

        for i in range(num):
            options = []
            start = pcode.find("```python", index)
            end = pcode.find("```", start + 3)
            stcontent = pcode.find("\n", start, end)
            index = end
            content = pcode[stcontent + 1: end]
            stopts = pcode.find("(", start, stcontent)

            if stopts > 0:
                endopts = pcode.rfind(")", stopts, stcontent)

                if endopts > 0:
                    options = re.sub(
                        r"\s+", " ", pcode[stopts + 1: endopts].strip()
                    ).replace('"', "'")
                    options = options.split(" ")

                    if "n" in options and content:
                        contentnumbered = ""
                        tab = content.splitlines()

                        for j, l in enumerate(tab):
                            contentnumbered += "".join(("{}:  ".format(j + 1), l, "\n"))

                        pcode = "".join(
                            (pcode[:start], "```python\n", contentnumbered, pcode[end:])
                        )
    except Exception:
        pass

    #  Matplotlib
    try:
        num = pcode.count("\n!graph")
        dir = os.path.join(TMP_DIR, "wRiTe_images")
        img_files = os.listdir(dir)
        
        for i in range(num):
            listopts = []
            legend = ""
            options = ""
            mode = "g"
            start = pcode.find("\n!graph")
            end = pcode.find(".graph", start + 7)
            stcontent = pcode.find("\n", start, end)
            stlegend = pcode.find("[", start, stcontent)
            endlegend = pcode.find("]", stlegend, stcontent)

            if stlegend + endlegend >= 0:
                legend = pcode[stlegend + 1: endlegend].strip()
                stopts = pcode.find("(", endlegend, stcontent)
            else:
                stopts = pcode.find("(", start, stcontent)

            if stopts > 0:
                endopts = pcode.rfind(")", stopts, stcontent)
                options = pcode[stopts + 1: endopts].strip().replace('"', "'")
                listopts = options.split(" ")

                if listopts:

                    if "export" in listopts:
                        listopts.remove("export")

                    for o in listopts:

                        if o not in [
                            "s",
                            "g",
                            "gs",
                            "sg",
                            "left",
                            "right",
                            "shadow",
                            "border",
                        ]:

                            if not re.findall(r"[-\d%pxrcm.]+", o):
                                listopts.remove(o)

            content = pcode[stcontent + 1: end]
            imgfile = img_files[i]
            imgfile = os.path.join(TMP_DIR, "wRiTe_images", imgfile)
            os.system("convert {} {}".format(imgfile, imgfile.replace('.svg', '.png')))
            imgfile = imgfile.replace('.svg', '.png')

            if listopts:
                lmode = [item for item in listopts if len(item) <= 2]
                if lmode:
                    if lmode[0] in ["g", "gs", "sg", "s"]:
                        mode = lmode[0]
                        listopts.remove(mode)
                    else:
                        mode = "g"
                else:
                    mode = "g"

                options = "".join((" {} ".format(str(item)) for item in listopts))
                options = options.strip()

            image = "![{}]({} {})".format(legend, imgfile, options)

            if "s" in mode:
                pcode = "".join((pcode[:start], "```python", pcode[stcontent:]))

            if mode in ["gs", "sg"]:
                shift = stcontent - start - 9
                pcode = "".join(
                    (
                        pcode[: end + 6 - shift],
                        "\n\n",
                        image,
                        "\n",
                        pcode[end + 6 - shift:],
                    )
                )
                os.makedirs(os.path.join(TMP_DIR, "wRiTe_images"), exist_ok=True)
                matplotlibToSvg(
                    content, imgfile, APP_PATH, data_path, export=False, imgformat="png"
                )

            elif "g" in mode:
                pcode = "".join((pcode[:start], image, "\n", pcode[end + 6:]))
                os.makedirs(os.path.join(TMP_DIR, "wRiTe_images"), exist_ok=True)
                matplotlibToSvg(
                    content, imgfile, APP_PATH, data_path, export=False, imgformat="png"
                )
    except Exception:
        pass
    
    #  Keep only 'width' opt and adapt for pandoc
    try:
        def img_rmopts(match):
            if 'texttt' not in match.group(0):
                width = "100%"
                descr = re.sub(r"\s+", " ", match.group(3).strip()).split(" ")
                img = descr[0]

                if not os.path.exists(img):
                    img = "".join((data_path, "/", os.path.basename(img)))
                opts = descr[1:]
                opts.sort()
                legend = match.group(2)
                if opts:
                    size = opts[0]
                    if "%" in size:
                        size = int(size.replace("%", ""))
                        width = "{}cm".format(int(19 * size / 100))
                    elif "cm" in size or "px" in size:
                        width = size
                    elif size.isdigit():
                        width = "".join((size, "px"))
                return "![{}]({}){{ width={} }}".format(legend, img, width)
            return match.group(0)

        pcode = re.sub(r"(.*)!\[([^]]*)\]\(([^)]+)\)", img_rmopts, pcode)
    except Exception:
        pass

    #  Remove blocks
    try:
        pcode = re.sub(r"^[!.][a-z\d]+.*$", "", pcode, flags=re.MULTILINE)
    except Exception:
        pass

    #  Remove emojis
    pcode = re.sub(r":[a-z\-]+?:", "", pcode)

    #  Big title
    pcode = re.sub(r"^#\!{1,2}\s", "# ", pcode, flags=re.M)

    return pcode
