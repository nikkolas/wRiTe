# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import os
import re
from importlib import import_module

textfuncs = (
    "add_footer",
    "add_header",
    "boxes",
    "buttons",
    "closedivs",
    "closelastnewpage",
    "codehl",
    "comments",
    "compatibility",
    "fakerefslinks",
    "font",
    "fontawesome",
    "images",
    "inlineTextInTitle",
    "inlinetextmodifs",
    "latexspaces",
    "lettrines",
    "mpl",
    "mermaidcharts",
    "mmdexporthtml",
    "multicolumn",
    "musicscores",
    "newline",
    "newpage",
    "numberedlists",
    "protectimages",
    "protectverbatims",
    "rules",
    "spaces",
    "tableopts",
    "textblockalign",
    "title",
    "verticalalign",
    "vhcenter",
    "wboxes",
)

htmlfuncs = (
    "html_backgroundimages",
    "html_bordershadows",
    "html_boxedpage",
    "html_buildhtmlheader",
    "html_elementssameline",
    "html_figurescorrection",
    "html_finishnumberedlists",
    "html_footnotes",
    "html_guillemets",
    "html_header",
    "html_latexspaces",
    "html_layoutmode",
    "html_lists",
    "html_restoreimages",
    "html_restoreverbatims",
    "html_tasklists",
    "html_upgradetables",
)

#  Load modules
for mod in [*textfuncs, *htmlfuncs]:
    try:
        locals()[mod] = getattr(import_module("classes.{}".format(mod)), mod)
    except ModuleNotFoundError:
        print("module classes.{} not found".format(mod))


def parseToHtml(
    mdfile, APP_PATH, TMP_DIR, code, path, data_path,
    recompileForExport, lang, firstload, footer_content,
    header_content
):

    # First, remove all trailing tabs and spaces
    code = re.sub(r"[ \t]+$", "", code, flags=re.MULTILINE)

    pagesizes = (
        "A0",
        "A1",
        "A2",
        "A3",
        "A4",
        "A5",
        "A6",
        "A7",
        "A8",
        "A9",
        "B0",
        "B1",
        "B2",
        "B3",
        "B4",
        "B5",
        "B6",
        "B7",
        "B8",
        "B9",
        "B10",
        "C5E",
        "Comm10E",
        "DLE",
        "Executive",
        "Folio",
        "Ledger",
        "Legal",
        "Letter",
        "Tabloid",
    )
    highlighter = False
    mermaid = False
    music = False
    highlight = "xcode.css"
    css_file = os.path.join(APP_PATH, "css/default.css")
    pagesize = "A4"
    fontSize = 11
    fontName = ""
    fontMath = ""
    fontTitle = ""
    mermaidTheme = "forest"
    layout = "portrait"
    pagenum = ""
    lineSpacing = 1.3
    bqcolor = ""
    backgroundcolor = ""
    backgroundimage = ""
    bgalpha = 0
    backgroundgradient = ""
    margincolor = ""
    fontcolor = "#000000"
    body = "\n    body {\n}\n"
    img_bsopts = []
    mleft, mright, mtop, mdown = [10] * 4
    pleft, pright, ptop, pdown = [0] * 4
    author = ""
    doctitle = ""
    encrypt = False
    katex = False
    pageList = None
    bqrgbcolor = ()
    newpages = 0
    rebuild = False
    videofiles = []
    muPdfCode = {}

    #  Grab options form header

    params = {}
    mh = re.search(r"^(\*|\-|\+){4,}\s*$", code, flags=re.MULTILINE)

    if mh:
        hend = mh.start() - 1
        enddelim = mh.end() + 1
        opts = (
            "MODE:",
            "STYLE:",
            "LINE:",
            "FONT:",
            "FONTMATH:",
            "FONTTITLE:",
            "FONTSIZE:",
            "FONTCOLOR:",
            "PAGENUM:",
            "MARGINS:",
            "MCOLOR:",
            "BGCOLOR:",
            "HIGHLIGHT:",
            "DIAGRAM:",
            "MLEFT:",
            "MRIGHT:",
            "MBOTTOM:",
            "MTOP",
            "SIZE:",
            "PADDING:",
            "PTOP:",
            "PLEFT:",
            "PRIGHT:",
            "PBOTTOM:",
            "BGIMAGE:",
            "BGGRADIENT:",
            "HEADER:",
            "AUTHOR:",
            "TITLE:",
            "ENCRYPT:",
            "LANG:",
            "EXPORTED_PAGES:",
            "BQCOLOR:",
        )

        if any([1 for m in opts if m in code[:hend]]):
            params = [s for s in code[:hend].strip().splitlines() if s]
            params = dict(
                [
                    list(map(str.strip, s.strip().split(":")))
                    for s in params
                    if (":" in s) and all(s.replace(" ", "").split(":"))
                ]
            )
            code = code[enddelim:]

    if params:
        if "EXPORTED_PAGES" in params:
            pageList = params["EXPORTED_PAGES"]

            def makelist(match):
                fpage = int(match.group(1))
                lpage = int(match.group(2))
                plist = str(list(range(fpage, lpage + 1)))
                plist = plist[1:-1]
                return plist

            pageList = re.sub(r"(\d+)-(\d+)", makelist, pageList)
            pageList = [int(e) for e in pageList.split(",")]

        if "LANG" in params:
            if len(params["LANG"]) == 2:
                lang = params["LANG"].lower()

        if "ENCRYPT" in params:
            if params["ENCRYPT"].lower() in ("true", "1", "ok", "yes", "y"):
                encrypt = True

        if "AUTHOR" in params:
            author = params.get("AUTHOR", author)

        if "TITLE" in params:
            doctitle = params.get("TITLE", doctitle)

        if "BQCOLOR" in params:
            bqcolor = params.get("BQCOLOR", bqcolor)

        file = "".join((params.get("STYLE", css_file), ".css"))

        if file in os.listdir(os.path.join(APP_PATH, "css/")):
            css_file = os.path.join(APP_PATH, "css/", file)

        elif file in os.listdir(data_path):
            css_file = os.path.join(data_path, file)

        if params.get("SIZE", pagesize) in pagesizes:
            pagesize = params.get("SIZE", pagesize)

        if (
            "LINE" in params
            and params["LINE"].replace(",", "").replace(".", "").replace('%', '').isdigit()
        ):
            lineSpacing = params["LINE"].replace(",", ".")
            if '%' in lineSpacing:
                try:
                    lineSpacing = float(lineSpacing.replace('%', '')) / 100
                except Exception:
                    pass

        fontName = params.get("FONT", fontName)
        fontMath = params.get("FONTMATH", fontMath)
        fontTitle = params.get("FONTTITLE", '')

        if (
            "FONTSIZE" in params
            and params["FONTSIZE"].replace(",", "").replace(".", "").isdigit()
        ):
            fontSize = float(params["FONTSIZE"])

        fontcolor = params.get("FONTCOLOR", fontcolor)

        if "#000000" not in fontcolor or "black" not in fontcolor:
            body = body.replace("}", "color: {};\n}}".format(fontcolor))

        pagenum = params.get("PAGENUM", pagenum)

        if pagenum and "!n" in pagenum:
            stnum = pagenum.find("!n")

            if stnum >= 0:
                pagenum = "".join((pagenum[:stnum], "{}", pagenum[stnum + 2:]))
            else:
                pagenum = ""

        if "MODE" in params:
            if params["MODE"] in ["portrait", "landscape"]:
                layout = params["MODE"]

        if "PADDING" in params:
            value = re.sub(r"(\d+)", r"\1", params["PADDING"])

            if value:

                try:
                    pleft, pright, ptop, pdown = [int(value)] * 4
                except Exception:
                    pass

        if "PLEFT" in params:
            value = re.sub(r"(\d+)", r"\1", params["PLEFT"])

            if value:

                try:
                    pleft = int(value)

                except Exception:
                    pass

        if "PRIGHT" in params:
            value = re.sub(r"(\d+)", r"\1", params["PRIGHT"])

            if value:

                try:
                    pright = int(value)

                except Exception:
                    pass

        if "PTOP" in params:
            value = re.sub(r"(\d+)", r"\1", params["PTOP"])

            if value:

                try:
                    ptop = int(value)

                except Exception:
                    pass

        if "PBOTTOM" in params:
            value = re.sub(r"(\d+)", r"\1", params["PBOTTOM"])

            if value:

                try:
                    pdown = int(value)

                except Exception:
                    pass

        if "MARGINS" in params:
            value = re.sub(r"(\d+)", r"\1", params["MARGINS"])

            if value:

                try:
                    mleft, mright, mtop, mdown = [int(value)] * 4

                except Exception:
                    pass

        if "MLEFT" in params:
            value = re.sub(r"(\d+)", r"\1", params["MLEFT"])

            if value:

                try:
                    mleft = int(value)

                except Exception:
                    pass

        if "MRIGHT" in params:
            value = re.sub(r"(\d+)", r"\1", params["MRIGHT"])

            if value:

                try:
                    mright = int(value)

                except Exception:
                    pass

        if "MTOP" in params:
            value = re.sub(r"(\d+)", r"\1", params["MTOP"])

            if value:

                try:
                    mtop = int(value)

                except Exception:
                    pass

        if "MBOTTOM" in params:
            value = re.sub(r"(\d+)", r"\1", params["MBOTTOM"])

            if value:

                try:
                    mdown = int(value)

                except Exception:
                    pass

        if "HIGHLIGHT" in params:
            file = "".join((params["HIGHLIGHT"], ".css"))

            if file in os.listdir(os.path.join(APP_PATH, "highlight/styles/")):
                highlight = file

        if "DIAGRAM" in params:
            mermaidcolors = {
                "yellow": "base",
                "purple": "default",
                "grey": "neutral",
                "gray": "neutral",
                "green": "forest",
                "black": "black",
            }

            if params["DIAGRAM"] in mermaidcolors:
                mermaidTheme = mermaidcolors[params["DIAGRAM"]]

        backgroundcolor = params.get("BGCOLOR", backgroundcolor)

        if backgroundcolor:
            body = body.replace(
                "}",
                "background-color: {};\n}}".format(backgroundcolor)
                )

        margincolor = params.get("MCOLOR", margincolor)

        backgroundimage = params.get("BGIMAGE", backgroundimage)
        backgroundimage = re.sub(r"\s+", " ", backgroundimage.strip())
        backgroundimage = backgroundimage.split(" ")

        if len(backgroundimage) == 2:

            if "%" in backgroundimage[0] and "%20" not in backgroundimage[0]:
                bgalpha, backgroundimage = backgroundimage
            else:
                backgroundimage, bgalpha = backgroundimage

            bgalpha = bgalpha.strip()

        elif len(backgroundimage) == 1:
            backgroundimage = backgroundimage[0]

        if backgroundimage and "/" not in backgroundimage:
            backgroundimage = backgroundimage.replace("%20", " ")
            backgroundimage = os.path.join(data_path, backgroundimage.strip())

        if backgroundimage:
            backgroundimage = "url('{}')".format(backgroundimage)

        backgroundgradient = params.get("BGGRADIENT", backgroundgradient).strip()

        if "lg(" in backgroundgradient:
            backgroundgradient = backgroundgradient.replace("lg(", "linear-gradient(")

    #  Activate hilighting if needed
    if "```" in code:
        highlighter = True

    # Katex if $
    if "$" in code:
        katex = True

    #  Initiate first page with headers
    code = "".join(
        (
            '\n\n<page style="padding: {}mm {}mm {}mm {}mm;">\n<header></header>\n<pagecontent>\n\n\n'.format(
                ptop, pright, pdown, pleft
            ),
            code,
        )
    )

     #  Process newpages
    if "\n!newpage" in code:
        newpages = code.count("\n!newpage")
        try:
            code = newpage(code, ptop, pright, pdown, pleft)
        except Exception:
            print("Error in newpage")

    # Close last newpage
    try:
        code = closelastnewpage(code)
    except Exception:
        print("Error in closelastnewpage")

    # Add header to pages
    try:
        code = add_header(code, header_content)
    except Exception:
        print("Error in add_header")

    # Add footer to pages
    try:
        code = add_footer(code, footer_content)
    except Exception:
        print("Error in add_footer")

    #  Protect verbatim code
    try:
        code, verbatims, inlineverbatims = protectverbatims(code)
    except Exception:
        print("Error in protectverbatims")

    #  Process fake refs links
    if not recompileForExport and "][" in code:
        try:
            code = fakerefslinks(code)
        except Exception:
            print("Error in fakerefslinks")

    #  Process comments
    if "%%" in code:
        try:
            code = comments(code)
        except Exception:
            print("Error in comments")

    #  Rules
    try:
        code = rules(code)
    except Exception:
        print("Error in rules")

    #  Inline text modif. in titles
    try:
        code = inlineTextInTitle(code)
    except Exception:
        print("Error in inlineTextInTitle")

    #  Title
    if "#!" in code:
        try:
            code = title(code)
        except Exception:
            print("Error in title")

    #  Fontawesome
    try:
        code = fontawesome(code)
    except Exception:
        print("Error in fontawesome")

    #  Process lettrines
    if "\n§" in code or "§" in code[0]:
        try:
            code = lettrines(code)
        except Exception:
            print("Error in lettrines")

    # Process spaces
    try:
        code = spaces(code)
    except Exception:
        print("Error in spaces")

    # Process buttons
    if re.search(r"\[[#@\da-z]*\|[^|]+?\|[#@\da-z]*\]", code):
        try:
            code = buttons(code, fontSize)
        except Exception:
            print('Error in buttons')

    # Process newlines
    try:
        code = newline(code)
    except Exception:
        print("Error in newline")

    # Process font environements
    try:
        code = font(code)
    except Exception:
        print("Error in font")

    #  Process vertical alignment
    if "!vcenter" in code:
        try:
            code = verticalalign(code)
        except Exception:
            print("Error in verticalalign")

    #  Process vertical and horizontal alignment
    if "!vhcenter" in code:
        try:
            code = vhcenter(code)
        except Exception:
            print("Error in vhcenter")

    # Process wboxes
    if "!wbox" in code:
        try:
            code = wboxes(code)
        except Exception:
            print("Error in wboxes")
    #  Process boxes
    if "!box" in code:
        try:
            code = boxes(code)
        except Exception:
            print("Error in boxes")

    # Process multi-column
    if "!mc" in code:
        try:
            code = multicolumn(code)
        except Exception:
            print("Error in multicolumn")

    # Process block text position
    try:
        code = textblockalign(code)
    except Exception:
        print("Error in textblockalign")

    # Compatibility mode
    if "==" in code or "~~" in code or "++" in code:
        try:
            code = compatibility(code)
        except Exception:
            print("Error in compatibility")

    # Process LaTeX spaces in display
    if katex:
        try:
            code = latexspaces(code, int(code.count("$$") / 2))
        except Exception:
            print("Error in latexspaces")

    #  Activate music if needed
    if "!gtab" in code:
        music = True
        try:
            code = musicscores(code, layout, pagesize)
        except Exception:
            print("Error in musicscores")

    # Code block highlight
    num = code.count("!code")

    if num:
        highlighter = True
        try:
            code = codehl(code, num, APP_PATH)
        except Exception:
            print("Error in pyth")

    # Traitement matplotlib
    if "!graph" in code:
        highlighter = True
        if firstload:
            rebuild = True
        try:
            code = mpl(code, mdfile, APP_PATH, data_path, TMP_DIR, firstload)
        except Exception:
            print("Error in matplotlib")

    # Block muPdfCode at start
    if "!muPDF" in code:
        if firstload:
            rebuild = True

    #  Protect native images
    if "<img src" in code:
        try:
            code = protectimages(code)
        except Exception:
            print("Error in protectimages")

    # Process enumerated lists
    try:
        code, numlists = numberedlists(code)
    except Exception:
        print("Error in numberedlists")

    # Mermaid diagrams
    if "!diagram" in code:
        mermaid = True
        try:
            code = mermaidcharts(code)
        except Exception:
            print("Error in mermaidcharts")

    #  Process table opts
    if "|\n[" in code:
        try:
            code = tableopts(code)
        except Exception:
            print("Error in tableopts")

    # Close DIVs
    try:
        code = closedivs(code)
    except Exception:
        print("Error in closedivs")

    # Text inline modifs
    try:
        code = inlinetextmodifs(code, fontSize)
    except Exception:
        print("Error in inlinetextmodifs")

    #  Process images
    num = code.count("![")

    if num:
        try:
            code, img_bsopts, videofiles = images(code, path, data_path, num, TMP_DIR)
        except Exception:
            print("Error in images")

    # HTML Export via MMD
    try:
        code = mmdexporthtml(code, lang)
    except Exception:
        print("Error in mmdexporthtml")

    try:
        code = html_finishnumberedlists(code)
    except Exception:
        print("Error in html_finishnumberedlists")

    # Construct header
    try:
        code, bqrgbcolor = html_header(
            code,
            APP_PATH,
            css_file,
            fontSize,
            fontName,
            fontMath,
            fontTitle,
            lineSpacing,
            bqcolor,
            body,
            layout,
            newpages
        )
    except Exception:
        print("Error in html_header")

    #  Process boxed page
    num = code.count("!pageframe")

    if num:
        try:
            code = html_boxedpage(code, num)
        except Exception:
            print("Error in html_boxedpage")

    #  Create footnotes if any
    num_of_pages = code.count("<page style")

    if num_of_pages:
        try:
            code = html_footnotes(code, num_of_pages)
        except Exception:
            print("Error in html_footnotes")

    #  Process elements on same line
    if "||" in code:
        try:
            code = html_elementssameline(code)
        except Exception:
            print("Error in html_elementssameline")

    #  Process tasklists
    if "[ ]" in code or "[X]" in code:
        try:
            code = html_tasklists(code)
        except Exception:
            print("Error in html_taslists")

    #  Lists correction
    if "<ol>" in code:
        try:
            code = html_lists(code, numlists)
        except Exception:
            print("Error in html_lists")

    #  Figure correction
    if "<figure" in code or "<img" in code:
        try:
            code = html_figurescorrection(code)
        except Exception:
            print("Error in html_figuescorrection")

    #  Add border or shadow in images
    numimg = code.count('<img src="')

    if numimg:
        try:
            code = html_bordershadows(code, numimg, img_bsopts)
        except Exception:
            print("Error in html_bordershadows")

    #  Restore native images
    if "wRiTeimgnative" in code:
        try:
            code = html_restoreimages(code)
        except Exception:
            print("Error in html_restoreimages")

    #  Background images and gradients
    numofbgimages = code.count("!bgimage(")
    numofbggradients = code.count("!bggradient(")

    try:
        code = html_backgroundimages(
            code,
            numofbgimages,
            numofbggradients,
            backgroundimage,
            bgalpha,
            backgroundgradient,
            data_path,
        )
    except Exception:
        print("Error in html_backgroundimages")

    #  Enough space between : and latex formula
    try:
        code = html_latexspaces(code)
    except Exception:
        print("Error in html_latexspaces")

    #  Space between french guillemets
    try:
        code = html_guillemets(code)
    except Exception:
        print("Error in html_guillemets")

    #  Apply layout mode on individual pages
    num = code.count("!portrait") + code.count("!landscape")

    if num:
        try:
            code = html_layoutmode(code, num)
        except Exception:
            print("Error in html_layoutmode")

    #  Apply rowspan, colors, size in tables

    numoftables = code.count("<table")

    if numoftables:
        try:
            code = html_upgradetables(code, numoftables)
        except Exception:
            print("Error in html_upgradetables")

    #  Restore verbatims
    if verbatims or inlineverbatims:
        try:
            code = html_restoreverbatims(code, verbatims, inlineverbatims)
        except Exception:
            print("Error in html_restoreverbatims")

    #  Finalize HTML header
    try:
        code = html_buildhtmlheader(
            code, APP_PATH, katex, highlighter, highlight, mermaid, mermaidTheme, music, fontSize
        )
    except Exception:
        print("Error in html_buildhtmlheader")
    #print(code)    
    return (
        code,
        margincolor,
        mleft,
        mright,
        mtop,
        mdown,
        layout,
        pagenum,
        pagesize,
        fontSize,
        author,
        doctitle,
        encrypt,
        pageList,
        bqrgbcolor,
        rebuild,
        videofiles,
    )
