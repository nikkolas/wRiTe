# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re


def protectverbatims(code):
    verbatims = []
    vs = re.findall("```([^`]+)```", code)

    if vs:

        for i, m in enumerate(vs):
            lang = re.findall("^([a-z]+)\n", m)

            if lang:
                lang = lang[0]
                verbatims.append(
                    [lang, m[len(lang):].replace("<", "&lt;").replace(">", "&gt;")]
                )
            else:
                lang = "plaintext"
                verbatims.append(
                    [lang, m[0:].replace("<", "&lt;").replace(">", "&gt;")]
                )

        for m in vs:
            code = code.replace("```{}```".format(m), "@@@VERBATIM@@@")

    #  Protect inline verbatim
    inlineverbatims = re.findall("`([^`]+?)`", code)

    if inlineverbatims:

        for i in range(len(inlineverbatims)):
            inlineverbatims[i] = (
                inlineverbatims[i].replace("<", "&lt;").replace(">", "&gt;")
            )

        code = re.sub("`([^`]+?)`", "@@@INLINEVERBATIM@@@", code)

    return code, verbatims, inlineverbatims
