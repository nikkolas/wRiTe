# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
from classes.colors import colors


def line(match):
    style = match.group(1)
    if not style or style not in ('solid', 'dashed', 'dotted'):
        style = 'solid'
    opts = match.group(2).strip()
    opts = re.sub(r"\s+", " ", opts.strip()).split(" ")
    opts.sort()
    color = 'black'
    lw = '1px'
    width = '100%'
    if opts:
        for opt in opts:
            if not opt:
                continue
            if '@' in opt:
                if opt[1:] in colors:
                    color = opt[1:]
            elif '#' in opt:
                color = opt
            elif 'w' in opt[:1]:
                width = opt[1:]
            else:
                lw = opt
    return '\n<hr style="margin: 0 auto 0 auto; border-top: {} {} {}; width: {};"/>\n'.format(lw, style, color, width)
    


def rules(code):
    if '---' in code:
        code = re.sub(
            r"^([#\s]*)([^-\n|]+)[-]{3}[^\w\n]*$",
            r'\1 <span class="linefillsolid">\2</span>',
            code,
            flags=re.MULTILINE,
        )
        code = re.sub(
            r"---.*$",
            r'\n<hr class="solid" />\n',
            code,
            flags=re.MULTILINE
            )
    if '- - -' in code:
        code = re.sub(
            r"^([#\s]*)([^-\n|]+)- - -[^\w\n]*$",
            r'\1 <span class="linefilldashed">\2</span>',
            code,
            flags=re.MULTILINE,
        )
        code = re.sub(
            r"^- - -.*$",
            r'\n<hr class="dashed" />\n',
            code,
            flags=re.MULTILINE
            )

    if ". . ." in code:
        code = re.sub(
            r"^([#\s]*)([^\.\n|]+)\. \. \.[^\w\n]*$",
            r'\1 <span class="linefilldotted">\2</span>',
            code,
            flags=re.MULTILINE,
        )
        code = re.sub(
            r"^\. \. \..*$",
            r'\n<hr class="dotted" />\n',
            code,
            flags=re.MULTILINE
            )

    if '!line' in code:
        code = re.sub(
                r"^!line(solid|dashed|dotted)*\(*([^)]*?)\)*\s*$",
                line,
                code,
                flags=re.M
                )
    return code
