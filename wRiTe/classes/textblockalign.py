# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re


def alignblock(match):
    add_float = ' style="clear: both;"'
    alignments = {"left": "start", "right": "end"}
    align = match.group(1)
    optalign = match.group(2).strip()
    if "float" in optalign:
        add_float = ''
        optalign = optalign.replace("float", "").strip()

    if align in "center":
        if optalign in ("left", "right"):
            return '\n<div style="break-inside: avoid-column; text-align: center;">\n<div style="display: inline-block; text-align: {};">\n\n {} \n\n</div>\n</div>\n'.format(optalign, match.group(3))
        else:
            return '\n<div align="center" style="break-inside: avoid-column;">\n\n {} \n\n</div>\n'.format(match.group(3))

    elif align in ("left", "right"):
        if not optalign:
            optalign = alignments[align]
        elif optalign in ("left", "right"):
            optalign = alignments[optalign]
        
        return ('\n<p{}></p>\n'
                '<div style="break-inside: avoid-column;'
                ' float: {}; text-align: {};">\n\n{}\n\n</div>\n{}'
                ).format(
                        add_float, 
                        align, 
                        optalign, 
                        match.group(3), 
                        '<p style="clear: both;"></p>' if 'right' in align else ''
                    )


def textblockalign(code):
    if "!center" in code:
        code = re.sub(
            r"^!(center)\(*([^)]*?)\)*\s*$(.+?)^\.center",
            alignblock,
            code,
            flags=re.DOTALL | re.MULTILINE,
        )

    if "!right" in code:
        code = re.sub(
            r"^!(right)\(*([^)]*?)\)*\s*$(.+?)^\.right",
            alignblock, code,
            flags=re.DOTALL | re.MULTILINE
            )

    if "!left" in code:
        code = re.sub(
            r"^!(left)\(*([^)]*?)\)*\s*$(.+?)^\.left",
            alignblock,
            code,
            flags=re.DOTALL | re.MULTILINE
            )
    return code
