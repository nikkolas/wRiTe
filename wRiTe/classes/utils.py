# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
from numpy import isclose


#  Float comparison
def gte(a, b, rtol=1e-9):
    if not isinstance(a, float):
        a = float(a)
    if not isinstance(b, float):
        b = float(b)
    return a >= b or isclose(a, b, rtol)


def lte(a, b, rtol=1e-9):
    if not isinstance(a, float):
        a = float(a)
    if not isinstance(b, float):
        b = float(b)
    return a <= b or isclose(a, b, rtol)


#  Significant digits
def sd(f, i):
    i -= 1
    if i < 0:
        i = 0
    num = "{:.{}e}".format(f, i)

    if "e+00" in num:
        num = num[:-4]
    return num


# NO USE
def isRoman(s):
    roman = False
    parts = tuple(s)
    for i in parts:
        roman = i in "IVXLCDM"
    return roman


def getPattern(s):
    # index = 0
    m = re.findall(r"^(#[#]*)\s([)-.a-zA-Z\d]+)", s)

    if m:

        index = len(m[0][0])
        pattern = m[0][1]

        if pattern.isalpha() and not pattern.isupper() and len(pattern) > 1:
            return 0, "", ""

        nums = re.findall(r"[\da-zA-Z]+", pattern)
        s = re.findall(r"[a-zA-Z]+", pattern)

        if s:
            s = s[0]

            if s.isupper() and isRoman(s):
                pattern = re.sub(r"[IVXLCDM]+", "{}", pattern)
            else:
                pattern = re.sub(r"[a-zA-Z0-9]", "{}", pattern)
        else:
            pattern = re.sub(r"[0-9]", "{}", pattern)

        if index and pattern and nums:
            return int(index), nums, pattern


def reinitVal(s):
    nl = [""] * len(s)

    for j, e in enumerate(s):
        if e.isdigit():
            nl[j] = "0"
        elif e.isupper():
            parts = tuple(e)
            isRom = False
            for i in parts:
                isRom = i in "IVXLCDM"
            if isRom:
                nl[j] = "Z"
        elif e.isalpha():
            nl[j] = "`"
    return nl


def increment(s):
    if s.isdigit():
        return str(int(s) + 1)

    if s.isupper():
        if s == "R0":
            return "I"
        if isRoman(s):
            return intToRoman(romanToInt(s) + 1)
        if (s.isalpha() or s == "`") and len(s) == 1:
            return chr(ord(s) + 1)
    elif (s.isalpha() or s == "`") and len(s) == 1:
        return chr(ord(s) + 1)


def decrement(s):
    if s.isdigit():
        return str(int(s) - 1)
    if s.isupper():
        if s == "I":
            return "R0"
        if isRoman(s):
            return intToRoman(romanToInt(s) - 1)
        if s.isalpha() and len(s) == 1:
            return chr(ord(s) - 1)
    elif s.isalpha() and len(s) == 1:
        return chr(ord(s) - 1)


#  NO USE
def isSuperior(s1, s2):
    r = 0
    if not s2:
        r = 1
    if (s1.isdigit() and s2.isdigit()) or (s1.isalpha() and s2.isalpha()):
        if s1 > s2:
            r = 1
    if s1.isupper() and s2.isupper():
        s1parts = tuple(s1)
        s2parts = tuple(s2)
        isRom = False
        for i in s1parts:
            isRom = i in "IVXLCDM"
        for i in s2parts:
            isRom = i in "IVXLCDM"
        if isRom:
            s1 = romanToInt(s1)
            s2 = romanToInt(s2)
            if int(s1) > int(s2):
                r = 1
    return r


def intToRoman(num):
    val = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
    syb = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]
    roman_num = ""
    i = 0

    while num > 0:
        for _ in range(num // val[i]):
            roman_num += syb[i]
            num -= val[i]
        i += 1
    return roman_num


#  NO USE
def romanToInt(s):
    roman = {
        "I": 1,
        "V": 5,
        "X": 10,
        "L": 50,
        "C": 100,
        "D": 500,
        "M": 1000,
        "IV": 4,
        "IX": 9,
        "XL": 40,
        "XC": 90,
        "CD": 400,
        "CM": 900,
    }
    i = 0
    num = 0

    while i < len(s):
        if i + 1 < len(s) and s[i: i + 2] in roman:
            num += roman[s[i: i + 2]]
            i += 2
        else:
            num += roman[s[i]]
            i += 1
    return num


def findBalanced(p, s, pos):
    d = {"[": "]", "(": ")", "{": "}"}
    sp = d[p]
    bropen = 1
    for i in range(pos, len(s)):
        if s[i] == sp and bropen:
            bropen -= 1
        if s[i] == p:
            bropen += 1
        if s[i] == sp and not bropen:
            return i
    return -1


def splitlinesgen(s):
    return (m.group(0) for m in re.finditer(r"^.*$", s, flags=re.M))


def longuest_common_string(s1, s2):
    # Find max number of common substring chars between two strings
    if len(s1) > len(s2):
        s1, s2 = s2, s1
    for i in range(len(s1), 0, -1):
        for j in range(len(s1) - i + 1):
            if s1[j:j + i] in s2:
                return i
    return 0
