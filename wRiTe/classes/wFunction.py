# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.


import matplotlib.pyplot as plt
import numpy as np


class wFunction:
    def __init__(self, f, **kwargs):
        self.type = "function"
        color = kwargs.get("color", "red")
        lw = kwargs.get("lw", 1.5)
        ls = kwargs.get("ls", "-")
        pts = kwargs.get("pts", 10000)
        label = kwargs.get("label", "")
        fill = kwargs.get("fill", False)
        fillinv = kwargs.get("fillinv", False)
        fillcolor = kwargs.get("fillcolor", "#eaeae5")
        fillalpha = kwargs.get("fillalpha", 1)
        restr = kwargs.get("restr", [])

        if "zorder" not in kwargs:
            kwargs = dict(kwargs, zorder=2)

        ax = plt.gca()
        xmin, xmax = ax.get_xlim()
        ymin, ymax = ax.get_ylim()
        x = np.linspace(xmin, xmax, pts)

        if restr:

            if isinstance(restr, list):

                if len(restr) == 2:
                    x = np.array([n for n in x if restr[0] <= n <= restr[1]])

        if isinstance(f, str):

            if "x" not in f:
                y = [float(y)] * len(x)
            else:
                y = eval(f)

            ax.plot(x, y, lw=lw, linestyle=ls, color=color, label=label)

            if fill:

                if fillinv:
                    ax.fill_between(x, y, np.max(y), color=fillcolor, alpha=fillalpha)

                else:
                    ax.fill_between(x, y, color=fillcolor, alpha=fillalpha)

        elif isinstance(f, list):

            if not isinstance(color, list):
                color = ["red"] * len(f)

            if len(f) > len(color):
                color = color + ["red"] * (len(f) - len(color))

            elif len(f) < len(color):
                color = color[: len(f)]

            if not isinstance(label, list):
                label = [""] * len(f)

            if len(f) > len(label):
                label = label + [""] * (len(f) - len(label))

            elif len(f) < len(label):
                label = label[: len(f)]

            if not isinstance(lw, list):
                lw = [lw] * len(f)

            if len(f) > len(lw):
                lw = lw + [1.5] * (len(f) - len(lw))

            elif len(f) < len(lw):
                lw = lw[: len(f)]

            if not isinstance(ls, list):
                ls = ["-"] * len(f)

            if len(f) > len(ls):
                ls = ls + ["-"] * (len(f) - len(ls))

            elif len(f) < len(ls):
                ls = ls[: len(f)]

            funcs = []

            for i, func in enumerate(f):
                funcs.append(func)
                y = eval("{0}".format(func))

                if "x" not in func:
                    y = [y] * len(x)

                ax.plot(x, y, lw=lw[i], ls=ls[i], color=color[i], label=label[i])

            if fill and len(f) <= 2:
                funcs = list(map(eval, funcs))
                ax.fill_between(x, *funcs, color=fillcolor, alpha=fillalpha)

        self.eq = f
