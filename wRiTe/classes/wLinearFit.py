# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.


import matplotlib.pyplot as plt
from numpy import (
    array,
    ndarray,
    polyfit,
    corrcoef,
    linspace
    )
from classes.utils import sd


class wLinearfit:
    def __init__(
        self,
        listx,
        listy,
        marker="x",
        var="x",
        color="black",
        fcolor="red",
        **kwargs
    ):
        ax = plt.gca()
        ax.scatter(listx, listy, marker=marker, color=color, **kwargs)
        xmin, xmax = ax.get_xlim()

        self.type = "linearfit"
        self.eq = ""
        self.legend = ""
        self.slope = 0
        self.offset = 0
        self.r = 0

        if not ((isinstance(listx, list) or isinstance(listx, ndarray))) or not (
            (isinstance(listy, list) or isinstance(listy, ndarray))
        ):
            return

        listx = array(listx)
        listy = array(listy)

        results = polyfit(listx, listy, 1)
        self.slope = results[0]
        self.offset = results[1]
        self.r = corrcoef(listx, listy)[0][1]

        if self.offset >= 0:
            self.legend = "".join(
                (
                    "{}.${}$ + {}   | r= {}".format(
                        sd(self.slope, 3).replace(".", ","),
                        var,
                        sd(self.offset, 3).replace(".", ","),
                        str(round(self.r, 5)).replace(".", ","),
                    )
                )
            )
        else:
            self.legend = "".join(
                (
                    "{}.${}$ - {}   | r= {}".format(
                        sd(self.slope, 3).replace(".", ","),
                        var,
                        sd(self.offset, 3).replace(".", ",")[1:],
                        str(round(self.r, 5)).replace(".", ","),
                    )
                )
            )

        self.eq = "{}*x + {}".format(self.slope, self.offset)
        x = linspace(xmin, xmax, 1000)
        ax.plot(x, eval(self.eq), label=self.legend, color=fcolor, **kwargs)
        ax.legend()
