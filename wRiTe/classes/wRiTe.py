# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import os
import sys
import re
from time import time
import hashlib

try:
    from tempfile import NamedTemporaryFile
except ModuleNotFoundError:
    print("tempfile module not found")

try:
    import numpy as np
except ModuleNotFoundError:
    print("numpy module not found")
    
try:
    from subprocess import run
except ModuleNotFoundError:
    print("subprocess module not found")

try:
    from shutil import rmtree, copyfile, which
except ModuleNotFoundError:
    print("shutil module not found")

try:
    import fitz
except ModuleNotFoundError:
    print("pymupdf module not found")

try:
    import qpageview
except ModuleNotFoundError:
    print("qpageview module not found")

try:
    from qpageview.magnifier import Magnifier
    from qpageview.rubberband import Rubberband
    import qpageview.viewactions
except ModuleNotFoundError:
    print("qpageview not fully installed")

try:
    from PyQt5.QtWidgets import (
        QGraphicsDropShadowEffect,
        QMessageBox,
        QPushButton,
        QHBoxLayout,
        QWidget,
        QApplication,
        QVBoxLayout,
        QLabel,
        QPlainTextEdit,
        QGridLayout,
        QShortcut,
        QFileDialog,
        QLineEdit,
        QScrollArea,
        QToolButton,
        QDialog,
        QFrame,
        QColorDialog,
        QCompleter,
        QSizePolicy
    )
    from PyQt5.QtWebEngineWidgets import QWebEnginePage, QWebEngineView
    from PyQt5.QtCore import Qt, QSize, QTimer, QPoint, QDir, QUrl, QMarginsF, QRect
    from PyQt5.QtGui import (
        QFont,
        QKeySequence,
        QTextCursor,
        QPageLayout,
        QPageSize,
        QPixmap,
        QIcon,
        QTextCharFormat,
        QBrush,
        QColor,
        QFontDatabase,
        QDesktopServices,
        QTextBlockFormat,
        QMovie,
    )
    from PyQt5.QtWebEngineWidgets import QWebEngineSettings
    from PyQt5.QtPrintSupport import QPrintDialog, QPrinter, QAbstractPrintDialog
except ModuleNotFoundError:
    print(
        "Verify that following Qt5 modules are installed:"
        "\nQtWidgets\nQtWebEngineWidgets\nQtCore\nQtGui"
        "\nQtWebEngineWidgets\nQtMultimedia\nQtMultimediaWidgets"
    )

from classes.parser import parseToHtml
from classes.pandocparser import parseToPandoc
from classes.utils import getPattern, reinitVal, increment, splitlinesgen, longuest_common_string
from classes.colors import colors
from classes.TextEdit import TextEdit
from classes.SpecialScrollArea import SpecialScrollArea
from classes.ImageScrollArea import ImageScrollArea
from classes.WebEnginePage import WebEnginePage
from classes.PageView import PageView
from classes.FileSystemView import FileSystemView
from classes.Pwd import Pwd
from classes.Settings import Settings
from classes.FontSelection import FontSelection
from classes.SManager import SManager
from classes.emojis import emojis_icons
from classes.fa_icon_names import fa_icon_names
from classes.katexCmd import KaTeX_commands
from classes.muPDF import muPDF
from classes.graphPaper import graphPaper


class wRiTe(QWidget):
    def __init__(self, mdFile, APP_PATH, TMP_DIR):
        super().__init__()
        settings = {}
        SM = SManager()
        first_run = False

        if not SM.check_sfile():
            if SM.create_sfile():
                settings = SM.get_settings()
                first_run = True
        else:
            settings = SM.get_settings()

        font = settings['Font'] if 'Font' in settings else "Calibri Light"
        fontsize = int(settings['Fontsize']) if 'Fontsize' in settings else 19
        wwidth = int(settings['Wwidth']) if 'Wwidth' in settings else 0
        wheight = int(settings['Wheight']) if 'Wheight' in settings else 0
        self.textEditBgColor = settings['Editorbgcolor'] \
            if 'Editorbgcolor' in settings else "#fffdfa"
        self.author = settings['Author'] if 'Author' in settings else ""

        self.scrollDict = {}
        self.isIndexing = False
        self.APP_PATH = APP_PATH
        self.TMP_DIR = TMP_DIR
        self.textEditFont = font
        self.textEditFontSize = fontsize
        self.md_file = mdFile
        self.docx_file = ""
        self.disp_filename = ""
        self.path = ""
        self.data_path = ""
        self.pdf_file = ""
        self.memoryPdfFile = NamedTemporaryFile(prefix="wRiTePdf", suffix=".pdf")
        self.memoryPageFile = NamedTemporaryFile(prefix="wRiTePdfPage", suffix=".pdf")
        self.helpIsShown = False
        pdf = fitz.Document()
        pdf.insert_page(0, "")
        pdf.save(self.memoryPdfFile.name, garbage=4)
        pdf.close()
        self.pages_to_delete = []
        self.new_pages_to_compile = []
        self.code_pages = []
        self.mode_changed_pages = []
        self.codeBackup = ()
        self.headerBackup = ""
        self.headerCBackup = ""
        self.footerBackup = ""
        self.tmp_index = ""
        self.tmp_md = ""
        self.justLoaded = True
        self.cursorPosition = -1
        self.deltaPos = -1
        self.firstLoad = True
        self.last_time_move = 0
        self.textEditWidgetsVisible = [False, False, False]
        self.F8 = False
        self.images = []
        self.lastimgfiles = []
        self.selection = ""
        self.lastPos = 0
        self.lastPosScroll = 0
        self.lastPageNumber = 0
        self.textEditLine = 0
        self.totalTextEditLines = 0
        self.totalLine = 0
        self.pdfLoaded = False
        self.numnp = 0
        self.scrollActivated = True
        self.lastLineNum = 0
        self.modified = False
        self.firstTextEditClick = True
        self.generated = True
        self.pageIndicator = ""
        self.newline = ""
        self.onePanel = 0
        self.maximized = False
        self.lastWindowState = 0
        self.lastkey = 0
        self.pdfDisplayed = False
        self.exported = False
        self.titleComplete = 0
        self.titlesRegister = [0]
        self.numOfTitles = 0
        self.lastTitleNum = 0
        self.titlesCount = 0
        self.bgcolor = ""
        self.mleft, self.mright, self.mtop, self.mdown = [10] * 4
        self.layout = "portrait"
        self.pagenum = ""
        self.fontsize = 12
        self.pagesize = "A4"
        self.countModeChangedPages = False
        self.scrollLines = [0]
        self.links = []
        self.lastSize = None
        self.lock = False
        self.lastZoomApplied = 1
        self.pdfconvStarted = False
        self.appLoaded = False
        self.firstAutocomplete = True
        self.highlightIsFinished = False
        self.loadTextIsFinished = False
        self.searchCursors = []
        self.currentSearchCursor = 0
        self.recompileForExport = False
        self.doctitle = ""
        self.encryption = False
        self.password = None
        self.lang = "en"
        self.langHasChanged = False
        self.pageList = None
        self.modalWindowOpened = False
        self.pdfIsExporting = False
        self.bqrgbcolor = None
        self.newpages = 0
        self.selectedFavorite = -1
        self.rebuild = False
        self.selectedSpecialChar = (0, 0)
        self.specialCharTableDim = (0, 0, 0)
        self.specialCharText = ""
        self.selectedImage = 0
        self.totalImages = 0
        self.imageGrid = None
        self.videos = []
        self.muPDFCode = {}
        self.firstCompilationNotNeeded = False
        self.textHash = 0
        # for QT6 (later)
        #self.pdfComplete = False
        #self.veryFirstPage = False

        self.timer = QTimer()
        self.timer.timeout.connect(self.isResizingFinished)

        self.searchTimer = QTimer()
        self.searchTimer.timeout.connect(self.searchTextEdit)

        vbox = QVBoxLayout()
        vbox.setContentsMargins(0, 0, 0, 0)

        self.mainGrid = QGridLayout()
        self.mainGrid.setSpacing(0)

        self.specialScrollArea = SpecialScrollArea()
        self.specialScrollArea.setVisible(0)
        self.specialScrollArea.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.specialScrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.specialScrollArea.keyPressed.connect(self.selectSpecialChar)

        self.imageScrollArea = ImageScrollArea()
        self.imageScrollArea.setVisible(0)
        self.imageScrollArea.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.imageScrollArea.keyPressed.connect(self.selectImage)

        self.textEdit = TextEdit()
        font = QFont()
        font.setPointSize(self.textEditFontSize)
        font.setFamily(self.textEditFont)
        self.textEdit.setFont(font)
        self.textEdit.highlightFinished.connect(self.hlFinished)
        self.textEdit.document().blockCountChanged.connect(self.countTitles)
        self.textEdit.document().blockCountChanged.connect(self.countTotalLines)
        self.textEdit.addNewTitle.connect(self.insertNewTitle)
        self.textEdit.keyPressed.connect(self.toggleSpellCheck)
        self.textEdit.keyPressed.connect(self.updateCompleter)
        self.textEdit.textEditSizeChanged.connect(self.textEditSizeChanged)
        self.textEdit.document().blockCountChanged.connect(
            lambda: self.updateCompleter(Qt.Key_Space)
        )
        self.textEdit.cursorPositionChanged.connect(self.deltaCursorPos)
        self.textEdit.textChanged.connect(self.autocomplete)
        self.textEdit.textChanged.connect(self.texthaschanged)
        self.textEdit.textChanged.connect(self.inlineCompletion)
        self.textEdit.cursorPositionChanged.connect(self.formulaPreview)
        self.textEdit.verticalScrollBar().valueChanged.connect(self.scrollPdf)
        self.updateTextEditBgColor(self.textEditBgColor)

        self.completer = QCompleter([])
        self.completer.setCompletionMode(QCompleter.PopupCompletion)
        self.textEdit.setCompleter(self.completer)

        self.searchBar = QWidget(self.textEdit)
        searchBarLayout = QHBoxLayout()
        searchBar = QWidget()
        searchreplLayout = QVBoxLayout()
        searchLayout = QHBoxLayout()
        searchWidget = QWidget()
        self.searchLineEdit = QLineEdit()
        self.searchLineEdit.setMinimumHeight(30)
        self.searchLineEdit.setMaximumHeight(30)
        self.replaceLineEdit = QLineEdit()
        self.replaceLineEdit.setFocusPolicy(Qt.StrongFocus)
        self.replaceLineEdit.returnPressed.connect(self.replaceExpression)
        self.replaceLineEdit.setMinimumHeight(30)
        self.replaceLineEdit.setMaximumHeight(30)
        self.replaceLineEdit.setHidden(True)
        self.nextPushButton = QPushButton()
        self.nextPushButton.setText(">")
        self.nextPushButton.setMaximumWidth(20)
        self.nextPushButton.clicked.connect(lambda: self.scrollToFoundText(">"))
        self.prevPushButton = QPushButton()
        self.prevPushButton.setText("<")
        self.prevPushButton.setMaximumWidth(20)
        self.prevPushButton.clicked.connect(lambda: self.scrollToFoundText("<"))
        self.replacePushButton = QPushButton()
        self.replacePushButton.setText("⌄")
        self.replacePushButton.setMaximumWidth(15)
        self.replacePushButton.clicked.connect(self.showReplaceLineEdit)
        searchBarLayout.addWidget(self.searchLineEdit)
        searchBarLayout.addWidget(self.prevPushButton)
        searchBarLayout.addWidget(self.nextPushButton)
        searchBarLayout.setContentsMargins(0, 0, 0, 0)
        searchBar.setLayout(searchBarLayout)
        searchreplLayout.addWidget(searchBar)
        searchreplLayout.addWidget(self.replaceLineEdit)
        searchreplLayout.setContentsMargins(0, 0, 0, 0)
        searchWidget.setLayout(searchreplLayout)
        searchLayout.addWidget(self.replacePushButton)
        searchLayout.addWidget(searchWidget)
        searchLayout.setContentsMargins(5, 5, 5, 5)
        self.searchBar.setLayout(searchLayout)
        self.searchBar.setMinimumWidth(300)
        self.searchBar.setMinimumHeight(30)
        self.searchBar.setAutoFillBackground(True)
        self.searchLineEdit.textChanged.connect(self.on_findButton_clicked)
        self.searchBar.setHidden(True)

        self.titlesOsd = QWidget(self.textEdit)
        titlesOsdLayout = QVBoxLayout()
        self.titlesOsdText = QPlainTextEdit()
        self.titlesOsdText.setReadOnly(1)
        self.titlesOsdText.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.titlesOsdText.setFrameStyle(QFrame.NoFrame)
        titlesOsdLayout.addWidget(self.titlesOsdText)
        self.titlesOsd.setLayout(titlesOsdLayout)
        titlesOsdShadow = QGraphicsDropShadowEffect()
        titlesOsdShadow.setBlurRadius(8)
        titlesOsdShadow.setOffset(3, 3)
        self.titlesOsd.setGraphicsEffect(titlesOsdShadow)
        self.titlesOsd.hide()

        self.osd = QWidget(self.textEdit)
        osdLayout = QVBoxLayout()
        self.osdText = QLabel()
        self.osdText.setTextFormat(Qt.MarkdownText)
        self.osdText.setAlignment(Qt.AlignHCenter)
        osdLayout.addWidget(self.osdText)
        self.osd.setLayout(osdLayout)
        osdShadow = QGraphicsDropShadowEffect()
        osdShadow.setBlurRadius(8)
        osdShadow.setOffset(3, 3)
        self.osd.setGraphicsEffect(osdShadow)
        self.osd.hide()

        self.isFirstTime = True
        self.searchCursors = []

        self.wep = QWebEnginePage()
        self.wep.loadFinished.connect(self.htmlToPdf)
        self.wep.pdfPrintingFinished.connect(self.pdfToQpv)

        self.wev = QWebEngineView()
        self.wev.setHidden(True)
        self.wev.setZoomFactor(1.2)
        self.wev.page().settings().setAttribute(
            QWebEngineSettings.ShowScrollBars, False
        )
        html = """
<!DOCTYPE html>
<html>
  <head>
    <style>
        body{
            background: linear-gradient(0.25turn, #eeeeee, #ffffff, #ffffff, #ffffff, #eeeeee);
        }
    </style>
  </head>
  <body>

  </body>
</html>
"""
        self.wev.setHtml(html)

        self.qpv = PageView()
        self.qpv.setRubberband(Rubberband())
        self.qpv.setMagnifier(Magnifier())
        self.qpv.magnifier().showbutton = Qt.MiddleButton
        self.qpv.magnifier().showmodifier = Qt.NoModifier
        self.qpv.wheelZoomingEnabled = False
        self.qpv.currentPageNumberChanged.connect(self.updatePageIndicator)
        self.qpv.keyPressed.connect(self.qpvKeyPressEvent)
        self.qpv.deleteUnusedOverlayWidgets = True
        self.qpv.linkClicked.connect(self.readlink)
        self.qpv.zoomFactorChanged.connect(self.saveZoomFactor)
        self.qpv.pageLayoutModeChanged.connect(self.qpvUpdated)
        self.qpv.clickedPosition.connect(self.insertClickedPosition)
        self.qpv.doubleClickedY.connect(self.scrollTextEdit)
        self.waitLogo = None
        self.qpv.loadPdf(self.memoryPdfFile.name)
                        
        shortcuts = {
            "Ctrl+Shift+P": self.showSettings,
            "Ctrl+Shift+F": self.showFontSelection,
            "Ctrl+S": lambda: self.saveText(0),
            "F1": lambda: self.alternateViews(True, False),
            "F2": lambda: self.alternateViews(False, True),
            "F3": self.showFileExplorer,
            "Ctrl+W": lambda: self.exportNotify(0),
            "Ctrl+Shift+E": self.savePdfAs,
            "F6": self.showFormulaPreview,
            "F4": self.showHideSpecialChar,
            "F5": self.showHideImageBar,
            "F11": self.toggleFullScreen,
            "Ctrl+D": self.svgEdit,
            "F8": self.toggleHelp,
            "Alt+V": self.vector,
            "Alt+A": self.tabAvancement,
            "Alt+T": self.insertTable,
            "Ctrl+Shift+S": self.saveasDoc,
            "Ctrl+I": self.imageEditor,
            "Ctrl+Space": self.insertSpace,
            "Ctrl+Return": self.insertNewPage,
            "Alt+Return": self.insertNewLine,
            "F9": self.iconViewer,
            "Ctrl+F": self.showHideSearchBar,
            "Alt+L": self.insertLink,
            "Alt+$": self.insertLatex,
            "Alt+I": self.insertImage,
            "Ctrl+Alt+B": lambda: self.insertBlock("B"),
            "Ctrl+Alt+W": lambda: self.insertBlock("W"),
            "Ctrl+Alt+V": lambda: self.insertBlock("V"),
            "Ctrl+Alt+H": lambda: self.insertBlock("H"),
            "Ctrl+Alt+C": lambda: self.insertBlock("C"),
            "Ctrl+Alt+R": lambda: self.insertBlock("R"),
            "Ctrl+Alt+L": lambda: self.insertBlock("L"),
            "Ctrl+Alt+P": lambda: self.insertBlock("P"),
            "Ctrl+Alt+G": lambda: self.insertBlock("G"),
            "Ctrl+Alt+M": lambda: self.insertBlock("M"),
            "Ctrl+Alt+F": lambda: self.insertBlock("F"),
            "Ctrl+Alt+D": lambda: self.insertBlock("D"),
            "Ctrl+O": self.openDoc,
            "Ctrl+N": self.newDoc,
            "Ctrl+P": self.handlePrint,
            "Alt+R": self.insertCrossReference,
            "Shift+F8": self.showHideHelp,
            "Alt+B": lambda: self.changeSelected(0),
            "Alt+*": lambda: self.changeSelected(1),
            "Ctrl+Shift+W": lambda: self.exportNotify(1),
            "Ctrl+M": self.molview,
            "Alt+U": lambda: self.changeSelected(2),
            "Alt+P": lambda: self.changeSelected(3),
            "Alt+M": self.insertTextModifier,
            "Alt+S": lambda: self.changeSelected(4),
            "Ctrl+T": self.showTitlesWindow,
            "Alt+G": self.insertMusic,
            "Alt+Q": self.evaluateCalc,
            "Alt+E": self.insertChemEq,
            "Alt+X": self.insertLatexTimes,
            "Alt+F": lambda: self.insertLatexFrac(0),
            "Alt+D": lambda: self.insertLatexFrac(1),
            "Alt+N": self.insertLatexUnits,
            "Alt+C": self.insertColor,
            "Ctrl+L": self.showHideLineNumbers,
        }

        for key in shortcuts:
            shortcut = QShortcut(QKeySequence(self.tr(key, "")), self)
            shortcut.activated.connect(shortcuts[key])

        shortcuts = {
            "QKeySequence.ZoomIn": self.increaseFont,
            "QKeySequence.ZoomOut": self.decreaseFont,
        }

        for sequence in shortcuts:
            shortcut = QShortcut(eval(sequence), self)
            shortcut.activated.connect(shortcuts[sequence])

        self.fsv = FileSystemView("/home/nicolas/")
        self.fsv.fileSelected.connect(self.fileIsSelected)
        self.fsv.setHidden(1)

        editBox = QVBoxLayout()
        editWidget = QWidget()
        editBox.addWidget(self.fsv)
        editBox.addWidget(self.specialScrollArea)
        editBox.addWidget(self.textEdit)
        editBox.addWidget(self.imageScrollArea)
        editBox.setContentsMargins(0, 0, 0, 0)
        editWidget.setLayout(editBox)
        viewLayout = QVBoxLayout()
        viewLayout.addWidget(self.wev)
        viewLayout.setContentsMargins(0, 0, 0, 0)
        self.mainGrid.addWidget(editWidget, 0, 0)
        self.mainGrid.addWidget(self.qpv, 0, 1)
        self.mainGrid.setContentsMargins(0, 0, 0, 0)
        vbox.addLayout(viewLayout)
        vbox.addLayout(self.mainGrid)
        vbox.setContentsMargins(0, 0, 0, 0)
        self.setLayout(vbox)
        screen = QApplication.primaryScreen()
        self.screenSize = screen.size()
        if all([wwidth, wheight]):
            self.resize(wwidth, wheight)
        else:
            self.resize(
                int(self.screenSize.width() * 0.7), int(self.screenSize.height() * 0.7)
                )
        self.setFocusPolicy(Qt.StrongFocus)
        self.setWindowTitle("wRiTe -- No file")
        
        if first_run:
            self.showHideHelp()
        if self.md_file:
            QTimer.singleShot(500, self.loadText)


    def insertClickedPosition(self, x, y):
        cursor = self.textEdit.textCursor()
        block = cursor.block().text()[:cursor.positionInBlock()]
        if "draw_line(" in block or "draw_polyline(" in block \
                or "draw_curve(" in block or "draw_bezier" in block:
            self.textEdit.insertPlainText("({}, {}), ".format(x, y))
        elif "draw_rect((" in block or "draw_quad((" in block:
            self.textEdit.insertPlainText("{}, {}, ".format(x, y))
        else:
            self.textEdit.insertPlainText("({}, {})".format(x, y))


    def showWaitLogo(self):
        self.waitLogo = QLabel(self)
        self.waitLogo.setFixedSize(80, 80)
        self.movie = QMovie(os.path.join(self.APP_PATH, "wait.gif"))
        self.movie.setScaledSize(self.waitLogo.size())
        self.waitLogo.setMovie(self.movie)
        self.movie.start()
        self.waitLogo.move(
                self.mainGrid.geometry().topRight() \
                        - QPoint(
                            self.waitLogo.width() + 20, 
                            -10
                            )
            )
        self.waitLogo.show()


    def hideWaitLogo(self):
        self.movie.stop()
        self.waitLogo.hide()


    def qpvUpdated(self):
        if self.firstLoad:
            return
        if self.isIndexing:
            return
        if not self.loadTextIsFinished or not self.highlightIsFinished:
            QTimer.singleShot(200, self.qpvUpdated)
            return
        QTimer.singleShot(0, self.indexPdf)


    def selectImage(self, key):
        pbutton = None
        nbutton = None

        if key == Qt.Key_Right:
            if self.selectedImage < self.totalImages-1:
                nbutton = self.imageGrid.itemAtPosition(0, self.selectedImage+1).widget()
                self.selectedImage += 1
                self.imageScrollArea.ensureWidgetVisible(nbutton, 200, 0)
            if self.selectedImage > 0:
                pbutton = self.imageGrid.itemAtPosition(0, self.selectedImage-1).widget()

        elif key == Qt.Key_Left:
            if self.selectedImage > 0:
                nbutton = self.imageGrid.itemAtPosition(0, self.selectedImage-1).widget()
                pbutton = self.imageGrid.itemAtPosition(0, self.selectedImage).widget()
                self.imageScrollArea.ensureWidgetVisible(nbutton, 200, 0)
                self.selectedImage -= 1

        elif key == Qt.Key_Return:
            button = self.imageGrid.itemAtPosition(0, self.selectedImage).widget()
            self.onClickInsertImage(False, button)
            self.imageScrollArea.setFocus(True)

        if nbutton is not None:
            nbutton.setStyleSheet(
            (
                "QToolButton {background-color: #D9FBDF; border: none;}"
            )
                )
        if pbutton is not None:
            pbutton.setStyleSheet("")

        self.imageGrid.update()


    def selectSpecialChar(self, key):
        maxRow, maxCol, total = self.specialCharTableDim
        cRow, cCol = self.selectedSpecialChar
        
        if key == Qt.Key_Right:
            if cCol < maxCol and (cRow*(maxCol+1)+cCol+1) < total:
                self.selectedSpecialChar = (cRow, cCol+1)
            elif cCol == maxCol:
                if cRow < maxRow:
                    self.selectedSpecialChar = (cRow+1, 0)
            else:
                self.selectedSpecialChar = (0, 0)
        elif key == Qt.Key_Left:
            if cCol > 0:
                self.selectedSpecialChar = (cRow, cCol-1)
            elif cCol == 0:
                if cRow > 0:
                    self.selectedSpecialChar = (cRow-1, maxCol)
                else:
                    self.selectedSpecialChar = (maxRow, total-maxRow*(maxCol+1)-1)
        elif key == Qt.Key_Up:
            if cRow-1 >= 0:
                self.selectedSpecialChar = (cRow-1, cCol)
            elif maxRow*(maxCol+1)+cCol+1 > total:
                self.selectedSpecialChar = (maxRow-1, cCol)
            else:
                self.selectedSpecialChar = (maxRow, cCol)
        elif key == Qt.Key_Down:
            if cRow+1 <= maxRow and (cRow+1)*(maxCol+1)+cCol < total:
                self.selectedSpecialChar = (cRow+1, cCol)
            else:
                self.selectedSpecialChar = (0, cCol)
        elif key == Qt.Key_Return:
            self.insertSpecialChar(self.specialCharText)
            self.specialScrollArea.setFocus(True)

        self.updateSpecialScrollArea()


    def updateSpecialScrollArea(self):
        self.specialCharsTable = {}
        SM = SManager()
        specials = SM.get_setting('Specials')
        specialGrid = QGridLayout()
        specialGrid.setContentsMargins(0, 0, 0, 5)
        specialchar = QWidget()
        btnsize = 28
        maxcol = int(self.textEdit.width() / (btnsize + 10))
        row = 0
        col = -1
        totalchar = 0

        for i in range(len(specials)):
            if col > maxcol:
                row += 1
                col = 0
            else:
                col += 1
            button = QPushButton()
            qf = QFont()
            qf.setFamilies([self.textEditFont, "Noto Color Emoji"])
            qf.setPointSize(16)
            button.setText(specials[i])
            button.setFont(qf)
            size = QSize(btnsize, btnsize)
            button.setMinimumSize(size)
            button.setMaximumSize(size)
            button.setFlat(1)

            if i == self.selectedFavorite:
                button.setStyleSheet(
                    "QPushButton {background-color: #82ffa4;"
                    " border: 1px outset black; }"
                    )
            if (row, col) == self.selectedSpecialChar:
                self.specialCharText = button.text()
                button.setStyleSheet(
                    "QPushButton {background-color: lightgreen;"
                    " border: 1px outset black; }"
                    )
            button.pressed.connect(self.insertSpecialChar)
            specialGrid.addWidget(button, row, col)
            self.specialCharsTable[specials[i]] = (row, col)
            totalchar += 1
        
        self.specialCharTableDim = (specialGrid.rowCount()-1, specialGrid.columnCount()-1, totalchar)
        specialchar.setLayout(specialGrid)
        specialchar.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.specialScrollArea.setWidget(specialchar)
        self.specialScrollArea.setContentsMargins(0, 0, 0, 0)
        self.selectedFavorite = -1


    def showSettings(self):
        self.modalWindowOpened = True
        currentFont = self.textEdit.font().family()
        currentSize = str(self.textEditFontSize)
        wSettings = Settings(currentFont, currentSize)
        wSettings.setModal(True)
        wSettings.textEditFontChanged.connect(self.setTextEditFont)
        wSettings.textEditBgColorChanged.connect(self.updateTextEditBgColor)
        wSettings.exec_()
        self.modalWindowOpened = False

    def setTextEditFont(self, font, fontsize):
        TEFont = QFont()
        TEFont.setPointSize(int(fontsize))
        TEFont.setFamily(font)
        self.textEdit.setFont(TEFont)
        self.textEditFont = font
        self.textEditFontSize = fontsize

    def updateTextEditBgColor(self, color):
        self.textEditBgColor = color
        self.textEdit.setStyleSheet(
            (
                "QPlainTextEdit{{background-color: {}; selection-color: white;"
                " selection-background-color: forestgreen;}} "
                "QToolTip {{border: 1px solid green; "
                "border-radius: 3px; padding: 2px;"
                " min-width: 550px; background-color:#efefef;}}".format(self.textEditBgColor)
            )
        )

    def showFontSelection(self):
        favoritesOpened = self.specialScrollArea.isVisible()
        fontselection = FontSelection(self.textEditFont, favoritesOpened)
        fontselection.sendSelectedEmoji.connect(self.insertSelectedEmoji)
        fontselection.favoritesUpdated.connect(self.favoritesUpdated)
        fontselection.favoritesPositionChanged.connect(self.highlightSelectedFavorite)
        fontselection.favoritesWindowToggled.connect(self.reHideSpecialChars)
        fontselection.exec_()

    def insertSelectedEmoji(self, emoji):
        self.textEdit.insertPlainText(emoji)

    def favoritesUpdated(self):
        if self.specialScrollArea.isHidden():
            self.specialScrollArea.setVisible(True)
        self.updateSpecialScrollArea()

    def highlightSelectedFavorite(self, val, state):
        if self.specialScrollArea.isHidden() and state:
            self.specialScrollArea.setVisible(True)
        self.selectedFavorite = val
        self.updateSpecialScrollArea()

    def reHideSpecialChars(self):
        self.specialScrollArea.setHidden(True)

    def showHideHelp(self):

        if not self.helpIsShown:
            self.pdfLoaded = False
            self.qpv.loadPdf(os.path.join(self.APP_PATH, "help.pdf"))
            self.qpv.setViewMode(qpageview.FitWidth)
        else:
            self.qpv.loadPdf(self.memoryPdfFile.name)
            self.pdfLoaded = True
            QTimer.singleShot(10, self.indexPdf)

        self.helpIsShown = not self.helpIsShown

    def savePdfAs(self):
        saveFile = str(
            QFileDialog.getSaveFileName(
                self,
                "Export PDF file as...",
                QDir.currentPath(),
                self.tr("PDF files (*.pdf )"),
            )[0]
        )

        if saveFile:
            if saveFile.split(".")[-1] not in "pdf":

                if saveFile[-1] != ".":
                    saveFile = "".join((saveFile, ".pdf"))
                else:
                    saveFile = "".join((saveFile, "pdf"))

            QTimer.singleShot(0, lambda: self.exportPdf(saveFile))
        return

    def showHideLineNumbers(self):
        self.textEdit.showLineNumbers = not self.textEdit.showLineNumbers
        self.textEdit.updateLineNumberAreaWidth(0)

    def setPwd(self, pwdtext):
        self.password = pwdtext

    def replaceExpression(self):

        if not self.replaceLineEdit.text().strip():
            return

        newText = self.replaceLineEdit.text()

        if self.searchCursors and self.currentSearchCursor > 0:
            cursor = self.searchCursors[self.currentSearchCursor - 1]
            cursor.removeSelectedText()
            cursor.insertText(newText)
            QTimer.singleShot(10, self.searchTextEdit)

    def showReplaceLineEdit(self):
        if self.replaceLineEdit.isHidden():
            self.replaceLineEdit.setVisible(True)
            self.replaceLineEdit.setFocus()
            self.textEdit.replaceBarIsShown = True
            self.searchBar.setMinimumHeight(80)
            self.replacePushButton.setMinimumHeight(70)

        else:
            self.replaceLineEdit.setHidden(True)
            self.textEdit.replaceBarIsShown = False
            self.searchBar.setMinimumHeight(40)
            self.searchBar.setMaximumHeight(40)
            self.replacePushButton.setMinimumHeight(30)

        self.searchBar.adjustSize()


    def hlFinished(self):
        self.highlightIsFinished = True


    def loadTextFinished(self):
        self.loadTextIsFinished = True
        self.textEdit.bqrgbcolor = self.bqrgbcolor
        self.textEdit.initializeHighlighter()

        if not self.md_file:
            return

        if self.justLoaded:
            self.justLoaded = False
            self.createVars(self.md_file)
            QTimer.singleShot(10, lambda: self.updateCompleter(Qt.Key_Space))
            QTimer.singleShot(10, self.countTitles)
            if os.path.exists(self.pdf_file):
                pdf = fitz.open(self.pdf_file)
                pdfHash = pdf.metadata['keywords']
                pdf.close()
                if self.textHash == pdfHash:
                    self.firstCompilationNotNeeded = True
                    with open(self.pdf_file, 'rb') as f:
                        with open(self.memoryPdfFile.name, 'wb') as temp_pdf:
                            temp_pdf.write(f.read())
                        self.qpv.setViewMode(qpageview.FitWidth)
                        try:
                            self.qpv.reload()
                            self.pdfLoaded = True
                        except Exception:
                            self.pdfLoaded = False
            self.textToHtml()
            

    def saveZoomFactor(self):
        leftPanel = self.mainGrid.itemAtPosition(0, 0).widget()
        if leftPanel.isHidden():
            self.lastZoomApplied = self.qpv.zoomFactor()


    def fileIsSelected(self, filePath, copyLocally):

        if not os.path.exists(filePath):
            return

        extension = filePath.split(".")[-1].strip().lower()

        if not extension:
            return

        if extension in ["md", "txt"]:
            self.md_file = filePath
            self.createVars(filePath)
            QTimer.singleShot(0, self.showFileExplorer)
            QTimer.singleShot(10, self.loadText)
            self.generated = True
            self.setWindowTitle(
                "wRiTe -- {} -- [{}]".format(self.disp_filename, self.lang)
            )

        elif extension in (
            "jpg",
            "jpeg",
            "gif",
            "svg",
            "png",
            "mp4",
            "avi",
            "mkv",
            "mpeg",
            "mov",
        ):
            cursor = self.textEdit.textCursor()
            cursor.movePosition(QTextCursor.StartOfLine, QTextCursor.KeepAnchor)

            if copyLocally:
                filename = os.path.basename(filePath)
                target = os.path.join(self.data_path, filename)
                copyfile(filePath, target)

                if "!bgimage(" in cursor.selectedText():
                    cursor.clearSelection()
                    self.textEdit.insertPlainText(
                        "{}".format(filename.replace(" ", "%20"))
                    )

                elif "BGIMAGE:" in cursor.selectedText():
                    cursor.clearSelection()
                    self.textEdit.insertPlainText(
                        "{}".format(filename.replace(" ", "%20"))
                    )
                else:
                    self.textEdit.insertPlainText(
                        "![]({})".format(filename.replace(" ", "%20"))
                    )
            else:
                if "!bgimage(" in cursor.selectedText():
                    cursor.clearSelection()
                    self.textEdit.insertPlainText(
                        "{}".format(filePath.replace(" ", "%20"))
                    )
                elif "BGIMAGE:" in cursor.selectedText():
                    cursor.clearSelection()
                    self.textEdit.insertPlainText(
                        "{}".format(filePath.replace(" ", "%20"))
                    )
                else:
                    self.textEdit.insertPlainText(
                        "![]({})".format(filePath.replace(" ", "%20"))
                    )
            QTimer.singleShot(0, self.showFileExplorer)

        elif extension in ["data", "csv"]:
            if copyLocally:
                filename = os.path.basename(filePath)
                target = os.path.join(self.data_path, filename)
                copyfile(filePath, target)
                self.textEdit.insertPlainText(
                    'varsList = np.loadtxt("{}", skiprows=1, unpack=True)'.format(
                        filename
                    )
                )
            else:
                if os.path.dirname(filePath) == self.data_path:
                    self.textEdit.insertPlainText(
                        'varsList = np.loadtxt("{}", skiprows=1, unpack=True)'.format(
                            os.path.basename(filePath)
                        )
                    )
                else:
                    self.textEdit.insertPlainText(
                        'varsList = np.loadtxt("{}", skiprows=1, unpack=True)'.format(
                            filePath
                        )
                    )
            QTimer.singleShot(0, self.showFileExplorer)

        elif extension in "pdf":
            content = ""
            pdf = fitz.open(filePath)
            i = 0

            for page in range(pdf.page_count):
                images_names = []
                totimg = 0

                if copyLocally:
                    imgxref_list = pdf.get_page_images(page)
                    for xref in imgxref_list:
                        xref = xref[0]
                        img = pdf.extract_image(xref)
                        imgfile = os.path.join(
                            self.data_path,
                            "{}_extracted_image_{}.{}".format(
                                os.path.basename(filePath), i, img["ext"]
                            ),
                        )
                        images_names.append(os.path.basename(imgfile))

                        with open(imgfile, "wb") as f:
                            f.write(img["image"])
                        i += 1
                        totimg += 1

                img = 0

                for block in pdf[page].get_text("blocks"):

                    if not block[6]:
                        content = "".join((content, block[4], "\n"))
                    elif block[6] == 1:
                        if copyLocally:
                            if img < totimg:
                                content = "".join(
                                    (
                                        content,
                                        "\n\n![]({})\n\n".format(images_names[img]),
                                    )
                                )
                                img += 1
                        else:
                            content = "".join(
                                (content, "\n\n%%IMAGE NOT EXTRACTED\n\n")
                            )
            self.textEdit.insertPlainText(content)
            pdf.close()
            QTimer.singleShot(0, self.showFileExplorer)
        else:
            if copyLocally:
                filename = os.path.basename(filePath)
                target = os.path.join(self.data_path, filename)
                copyfile(filePath, target)
                self.textEdit.insertPlainText(filename)
            else:
                self.textEdit.insertPlainText(filePath)
            QTimer.singleShot(0, self.showFileExplorer)

    def showFileExplorer(self):
        leftPanel = self.mainGrid.itemAtPosition(0, 0).widget()
        if leftPanel.isVisible():
            self.fsv.setVisible(self.fsv.isHidden())


    def readlink(self, event, page, link):
        linkurl = link.url.strip()
        linkarea = link.area
        links = []

        #  Hack beause of QPV bug, retreive links for files by comparing Rects
        if not linkurl:
            links = [p for p in self.links if page == p[0]]

            for link in links:
                delta = (
                    (linkarea.left - link[1])
                    + (linkarea.top - link[2])
                    + (linkarea.right - link[3])
                    + (linkarea.bottom - link[4])
                )

                #  Tolerance
                if abs(delta) < 0.01:
                    linkurl = link[5]
                    break
                else:
                    return

        width = page.width
        height = page.height
        x = int(linkarea.left * width) - 2
        y = int(linkarea.top * height) - 2
        w = round(linkarea.right * width - x) + 2
        h = round(linkarea.bottom * height - y) + 2

        url = QUrl(linkurl)

        if url.isLocalFile():
            filename = url.path()

            if not os.path.exists(filename):
                filename = os.path.join(self.data_path, os.path.basename(filename))
                url = QUrl.fromLocalFile(filename)

                if not os.path.exists(filename):
                    return

        if "http" in linkurl or "file" in linkurl:
            QDesktopServices.openUrl(url)

    def inlineCompletion(self):
        cursor = self.textEdit.textCursor()
        text = cursor.block().text()
        cursor.movePosition(QTextCursor.PreviousCharacter, QTextCursor.KeepAnchor)
        if "){" in text.split("(")[-1] or "!font(" in text:
            
            if cursor.selectedText().isalpha():
                cursor.clearSelection()
                cursor.movePosition(QTextCursor.PreviousCharacter, QTextCursor.KeepAnchor)
                if not cursor.selectedText().isalpha():
                    self.updateCompleter(0)
        elif text in  ("FONT: ", "FONTMATH: ", "FONTTITLE: "):
            self.updateCompleter(0)
        elif "@" in cursor.selectedText():
            self.updateCompleter(1)
        elif cursor.block().text() in ["BGCOLOR: ", "FONTCOLOR: ", "MCOLOR: ", "BQCOLOR: "]:
            self.updateCompleter(1)
        elif ":" in cursor.selectedText():
            self.updateCompleter(2)
        elif "\\" in cursor.selectedText():
            self.updateCompleter(3)


    def evaluateCalc(self):
        cursor = self.textEdit.textCursor()
        expression = cursor.selectedText()
        expression = expression.replace("os.system", "")
        expression = expression.replace("import os", "")
        expression = expression.replace("from os", "")
        expression = expression.replace("shutils", "")
        expression = expression.replace("rm", "")
        expression = expression.replace("-rf", "")
        cursor.setPosition(cursor.selectionEnd())
        result = ""
        try:
            if "latex:" in expression:
                expression = expression[6:]
                result = eval(expression)
                result = f"{result:.3E}"

                if "E+" in result:
                    val, power = result.split("E+")
                    result = "".join(
                        (val.replace(".", ","), "\\times 10^{{{}}}".format(int(power)))
                    )
                else:
                    val, power = result.split("E-")
                    result = "".join(
                        (val.replace(".", ","), "\\times 10^{{-{}}}".format(int(power)))
                    )
            else:
                result = eval(expression)
                result = f"{result:.3E}"
                if 'E+00' in result:
                    result = result.replace('E+00', '')
            self.textEdit.insertPlainText(result)
        except Exception:
            self.textEdit.insertPlainText("💣 expression error...")

    def qpvKeyPressEvent(self, key):
        if key in (Qt.Key_Enter, Qt.Key_Return) and self.qpv.rubberband().hasSelection:
            QTimer.singleShot(0, self.exportSelected)

        if key == Qt.Key_Escape:
            self.qpv.rubberband().clearSelection()
            self.unsetCursor()

    def countTotalLines(self):
        lines = 0
        textblock = self.textEdit.document().begin()

        while textblock.isValid():
            if not textblock.layout():
                continue
            lines += textblock.layout().lineCount()
            textblock = textblock.next()
        self.totalTextEditLines = lines

    def insertLatex(self):
        cursor = self.textEdit.textCursor()
        selection = cursor.selectedText()

        if selection:
            self.textEdit.insertPlainText("${}$".format(selection))
            cursor.movePosition(
                QTextCursor.PreviousCharacter, QTextCursor.MoveAnchor)
            cursor.movePosition(
                QTextCursor.PreviousCharacter, QTextCursor.KeepAnchor, len(selection)
            )
            self.textEdit.setTextCursor(cursor)
        else:
            self.textEdit.insertPlainText("$$")
            cursor.movePosition(QTextCursor.PreviousCharacter)
            self.textEdit.setTextCursor(cursor)

    def toggleHelp(self):
        self.F8 = not self.F8
        self.textEdit.F8 = self.F8
        message = "Help disabled"

        if self.F8:
            message = "Help ativated"

        QTimer.singleShot(
            0,
            lambda: self.showNotificationWindow(
                "**{}**".format(message), "#ed00b5", 2000, -1
            ),
        )
        QTimer.singleShot(10, self.textEdit.tooltip)

    def insertMusic(self):
        text = """!gtab
time=4/4 key=A tuning=standard
notes :q (5/2.5/3.7/4) $A7#9$ 5h6/3 7/4 |
notes :8 7/4 $.italic.sweep$ 6/3 5/2 3v/1 :q 7v/5 $P.H$ :8 3s5/5

notes :8 2/4 1/1 2/4 1/1 :q 3/4 2/3 | :8 2/4 1/1 2/4 1/1 :q 3/4 2/3 |
notes :8 2/4 1/1 2/4 1/1 :q 3/4 2/3 | :8 2/4 1/1 2/4 1/1 :q 3/4 2/3 |
.gtab
"""
        self.textEdit.insertPlainText(text)

    def showTitlesWindow(self):
        leftPanel = self.mainGrid.itemAtPosition(0, 0).widget()

        if leftPanel.isHidden():
            return

        if leftPanel.isVisible() and self.titlesOsd.isVisible():
            self.titlesOsd.hide()
            return

        pdf = fitz.open(self.memoryPdfFile.name)
        characters = len(pdf[0].get_text("text"))
        words = 0
        lines = 0

        for page in pdf:
            w = page.get_text("words")
            w = [e for e in w if len(e[4]) != 1]
            words += len(w)
            lines += len(page.get_text("text").split("\n")[:-1])

        text = self.textEdit.toPlainText().strip()
        titles = re.findall(r"^(#[#!]*\s.+)", text, re.MULTILINE)
        text = """
<h3>¤ Informations ¤</h2>
<hr>
<span style="color: lightgreen;">&nbsp;&nbsp;Lines:&nbsp;&nbsp;{}</span>
<br />
<span style="color: lightblue;">&nbsp;&nbsp;Words:&nbsp;&nbsp;{}</span>
<br />
<span style="color: lightpink;">&nbsp;&nbsp;Characters:&nbsp;&nbsp;{}</span>
<hr>
<h3>¤ Document's Plan ¤</h3>
<hr>
""".format(
            lines, words, characters
        )

        def t2html(match):
            m = match.group(1)

            if "#!" in m:
                return r"<h1>{}</h1>".format(match.group(2))
            else:
                n = len(m) + 1
                return "<h{}>{}</h{}>".format(n, match.group(2), n)

        for title in titles:
            title = re.sub(r"^(#[#!]*)\s+(.+)$", t2html, title)
            text = "".join((text, title, "\n<hr>"))

        self.titlesOsdText.document().setHtml(text)
        self.titlesOsd.setMinimumSize(
            int(self.textEdit.width() * 3 / 7), self.textEdit.height() - 10
        )
        self.titlesOsd.adjustSize()
        self.titlesOsd.move(5, 5)
        self.titlesOsd.setStyleSheet(
            "background-color: dimgray; color: white; border-radius: 4;"
        )
        self.titlesOsdText.setStyleSheet("background-color: dimgray; font-size: 15px;")
        cursor = self.titlesOsdText.textCursor()
        cursor.movePosition(QTextCursor.Start)
        self.titlesOsdText.setTextCursor(cursor)
        self.titlesOsd.show()

    def insertTextModifier(self):
        cur = self.textEdit.textCursor()
        text = cur.selectedText()

        if text:
            text = "".join(("(){", text, "}"))
            self.textEdit.insertPlainText(text)
            cur.movePosition(
                QTextCursor.PreviousCharacter, QTextCursor.MoveAnchor, len(text) - 1
            )
            self.textEdit.setTextCursor(cur)
        else:
            self.textEdit.insertPlainText("(){}")
            cur.movePosition(QTextCursor.PreviousCharacter, QTextCursor.MoveAnchor, 3)
            self.textEdit.setTextCursor(cur)

    def exportToDOCX(self):
        if not which("pandoc"):
            QTimer.singleShot(
                0,
                lambda: self.showNotificationWindow(
                    "**Pandoc not installed ! Aborting...**", "darkred", 5000, -1
                ),
            )
            return

        self.memoryDocx = NamedTemporaryFile(prefix="wRiTeMDtoDocx", suffix=".md")
        content = parseToPandoc(
            self.md_file,
            self.APP_PATH,
            self.TMP_DIR,
            self.textEdit.toPlainText(),
            self.path,
            self.data_path,
        )

        with open(self.memoryDocx.name, "w") as f:
            f.write(content)

        output = run(
            [
                "pandoc",
                "-s",
                self.memoryDocx.name,
                "--reference-doc={}/reference.docx".format(self.APP_PATH),
                "--lua-filter",
                "{}/newpage.lua".format(self.APP_PATH),
                "-o",
                self.docx_file,
            ]
        )
        self.memoryDocx.close()
        QTimer.singleShot(
            0,
            lambda: self.showNotificationWindow(
                "**Success !**", "forestgreen", 2000, -1
            ),
        )

    def showNotificationWindow(self, text, color, duration, posx):
        leftPanel = self.mainGrid.itemAtPosition(0, 0).widget()

        if leftPanel.isHidden():
            return

        if leftPanel.isVisible() and self.osd.isVisible():
            self.osd.hide()

        self.osdText.setText(text)
        self.osd.adjustSize()

        if posx >= 0:
            self.osd.move(posx, 5)
        else:
            self.osd.move(
                int(
                    self.textEdit.width() / 2
                    - self.osd.width() / 2
                    - self.textEdit.verticalScrollBar().width()
                ),
                5,
            )

        self.osd.setStyleSheet(
            "background-color: {}; color: white; border-radius: 5;".format(color)
        )
        self.osdText.setStyleSheet("background-color: {};".format(color))
        self.osd.show()
        QTimer.singleShot(duration, self.hideNotificationWindow)

    def hideNotificationWindow(self):
        self.osd.hide()

    def scrollToFoundText(self, s):
        if not self.searchCursors:
            return

        if self.currentSearchCursor >= len(self.searchCursors):
            self.currentSearchCursor = 0
        elif self.currentSearchCursor < 0:
            self.currentSearchCursor = len(self.searchCursors) - 1

        hlcursor = self.searchCursors[self.currentSearchCursor]
        nblock = hlcursor.block().blockNumber()
        line = (
            hlcursor.block()
            .layout()
            .lineForTextPosition(hlcursor.block().position())
            .lineNumber()
        )

        try:
            value = tuple(self.scrollDict.keys()).index((nblock, line))
            self.textEdit.verticalScrollBar().setValue(value - 2)
        except Exception:
            pass

        if self.replaceLineEdit.isHidden():
            self.searchLineEdit.setFocus()

        if s == ">":
            self.currentSearchCursor += 1
        elif s == "<":
            self.currentSearchCursor -= 1

    def finishedResizing(self):
        if self.lock:
            return
        self.lock = True
        self.timer.stop()
        self.lastSize = self.size()
        self.timer.start(500)

    def isResizingFinished(self):
        if self.size() == self.lastSize:
            self.timer.stop()
            self.lock = False
            QTimer.singleShot(50, self.qpvUpdated)
        else:
            self.lock = False
            QTimer.singleShot(0, self.finishedResizing)

    def resizeEvent(self, event):
        QTimer.singleShot(0, self.updateSpecialScrollArea)
        leftPanel = self.mainGrid.itemAtPosition(0, 0).widget()
        rightPanel = self.mainGrid.itemAtPosition(0, 1).widget()

        if leftPanel.isVisible() and rightPanel.isVisible():
            if not self.qpv.pageLayout().empty():
                QTimer.singleShot(0, self.finishedResizing)

        if leftPanel.isVisible():
            self.searchBar.move(
                self.textEdit.width()
                - self.searchBar.width()
                - self.textEdit.verticalScrollBar().width(),
                2,
            )

        if self.osd.isVisible():
            posx = self.osd.pos().x()
            if posx > 5:
                self.osd.move(
                    int(
                        self.textEdit.width() / 2
                        - self.osd.width() / 2
                        - self.textEdit.verticalScrollBar().width()
                    ),
                    5,
                )

    def handlePrint(self):
        printer = QPrinter(QPrinter.HighResolution)
        printdialog = QPrintDialog(printer, self)
        printdialog.setModal(True)
        printdialog.setOptions(
            QAbstractPrintDialog.PrintCurrentPage
            | QAbstractPrintDialog.PrintPageRange
            | QAbstractPrintDialog.PrintShowPageSize
            | QAbstractPrintDialog.PrintCollateCopies
        )

        if printdialog.exec_() == QPrintDialog.Accepted:
            self.qpv.print(printer=printer, showDialog=False)

    def createTitlesRegister(self):
        #  create numbered titles register
        cursor = self.textEdit.textCursor()
        pos = cursor.position()
        text = self.textEdit.toPlainText()[:pos]
        titles = re.findall(
            r"^#[#]*\s[a-zA-Z\d]+[).-]+[)-.a-zA-Z\d]*\s", text, re.MULTILINE
        )
        titlesRegister = [0]

        if titles:
            lastindex = 0
            for t in titles:
                index, nums, pattern = getPattern(t)

                if not index:
                    titlesRegister[0] = 1

                if index and nums and pattern:
                    if (l := len(titlesRegister)) <= index:
                        titlesRegister += [""] * (index + 1 - l)
                    titlesRegister[index] = [nums, pattern]

                    if index < lastindex and len(titlesRegister) > index + 1:
                        titlesRegister[index + 1] = [
                            reinitVal(titlesRegister[index + 1][0]),
                            titlesRegister[index + 1][1],
                        ]
                        if len(titlesRegister[index + 1][0]) > 1:
                            for i in range(len(titlesRegister[index + 1][0][:-1])):
                                titlesRegister[index + 1][0][i] = increment(
                                    titlesRegister[index + 1][0][i]
                                )
                lastindex = index
            self.lastTitleNum = lastindex
        return titlesRegister

    def updateTitleNumbers(self):
        cursor = self.textEdit.textCursor()
        pos = cursor.position()
        curline = cursor.block().text()

        titleInLine = re.findall(r"^#[#]*\s[a-zA-Z\d]+[).-]+[)-.a-zA-Z\d]*\s", curline)

        if titleInLine:
            index, nums, pattern = getPattern(curline)
            text = self.textEdit.toPlainText()
            end = -1

            if index > 1:
                p = re.compile(r"^{}".format("#" * (index - 1) + r"\s.*"), re.MULTILINE)
                posp = (m for m in p.finditer(text, pos))
                if posp:
                    end = posp[0].start()

            diff = text[pos:end]

            p = r"^{}\s([a-zA-Z\d]+[).-]+[)-.a-zA-Z\d]*)\s(.*)".format("#" * index)
            p = re.compile(p, re.MULTILINE)

            def inctn(match):
                nums[-1] = increment(nums[-1])
                s = "{} {} {}".format(
                    "#" * index, pattern.format(*nums), match.group(2)
                )
                return s

            diff = p.sub(inctn, diff)
            cursor.setPosition(pos)
            cursor.movePosition(
                QTextCursor.NextCharacter, QTextCursor.KeepAnchor, len(diff)
            )
            cursor.insertText(diff)
            cursor.setPosition(pos)
            self.textEdit.setTextCursor(cursor)

        self.countTitles()
        cursor.movePosition(QTextCursor.Left)
        cursor.movePosition(QTextCursor.Right, QTextCursor.KeepAnchor, 2)


    def insertNewTitle(self, ok):
        if not ok or self.titleComplete:
            return

        # Insère le nouveau titre si modèle précédent présent
        cursor = self.textEdit.textCursor()
        pos = cursor.position()
        text = self.textEdit.toPlainText()[:pos]
        n = text.count("```")

        if not n % 2:
            curline = cursor.block().text()
            titleInLine = re.findall(r"^(#[#]*)\s$", curline)
            titlesRegister = self.createTitlesRegister()

            if titleInLine:
                index = len(titleInLine[0])
                if len(titlesRegister) >= index + 1 and titlesRegister[index]:
                    nums, pattern = titlesRegister[index]
                    nums[-1] = increment(nums[-1])
                    try:
                        s = pattern.format(*nums)
                        self.textEdit.insertPlainText("".join((s, " ")))
                        self.titleComplete = 1
                        self.titlesCount += 1
                    except Exception:
                        pass
                    self.updateTitleNumbers()

    def countTitles(self):
        text = self.textEdit.toPlainText()
        self.titlesCount = len(
            re.findall(
                r"^#[#]*\s[a-zA-Z\d]+[).-]+[)-.a-zA-Z\d]*\s", text, re.MULTILINE
                )
            )

    def toggleSpellCheck(self, key):
        if key == Qt.Key_F7:
            self.textEdit.highlighter.spellCheck = (
                not self.textEdit.highlighter.spellCheck
            )
            self.textEdit.highlighter.rehighlight()

    def insertTable(self):
        align = "c"
        patterns = {"c": ":-:", "l": ":- ", "r": " -:"}
        head = 0
        left = 0
        tab = ""
        cursor = self.textEdit.textCursor()
        pos = cursor.position()
        text = cursor.selectedText().strip()

        if not text or "x" not in text:
            return

        if "hw" in text or "wh" in text or "lh" in text or "hl" in text:
            text = re.sub("[hw]+", "", text)
            left = 1
        elif "h" in text or "ht" in text or "th" in text:
            text = re.sub("[ht]+", "", text)
            head = 1

        if "l" in text:
            align = "l"
            text = text.replace("l", "")
        elif "r" in text:
            align = "r"
            text = text.replace("r", "")

        row, col = text.split("x")

        if not row.isdigit() or not col.isdigit():
            return

        row, col = int(row), int(col)

        if head:
            tab = "|"
            for j in range(col):
                tab = "".join((tab, "    |"))
            tab = "".join((tab, "\n"))
        elif left:
            tab = "|@@#f8f8f8|\n"
            col += 1

        tab = "".join((tab, "|"))

        for j in range(col):
            tab = "".join((tab, "{}|".format(patterns[align])))

        tab = "".join((tab, "\n"))

        if row > 0:
            tab = "".join((tab, "|"))

        for i in range(row):
            for j in range(col):
                if left and not j:
                    tab = "".join((tab, " ****  |"))
                else:
                    tab = "".join((tab, "    |"))
            tab = "".join((tab, "\n"))
            if i < row - 1:
                tab = "".join((tab, "|"))
        self.textEdit.insertPlainText(tab)
        cursor.setPosition(pos+3)
        self.textEdit.setTextCursor(cursor)

    def insertColor(self):
        color = QColorDialog.getColor()

        if color.isValid():
            color = color.name()
            if color.upper() in tuple(colors.values()):
                color = tuple(colors.keys())[tuple(colors.values()).index(color.upper())]
            self.textEdit.insertPlainText(color)

    def updateCompleter(self, key):
        cursor = self.textEdit.textCursor()
        cursor.select(QTextCursor.LineUnderCursor)
        text = cursor.selectedText()
        
        if not key:
            fontBase = QFontDatabase()
            systemFonts = fontBase.families()
            systemFonts = [
                font.split("[")[0].strip() if "[" in font else font
                for font in systemFonts
                ]
            
            if "FONT" not in text:
                systemFonts = [
                    font.strip().replace(" ", ".") if " " in font else font
                    for font in systemFonts
                ]
            popup = self.completer.popup()
            popup.setStyleSheet(
                "background-color: white; selection-color: white; selection-background-color: seagreen;"
            )
            model = self.completer.model()
            model.setStringList(systemFonts)
            popup.setVisible(False)
            return

        elif key == 1:
            colorCompletion = [c for c in tuple(colors) if len(c) > 2]
            popup = self.completer.popup()
            popup.setStyleSheet(
                "background-color: white; selection-color: white; selection-background-color: steelblue;"
            )
            model = self.completer.model()
            model.setStringList(colorCompletion)
            popup.setVisible(False)
            return

        elif key == 2:
            popup = self.completer.popup()
            popup.setStyleSheet(
                "background-color: white; selection-color: black; selection-background-color: #ffd75e;"
            )
            model = self.completer.model()
            model.setStringList(fa_icon_names)
            popup.setVisible(False)
            return
        
        elif key == 3:
            popup = self.completer.popup()
            popup.setStyleSheet(
                "background-color: white; selection-color: black; selection-background-color: cyan;"
            )
            model = self.completer.model()
            model.setStringList(KaTeX_commands)
            popup.setVisible(False)
            return

        addcompletion = (
            "LINE: ",
            "MCOLOR: ",
            "FONTCOLOR: ",
            "BGCOLOR: ",
            "FONT: ",
            "FONTMATH: ",
            "FONTTITLE: ",
            "LANG: ",
            "EXPORTED_PAGES: ",
            "FONTSIZE: ",
            "PAGENUM: ",
            "MODE: ",
            "MARGINS: ",
            "STYLE: ",
            "MLEFT: ",
            "MRIGHT: ",
            "MTOP: ",
            "MBOTTOM: ",
            "DIAGRAM: ",
            "HIGHLIGHT: ",
            "SIZE: ",
            "PADDING: ",
            "PTOP: ",
            "PLEFT: ",
            "PRIGHT: ",
            "PBOTTOM: ",
            "BGIMAGE: ",
            "BGGRADIENT: ",
            "AUTHOR: ",
            "TITLE: ",
            "ENCRYPT: ",
            "BQCOLOR: ",
            "newpage",
            "wFitCurve()",
            "wParams()",
            "wBar()",
            "wPie()",
            "wTangent()",
            "wFunction()",
            "wFill()",
            "wDots()",
            "wAngle()",
            "wIntersect()",
            "wBigdot()",
            "wLine()",
            "wPolygon()",
            "wLength()",
            "wCurve()",
            "wCircle()",
            "wRectangle()",
            "wEllipse()",
            "wText()",
            "wArrow()",
            "wLinearfit()",
            "wInteg()",
            "wDeriv()",
            "bgimage()",
            "bggradient()",
            "box",
            "center",
            "vcenter",
            "left",
            "right",
            "vhcenter",
            "wbox",
            "gtab",
            "diagram",
            "code",
            "graph",
            "font",
            "footer",
            "header",
            "pageframe",
            "muPDF",
            "graphPaper",
            "draw_bezier(",
            "draw_arrow(",
            "draw_milli(",
            "draw_circle(",
            "draw_curve(",
            "draw_line(",
            "draw_oval(",
            "draw_polyline(",
            "draw_quad(",
            "draw_rect(",
            "draw_sector(",
            "draw_squiggle(",
            "draw_zigzag(",
            "insert_text(",
            "insert_textbox(",
            "show_grid()",
        )

        if key == Qt.Key_Space:
            completionwords = [
                word
                for word in re.findall(r"\b(\w+)\b", self.textEdit.toPlainText())
                if (len(word) > 4)
            ]
            completionwords += addcompletion
            popup = self.completer.popup()
            popup.setStyleSheet(
                "background-color: white; selection-color: white; selection-background-color: #555555;"
            )
            model = self.completer.model()
            model.setStringList(tuple(set(completionwords)))
            popup.setVisible(False)
                        

    def showFormulaPreview(self):
        self.wev.setMaximumHeight(int(self.textEdit.height() / 6.2))
        self.wev.setMinimumHeight(int(self.textEdit.height() / 6.2))

        if self.wev.isVisible():
            self.wev.setHtml("")
            self.wev.setHidden(True)
        else:
            self.wev.setVisible(True)
            QTimer.singleShot(0, self.formulaPreview)

    def formulaPreview(self):
        if self.wev.isHidden():
            return

        if not which("multimarkdown"):
            QTimer.singleShot(
                0,
                lambda: self.showNotificationWindow(
                    "**Multimarkdown not installed ! Aborting...**", "darkred", 5000, -1
                ),
            )
            return

        content = ""
        cursor = self.textEdit.textCursor()
        position = cursor.position()
        start = self.textEdit.toPlainText().rfind("$", 0, position)
        end = self.textEdit.toPlainText().find("$", position)

        if start >= 0 and end > 0 and (end - start) > 1:
            if (
                "$" in self.textEdit.toPlainText()[start - 1 : start]
                and "$" in self.textEdit.toPlainText()[end + 1 : end + 2]
            ):

                if not self.textEdit.toPlainText()[: start - 1].count("$$") % 2:
                    content = "".join(
                        (
                            "$$",
                            self.textEdit.toPlainText()[start + 1 : end].strip(),
                            "$$",
                        )
                    )
                    content = content.replace("\n", "")
            else:
                if not self.textEdit.toPlainText()[:start].count("$") % 2:
                    content = "".join(
                        ("$", self.textEdit.toPlainText()[start + 1 : end].strip(), "$")
                    )
                    content = content.replace("\n", "")
        else:
            self.wev.setHtml("")
            return

        try:
            output = run(
                ["multimarkdown", "-s", "--unique", "--lang=fr"],
                input=bytes(content, "utf8"),
                capture_output=True,
            )

            if not output.returncode:
                html = format(output.stdout.decode("utf8"))
            else:
                html = "<h1>ERREUR MULTIMARKDOWN !</h1>"
        except Exception:
            print("Conversion showFormula error!")
            return

        head = """
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="{}/katex/katex.css"></link>
<script src="{}/katex/katex.js"></script>
<script src="{}/katex/contrib/auto-render.min.js"></script>
<script src="{}/katex/contrib/mhchem.min.js"></script>
<style>
    * {{
        margin: 0;
        padding: 0;
    }}
    body{{
        background: linear-gradient(0.25turn, #eeeeee, #ffffff, #ffffff, #ffffff, #eeeeee);
    }}
</style>
</head>
<body>
<p><span class="math">\[\\global\\def\\dv#1#2{{\\dfrac{{\\textrm{{d}}#1}}{{\\textrm{{d}}#2}}}}\]</span></p>
<p><span class="math">\[\\global\\def\\e{{\\mathrm{{e}}}}\]</span></p>
<p><span class="math">\[\\global\\def\\d{{\\mathrm{{d}}}}\]</span></p>
""".format(
            self.APP_PATH, self.APP_PATH, self.APP_PATH, self.APP_PATH
        )
        ending = "<script>renderMathInElement(document.body,{strict: false, delimiters: [{left: '\\\\[', right: '\\\\]', display: true}, {left: '\\\\(', right: '\\\\)', display: false}]});</script>\n</body>\n</html>"
        html = "".join((head, "\n<center>\n", html, "\n</center>\n", ending))

        if self.wev.isVisible():
            self.wev.setHtml(html, QUrl.fromLocalFile("/"))

    def iconViewer(self):
        dialog = QDialog()
        vbox = QVBoxLayout()
        view = QWebEngineView()
        page = WebEnginePage(view)
        page.data_path = self.data_path
        view.setPage(page)
        path = "file://{}/fontawesome/icons.html".format(self.APP_PATH)
        view.load(QUrl(path))
        view.setFixedSize(1300, 900)
        view.setZoomFactor(1.6)
        vbox.addWidget(view)
        dialog.setLayout(vbox)
        dialog.setWindowTitle("wRiTe - Fontawesome Icons")
        dialog.setWindowModality(Qt.ApplicationModal)
        dialog.exec_()

    def svgEdit(self):
        dialog = QDialog()
        vbox = QVBoxLayout()
        view = QWebEngineView()
        page = WebEnginePage(view)
        page.data_path = self.data_path
        page.imgType = "Images (*.svg)"
        page.senderFunc = "svgedit"
        page.downloadFinished.connect(self.downloadFinished)
        view.setPage(page)
        path = "file://{}/svgedit/editor/svg-editor.html?forceStorage=true&iconsize='m'".format(
            self.APP_PATH
        )
        view.load(QUrl(path))
        view.setFixedSize(1300, 900)
        view.setZoomFactor(1.2)
        vbox.addWidget(view)
        dialog.setLayout(vbox)
        dialog.setWindowTitle("wRiTe SVG Editor")
        dialog.setWindowModality(Qt.ApplicationModal)
        dialog.exec_()

    def imageEditor(self):
        cursor = self.textEdit.textCursor()
        pos = cursor.positionInBlock()
        cursor.select(QTextCursor.BlockUnderCursor)
        text = cursor.selectedText().strip()
        start = text.rfind('!', 0, pos+1)
        end = text.find(')', pos+1) + 1
        image = re.sub(r"!\[[^\]]*?\]\((.+?\.[a-zA-Z]{3,4})\)", r"\1", text[start:end])

        if image:
            ext = image.split(".")[-1]
            if len(ext) >= 3:
                if ext.lower() in ["jpg", "jpeg", "png", "gif"]:
                    if not os.path.isfile(image) or ("/" not in image):
                        image = os.path.join(self.data_path, image)
                        if not os.path.isfile(image):
                            image = ""

        dialog = QDialog()
        vbox = QVBoxLayout()
        view = QWebEngineView()
        page = WebEnginePage(view)
        page.data_path = self.data_path
        page.imgType = "Images (*.png *.jpg *.jpeg *.gif)"
        page.senderFunc = "imageeditor"
        page.downloadFinished.connect(self.downloadFinished)

        with open(
            "{}/tui-image-editor/tui-image-editor.html".format(self.APP_PATH), "r"
        ) as f:
            html = f.read()
            html = html.format(
                self.APP_PATH,
                self.APP_PATH,
                self.APP_PATH,
                self.APP_PATH,
                self.APP_PATH,
                self.APP_PATH,
                self.APP_PATH,
                image,
            )

        page.setHtml(html, QUrl.fromLocalFile("/"))
        view.setPage(page)
        view.setFixedSize(1500, 950)
        view.setZoomFactor(1.0)
        vbox.addWidget(view)
        dialog.setLayout(vbox)
        dialog.setWindowTitle(
            "wRiTe - Image Editor - {}".format(os.path.basename(image))
        )
        dialog.setWindowModality(Qt.ApplicationModal)
        dialog.exec_()

    def molview(self):
        dialog = QDialog()
        vbox = QVBoxLayout()
        view = QWebEngineView()
        page = WebEnginePage(view)
        page.data_path = self.data_path
        page.imgType = "Images (*.png)"
        page.senderFunc = "molview"
        page.downloadFinished.connect(self.downloadFinished)
        page.load(QUrl("https://molview.org/"))
        view.setPage(page)
        view.setFixedSize(1500, 950)
        view.setZoomFactor(1.0)
        vbox.addWidget(view)
        dialog.setLayout(vbox)
        dialog.setWindowTitle("wRiTe - MolView - Molecular editor/viewer")
        dialog.setWindowModality(Qt.ApplicationModal)
        dialog.exec_()


    def downloadFinished(self, saveFile, senderFunc):
        cursor = self.textEdit.textCursor()

        if not cursor.selectedText():
            filename = os.path.basename(saveFile)
            ext = filename.split(".")[-1]
            if ext.lower() in ["svg", "png", "gif", "jpg", "jpeg"]:
                image = "![]({})".format(filename)
                self.textEdit.insertPlainText(image)

        # Remove white border if molview image
        if senderFunc == "molview":
            QTimer.singleShot(10, lambda: self.trimImage(saveFile))


    def trimImage(self, image):
        if not which("magick"):
            return
        if os.path.exists(image):
            os.system("magick '{}' -trim '{}'".format(image, image))

    def toggleFullScreen(self):

        if self.isFullScreen():
            self.showNormal()
        else:
            self.showFullScreen()

    def scrollTextEdit(self, pos, y):

        if self.helpIsShown:
            return

        leftPanel = self.mainGrid.itemAtPosition(0, 0).widget()

        if leftPanel.isHidden():
            return

        self.scrollActivated = False
        TEPos = min(
            [(abs(p - pos), i) for i, p in enumerate(tuple(self.scrollDict.values()))]
        )[1]
        TEPos = tuple(self.scrollDict.keys())[TEPos]
        block, blockline = TEPos
        value = tuple(self.scrollDict.keys()).index((block, blockline))

        #  nombre de lignes visibles (à affiner avec les sous-lignes)
        cursor = self.textEdit.textCursor()
        fpos = self.textEdit.cursorForPosition(QPoint(0, 0)).position()
        cursor.setPosition(fpos)
        first_block = cursor.blockNumber()
        first_block_line = (
            cursor.block()
            .layout()
            .lineForTextPosition(cursor.positionInBlock())
            .lineNumber()
        )

        end_pos = self.textEdit.cursorForPosition(
            QPoint(self.textEdit.viewport().width(), self.textEdit.viewport().height())
        ).position()
        cursor.setPosition(end_pos)
        end_block = cursor.blockNumber()
        end_block_line = (
            cursor.block()
            .layout()
            .lineForTextPosition(cursor.positionInBlock())
            .lineNumber()
        )

        try:
            start = tuple(self.scrollDict.keys()).index((first_block, first_block_line))
            end = tuple(self.scrollDict.keys()).index((end_block, end_block_line))
            total_visible_lines = len(tuple(self.scrollDict.keys())[start:end])

            delta = total_visible_lines * (
                y / self.qpv.viewport().rect().height()
            )
            self.textEdit.verticalScrollBar().setValue(value - round(delta))
        except Exception:
            self.scrollActivated = True
            return

        self.scrollActivated = True


    def indexPdf(self):

        if not self.highlightIsFinished:
            QTimer.singleShot(100, self.indexPdf)
            return

        #  Assign each line of textEdit a tuple (blocknb, lineInBlockNb)
        #  and get textEdit content with real linebreaks
        leftPanel = self.mainGrid.itemAtPosition(0, 0).widget()
        rightPanel = self.mainGrid.itemAtPosition(0, 1).widget()

        if leftPanel.isHidden() or rightPanel.isHidden():
            return

        if (self.qpv.pageLayout().empty()):
            return

        if self.isIndexing:
            return
        self.isIndexing = True
        doc = ""
        self.scrollDict = {}
        block = self.textEdit.document().begin()
        nbblocks = self.textEdit.document().blockCount()
        linecount = 0
        pageWidth = self.textEdit.width()-self.textEdit.document().documentMargin()

        for b in range(nbblocks):
            text = block.text()
            blocklines = block.lineCount()
            layout = block.layout()

            for bl in range(blocklines):
                if bl > 0:
                    linecount += 1
                self.scrollDict[(b, bl)] = linecount
                if layout.lineCount():
                    line = layout.lineAt(bl)
                    doc = "".join(
                        (
                            doc,
                            text[
                                line.textStart():line.textStart()+line.textLength()
                            ],
                            "\n",
                        )
                    )
                else:
                    if not bl:
                        doc = "".join((doc, text, "\n"))
                    else:
                        textwidth = self.textEdit.fontMetrics().width(text)
                        if textwidth/pageWidth > blocklines and bl == blocklines-1:
                            doc = "".join((doc, " \n\n"))
                        else:
                            doc = "".join((doc, " \n"))
            block = block.next()
            linecount += 1
        
        #  Remove all content that pyMuPdf can't parse
        def remove_non_pymupdf_compatible(doc):
            if not doc.strip():
                return ""
            
            mh = re.search(r"^(\*|\-|\+){4,}\s*$", doc, flags=re.MULTILINE)

            if mh:
                hend = mh.start() - 1
                enddelim = mh.end() + 1
                ln = doc[:enddelim].count("\n")
                doc = "".join(("\n" * ln, doc[enddelim:]))

            # remove block commands with text containt
            for e in (
                "wbox",
                "box",
                "pageframe",
                "left",
                "right",
                "center",
                "vcenter",
                "vhcenter",
                "mc",
                "code",
                "font",
                "header",
                "landscape",
                "portrait",
                "footer",
                "header",
                "muPDF",
                "line",
                "linesolid",
                "linedotted",
                "linedashed",
            ):
                if "!{}".format(e) not in doc:
                    continue
                doc = re.sub("^!{}.*$".format(e), "", doc, flags=re.MULTILINE)
                doc = re.sub(r"^\.{}.*$".format(e), "", doc, flags=re.MULTILINE)

            def latexd(match):
                text = ""
                if "\\text{" in match.group(0):
                    try:
                        text = re.findall(r"\\text\{([^}]+)\}", match.group(0))[0]
                    except Exception:
                        pass
                if text:
                    return "".join(("^", text, "\n" * match.group(0).count("\n")))
                return "\n" * match.group(0).count("\n")

            def latexi(match):
                text = ""
                if "\\text{" in match.group(0):
                    try:
                        text = re.findall(r"\\text\{([^}]+)\}", match.group(0))[0]
                    except Exception:
                        pass
                if text:
                    return "".join(("^", text, "\n" * match.group(0).count("\n")))
                return "".join(("^", "\n" * match.group(0).count("\n")))

            def last_closed_parenthesis(match):
                if match.group(0).count(")") > 0 and not match.group(0).count("("):
                    return match.group(0).replace(")", "")
                return match.group(0)

            def emptylines(match):
                return "\n" * match.group(0).count("\n")
            
            #  LaTeX display
            if doc.count("$$") > 1:
                doc = re.sub(
                    r"(?<!\\\\)\$[^$\w\d\\\\]{,1}\$[^$]+\$[^$\w\d\\\\]{,1}\$",
                    latexd,
                    doc,
                )
            #  LaTeX inline
            if doc.count("$") > 1:
                doc = re.sub(
                    r"(?<!\\\\)\$[^$\w\d\\\\]{,1}[^$]+[^$\w\d\\\\]{,1}\$", latexi, doc
                )
            
            #  Images
            if "![" in doc and "](" in doc:
                doc = re.sub(r"!\[[^\]]*?\]\([^)]+?\)", emptylines, doc)
                doc = doc.replace("![", "")
            
            #  Links
            if "](" in doc:
                doc = re.sub(r"(?<![a-zA-Z])\[([^\]]+?)\]\([^)\n]+", r"\1", doc)
                #  End of links
                doc = re.sub(
                    r"^.*\).*$", last_closed_parenthesis, doc, flags=re.MULTILINE
                )
            
            #  Code blocks
            if "```" in doc:
                doc = re.sub(
                    r"^```[a-z]*$", "", doc, flags=re.MULTILINE
                )
                # Inline code blocks
                doc = re.sub(r"```([^`]+?)```", r"\1", doc)
                
            #  Inline code blocks 2
            if doc.count('`') > 1:
                doc = re.sub(r"`([^`]+?)`", r"\1", doc)
                
            # Quotes
            if ">" in doc:
                doc = re.sub(r"^>", "", doc, flags=re.MULTILINE)

            #  Lettrines
            if "§" in doc:
                doc = re.sub(r"^§[\w]", "", doc, flags=re.MULTILINE)

            # bold and italic
            doc = doc.replace("*", "")

            # Italic2
            if doc.count("_") > 1:
                doc = doc.replace("_", "")

            # Pen
            if doc.count("==") > 1:
                doc = doc.replace("++", "")

            # Underline
            if doc.count("++") > 1:
                doc = doc.replace("++", "")

            # Comments
            if "%%" in doc:
                doc = re.sub(r"^\%\%.*$", "", doc, flags=re.MULTILINE)

            # hashtag (titles)
            if "#" in doc:
                doc = re.sub(r"#[!]*", "", doc)

            #  Powers
            if "^" in doc:
                doc = doc.replace("^", "")

            #  Indices and strokes
            if "~" in doc:
                doc = doc.replace("~", "")

            # fa icons
            doc = re.sub(r":[a-z\-\+]+?:", "", doc)
            
            #  Inline text modif
            if "){" in doc:
                doc = re.sub(
                    r"\([^)]+?\)\{([^}]*?)\}", r"\1", doc
                    )

            #  remove TOC
            if "{{TOC}}" in doc:
                doc = re.sub(r"\{\{TOC\}\}", "", doc)

            #  remove tables
            if "|" in doc:
                doc = re.sub(r"^\|.*$", "", doc, flags=re.MULTILINE)

            # remove extra spaces
            doc = re.sub(r"/(\s+?)/", "", doc)

            # remove hrules
            if "---" in doc:
                doc = doc.replace("---", "")
            if "- - -" in doc:
                doc = doc.replace("- - -", "")
            if ". . ." in doc:
                doc = doc.replace(". . .", "")

            # modify wrong tildes
            doc = doc.replace("’", "'")
            doc = doc.replace("--", "–")
            doc = re.sub(r"\.{3}", "", doc)

            #  Remove line breaks
            doc = re.sub(r"\\\d+!", "", doc)
            doc = doc.replace('\\', '')

            # Buttons
            doc = re.sub(r"\[[a-zA-F\d@#]*?\|(.+?)\|[a-zA-F\d@#]*?\]", r"\1", doc)

            # remove entire commands blocks which are interpreted
            for e in ("graph", "gtab", "diagram", "footer", "header"):
                if "!{}".format(e) in doc:
                    doc = re.sub(
                        "\n!{0}.*?\n\.{0}".format(e),
                        emptylines,
                        doc,
                        flags=re.S
                        )
            return doc

        #  Only !newpage on a line before split
        doc = re.sub("^!newpage.*$", "!newpage", doc, flags=re.MULTILINE)

        pdf = fitz.open(self.memoryPdfFile.name)
        numOfPages = pdf.page_count

        if doc.count("\n!newpage")+1 != numOfPages:
            self.isIndexing = False
            return

        indexedPdf = []
        
        for page, textpage in enumerate(
                map(remove_non_pymupdf_compatible, doc.split("\n!newpage\n"))
                ):
            posPage = self.qpv.page(page + 1).y

            # Scroll even if no text, with !newpage positions
            if page:
                indexedPdf.append(posPage)

            pdfpage = pdf[page]
            blocks = pdfpage.get_text("blocks")
            # unordered data: put lines in order by y position before search
            blocks.sort(key=lambda x: x[1])
            ph = pdfpage.rect.height
            block_num = 0
            lastblock_num = 0
            previousblock_num = 0
            lastpos = -1
            lastlinetext = []

            for linetext in splitlinesgen(textpage):
                linetext = linetext.strip()
                pos = -1
                searching = False
                totalfinish = 0

                if len(linetext) < 3:
                    linetext = ""

                if linetext:
                    for block in blocks[block_num:]:
                        if block[-1]:
                            block_num += 1
                            continue
                        blocktextlist = [
                            e.replace("’", "'").replace("«", '"').replace("»", '"')
                            for e in block[4].split("\n")[:-1]
                                ]
                        percent = longuest_common_string(
                            linetext, " ".join(blocktextlist)
                            ) / len(linetext)

                        if percent == 1.0 and len(blocktextlist) == 1:
                            idxm = 0
                        elif percent > 0.95:
                            percents = [
                                longuest_common_string(linetext, e) / len(linetext)
                                for e in blocktextlist
                                    ]
                            lbtl = len(blocktextlist)
                            maxp = max(percents)
                            idxm = percents.index(maxp)

                            if len(percents) == 2 and 0.98 <= sum(percents) <= 1 \
                                    and idxm == 1:
                                idxm = 0
                        else:
                            block_num += 1

                            if not searching:
                                searching = True
                                previousblock_num = block_num
                                totalfinish = block_num + len(blocks[block_num:])
                            continue

                        searching = False
                        deltay = 0

                        if idxm > 0 and lbtl > 1:
                            ymin = block[1]
                            ymax = block[3]
                            deltay = (ymax - ymin) / lbtl * idxm

                        if block[1] < ph:
                            block = (block[0], block[1]+deltay, *block[2:])
                            res = fitz.Rect(*block[:4])
                            pos = res.normalize().round().y0
                            heightOfPage = self.qpv.page(page + 1).height
                            pos = round(posPage + (pos / ph * heightOfPage))

                            if pos < lastpos:
                                block_num += 1
                                continue

                            if block_num == lastblock_num:

                                if not lastlinetext:
                                    lastlinetext.append(linetext)
                                else:
                                    mpercent = max([
                                        longuest_common_string(linetext, e)/len(linetext)
                                        for e in lastlinetext])
                                    if mpercent < 0.98:
                                        lastlinetext.append(linetext)
                                    else:
                                        block_num += 1
                                        lastlinetext = [linetext]
                                        continue
                            else:
                                lastlinetext = []
                                lastlinetext.append(linetext)
                            lastpos = pos
                            lastblock_num = block_num
                            break
                        break

                if searching and block_num == totalfinish:
                    searching = False
                    block_num = previousblock_num
                indexedPdf.append(pos)
        start = -1
        end = -1
        lenIP = len(indexedPdf)

        for i, index in enumerate(indexedPdf):
            if index < 0 and i + 1 < lenIP:
                if start < 0:
                    start = i
                    end = start
                else:
                    end += 1

            elif i+1 == lenIP and index < 0:
                if start < 0:
                    start = i
                    end = i
                else:
                    end += 1

                if (start - 2) >= 0:
                    pas = int(indexedPdf[start - 1] - indexedPdf[start - 2])
                    for j in range(start, end + 1):
                        indexedPdf[j] = indexedPdf[j - 1] + pas
                else:
                    for j in range(start, end + 1):
                        indexedPdf[j] = indexedPdf[j - 1] + 15

                start = -1
                end = -1

            elif start >= 0:
                if start == 0:
                    pas = int(indexedPdf[end + 1] / (end + 2))
                    for j in range(start, end + 1):
                        indexedPdf[j] = (j + 1) * pas
                else:
                    pas = int(
                        (indexedPdf[end + 1] - indexedPdf[start - 1])
                        / (end - start + 2)
                    )
                    for j in range(start, end + 1):
                        indexedPdf[j] = indexedPdf[j - 1] + pas
                start = -1
                end = -1

        for i, key in enumerate(self.scrollDict):
            if i < lenIP:
                self.scrollDict[key] = indexedPdf[i]
            else:
                break
        pdf.close()
        self.isIndexing = False
        self.hideWaitLogo()

        if self.qpv.isVisible():
            QTimer.singleShot(20, self.scrollPdf)


    def exportSelected(self):
        image = self.qpv.rubberband().selectedImage(600)

        if image is not None:
            cb = QApplication.clipboard()
            cb.clear(mode=cb.Clipboard)
            cb.setImage(image, mode=cb.Clipboard)
            img = "wRiTe_capture_{}.png".format(int(time()))
            image.save(os.path.join(self.data_path, img))
            self.showNotificationWindow("**Image saved !**", "blue", 4000, -1)


    def onClickInsertImage(self, state, button=None):
        if button is None:
            button = self.sender()
        text = "![]({0})".format("".join((button.text())))
        self.textEdit.insertPlainText(text)
        self.imageScrollArea.setFocus(True)


    def showHideImageBar(self):
        img_extensions = ("jpg", "jpeg", "png", "svg", "gif")
        leftPanel = self.mainGrid.itemAtPosition(0, 0).widget()
        
        if leftPanel.isVisible():

            if self.imageScrollArea.isHidden():
                self.images = []

                if os.path.exists(self.data_path):
                    imgfiles = sorted(os.listdir(self.data_path))
                    imgfiles = [im for im in imgfiles if im.split(".")[-1].lower() in img_extensions]
                    self.totalImages = len(imgfiles)
                    
                    if self.lastimgfiles == imgfiles:
                        self.imageScrollArea.setVisible(True)
                        self.imageScrollArea.setFocus(True)
                        return

                    self.imageGrid = QGridLayout()
                    imageWidget = QWidget()
                    imageWidget.setAttribute(Qt.WA_DeleteOnClose, True)
                    imageWidget.setMinimumHeight(200)

                    for i, file in enumerate(imgfiles):
                        file = os.path.join(self.data_path, file)
                        self.images.append(file)
                        button = QToolButton()
                        button.setMinimumSize(QSize(200, 200))
                        button.setMaximumSize(QSize(200, 200))
                        pixmap = QPixmap(file)
                        icon = QIcon(pixmap.scaled(button.size()))
                        button.setIcon(icon)
                        button.setIconSize(QSize(200, 150))
                        button.setText(os.path.basename(file))
                        button.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
                        button.clicked.connect(self.onClickInsertImage)
                        button.setAttribute(Qt.WA_DeleteOnClose, True)
                        if i == self.selectedImage:
                            button.setStyleSheet(
                                (
                                    "QToolButton {background-color: #D9FBDF; border: none;}"
                                )
                            )
                        self.imageGrid.addWidget(button, 0, i)
                        self.imageGrid.update()

                    imageWidget.setLayout(self.imageGrid)
                    imageWidget.setFocusPolicy(Qt.StrongFocus)
                    self.imageScrollArea.setWidget(imageWidget)
                    self.lastimgfiles = imgfiles

                self.imageScrollArea.setVisible(True)
                self.imageScrollArea.setFocus(True)
                self.imageScrollAreaselectedImage = 0

            else:
                self.imageScrollArea.setHidden(True)
                self.textEdit.setFocus(True)


    def showHideSpecialChar(self):
        leftPanel = self.mainGrid.itemAtPosition(0, 0).widget()

        if leftPanel.isVisible():

            if self.specialScrollArea.isHidden():
                self.updateSpecialScrollArea()
                self.specialScrollArea.setVisible(True)
                self.specialScrollArea.setFocus(True)
            else:
                self.specialScrollArea.setHidden(1)
                self.textEdit.setFocus(True)


    def insertChemEq(self):
        cursor = self.textEdit.textCursor()
        text = cursor.selectedText()

        if text:
            text = "\\ce{{{0}}}".format(text)
            self.textEdit.insertPlainText(text)
            cursor.movePosition(
                QTextCursor.PreviousCharacter, QTextCursor.KeepAnchor, len(text)
            )
            self.textEdit.setTextCursor(cursor)
        else:
            self.textEdit.insertPlainText(r"\ce{}")
            cursor.movePosition(QTextCursor.Left)
            self.textEdit.setTextCursor(cursor)


    def insertLatexTimes(self):
        cursor = self.textEdit.textCursor()
        pos = cursor.positionInBlock()
        cursor.select(QTextCursor.BlockUnderCursor)
        textbefore = cursor.selectedText()[:pos+1]
        single = textbefore.count("$")
        double = textbefore.count("$$")
        latex = False
        if double and double % 2:
            latex = True
        elif single and single % 2:
            latex = True
        cursor.clearSelection()
        if latex:
            self.textEdit.insertPlainText(" \\times ")
        else:
            self.textEdit.insertPlainText(" × ")


    def insertLatexUnits(self):
        cursor = self.textEdit.textCursor()
        selection = cursor.selectedText()

        if selection:
            self.textEdit.insertPlainText("\\pu{{{}}}".format(selection))
        else:
            self.textEdit.insertPlainText("~\\pu{}")
            cursor.movePosition(QTextCursor.Left)
            self.textEdit.setTextCursor(cursor)


    def insertLatexFrac(self, typefrac):
        cur = self.textEdit.textCursor()

        if not typefrac:
            self.textEdit.insertPlainText("\\frac{}{}")
        else:
            self.textEdit.insertPlainText("\\dfrac{}{}")

        cur.movePosition(QTextCursor.Left, QTextCursor.MoveAnchor, 3)
        self.textEdit.setTextCursor(cur)


    def insertImage(self):
        cur = self.textEdit.textCursor()
        self.textEdit.insertPlainText("![]()")
        cur.movePosition(QTextCursor.Left, QTextCursor.MoveAnchor, 3)
        self.textEdit.setTextCursor(cur)


    def insertCrossReference(self):
        cur = self.textEdit.textCursor()
        stext = cur.selectedText()

        if stext:
            self.textEdit.insertPlainText("[{}][]".format(stext))
            cur.movePosition(QTextCursor.Left, QTextCursor.MoveAnchor, 1)
        else:
            self.textEdit.insertPlainText("[][]")
            cur.movePosition(QTextCursor.Left, QTextCursor.MoveAnchor, 3)

        self.textEdit.setTextCursor(cur)


    def insertSpecialChar(self, text=None):
        if text is None:
            button = self.sender()
            text = button.text()
        self.selectedSpecialChar = self.specialCharsTable[text]

        if text in emojis_icons:
            self.textEdit.insertPlainText(emojis_icons[text])
        else:
            self.textEdit.insertPlainText(text)
            
        self.updateSpecialScrollArea()
        self.specialScrollArea.setFocus(True)


    def changeSelected(self, action):
        add = ("**{0}**", "*{0}*", "++{0}++", "=={0}==", "~~{0}~~")
        cur = self.textEdit.textCursor()
        stext = cur.selectedText()
        text = add[action].format(stext)
        self.textEdit.insertPlainText(text)

        if not stext:
            cur.movePosition(
                QTextCursor.PreviousCharacter,
                QTextCursor.MoveAnchor,
                int(len(add[action]) / 2 - 1),
            )
            self.textEdit.setTextCursor(cur)


    def insertLink(self):
        cur = self.textEdit.textCursor()
        text = cur.selectedText()

        if text:
            text = "".join(("[", text, "]()"))
            self.textEdit.insertPlainText(text)
            cur.movePosition(QTextCursor.PreviousCharacter)
        else:
            self.textEdit.insertPlainText("[]()")
            cur.movePosition(QTextCursor.PreviousCharacter, QTextCursor.MoveAnchor, 3)

        self.textEdit.setTextCursor(cur)


    def exportNotify(self, pandoc):
        if pandoc:
            QTimer.singleShot(
                0,
                lambda: self.showNotificationWindow(
                    "**DOCX conversion**", "chocolate", 10000, -1
                ),
            )
            QTimer.singleShot(1000, self.exportToDOCX)
        else:
            QTimer.singleShot(
                0,
                lambda: self.showNotificationWindow(
                    "**PDF conversion**", "chocolate", 10000, -1
                ),
            )
            QTimer.singleShot(1000, self.exportPdf)


    def exportPdf(self, saveFile=None):
        if self.modalWindowOpened or self.pdfIsExporting:
            return

        self.pdfIsExporting = True

        titlerefs = re.compile(r"^#[#]*.+\s+\[[^\]]+\]\s*$", flags=re.MULTILINE)
        textrefs = re.compile(r"\[[^\]]+\]\[[^\]]+\]")

        if not self.recompileForExport:
            #  Recompile all document to match titles references
            if titlerefs.findall(self.textEdit.toPlainText()) and textrefs.findall(
                self.textEdit.toPlainText()
            ):
                self.recompileForExport = True
                QTimer.singleShot(0, self.textToHtml)
                self.pdfIsExporting = False
                self.modalWindowOpened = False
                return
        else:
            self.recompileForExport = False

        if os.path.exists(self.pdf_file):
            self.modalWindowOpened = True

            yn = QMessageBox.question(
                self,
                "File already exists",
                "Overwrite ?",
                QMessageBox.Yes | QMessageBox.No,
                QMessageBox.Yes,
            )

            if yn == QMessageBox.No:
                QTimer.singleShot(
                    0,
                    lambda: self.showNotificationWindow(
                        "**Export aborted**", "tomato", 2000, -1
                    ),
                )
                self.modalWindowOpened = False
                self.pdfIsExporting = False
                return

        pdf = fitz.open(self.memoryPdfFile.name)

        if self.videos:
            pdf = fitz.open(self.memoryPdfFile.name)
            for p in range(pdf.page_count):
                page = pdf[p]
                links = page.get_links()

                for i, link in enumerate(links):
                    if 'file' in link:
                        for video in self.videos:
                            if video in link['file']:
                                link['file'] = video
                                page.update_link(link)

        if self.pageList:
            pages_to_delete = []

            for p in range(pdf.page_count):

                if not p + 1 in self.pageList:
                    pages_to_delete.append(p)

            if pages_to_delete:
                pdf.delete_pages(pages_to_delete)

            self.pageList = None

        meta = pdf.metadata
        meta["creator"] = "wRiTe by Nicolas CLÉMENT"
        meta["producer"] = "Qt5"
        meta["author"] = self.author
        meta["title"] = self.doctitle
        meta["keywords"] = self.textHash
        pdf.set_metadata(meta)
        encrypt = fitz.PDF_ENCRYPT_NONE

        if self.encryption:
            self.modalWindowOpened = True
            pwd = Pwd()
            pwd.setModal(True)
            pwd.pwdok.connect(self.setPwd)

            if pwd.exec_() == QDialog.Accepted:
                encrypt = fitz.PDF_ENCRYPT_AES_256
            else:
                self.password = None
                self.exported = False
                QTimer.singleShot(
                    0,
                    lambda: self.showNotificationWindow(
                        "**Export aborted**", "tomato", 2000, -1
                    ),
                )
                self.modalWindowOpened = False
                self.pdfIsExporting = False
                return

        if not saveFile:
            saveFile = self.pdf_file

        pdf.save(
            saveFile,
            clean=True,
            garbage=4,
            deflate=True,
            pretty=True,
            encryption=encrypt,
            owner_pw=None,
            user_pw=self.password,
        )
        pdf.close()
        self.modalWindowOpened = False
        self.password = None
        self.exported = True
        self.pdfIsExporting = False
        QTimer.singleShot(
            0,
            lambda: self.showNotificationWindow(
                "**Success !**", "forestgreen", 2000, -1
            ),
        )


    def pdfToQpv(self, filepath, check):
        if self.helpIsShown:
            self.hideWaitLogo()
            return

        if self.recompileForExport:
            QTimer.singleShot(0, self.exportPdf)
            self.hideWaitLogo()
            return
        
        pdf = None
        
        if not os.path.getsize(self.memoryPdfFile.name):
            pdf = fitz.Document()
            pdf.insert_page(0, "")
            pdf.save(self.memoryPdfFile.name, garbage=4)
        else:
            pdf = fitz.open(self.memoryPdfFile.name)

        #  For Qt6 (later): first load page by page
        # if not self.pdfComplete:
        #     if self.veryFistPage:
        #         self.qpv.load(self.memoryPdfFile.name)
        #     page = fitz.open(self.memoryPageFile.name)
        #     pdf.insert_page(page)
        #     pdf.saveIncr()
        #     self.qpv.reload()
        #     pdf.close()
        #     return

            #  Delete last empty page due to KateX
            if pdf.page_count != len(self.codeBackup):
                pages_to_delete = []

                for p in range(pdf.page_count):
                    if not pdf[p].get_text("dict")["blocks"] and p >= len(
                        self.codeBackup
                    ):
                        pages_to_delete.append(p)

                if pages_to_delete:
                    pdf.delete_pages(pages_to_delete)

            if self.code_pages:
                if self.pages_to_delete:
                    try:
                        pdf.delete_pages(self.pages_to_delete)
                    except Exception:
                        pass

                modpages = fitz.open(self.memoryPageFile.name)

                #  Delete unwanted generated empty pages
                if modpages.page_count != len(self.new_pages_to_compile):
                    pages_to_delete = []

                    pages_to_delete = [
                        p for p in range(modpages.page_count)
                        if not modpages[p].get_text("dict")["blocks"]
                    ]

                    if not len(pages_to_delete) == modpages.page_count:
                        modpages.delete_pages(pages_to_delete)
                    else:
                        modpages.delete_pages(pages_to_delete[1:])

                for i, page in enumerate(self.new_pages_to_compile):
                    if page < self.code_pages[0]:
                        pdf.insert_pdf(modpages, from_page=i, to_page=i, start_at=0)
                        self.code_pages.insert(0, page)
                    elif page > self.code_pages[-1]:
                        pdf.insert_pdf(modpages, from_page=i, to_page=i, start_at=-1)
                        self.code_pages.append(page)
                    else:
                        for j, cpage in enumerate(self.code_pages):
                            if page < cpage:
                                pdf.insert_pdf(
                                    modpages, from_page=i, to_page=i, start_at=j
                                )
                                self.code_pages.insert(j, page)
                                break
                self.code_pages = []

            if self.mode_changed_pages:
                QTimer.singleShot(0, self.textToHtml)
                print("abort pdftoqpv mode_changed_pages")
                self.hideWaitLogo()
                return

            pagerange = range(pdf.page_count)

            if self.new_pages_to_compile:
                pagerange = self.new_pages_to_compile

            if self.bgcolor:
                if self.bgcolor:
                    if self.bgcolor.isalpha():
                        color = fitz.utils.getColor(self.bgcolor)
                    elif "#" in self.bgcolor:
                        color = tuple(
                            int(self.bgcolor[1:][i:i+2], 16) for i in (0, 2, 4)
                        )
                        color = tuple(map(lambda x: x / 255, color))
                    elif "rgba(" in self.bgcolor:
                        color = eval(self.bgcolor[4:])
                        a = color[-1]
                        r = ((1 - a) * 255 + a * color[0]) / 255
                        g = ((1 - a) * 255 + a * color[1]) / 255
                        b = ((1 - a) * 255 + a * color[2]) / 255
                        color = (r, g, b)
                    elif "rgb(" in self.bgcolor:
                        color = eval(self.bgcolor[3:])
                        color = tuple(map(lambda x: x / 255, color))
                    else:
                        color = (1, 1, 1)

                    for i in pagerange:
                        page = pdf[i]
                        page.draw_rect(
                            page.rect, color=color, fill=color, overlay=False
                        )

            if self.pagenum:
                pos = "bc"
                fontsize = 10
                fontname = "Times-Roman"
                align = 1

                if "@" in self.pagenum:
                    pagenum, pos = self.pagenum.split("@")
                    fsize = "".join([c for c in pos if c.isdigit()])

                    if fsize:
                        fsize = int(fsize)
                    else:
                        fsize = 0

                    pos = "".join([c for c in pos if c.isalpha()])

                    if fsize >= 7:
                        fontsize = fsize

                    posits = ("tl", "tc", "tr", "bl", "bc", "br")

                    if not any([p for p in posits if p in pos]):
                        pos = "".join((pos, "bc"))
                else:
                    pagenum = self.pagenum

                if "l" in pos:
                    align = 0
                elif "c" in pos:
                    align = 1
                elif "r" in pos:
                    align = 2

                if "C" in pos:
                    fontname = "Courier"
                elif "H" in pos:
                    fontname = "Helvetica"
                for p in pagerange:
                    page = pdf[p]
                    page.clean_contents(sanitize=False)
                    ptext = pagenum.format(p + 1)
                    x = 15
                    y = 0
                    delta_y = 0

                    if "b" in pos:
                        y = page.rect.height - fontsize - 14
                    elif "t" in pos:
                        y = 7

                    delta_x = page.rect.width - 15

                    if "b" in pos:
                        delta_y = page.rect.height
                    elif "t" in pos:
                        delta_y = y + fontsize + 10

                    rect = (x, y, delta_x, delta_y)
                    page.insert_textbox(
                        rect, ptext, fontsize=fontsize, fontname=fontname, align=align
                    )
                    
            if not self.rebuild:                
                for p in pagerange:
                    page = pdf[p]
                    cm = abs(page.rect.bl.y-page.rect.tl.y) \
                        / page.rect.tl.distance_to(page.rect.bl, 'cm')
                    mm = cm/10
                    mp = muPDF(page, cm)
                    page.clean_contents(sanitize=False)
                    page.draw_arrow = mp.draw_arrow
                    page.draw_milli = mp.draw_milli
                    page.show_grid = mp.show_grid
                    pr = page.rect
                    pw = page.rect.width
                    ph = page.rect.height
                    
                    if p in self.muPDFCode:
                        mucode = self.muPDFCode[p]
                        if mucode:
                            if 'pw' in mucode or 'ph' in mucode:
                                mucode = re.sub(
                                        r'^(.+)(pw|ph)(.+unit=[\'"])(cm|mm)(.+)$',
                                        r'\1\2/\4\3\4\5',
                                        mucode,
                                        flags=re.M
                                        )
                            if 'fill=' in mucode or 'color=' in  mucode:
                                def filtercolor(match):
                                    command = match.group(1)
                                    color = match.group(2).lower()
                                    if color in colors:
                                        color = colors[color]
                                    if '#' in color and len(color) == 7:
                                        color = tuple(
                                            int(color[1:][i:i+2], 16)/255 for i in (0, 2, 4)
                                            )
                                    return "{}={}".format(command, color)

                                mucode = re.sub(r'(fill|color)=[\'"]([a-z#A-F\d]+)[\'"]', filtercolor, mucode)
                            try:
                                exec(mucode)
                            except Exception:
                                pass

            self.lastPageCount = pdf.page_count
            pdf.saveIncr()
        
        if self.firstLoad:
            self.modified = False
            self.qpv.setViewMode(qpageview.FitWidth)
            try:
                self.qpv.reload()
                self.pdfLoaded = True
                #self.hideWaitLogo()
            except Exception:
                self.pdfLoaded = False
        else:
            try:
                #self.hideWaitLogo()
                self.qpv.reload()
                                #  Hack -- QPV bug -- Retreive missing files links urls and Rects
                self.links = []
                for p in self.new_pages_to_compile:
                    page = pdf[p]
                    ph = pdf[0].rect.height
                    pw = pdf[0].rect.width
                    links = page.get_links()

                    for link in links:
                        linkrect = link["from"]
                        if "file" in link:
                            lfile = "".join(("file:///", link["file"][1:]))
                            self.links.append(
                                [
                                    self.qpv.page(p + 1),
                                    linkrect[0] / pw,
                                    linkrect[1] / ph,
                                    linkrect[2] / pw,
                                    linkrect[3] / ph,
                                    lfile,
                                ]
                            )
                self.pdfLoaded = True
            except Exception:
                self.pdfLoaded = False

            QTimer.singleShot(10, self.updatePageIndicator)

        self.pdfDisplayed = True

        if pdf.page_count - 1 != self.newpages:
            self.showNotificationWindow(
                "**No pages. Scroll disabled**", 
                "tomato", 
                5000, 
                -1
                )
            self.hideWaitLogo()
            
        pdf.close()
        self.new_pages_to_compile = []
        self.firstLoad = False
        self.qpv.pageLayoutModeChanged.emit("")
        if self.rebuild:
            self.warnRebuildGraphs()


    def warnRebuildGraphs(self):
        yn = QMessageBox.question(
                self,
                "!! Security Lock !!",
                (
                    "Python code is about to be executed in \n !graph or !muPDF sections."
                    "\nExecute now (trusted code)?"
                    "\n\nIf [No], code will be executed after next save."
                ),
                QMessageBox.Yes | QMessageBox.No,
                QMessageBox.No,
            )

        if yn == QMessageBox.Yes:
            self.textToHtml()


    def deltaCursorPos(self):
        cursorpos = self.textEdit.textCursor().position()
        if self.cursorPosition != cursorpos:
            self.deltaPos = cursorpos - self.cursorPosition
            self.cursorPosition = cursorpos
            #  delete enable title completion
            self.titleComplete = 0


    def autocomplete(self):
        if "fr" not in self.lang:
            return

        if self.firstAutocomplete:
            self.firstAutocomplete = False
            return

        cursor = self.textEdit.textCursor()

        # Autocorrection for french
        words = {
            "oeil": "œil",
            "stoechio": "stœchio",
            "coeur": "cœur",
            "soeur": "sœur",
            "choeur": "chœur",
            "oeuvr": "œuvr",
            "oeuf": "œuf",
            "oeillet": "œillet",
            "aequo": "æquo",
            "Laetitia": "Lætitia",
            "voeu": "vœu",
            "noeud": "nœud",
            "acoelien": "acœlien",
            "amoebicide": "amœbicide",
            "amoebocyte": "amœbocyte",
            "angstroem": "angstrœm",
            "boeuf": "bœuf",
            "biocoenose": "biocœnose",
            "boevonnage": "bœuvonnage",
            "foederis": "fœderis",
            "coel": "cœl",
            "coen": "cœn",
            "oeuvr": "œuvr",
            "foet": "fœt",
            "moel": "mœl",
            "moer": "mœr",
            "périoe": "périœ",
            "phoe": "phœ",
            "poec": "pœc",
            "pomoerium": "pomœrium",
            "noec": "nœc",
            "toec": "tœc",
            "moeb": "mœb",
            "coec": "cœc",
            "ioec": "iœc",
            "oen": "œn",
            "oestr": "œstr",
            "roed": "rœd",
            "doeum": "dœum",
            "noek": "nœk",
            "noeq": "nœq",
            "oed": "œd",
        }

        cursor.movePosition(QTextCursor.StartOfWord, QTextCursor.KeepAnchor, 1)
        word = cursor.selectedText()

        if len(word) > 3:
            for w in words:
                if w in word:
                    cursor.deleteChar()
                    word = word.replace(w, words[w])
                    self.textEdit.insertPlainText(word)
                    self.textEdit.setTextCursor(cursor)
                    break


    def createVars(self, mdfile):
        self.md_file = mdfile

        if not self.md_file:
            return

        self.disp_filename = os.path.basename(self.md_file)
        self.path = os.path.dirname(os.path.abspath(self.md_file))
        QDir.setCurrent(self.path)
        self.data_path = os.path.join(
            self.path, "".join((self.disp_filename.split(".")[0], "_data"))
        )
        self.textEdit.data_path = self.data_path

        try:
            self.lang = self.textEdit.highlighter.dict().tag.split("_")[0]
        except Exception:
            pass

        os.makedirs(self.data_path, exist_ok=True)
        self.pdf_file = "".join((os.path.splitext(self.md_file)[0], ".pdf"))
        self.docx_file = "".join((os.path.splitext(self.md_file)[0], ".docx"))
        self.tmp_index = int(time())


    def newDoc(self):
        self.md_file = ""
        self.loadText()


    def openDoc(self):
        filename = str(
            QFileDialog.getOpenFileName(
                self,
                self.tr("Open Markdown file..."),
                QDir.currentPath(),
                self.tr("Markdown files (*.md )"),
            )[0]
        )

        if filename:
            if os.path.isfile(filename):
                self.firstLoad = True
                self.pdfLoaded = False
                self.qpv.clear()
                self.md_file = filename
                self.createVars(filename)
                self.loadText()
                self.generated = True
                self.setWindowTitle(
                    "wRiTe -- {} -- [{}]".format(self.disp_filename, self.lang)
                )

    #  A FINIR: gérer le dossier des datas
    def saveasDoc(self):
        saveFile = str(
            QFileDialog.getSaveFileName(
                self,
                "Save text as...",
                QDir.currentPath(),
                self.tr("Markdown files (*.md )"),
            )[0]
        )

        if saveFile:

            if saveFile.split(".")[-1] != "md":

                if saveFile[-1] != ".":
                    saveFile = "".join((saveFile, ".md"))
                else:
                    saveFile = "".join((saveFile, "md"))

            datapath = self.data_path
            self.createVars(saveFile)

            if os.path.isdir(datapath):
                files_to_copy = os.listdir(datapath)

                if files_to_copy:

                    for f in files_to_copy:
                        copyfile(
                            os.path.join(datapath, f), os.path.join(self.data_path, f)
                        )

            text = self.textEdit.toPlainText()

            try:
                with open(self.md_file, "w") as f:
                    f.write(text)
            except Exception:
                print("Can't write to file", self.md_file)

            self.generated = True
            self.loadText()
            self.setWindowTitle(
                "wRiTe -- {} -- [{}".format(self.disp_filename, self.lang)
            )


    def insertSpace(self):
        cursor = self.textEdit.textCursor()
        cursor.movePosition(QTextCursor.PreviousCharacter, QTextCursor.KeepAnchor, 2)
        pattern = cursor.selectedText()

        if pattern == " /":
            cursor.movePosition(QTextCursor.NextCharacter, 1)
            cursor.clearSelection()
            self.textEdit.setTextCursor(cursor)
            self.textEdit.insertPlainText(" ")
            cursor.movePosition(QTextCursor.NextCharacter, 1)
            cursor.clearSelection()
            self.textEdit.setTextCursor(cursor)
        else:
            cursor.movePosition(QTextCursor.NextCharacter, 2)
            self.textEdit.insertPlainText("/ /")


    def insertNewPage(self):
        self.textEdit.insertPlainText("!newpage")


    def insertNewLine(self):
        cursor = self.textEdit.textCursor()
        cursor.movePosition(QTextCursor.PreviousCharacter, QTextCursor.KeepAnchor, 4)
        selection = cursor.selectedText()

        m = re.search(r"\\(\d*)!", selection)

        if m:
            d = m.group(1)

            if len(d) == 1:
                cursor.clearSelection()
                cursor.movePosition(QTextCursor.NextCharacter)
                cursor.movePosition(
                    QTextCursor.NextCharacter, QTextCursor.KeepAnchor, 3
                )

            if d:
                d = int(d) + 1
                cursor.removeSelectedText()
                self.textEdit.insertPlainText("\\{}!".format(d))
            else:
                d = 2
                cursor.removeSelectedText()
                self.textEdit.insertPlainText("{}\\{}!".format(selection[:2], d))
        else:
            self.textEdit.insertPlainText("\\1!")


    def insertBlock(self, type):
        cur = self.textEdit.textCursor()
        text = cur.selectedText()
        blocks = {
            "B": "box",
            "W": "wbox",
            "V": "vcenter",
            "H": "vhcenter",
            "C": "center",
            "R": "right",
            "L": "left",
            "P": "code",
            "G": "graph",
            "M": "mc2",
            "F": "font",
            "D": "diagram",
        }

        if text:
            text = "".join(
                ("!{}\n".format(blocks[type]), text, "\n.{}".format(blocks[type]))
            )
            self.textEdit.insertPlainText(text)
        else:
            self.textEdit.insertPlainText("!{0}\n\n.{0}".format(blocks[type]))
            cur.movePosition(QTextCursor.Up)
            self.textEdit.setTextCursor(cur)


    def vector(self):
        vector = r"""$$
\overrightarrow{OM}~\left|
\begin{align*}
x(t) &= 0\\
y(t) &= 0\\
z(t) &= 0
\end{align*}
\right.
$$"""
        self.textEdit.insertPlainText(vector)


    def tabAvancement(self):
        tab = r"""
|:--|:--:|:--:|:--:|:--:|:--:|
|**Équation**|| $\ce{aA ~~~ + ~~~ bB ~~~ <=> ~~~ cC ~~~ + ~~~ dD}$ ||||
|**États**|**Avancement**\!**(mol)**|**Quantités de matière**\!**(mol)**||||
|**État initial**| $x=0$ | $n_i(\rm A)$ | $n_i(\rm B)$ | 0 | 0 |
|**État intermédiaire**| $x$ | $n_i(\rm A) - ax$ | $n_i(\rm B) - bx$ |$cx$ | $dx$ |
|**État final**| $x_f$ | $n_i(\rm A) - ax_f$ | $n_i(\rm B) - bx_f$ |$cx_f$ | $dx_f$ |
|**État final**<br>**(si totale)** | $x_{max}$ | $n_i(\rm A) - ax_{max}$ | $n_i(\rm B) - bx_{max}$ | $cx_{max}$ | $dx_{\rm max}$ |
[Fig.1: Mon tableau d'avancement]
"""
        self.textEdit.insertPlainText(tab)


    def showHideSearchBar(self):

        if self.searchBar.isHidden():
            cursor = self.textEdit.textCursor()

            if cursor.selectedText():
                self.searchLineEdit.setText(cursor.selectedText())

            self.searchBar.move(
                self.textEdit.width()
                - self.searchBar.width()
                - self.textEdit.verticalScrollBar().width(),
                2,
            )
            self.searchBar.setVisible(1)

            if self.searchLineEdit.text():
                self.searchTextEdit()

            self.searchLineEdit.setFocus()
        else:
            self.searchBar.setHidden(1)

            for curs in self.searchCursors:
                removeformat = QTextCharFormat()
                removeformat.setBackground(QBrush(Qt.NoBrush))
                removeformat.setForeground(Qt.black)
                curs.movePosition(QTextCursor.WordRight, QTextCursor.KeepAnchor)
                curs.mergeCharFormat(removeformat)

            self.textEdit.viewport().update(self.textEdit.viewport().visibleRegion())


    def texthaschanged(self):

        if self.firstTextEditClick:
            self.firstTextEditClick = False
            return

        if self.firstLoad or not self.highlightIsFinished:
            return

        self.modified = True
        QTimer.singleShot(10, self.updatePageIndicator)


    def updatePageIndicator(self):
        n = "{{:0>{0}}}".format(len(str(self.qpv.pageCount()))).format(
            self.qpv.currentPageNumber()
        )
        self.pageIndicator = "@page {0}/{1}".format(n, self.qpv.pageCount())
        QTimer.singleShot(10, self.warnModified)


    def warnModified(self):
        if self.md_file:
            self.setWindowTitle(
                "wRiTe -- {} {} -- [{}] {}".format(
                    self.disp_filename,
                    self.pageIndicator,
                    self.lang,
                    "    ■" if self.modified else "     ",
                )
            )
        else:
            self.setWindowTitle(
                "wRiTe -- {} -- [{}] {}".format(
                    "Untitled", self.lang, "    ■" if self.modified else "     "
                )
            )


    def increaseFont(self):
        cursor = self.textEdit.textCursor()
        self.textEdit.selectAll()
        self.textEditFontSize += 1
        self.textEdit.highlighter.textEditFontSize = self.textEditFontSize
        font = self.textEdit.font()
        font.setPointSize(self.textEditFontSize)
        self.textEdit.setFont(font)
        self.textEdit.setTextCursor(cursor)
        QTimer.singleShot(10, self.indexPdf)


    def decreaseFont(self):
        cursor = self.textEdit.textCursor()
        self.textEdit.selectAll()
        self.textEditFontSize -= 1
        self.textEdit.highlighter.textEditFontSize = self.textEditFontSize
        font = self.textEdit.font()
        font.setPointSize(self.textEditFontSize)
        self.textEdit.setFont(font)
        self.textEdit.setTextCursor(cursor)
        QTimer.singleShot(10, self.indexPdf)


    def scrollPdf(self):

        if not self.scrollActivated:
            return

        if self.qpv.isHidden():
            return

        if not self.pdfLoaded:
            return

        cursor = self.textEdit.textCursor()
        firstLinePos = self.textEdit.cursorForPosition(QPoint(0, 0)).position()
        cursor.setPosition(firstLinePos)
        currentLine = cursor.blockNumber()
        currentSubLine = (
            cursor.block()
            .layout()
            .lineForTextPosition(cursor.positionInBlock())
            .lineNumber()
        )

        if (currentLine, currentSubLine) == (0, 0):
            self.qpv.scrollTo(QPoint(0, 0))

        elif (currentLine, currentSubLine) in self.scrollDict:

            try:
                self.qpv.scrollTo(
                    QPoint(0, self.scrollDict[(currentLine, currentSubLine)])
                )
            except Exception:
                pass


    def updateTextHash(self):
        if os.path.exists(self.md_file):
            with open(self.md_file, "r") as f:
                text = f.read()
                self.textHash = hashlib.md5(text.encode()).hexdigest()


    def loadText(self):
        text = ""
        textHash = 0

        if os.path.exists(self.md_file):
            with open(self.md_file, "r") as f:
                text = f.read()

            self.textEdit.isReady = False
            self.updateTextHash()
            he = text.find("\n****")
            if he >= 0:
                if "STYLE" in text[:he] and "colored" in text[:he]:
                    self.bqrgbcolor = (221, 54, 54)
                cst = text[:he].find('BQCOLOR')
                if cst >= 0:
                    ce = text.find("\n", cst+7, he+1)
                    if ce >= 0:
                        self.bqrgbcolor = text[cst+7:ce].replace(":", "").strip()

            text = "".join((text, "\n" * 53))
            self.newpages = len(re.findall(r"^!newpage\s*$", text, re.M))
            self.justLoaded = True
            self.firstAutocomplete = True
            self.textEdit.setPlainText(text)
            cursor = self.textEdit.textCursor()
            cursor.select(QTextCursor.Document)
            fmt = QTextBlockFormat()
            fmt.setLineHeight(150, QTextBlockFormat.FixedHeight)
            cursor.setBlockFormat(fmt) 
            self.setWindowTitle(
                "wRiTe -- {} -- [{}]".format(self.disp_filename, self.lang)
            )
            if self.firstLoad:
                self.textEdit.deleteHighlighter()
            self.loadTextFinished()
            return
        self.setWindowTitle("wRiTe -- {} -- [{}]".format("Untitled", self.lang))


    def saveText(self, notify):
        if self.md_file:
            self.hideWaitLogo()
            text = self.textEdit.toPlainText().rstrip()

            try:
                with open(self.md_file, "w") as f:
                    f.write(text)
                    self.modified = False
                    self.newpages = len(re.findall(r"^!newpage\s*$", text, re.M))
                    if notify:
                        QTimer.singleShot(
                            10,
                            lambda: self.showNotificationWindow(
                                "**Text saved...**", "forestgreen", 2000, -1
                            ),
                        )
            except Exception:
                print("Can't write to file", self.md_file)
                QTimer.singleShot(
                    10,
                    lambda: self.showNotificationWindow(
                        "**Text not saved...**", "red", 2000, -1
                    ),
                )
            self.updateTextHash()
            self.textToHtml()
        else:
            self.saveasDoc()
            if self.helpIsShown:
                self.showHideHelp()
            self.modified = False

    def callback_html(self, html):
        pass

    def htmlToPdf(self, check):
        self.wep.runJavaScript(
            "document.getElementsByTagName('html')[0].innerHTML", self.callback_html
        )
        margins = QMarginsF(self.mleft, self.mtop, self.mright, self.mdown)
        pageLayout = QPageLayout.Portrait
        pagesizes = (
            "A0",
            "A1",
            "A2",
            "A3",
            "A4",
            "A5",
            "A6",
            "A7",
            "A8",
            "A9",
            "B0",
            "B1",
            "B2",
            "B3",
            "B4",
            "B5",
            "B6",
            "B7",
            "B8",
            "B9",
            "B10",
            "C5E",
            "Comm10E",
            "DLE",
            "Executive",
            "Folio",
            "Ledger",
            "Legal",
            "Letter",
            "Tabloid",
        )
        pagesize = "A4"

        if self.pagesize in pagesizes:
            pagesize = self.pagesize

        pagesize = eval("QPageSize.{}".format(pagesize))

        if self.layout == "landscape":
            pageLayout = QPageLayout.Landscape

        layout = QPageLayout(
            QPageSize(pagesize), pageLayout, margins, QPageLayout.Millimeter, margins
        )

        if self.code_pages:
            self.wep.printToPdf(self.memoryPageFile.name, layout)
        # for Qt6 (later) first load page by page
        # elif self.firstLoad:
        #     if self.new_pages_to_compile:
        #         self.pdfComplete = False
        #         qrange = QPageRanges()
        #         for p in self.new_pages_to_compile:
        #             qrange.addPage(p)
        #             if not p:
        #                 self.veryFirstPage = True
        #             self.wep.printToPdf(self.memoryPageFile.name, layout, qrange)
        #             qrange.clear()
        #         self.pdfComplete = True

        else:
            self.wep.printToPdf(self.memoryPdfFile.name, layout)


    def textEditSizeChanged(self):
        self.updateSpecialScrollArea()


    def alternateViews(self, F1, F2):
        leftPanel = self.mainGrid.itemAtPosition(0, 0).widget()
        rightPanel = self.mainGrid.itemAtPosition(0, 1).widget()

        if F1:
            if self.width() < int(self.screenSize.width() / 2):
                rightPanel.setHidden(True)
                leftPanel.setVisible(True)
            elif leftPanel.isHidden():
                leftPanel.setVisible(True)
                self.qpv.wheelZoomingEnabled = False
                self.qpv.setViewMode(qpageview.FitWidth)
                QTimer.singleShot(0, self.scrollPdf)
            else:
                rightPanel.setVisible(rightPanel.isHidden())
                self.qpv.wheelZoomingEnabled = False
                self.qpv.setViewMode(qpageview.FitWidth)
                QTimer.singleShot(0, self.scrollPdf)

        if F2:
            if self.width() < int(self.screenSize.width() / 2):
                leftPanel.setHidden(True)
                rightPanel.setVisible(True)
                self.qpv.wheelZoomingEnabled = True
                self.qpv.setZoomFactor(self.lastZoomApplied)
            elif rightPanel.isHidden():
                rightPanel.setVisible(True)
                self.qpv.wheelZoomingEnabled = False
                self.qpv.setViewMode(qpageview.FitWidth)
                QTimer.singleShot(0, self.scrollPdf)
                if leftPanel.isHidden():
                    self.qpv.wheelZoomingEnabled = True
                    self.qpv.setZoomFactor(self.lastZoomApplied)
            else:
                if self.qpv.currentPage() is not None:
                    if self.md_file:
                        scale = (
                            self.qpv.verticalScrollBar().value()
                            / self.qpv.currentPage().height
                        )
                    leftPanel.setVisible(leftPanel.isHidden())
                    if leftPanel.isHidden():
                        self.qpv.wheelZoomingEnabled = True
                        self.qpv.setZoomFactor(self.lastZoomApplied)
                        if self.md_file:
                            self.qpv.verticalScrollBar().setValue(
                                int(scale * self.qpv.currentPage().height)
                            )
                    else:
                        self.qpv.wheelZoomingEnabled = False
                        self.qpv.setViewMode(qpageview.FitWidth)
                        QTimer.singleShot(0, self.scrollPdf)


    def textToHtml(self):
        self.showWaitLogo()
        try:
            lang = self.textEdit.highlighter.dict().tag.split("_")[0]

            if lang not in self.lang:
                self.langHasChanged = True
                self.codeBackup = ()
                QTimer.singleShot(
                    0,
                    lambda: self.showNotificationWindow(
                        "**Language changed - Recompiling...**", "#E98D28", 3000, -1
                    ),
                )

            self.lang = lang
        except Exception:
            pass

        self.pdfLoaded = False
        mode = "portrait"
        has_mode = False
        code = self.textEdit.toPlainText()
        coloredstyle = False
        compile_because_of_graphs = False
        self.new_pages_to_compile = []
        
        #  Detect mode if any
        if "\n!portrait" in code or "\n!landscape" in code:
            has_mode = True

        #  Grab header
        header = ""
        mh = re.search(r"^(\*|\-|\+){4,}\s*$", code, flags=re.MULTILINE)

        if mh:
            hend = mh.start() - 1
            header = code[:hend]

            if "landscape" in header:
                mode = "landscape"
            if "colored" in code[:hend]:
                coloredstyle = True

        #  Only !newpage on a line before split
        code = code.replace("!newpage\n!newpage", "!newpage\n \n!newpage")
        code = re.sub("^!newpage.*$", "!newpage", code, flags=re.MULTILINE)

        #  Get footer
        footer_content = ""
        footer_changed = False 
        if "!footer" in code:
            start = code.find('\n!footer') + 1
            if start >= 0:
                end = code.find('\n.footer', start) + 1
                if end:
                    footer_content = code[start+8:end]
                    code = "".join((code[:start], code[end+7:]))
        
        if footer_content != self.footerBackup:
            self.footerBackup = footer_content
            footer_changed = True

        #  Get header
        header_content = ""
        header_changed = False
        if "!header" in code:
            start = code.find('\n!header') + 1
            if start >= 0:
                end = code.find('\n.header', start) + 1
                if end:
                    header_content = code[start+8:end]
                    code = "".join((code[:start], code[end+7:]))
        
        if header_content != self.headerCBackup:
            self.headerCBackup = header_content
            header_changed = True
         
        #  Turn !graphPaper into muPDF code
        if "!graphPaper" in code:
            try:
                code = graphPaper(code)
            except Exception:
                print("Error in graphPaper")
        #  Remove useless line breaks but keep enough for centering images
        code = tuple((page.rstrip() for page in code.split("\n!newpage\n")))

        #  Prevent LaTeX tag issues (numbering)
        re_ltx_num = re.compile(r"\\begin{(equation|align|gather|alignat)}")
        ltx_pages = [i for i,page in enumerate(code) if re_ltx_num.search(page)]

        #  Extact muPDFCode
        for i, page in enumerate(code):
            page = re.sub("^```[^`]+?```", '', page, flags=re.S|re.M)
            pdfCode = re.findall(r"^!muPDF(.+?)\.muPDF", page, flags=re.DOTALL|re.M)
            self.muPDFCode[i] =  '\n'.join(pdfCode)
        
        graphs = ()
        #  Detect graphs in pages
        if self.rebuild:
            graphs = tuple(i for i, page in enumerate(code) if "!graph" in page or "!muPDF" in page)
            self.rebuild = False
            compile_because_of_graphs = True

        codehash = tuple(hash(page) for page in code)

        if (
                self.codeBackup == codehash
                and header == self.headerBackup
                and not header_changed
                and not footer_changed
                and not self.mode_changed_pages
                and not self.recompileForExport
                and not compile_because_of_graphs
        ) \
                or self.firstCompilationNotNeeded:
            self.modified = False
            self.firstCompilationNotNeeded = False
            if header:
                self.headerBackup = header
            self.codeBackup = codehash
            self.warnModified()
            self.pdfLoaded = True
            self.indexPdf()
            #print("hide from texttohtml")
            #self.hideWaitLogo()
            return

        if not self.mode_changed_pages:
            self.code_pages = []
            self.pages_to_delete = list(range(len(self.codeBackup)))

            if (
                not self.langHasChanged
                and header == self.headerBackup
                and not footer_changed
                and not header_changed
                and not self.recompileForExport
            ):
                lastidx = 0
                for i, pagehash in enumerate(codehash):
                    if pagehash in self.codeBackup and i not in ltx_pages:
                        idx = self.codeBackup.index(pagehash, lastidx)
                        if idx in self.pages_to_delete:
                            self.pages_to_delete.remove(idx)
                        self.code_pages.append(i)
                        lastidx = idx
                    else:
                        self.new_pages_to_compile.append(i)
            else:
                self.new_pages_to_compile = list((range(len(codehash))))
                self.langHasChanged = False

            if has_mode:

                for i in self.new_pages_to_compile:
                    page_mode = re.search(
                        "^!(portrait|landscape)", code[i], flags=re.MULTILINE
                    )

                    if page_mode:

                        if (
                            page_mode.group(1) not in mode
                            and len(self.new_pages_to_compile) > 1
                        ):
                            self.mode_changed_pages.append(i)

        else:
            self.new_pages_to_compile = self.mode_changed_pages
            self.pages_to_delete = self.mode_changed_pages
            self.code_pages = [
                e for e in range(len(codehash)) if e not in self.mode_changed_pages
            ]
            self.mode_changed_pages = []

        code_to_compile = ""

        if graphs:
            self.new_pages_to_compile = list(set(self.new_pages_to_compile).union(set(graphs)))
            self.pages_to_delete = list(set(self.pages_to_delete).union(set(graphs)))
            self.code_pages = [e for e in range(len(codehash)) if e not in self.new_pages_to_compile]            

        if 0 not in self.new_pages_to_compile:
            if header.strip():
                code_to_compile = "{}\n***********\n".format(header)
        
        for i, page in enumerate(self.new_pages_to_compile):
            if not i:
                if i not in self.mode_changed_pages:
                    code_to_compile = "".join((code_to_compile, code[page], "\n\n"))
            else:
                if i in self.mode_changed_pages:
                    code_to_compile = "".join((code_to_compile, "\n!newpage\n", "\n\n"))
                else:
                    code_to_compile = "".join(
                        (code_to_compile, "\n!newpage\n", code[page], "\n\n")
                    )
        (
            html,
            self.bgcolor,
            self.mleft,
            self.mright,
            self.mtop,
            self.mdown,
            self.layout,
            self.pagenum,
            self.pagesize,
            self.fontsize,
            author,
            self.doctitle,
            self.encryption,
            self.pageList,
            bqrgbcolor,
            self.rebuild,
            self.videos,
        ) = parseToHtml(
            self.md_file,
            self.APP_PATH,
            self.TMP_DIR,
            code_to_compile,
            self.path,
            self.data_path,
            self.recompileForExport,
            self.lang,
            self.firstLoad,
            footer_content,
            header_content
        )

        # Set author if different from local one
        if author.strip() and author != self.author:
            self.author = author

        # set blockquote color
        if coloredstyle and bqrgbcolor is None:
            bqrgbcolor = (221, 54, 54)
        if bqrgbcolor != self.bqrgbcolor:
            self.bqrgbcolor = bqrgbcolor
            if not self.firstLoad:
                self.textEdit.highlighter.bqrgbcolor = bqrgbcolor
                self.textEdit.highlighter.go = True
                self.textEdit.highlighter.stop = True
                self.textEdit.highlighter.rehighlight()

        # self.wep.profile().clearHttpCache()
        self.wep.setHtml(html, QUrl.fromLocalFile("/"))
        self.generated = False
        if header:
            self.headerBackup = header
        self.codeBackup = codehash


    def closeEvent(self, event):
        if self.modified:
            yn = QMessageBox.warning(
                self,
                "Warning",
                "Save before quit ?",
                QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel,
                QMessageBox.Yes,
            )

            if yn == QMessageBox.Yes:
                self.modified = False
                self.saveText(False)
            elif yn == QMessageBox.Cancel:
                event.ignore()
                return

        if self.md_file:
            #  Remove memory files
            self.memoryPdfFile.close()
            self.memoryPageFile.close()

        if os.path.isdir(os.path.join(self.TMP_DIR, "wRiTe_images")):
            rmtree(os.path.join(self.TMP_DIR, "wRiTe_images"), ignore_errors=True)

        if os.path.isdir(self.data_path):
            if not os.listdir(self.data_path):
                os.rmdir(self.data_path)

        for rf in os.listdir("/dev/shm/"):
            if "wRiTe" in rf:
                os.remove(os.path.join("/dev/shm", rf))

        SM = SManager()
        settings = SM.get_settings()
        settings['Wwidth'] = self.width()
        settings['Wheight'] = self.height()
        SM.save_settings(settings)
        sys.exit()


    def on_findButton_clicked(self):
        if not self.searchTimer.isActive():
            self.searchTimer.start(500)
        else:
            self.searchTimer.stop()
            self.searchTimer.start(500)


    def searchTextEdit(self):
        if self.searchTimer.isActive():
            self.searchTimer.stop()

        if not self.isFirstTime:
            for curs in self.searchCursors:
                removeformat = QTextCharFormat()
                removeformat.setBackground(QBrush(Qt.NoBrush))
                removeformat.setForeground(Qt.black)
                curs.movePosition(QTextCursor.WordRight, QTextCursor.KeepAnchor)
                curs.mergeCharFormat(removeformat)
            self.searchCursors = []

        searchString = self.searchLineEdit.text()
        document = self.textEdit.document()
        found = False

        if searchString:
            highlightCursor = QTextCursor(document)
            cursor = QTextCursor(document)
            cursor.beginEditBlock()
            while not highlightCursor.isNull() and not highlightCursor.atEnd():
                highlightCursor = document.find(searchString, highlightCursor)
                plainFormat = QTextCharFormat(highlightCursor.charFormat())
                colorFormat = QTextCharFormat(plainFormat)
                colorFormat.setBackground(QColor("#02c800"))
                colorFormat.setForeground(Qt.white)
                if not highlightCursor.isNull():
                    found = True
                    self.currentSearchCursor = 0
                    highlightCursor.mergeCharFormat(colorFormat)
                    self.searchCursors.append(highlightCursor)
                    self.searchLineEdit.setStyleSheet(
                        "QLineEdit { background: rgb(210, 255, 213); }"
                    )
                    self.isFirstTime = False
                    self.scrollToFoundText(">")
            cursor.endEditBlock()
            if not found:
                self.searchLineEdit.setStyleSheet(
                    "QLineEdit { background: rgb(255, 200, 200); }"
                )
        else:
            self.searchLineEdit.setStyleSheet(
                "QLineEdit { background: rgb(255, 255, 255); }"
            )

        self.textEdit.viewport().update(self.textEdit.viewport().visibleRegion())
