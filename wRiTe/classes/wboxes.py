# -*- coding: utf-8 -*-
#
# This file is part of the wRiTe package.
#
# Copyright (c) 2023 by Nicolas CLÉMENT
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
# See http://www.gnu.org/licenses/ for more information.

import re
from classes.colors import colors


def wboxopt(match):
    shadow = "0"
    title = "My title"
    opts = ""
    width = "100%"
    hasframe = False
    hasbackground = False
    cols = ()
    m = match.group(1)
    stitle = m.find("[")
    etitle = m.find("]", stitle)
    content = match.group(2)

    if stitle >= 0 and etitle >= 0:
        title = m[stitle + 1: etitle].strip()
    else:
        etitle = 0

    sopts = m.find("(", etitle)
    eopts = m.find(")", sopts)

    if sopts >= 0 and eopts >= 0:
        opts = m[sopts + 1: eopts].strip()

    if opts:
        opts = tuple(re.sub(r"\s+", " ", opts).split(" "))

        if len(opts) > 4:
            opts = opts[:4]

        for opt in opts:

            if "shadow" in opt:
                shadow = "2px 2px 2px gray"
                continue

            if "@" not in opt:
                width = opt
                continue

            if "@" in opt:
                if not hasframe:
                    hasframe = True
                    col = opt.replace("@", "")
                    if col in colors:
                        col = colors[col]
                    cols = (col,)
                    continue
                else:
                    hasbackground = True
                    col = opt.replace("@", "")
                    if col in colors:
                        col = colors[col]
                    cols += (col,)

        if hasframe and hasbackground:
            return ('\n<div class="wbox" style="border: 1.5px'
                    ' solid {0}; width: {4}; background-color: {1};'
                    ' box-shadow: {3};">\n<span class="wboxtitle"'
                    ' style="background-color: {0};">'
                    '<strong>{2}</strong></span>\n\n{5}\n\n</div>\n\n'
                    ).format(
                *cols, title, shadow, width, content
            )

        elif hasframe and not hasbackground:

            return (
                '\n<div class="wbox" style="border: 1.5px'
                ' solid {0}; width: {3}; box-shadow: {2};">\n'
                '<span class="wboxtitle" style="background-color:'
                ' {0};">{1}</span>\n\n{4}\n\n</div>\n\n'
                ).format(
                *cols, title, shadow, width, content
            )

    return ('\n<div class="wbox" style="box-shadow:'
            ' {1}; width: {2};" >\n<span class="wboxtitle">'
            '{0}</span>\n\n{3}\n\n</div>\n\n'
            ).format(
        title, shadow, width, content
    )


def wboxes(code):
    return re.sub(r"^!wbox(.*?)\s*$(.+?)^\.wbox", wboxopt, code, flags=re.M | re.DOTALL)
